!function(root, factory) {
    "object" == typeof exports && "object" == typeof module ? module.exports = factory(require("React"), require("ReactDOM")) : "function" == typeof define && define.amd ? define([ "React", "ReactDOM" ], factory) : "object" == typeof exports ? exports.ZebulonGrid = factory(require("React"), require("ReactDOM")) : root.ZebulonGrid = factory(root.React, root.ReactDOM);
}(this, function(__WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_40__) {
    /******/
    return function(modules) {
        /******/
        /******/
        // The require function
        /******/
        function __webpack_require__(moduleId) {
            /******/
            /******/
            // Check if module is in cache
            /******/
            if (installedModules[moduleId]) /******/
            return installedModules[moduleId].exports;
            /******/
            /******/
            // Create a new module (and put it into the cache)
            /******/
            var module = installedModules[moduleId] = {
                /******/
                exports: {},
                /******/
                id: moduleId,
                /******/
                loaded: !1
            };
            /******/
            /******/
            // Return the exports of the module
            /******/
            /******/
            /******/
            // Execute the module function
            /******/
            /******/
            /******/
            // Flag the module as loaded
            /******/
            return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), 
            module.loaded = !0, module.exports;
        }
        // webpackBootstrap
        /******/
        // The module cache
        /******/
        var installedModules = {};
        /******/
        /******/
        // Load entry module and return exports
        /******/
        /******/
        /******/
        /******/
        // expose the modules object (__webpack_modules__)
        /******/
        /******/
        /******/
        // expose the module cache
        /******/
        /******/
        /******/
        // __webpack_public_path__
        /******/
        return __webpack_require__.m = modules, __webpack_require__.c = installedModules, 
        __webpack_require__.p = "", __webpack_require__(0);
    }([ /* 0 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireWildcard(obj) {
            if (obj && obj.__esModule) return obj;
            var newObj = {};
            if (null != obj) for (var key in obj) Object.prototype.hasOwnProperty.call(obj, key) && (newObj[key] = obj[key]);
            return newObj.default = obj, newObj;
        }
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.hydrateStore = exports.actions = exports.reducer = exports.WrappedGrid = exports.PivotGrid = void 0;
        var _PivotGrid = __webpack_require__(1), _PivotGrid2 = _interopRequireDefault(_PivotGrid), _WrappedGrid = __webpack_require__(239), _WrappedGrid2 = _interopRequireDefault(_WrappedGrid), _reducers = __webpack_require__(277), _reducers2 = _interopRequireDefault(_reducers), _actions = __webpack_require__(232), actions = _interopRequireWildcard(_actions), _hydrateStore = __webpack_require__(285), _hydrateStore2 = _interopRequireDefault(_hydrateStore);
        // import './index.css';
        // export { GridConfiguration };
        // export { ChartConfiguration };
        exports.PivotGrid = _PivotGrid2.default, // import Chart from './components/Chart/Chart';
        // import GridConfiguration from './containers/GridConfiguration';
        // import ChartConfiguration from './components/ChartConfiguration';
        exports.WrappedGrid = _WrappedGrid2.default, // export { Chart };
        exports.reducer = _reducers2.default, exports.actions = actions, exports.hydrateStore = _hydrateStore2.default;
    }, /* 1 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _reactRedux = __webpack_require__(2), _Grid = __webpack_require__(28), _Grid2 = _interopRequireDefault(_Grid), _selectors = __webpack_require__(201), _actions = __webpack_require__(232), _Axis = __webpack_require__(203), mapStateToProps = function(state) {
            return {
                width: state.config.width,
                layout: (0, _selectors.getLayout)(state),
                defaultCellSizes: (0, _selectors.getCellSizes)(state),
                headerSizes: (0, _selectors.getHeaderSizes)(state),
                sizes: state.sizes,
                columnHeaders: (0, _selectors.getColumnUiAxis)(state).headers,
                rowHeaders: (0, _selectors.getRowUiAxis)(state).headers,
                rowFields: (0, _selectors.getRowAxis)(state).fields,
                columnFields: (0, _selectors.getColumnAxis)(state).fields,
                dataFieldsCount: (0, _selectors.getActivatedDataFields)(state).length
            };
        }, mapDispatchToProps = function(dispatch) {
            return {
                updateCellSize: function(_ref) {
                    var handle = _ref.handle, offset = _ref.offset, initialOffset = _ref.initialOffset, sizes = _ref.sizes, defaultCellSizes = _ref.defaultCellSizes;
                    handle.leafSubheaders && handle.leafSubheaders.length && (handle.axis === _Axis.AxisType.COLUMNS && "right" === handle.position || handle.axis === _Axis.AxisType.ROWS && "bottom" === handle.position) ? !function() {
                        var fractionalOffset = {
                            x: (offset.x - initialOffset.x) / handle.leafSubheaders.length,
                            y: (offset.y - initialOffset.y) / handle.leafSubheaders.length
                        };
                        handle.leafSubheaders.forEach(function(subheader) {
                            dispatch((0, _actions.updateCellSize)({
                                handle: _extends({}, handle, {
                                    leafSubheaders: [],
                                    id: subheader.key
                                }),
                                offset: fractionalOffset,
                                initialOffset: {
                                    x: 0,
                                    y: 0
                                },
                                defaultCellSizes: defaultCellSizes,
                                sizes: sizes
                            }));
                        });
                    }() : dispatch((0, _actions.updateCellSize)({
                        handle: handle,
                        offset: offset,
                        initialOffset: initialOffset,
                        sizes: sizes,
                        defaultCellSizes: defaultCellSizes
                    }));
                },
                setSizes: function(_ref2) {
                    var height = _ref2.height, width = _ref2.width;
                    height && dispatch((0, _actions.setConfigProperty)({
                        height: height,
                        width: width
                    }, "height")), width && dispatch((0, _actions.setConfigProperty)({
                        height: height,
                        width: width
                    }, "width"));
                }
            };
        }, mergeProps = function(_ref3, _ref4, ownProps) {
            var width = _ref3.width, layout = _ref3.layout, headerSizes = _ref3.headerSizes, sizes = _ref3.sizes, defaultCellSizes = _ref3.defaultCellSizes, columnHeaders = _ref3.columnHeaders, rowHeaders = _ref3.rowHeaders, rowFields = _ref3.rowFields, columnFields = _ref3.columnFields, dataFieldsCount = _ref3.dataFieldsCount, _updateCellSize2 = _ref4.updateCellSize, setSizes = _ref4.setSizes;
            return _extends({
                rowFields: rowFields,
                columnFields: columnFields,
                columnHeaders: columnHeaders,
                rowHeaders: rowHeaders,
                width: width,
                layout: layout,
                dataFieldsCount: dataFieldsCount,
                headerSizes: headerSizes,
                updateCellSize: function(_ref5) {
                    var handle = _ref5.handle, offset = _ref5.offset, initialOffset = _ref5.initialOffset;
                    return _updateCellSize2({
                        handle: handle,
                        offset: offset,
                        initialOffset: initialOffset,
                        sizes: sizes,
                        defaultCellSizes: defaultCellSizes
                    });
                },
                setSizes: setSizes
            }, ownProps);
        };
        exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps, mergeProps)(_Grid2.default);
    }, /* 2 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        exports.__esModule = !0, exports.connect = exports.Provider = void 0;
        var _Provider = __webpack_require__(3), _Provider2 = _interopRequireDefault(_Provider), _connect = __webpack_require__(8), _connect2 = _interopRequireDefault(_connect);
        exports.Provider = _Provider2.default, exports.connect = _connect2.default;
    }, /* 3 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            function warnAboutReceivingStore() {
                didWarnAboutReceivingStore || (didWarnAboutReceivingStore = !0, (0, _warning2.default)("<Provider> does not support changing `store` on the fly. It is most likely that you see this error because you updated to Redux 2.x and React Redux 2.x which no longer hot reload reducers automatically. See https://github.com/reactjs/react-redux/releases/tag/v2.0.0 for the migration instructions."));
            }
            exports.__esModule = !0, exports.default = void 0;
            var _react = __webpack_require__(5), _storeShape = __webpack_require__(6), _storeShape2 = _interopRequireDefault(_storeShape), _warning = __webpack_require__(7), _warning2 = _interopRequireDefault(_warning), didWarnAboutReceivingStore = !1, Provider = function(_Component) {
                function Provider(props, context) {
                    _classCallCheck(this, Provider);
                    var _this = _possibleConstructorReturn(this, _Component.call(this, props, context));
                    return _this.store = props.store, _this;
                }
                return _inherits(Provider, _Component), Provider.prototype.getChildContext = function() {
                    return {
                        store: this.store
                    };
                }, Provider.prototype.render = function() {
                    return _react.Children.only(this.props.children);
                }, Provider;
            }(_react.Component);
            exports.default = Provider, "production" !== process.env.NODE_ENV && (Provider.prototype.componentWillReceiveProps = function(nextProps) {
                var store = this.store, nextStore = nextProps.store;
                store !== nextStore && warnAboutReceivingStore();
            }), Provider.propTypes = {
                store: _storeShape2.default.isRequired,
                children: _react.PropTypes.element.isRequired
            }, Provider.childContextTypes = {
                store: _storeShape2.default.isRequired
            };
        }).call(exports, __webpack_require__(4));
    }, /* 4 */
    /***/
    function(module, exports) {
        function defaultSetTimout() {
            throw new Error("setTimeout has not been defined");
        }
        function defaultClearTimeout() {
            throw new Error("clearTimeout has not been defined");
        }
        function runTimeout(fun) {
            if (cachedSetTimeout === setTimeout) //normal enviroments in sane situations
            return setTimeout(fun, 0);
            // if setTimeout wasn't available but was latter defined
            if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) return cachedSetTimeout = setTimeout, 
            setTimeout(fun, 0);
            try {
                // when when somebody has screwed with setTimeout but no I.E. maddness
                return cachedSetTimeout(fun, 0);
            } catch (e) {
                try {
                    // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
                    return cachedSetTimeout.call(null, fun, 0);
                } catch (e) {
                    // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
                    return cachedSetTimeout.call(this, fun, 0);
                }
            }
        }
        function runClearTimeout(marker) {
            if (cachedClearTimeout === clearTimeout) //normal enviroments in sane situations
            return clearTimeout(marker);
            // if clearTimeout wasn't available but was latter defined
            if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) return cachedClearTimeout = clearTimeout, 
            clearTimeout(marker);
            try {
                // when when somebody has screwed with setTimeout but no I.E. maddness
                return cachedClearTimeout(marker);
            } catch (e) {
                try {
                    // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
                    return cachedClearTimeout.call(null, marker);
                } catch (e) {
                    // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
                    // Some versions of I.E. have different rules for clearTimeout vs setTimeout
                    return cachedClearTimeout.call(this, marker);
                }
            }
        }
        function cleanUpNextTick() {
            draining && currentQueue && (draining = !1, currentQueue.length ? queue = currentQueue.concat(queue) : queueIndex = -1, 
            queue.length && drainQueue());
        }
        function drainQueue() {
            if (!draining) {
                var timeout = runTimeout(cleanUpNextTick);
                draining = !0;
                for (var len = queue.length; len; ) {
                    for (currentQueue = queue, queue = []; ++queueIndex < len; ) currentQueue && currentQueue[queueIndex].run();
                    queueIndex = -1, len = queue.length;
                }
                currentQueue = null, draining = !1, runClearTimeout(timeout);
            }
        }
        // v8 likes predictible objects
        function Item(fun, array) {
            this.fun = fun, this.array = array;
        }
        function noop() {}
        // shim for using process in browser
        var cachedSetTimeout, cachedClearTimeout, process = module.exports = {};
        !function() {
            try {
                cachedSetTimeout = "function" == typeof setTimeout ? setTimeout : defaultSetTimout;
            } catch (e) {
                cachedSetTimeout = defaultSetTimout;
            }
            try {
                cachedClearTimeout = "function" == typeof clearTimeout ? clearTimeout : defaultClearTimeout;
            } catch (e) {
                cachedClearTimeout = defaultClearTimeout;
            }
        }();
        var currentQueue, queue = [], draining = !1, queueIndex = -1;
        process.nextTick = function(fun) {
            var args = new Array(arguments.length - 1);
            if (arguments.length > 1) for (var i = 1; i < arguments.length; i++) args[i - 1] = arguments[i];
            queue.push(new Item(fun, args)), 1 !== queue.length || draining || runTimeout(drainQueue);
        }, Item.prototype.run = function() {
            this.fun.apply(null, this.array);
        }, process.title = "browser", process.browser = !0, process.env = {}, process.argv = [], 
        process.version = "", // empty string to avoid regexp issues
        process.versions = {}, process.on = noop, process.addListener = noop, process.once = noop, 
        process.off = noop, process.removeListener = noop, process.removeAllListeners = noop, 
        process.emit = noop, process.binding = function(name) {
            throw new Error("process.binding is not supported");
        }, process.cwd = function() {
            return "/";
        }, process.chdir = function(dir) {
            throw new Error("process.chdir is not supported");
        }, process.umask = function() {
            return 0;
        };
    }, /* 5 */
    /***/
    function(module, exports) {
        module.exports = __WEBPACK_EXTERNAL_MODULE_5__;
    }, /* 6 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        exports.__esModule = !0;
        var _react = __webpack_require__(5);
        exports.default = _react.PropTypes.shape({
            subscribe: _react.PropTypes.func.isRequired,
            dispatch: _react.PropTypes.func.isRequired,
            getState: _react.PropTypes.func.isRequired
        });
    }, /* 7 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Prints a warning in the console if it exists.
	 *
	 * @param {String} message The warning message.
	 * @returns {void}
	 */
        function warning(message) {
            /* eslint-disable no-console */
            "undefined" != typeof console && "function" == typeof console.error && console.error(message);
            /* eslint-enable no-console */
            try {
                // This error was thrown as a convenience so that if you enable
                // "break on all exceptions" in your console,
                // it would pause the execution at this line.
                throw new Error(message);
            } catch (e) {}
        }
        exports.__esModule = !0, exports.default = warning;
    }, /* 8 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            function getDisplayName(WrappedComponent) {
                return WrappedComponent.displayName || WrappedComponent.name || "Component";
            }
            function tryCatch(fn, ctx) {
                try {
                    return fn.apply(ctx);
                } catch (e) {
                    return errorObject.value = e, errorObject;
                }
            }
            function connect(mapStateToProps, mapDispatchToProps, mergeProps) {
                var options = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {}, shouldSubscribe = Boolean(mapStateToProps), mapState = mapStateToProps || defaultMapStateToProps, mapDispatch = void 0;
                mapDispatch = "function" == typeof mapDispatchToProps ? mapDispatchToProps : mapDispatchToProps ? (0, 
                _wrapActionCreators2.default)(mapDispatchToProps) : defaultMapDispatchToProps;
                var finalMergeProps = mergeProps || defaultMergeProps, _options$pure = options.pure, pure = void 0 === _options$pure || _options$pure, _options$withRef = options.withRef, withRef = void 0 !== _options$withRef && _options$withRef, checkMergedEquals = pure && finalMergeProps !== defaultMergeProps, version = nextVersion++;
                return function(WrappedComponent) {
                    function checkStateShape(props, methodName) {
                        (0, _isPlainObject2.default)(props) || (0, _warning2.default)(methodName + "() in " + connectDisplayName + " must return a plain object. " + ("Instead received " + props + "."));
                    }
                    function computeMergedProps(stateProps, dispatchProps, parentProps) {
                        var mergedProps = finalMergeProps(stateProps, dispatchProps, parentProps);
                        return "production" !== process.env.NODE_ENV && checkStateShape(mergedProps, "mergeProps"), 
                        mergedProps;
                    }
                    var connectDisplayName = "Connect(" + getDisplayName(WrappedComponent) + ")", Connect = function(_Component) {
                        function Connect(props, context) {
                            _classCallCheck(this, Connect);
                            var _this = _possibleConstructorReturn(this, _Component.call(this, props, context));
                            _this.version = version, _this.store = props.store || context.store, (0, _invariant2.default)(_this.store, 'Could not find "store" in either the context or ' + ('props of "' + connectDisplayName + '". ') + "Either wrap the root component in a <Provider>, " + ('or explicitly pass "store" as a prop to "' + connectDisplayName + '".'));
                            var storeState = _this.store.getState();
                            return _this.state = {
                                storeState: storeState
                            }, _this.clearCache(), _this;
                        }
                        return _inherits(Connect, _Component), Connect.prototype.shouldComponentUpdate = function() {
                            return !pure || this.haveOwnPropsChanged || this.hasStoreStateChanged;
                        }, Connect.prototype.computeStateProps = function(store, props) {
                            if (!this.finalMapStateToProps) return this.configureFinalMapState(store, props);
                            var state = store.getState(), stateProps = this.doStatePropsDependOnOwnProps ? this.finalMapStateToProps(state, props) : this.finalMapStateToProps(state);
                            return "production" !== process.env.NODE_ENV && checkStateShape(stateProps, "mapStateToProps"), 
                            stateProps;
                        }, Connect.prototype.configureFinalMapState = function(store, props) {
                            var mappedState = mapState(store.getState(), props), isFactory = "function" == typeof mappedState;
                            return this.finalMapStateToProps = isFactory ? mappedState : mapState, this.doStatePropsDependOnOwnProps = 1 !== this.finalMapStateToProps.length, 
                            isFactory ? this.computeStateProps(store, props) : ("production" !== process.env.NODE_ENV && checkStateShape(mappedState, "mapStateToProps"), 
                            mappedState);
                        }, Connect.prototype.computeDispatchProps = function(store, props) {
                            if (!this.finalMapDispatchToProps) return this.configureFinalMapDispatch(store, props);
                            var dispatch = store.dispatch, dispatchProps = this.doDispatchPropsDependOnOwnProps ? this.finalMapDispatchToProps(dispatch, props) : this.finalMapDispatchToProps(dispatch);
                            return "production" !== process.env.NODE_ENV && checkStateShape(dispatchProps, "mapDispatchToProps"), 
                            dispatchProps;
                        }, Connect.prototype.configureFinalMapDispatch = function(store, props) {
                            var mappedDispatch = mapDispatch(store.dispatch, props), isFactory = "function" == typeof mappedDispatch;
                            return this.finalMapDispatchToProps = isFactory ? mappedDispatch : mapDispatch, 
                            this.doDispatchPropsDependOnOwnProps = 1 !== this.finalMapDispatchToProps.length, 
                            isFactory ? this.computeDispatchProps(store, props) : ("production" !== process.env.NODE_ENV && checkStateShape(mappedDispatch, "mapDispatchToProps"), 
                            mappedDispatch);
                        }, Connect.prototype.updateStatePropsIfNeeded = function() {
                            var nextStateProps = this.computeStateProps(this.store, this.props);
                            return (!this.stateProps || !(0, _shallowEqual2.default)(nextStateProps, this.stateProps)) && (this.stateProps = nextStateProps, 
                            !0);
                        }, Connect.prototype.updateDispatchPropsIfNeeded = function() {
                            var nextDispatchProps = this.computeDispatchProps(this.store, this.props);
                            return (!this.dispatchProps || !(0, _shallowEqual2.default)(nextDispatchProps, this.dispatchProps)) && (this.dispatchProps = nextDispatchProps, 
                            !0);
                        }, Connect.prototype.updateMergedPropsIfNeeded = function() {
                            var nextMergedProps = computeMergedProps(this.stateProps, this.dispatchProps, this.props);
                            return !(this.mergedProps && checkMergedEquals && (0, _shallowEqual2.default)(nextMergedProps, this.mergedProps)) && (this.mergedProps = nextMergedProps, 
                            !0);
                        }, Connect.prototype.isSubscribed = function() {
                            return "function" == typeof this.unsubscribe;
                        }, Connect.prototype.trySubscribe = function() {
                            shouldSubscribe && !this.unsubscribe && (this.unsubscribe = this.store.subscribe(this.handleChange.bind(this)), 
                            this.handleChange());
                        }, Connect.prototype.tryUnsubscribe = function() {
                            this.unsubscribe && (this.unsubscribe(), this.unsubscribe = null);
                        }, Connect.prototype.componentDidMount = function() {
                            this.trySubscribe();
                        }, Connect.prototype.componentWillReceiveProps = function(nextProps) {
                            pure && (0, _shallowEqual2.default)(nextProps, this.props) || (this.haveOwnPropsChanged = !0);
                        }, Connect.prototype.componentWillUnmount = function() {
                            this.tryUnsubscribe(), this.clearCache();
                        }, Connect.prototype.clearCache = function() {
                            this.dispatchProps = null, this.stateProps = null, this.mergedProps = null, this.haveOwnPropsChanged = !0, 
                            this.hasStoreStateChanged = !0, this.haveStatePropsBeenPrecalculated = !1, this.statePropsPrecalculationError = null, 
                            this.renderedElement = null, this.finalMapDispatchToProps = null, this.finalMapStateToProps = null;
                        }, Connect.prototype.handleChange = function() {
                            if (this.unsubscribe) {
                                var storeState = this.store.getState(), prevStoreState = this.state.storeState;
                                if (!pure || prevStoreState !== storeState) {
                                    if (pure && !this.doStatePropsDependOnOwnProps) {
                                        var haveStatePropsChanged = tryCatch(this.updateStatePropsIfNeeded, this);
                                        if (!haveStatePropsChanged) return;
                                        haveStatePropsChanged === errorObject && (this.statePropsPrecalculationError = errorObject.value), 
                                        this.haveStatePropsBeenPrecalculated = !0;
                                    }
                                    this.hasStoreStateChanged = !0, this.setState({
                                        storeState: storeState
                                    });
                                }
                            }
                        }, Connect.prototype.getWrappedInstance = function() {
                            return (0, _invariant2.default)(withRef, "To access the wrapped instance, you need to specify { withRef: true } as the fourth argument of the connect() call."), 
                            this.refs.wrappedInstance;
                        }, Connect.prototype.render = function() {
                            var haveOwnPropsChanged = this.haveOwnPropsChanged, hasStoreStateChanged = this.hasStoreStateChanged, haveStatePropsBeenPrecalculated = this.haveStatePropsBeenPrecalculated, statePropsPrecalculationError = this.statePropsPrecalculationError, renderedElement = this.renderedElement;
                            if (this.haveOwnPropsChanged = !1, this.hasStoreStateChanged = !1, this.haveStatePropsBeenPrecalculated = !1, 
                            this.statePropsPrecalculationError = null, statePropsPrecalculationError) throw statePropsPrecalculationError;
                            var shouldUpdateStateProps = !0, shouldUpdateDispatchProps = !0;
                            pure && renderedElement && (shouldUpdateStateProps = hasStoreStateChanged || haveOwnPropsChanged && this.doStatePropsDependOnOwnProps, 
                            shouldUpdateDispatchProps = haveOwnPropsChanged && this.doDispatchPropsDependOnOwnProps);
                            var haveStatePropsChanged = !1, haveDispatchPropsChanged = !1;
                            haveStatePropsBeenPrecalculated ? haveStatePropsChanged = !0 : shouldUpdateStateProps && (haveStatePropsChanged = this.updateStatePropsIfNeeded()), 
                            shouldUpdateDispatchProps && (haveDispatchPropsChanged = this.updateDispatchPropsIfNeeded());
                            var haveMergedPropsChanged = !0;
                            return haveMergedPropsChanged = !!(haveStatePropsChanged || haveDispatchPropsChanged || haveOwnPropsChanged) && this.updateMergedPropsIfNeeded(), 
                            !haveMergedPropsChanged && renderedElement ? renderedElement : (withRef ? this.renderedElement = (0, 
                            _react.createElement)(WrappedComponent, _extends({}, this.mergedProps, {
                                ref: "wrappedInstance"
                            })) : this.renderedElement = (0, _react.createElement)(WrappedComponent, this.mergedProps), 
                            this.renderedElement);
                        }, Connect;
                    }(_react.Component);
                    return Connect.displayName = connectDisplayName, Connect.WrappedComponent = WrappedComponent, 
                    Connect.contextTypes = {
                        store: _storeShape2.default
                    }, Connect.propTypes = {
                        store: _storeShape2.default
                    }, "production" !== process.env.NODE_ENV && (Connect.prototype.componentWillUpdate = function() {
                        this.version !== version && (// We are hot reloading!
                        this.version = version, this.trySubscribe(), this.clearCache());
                    }), (0, _hoistNonReactStatics2.default)(Connect, WrappedComponent);
                };
            }
            exports.__esModule = !0;
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            };
            exports.default = connect;
            var _react = __webpack_require__(5), _storeShape = __webpack_require__(6), _storeShape2 = _interopRequireDefault(_storeShape), _shallowEqual = __webpack_require__(9), _shallowEqual2 = _interopRequireDefault(_shallowEqual), _wrapActionCreators = __webpack_require__(10), _wrapActionCreators2 = _interopRequireDefault(_wrapActionCreators), _warning = __webpack_require__(7), _warning2 = _interopRequireDefault(_warning), _isPlainObject = __webpack_require__(13), _isPlainObject2 = _interopRequireDefault(_isPlainObject), _hoistNonReactStatics = __webpack_require__(26), _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics), _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), defaultMapStateToProps = function(state) {
                return {};
            }, defaultMapDispatchToProps = function(dispatch) {
                return {
                    dispatch: dispatch
                };
            }, defaultMergeProps = function(stateProps, dispatchProps, parentProps) {
                return _extends({}, parentProps, stateProps, dispatchProps);
            }, errorObject = {
                value: null
            }, nextVersion = 0;
        }).call(exports, __webpack_require__(4));
    }, /* 9 */
    /***/
    function(module, exports) {
        "use strict";
        function shallowEqual(objA, objB) {
            if (objA === objB) return !0;
            var keysA = Object.keys(objA), keysB = Object.keys(objB);
            if (keysA.length !== keysB.length) return !1;
            for (var hasOwn = Object.prototype.hasOwnProperty, i = 0; i < keysA.length; i++) if (!hasOwn.call(objB, keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) return !1;
            return !0;
        }
        exports.__esModule = !0, exports.default = shallowEqual;
    }, /* 10 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function wrapActionCreators(actionCreators) {
            return function(dispatch) {
                return (0, _redux.bindActionCreators)(actionCreators, dispatch);
            };
        }
        exports.__esModule = !0, exports.default = wrapActionCreators;
        var _redux = __webpack_require__(11);
    }, /* 11 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            /*
	* This is a dummy function to check if the function name has been altered by minification.
	* If the function has been minified and NODE_ENV !== 'production', warn the user.
	*/
            function isCrushed() {}
            exports.__esModule = !0, exports.compose = exports.applyMiddleware = exports.bindActionCreators = exports.combineReducers = exports.createStore = void 0;
            var _createStore = __webpack_require__(12), _createStore2 = _interopRequireDefault(_createStore), _combineReducers = __webpack_require__(21), _combineReducers2 = _interopRequireDefault(_combineReducers), _bindActionCreators = __webpack_require__(23), _bindActionCreators2 = _interopRequireDefault(_bindActionCreators), _applyMiddleware = __webpack_require__(24), _applyMiddleware2 = _interopRequireDefault(_applyMiddleware), _compose = __webpack_require__(25), _compose2 = _interopRequireDefault(_compose), _warning = __webpack_require__(22), _warning2 = _interopRequireDefault(_warning);
            "production" !== process.env.NODE_ENV && "string" == typeof isCrushed.name && "isCrushed" !== isCrushed.name && (0, 
            _warning2.default)("You are currently using minified code outside of NODE_ENV === 'production'. This means that you are running a slower development build of Redux. You can use loose-envify (https://github.com/zertosh/loose-envify) for browserify or DefinePlugin for webpack (http://stackoverflow.com/questions/30030031) to ensure you have the correct code for your production build."), 
            exports.createStore = _createStore2.default, exports.combineReducers = _combineReducers2.default, 
            exports.bindActionCreators = _bindActionCreators2.default, exports.applyMiddleware = _applyMiddleware2.default, 
            exports.compose = _compose2.default;
        }).call(exports, __webpack_require__(4));
    }, /* 12 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        /**
	 * Creates a Redux store that holds the state tree.
	 * The only way to change the data in the store is to call `dispatch()` on it.
	 *
	 * There should only be a single store in your app. To specify how different
	 * parts of the state tree respond to actions, you may combine several reducers
	 * into a single reducer function by using `combineReducers`.
	 *
	 * @param {Function} reducer A function that returns the next state tree, given
	 * the current state tree and the action to handle.
	 *
	 * @param {any} [preloadedState] The initial state. You may optionally specify it
	 * to hydrate the state from the server in universal apps, or to restore a
	 * previously serialized user session.
	 * If you use `combineReducers` to produce the root reducer function, this must be
	 * an object with the same shape as `combineReducers` keys.
	 *
	 * @param {Function} enhancer The store enhancer. You may optionally specify it
	 * to enhance the store with third-party capabilities such as middleware,
	 * time travel, persistence, etc. The only store enhancer that ships with Redux
	 * is `applyMiddleware()`.
	 *
	 * @returns {Store} A Redux store that lets you read the state, dispatch actions
	 * and subscribe to changes.
	 */
        function createStore(reducer, preloadedState, enhancer) {
            function ensureCanMutateNextListeners() {
                nextListeners === currentListeners && (nextListeners = currentListeners.slice());
            }
            /**
	   * Reads the state tree managed by the store.
	   *
	   * @returns {any} The current state tree of your application.
	   */
            function getState() {
                return currentState;
            }
            /**
	   * Adds a change listener. It will be called any time an action is dispatched,
	   * and some part of the state tree may potentially have changed. You may then
	   * call `getState()` to read the current state tree inside the callback.
	   *
	   * You may call `dispatch()` from a change listener, with the following
	   * caveats:
	   *
	   * 1. The subscriptions are snapshotted just before every `dispatch()` call.
	   * If you subscribe or unsubscribe while the listeners are being invoked, this
	   * will not have any effect on the `dispatch()` that is currently in progress.
	   * However, the next `dispatch()` call, whether nested or not, will use a more
	   * recent snapshot of the subscription list.
	   *
	   * 2. The listener should not expect to see all state changes, as the state
	   * might have been updated multiple times during a nested `dispatch()` before
	   * the listener is called. It is, however, guaranteed that all subscribers
	   * registered before the `dispatch()` started will be called with the latest
	   * state by the time it exits.
	   *
	   * @param {Function} listener A callback to be invoked on every dispatch.
	   * @returns {Function} A function to remove this change listener.
	   */
            function subscribe(listener) {
                if ("function" != typeof listener) throw new Error("Expected listener to be a function.");
                var isSubscribed = !0;
                return ensureCanMutateNextListeners(), nextListeners.push(listener), function() {
                    if (isSubscribed) {
                        isSubscribed = !1, ensureCanMutateNextListeners();
                        var index = nextListeners.indexOf(listener);
                        nextListeners.splice(index, 1);
                    }
                };
            }
            /**
	   * Dispatches an action. It is the only way to trigger a state change.
	   *
	   * The `reducer` function, used to create the store, will be called with the
	   * current state tree and the given `action`. Its return value will
	   * be considered the **next** state of the tree, and the change listeners
	   * will be notified.
	   *
	   * The base implementation only supports plain object actions. If you want to
	   * dispatch a Promise, an Observable, a thunk, or something else, you need to
	   * wrap your store creating function into the corresponding middleware. For
	   * example, see the documentation for the `redux-thunk` package. Even the
	   * middleware will eventually dispatch plain object actions using this method.
	   *
	   * @param {Object} action A plain object representing “what changed”. It is
	   * a good idea to keep actions serializable so you can record and replay user
	   * sessions, or use the time travelling `redux-devtools`. An action must have
	   * a `type` property which may not be `undefined`. It is a good idea to use
	   * string constants for action types.
	   *
	   * @returns {Object} For convenience, the same action object you dispatched.
	   *
	   * Note that, if you use a custom middleware, it may wrap `dispatch()` to
	   * return something else (for example, a Promise you can await).
	   */
            function dispatch(action) {
                if (!(0, _isPlainObject2.default)(action)) throw new Error("Actions must be plain objects. Use custom middleware for async actions.");
                if ("undefined" == typeof action.type) throw new Error('Actions may not have an undefined "type" property. Have you misspelled a constant?');
                if (isDispatching) throw new Error("Reducers may not dispatch actions.");
                try {
                    isDispatching = !0, currentState = currentReducer(currentState, action);
                } finally {
                    isDispatching = !1;
                }
                for (var listeners = currentListeners = nextListeners, i = 0; i < listeners.length; i++) listeners[i]();
                return action;
            }
            /**
	   * Replaces the reducer currently used by the store to calculate the state.
	   *
	   * You might need this if your app implements code splitting and you want to
	   * load some of the reducers dynamically. You might also need this if you
	   * implement a hot reloading mechanism for Redux.
	   *
	   * @param {Function} nextReducer The reducer for the store to use instead.
	   * @returns {void}
	   */
            function replaceReducer(nextReducer) {
                if ("function" != typeof nextReducer) throw new Error("Expected the nextReducer to be a function.");
                currentReducer = nextReducer, dispatch({
                    type: ActionTypes.INIT
                });
            }
            /**
	   * Interoperability point for observable/reactive libraries.
	   * @returns {observable} A minimal observable of state changes.
	   * For more information, see the observable proposal:
	   * https://github.com/zenparsing/es-observable
	   */
            function observable() {
                var _ref, outerSubscribe = subscribe;
                return _ref = {
                    /**
	       * The minimal observable subscription method.
	       * @param {Object} observer Any object that can be used as an observer.
	       * The observer object should have a `next` method.
	       * @returns {subscription} An object with an `unsubscribe` method that can
	       * be used to unsubscribe the observable from the store, and prevent further
	       * emission of values from the observable.
	       */
                    subscribe: function(observer) {
                        function observeState() {
                            observer.next && observer.next(getState());
                        }
                        if ("object" != typeof observer) throw new TypeError("Expected the observer to be an object.");
                        observeState();
                        var unsubscribe = outerSubscribe(observeState);
                        return {
                            unsubscribe: unsubscribe
                        };
                    }
                }, _ref[_symbolObservable2.default] = function() {
                    return this;
                }, _ref;
            }
            var _ref2;
            if ("function" == typeof preloadedState && "undefined" == typeof enhancer && (enhancer = preloadedState, 
            preloadedState = void 0), "undefined" != typeof enhancer) {
                if ("function" != typeof enhancer) throw new Error("Expected the enhancer to be a function.");
                return enhancer(createStore)(reducer, preloadedState);
            }
            if ("function" != typeof reducer) throw new Error("Expected the reducer to be a function.");
            var currentReducer = reducer, currentState = preloadedState, currentListeners = [], nextListeners = currentListeners, isDispatching = !1;
            // When a store is created, an "INIT" action is dispatched so that every
            // reducer returns their initial state. This effectively populates
            // the initial state tree.
            return dispatch({
                type: ActionTypes.INIT
            }), _ref2 = {
                dispatch: dispatch,
                subscribe: subscribe,
                getState: getState,
                replaceReducer: replaceReducer
            }, _ref2[_symbolObservable2.default] = observable, _ref2;
        }
        exports.__esModule = !0, exports.ActionTypes = void 0, exports.default = createStore;
        var _isPlainObject = __webpack_require__(13), _isPlainObject2 = _interopRequireDefault(_isPlainObject), _symbolObservable = __webpack_require__(17), _symbolObservable2 = _interopRequireDefault(_symbolObservable), ActionTypes = exports.ActionTypes = {
            INIT: "@@redux/INIT"
        };
    }, /* 13 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if `value` is a plain object, that is, an object created by the
	 * `Object` constructor or one with a `[[Prototype]]` of `null`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.8.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 * }
	 *
	 * _.isPlainObject(new Foo);
	 * // => false
	 *
	 * _.isPlainObject([1, 2, 3]);
	 * // => false
	 *
	 * _.isPlainObject({ 'x': 0, 'y': 0 });
	 * // => true
	 *
	 * _.isPlainObject(Object.create(null));
	 * // => true
	 */
        function isPlainObject(value) {
            if (!isObjectLike(value) || objectToString.call(value) != objectTag) return !1;
            var proto = getPrototype(value);
            if (null === proto) return !0;
            var Ctor = hasOwnProperty.call(proto, "constructor") && proto.constructor;
            return "function" == typeof Ctor && Ctor instanceof Ctor && funcToString.call(Ctor) == objectCtorString;
        }
        var getPrototype = __webpack_require__(14), isObjectLike = __webpack_require__(16), objectTag = "[object Object]", funcProto = Function.prototype, objectProto = Object.prototype, funcToString = funcProto.toString, hasOwnProperty = objectProto.hasOwnProperty, objectCtorString = funcToString.call(Object), objectToString = objectProto.toString;
        module.exports = isPlainObject;
    }, /* 14 */
    /***/
    function(module, exports, __webpack_require__) {
        var overArg = __webpack_require__(15), getPrototype = overArg(Object.getPrototypeOf, Object);
        module.exports = getPrototype;
    }, /* 15 */
    /***/
    function(module, exports) {
        /**
	 * Creates a unary function that invokes `func` with its argument transformed.
	 *
	 * @private
	 * @param {Function} func The function to wrap.
	 * @param {Function} transform The argument transform.
	 * @returns {Function} Returns the new function.
	 */
        function overArg(func, transform) {
            return function(arg) {
                return func(transform(arg));
            };
        }
        module.exports = overArg;
    }, /* 16 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is object-like. A value is object-like if it's not `null`
	 * and has a `typeof` result of "object".
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
	 * @example
	 *
	 * _.isObjectLike({});
	 * // => true
	 *
	 * _.isObjectLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isObjectLike(_.noop);
	 * // => false
	 *
	 * _.isObjectLike(null);
	 * // => false
	 */
        function isObjectLike(value) {
            return null != value && "object" == typeof value;
        }
        module.exports = isObjectLike;
    }, /* 17 */
    /***/
    function(module, exports, __webpack_require__) {
        module.exports = __webpack_require__(18);
    }, /* 18 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(module, global) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _ponyfill = __webpack_require__(20), _ponyfill2 = _interopRequireDefault(_ponyfill), root = module;
            /* global window */
            root = "undefined" != typeof self ? self : "undefined" != typeof window ? window : "undefined" != typeof global ? global : Function("return this")();
            var result = (0, _ponyfill2.default)(root);
            exports.default = result;
        }).call(exports, __webpack_require__(19)(module), function() {
            return this;
        }());
    }, /* 19 */
    /***/
    function(module, exports) {
        module.exports = function(module) {
            // module.parent = undefined by default
            return module.webpackPolyfill || (module.deprecate = function() {}, module.paths = [], 
            module.children = [], module.webpackPolyfill = 1), module;
        };
    }, /* 20 */
    /***/
    function(module, exports) {
        "use strict";
        function symbolObservablePonyfill(root) {
            var result, _Symbol = root.Symbol;
            return "function" == typeof _Symbol ? _Symbol.observable ? result = _Symbol.observable : (result = _Symbol("observable"), 
            _Symbol.observable = result) : result = "@@observable", result;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = symbolObservablePonyfill;
    }, /* 21 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function getUndefinedStateErrorMessage(key, action) {
                var actionType = action && action.type, actionName = actionType && '"' + actionType.toString() + '"' || "an action";
                return "Given action " + actionName + ', reducer "' + key + '" returned undefined. To ignore an action, you must explicitly return the previous state.';
            }
            function getUnexpectedStateShapeWarningMessage(inputState, reducers, action, unexpectedKeyCache) {
                var reducerKeys = Object.keys(reducers), argumentName = action && action.type === _createStore.ActionTypes.INIT ? "preloadedState argument passed to createStore" : "previous state received by the reducer";
                if (0 === reducerKeys.length) return "Store does not have a valid reducer. Make sure the argument passed to combineReducers is an object whose values are reducers.";
                if (!(0, _isPlainObject2.default)(inputState)) return "The " + argumentName + ' has unexpected type of "' + {}.toString.call(inputState).match(/\s([a-z|A-Z]+)/)[1] + '". Expected argument to be an object with the following ' + ('keys: "' + reducerKeys.join('", "') + '"');
                var unexpectedKeys = Object.keys(inputState).filter(function(key) {
                    return !reducers.hasOwnProperty(key) && !unexpectedKeyCache[key];
                });
                return unexpectedKeys.forEach(function(key) {
                    unexpectedKeyCache[key] = !0;
                }), unexpectedKeys.length > 0 ? "Unexpected " + (unexpectedKeys.length > 1 ? "keys" : "key") + " " + ('"' + unexpectedKeys.join('", "') + '" found in ' + argumentName + ". ") + "Expected to find one of the known reducer keys instead: " + ('"' + reducerKeys.join('", "') + '". Unexpected keys will be ignored.') : void 0;
            }
            function assertReducerSanity(reducers) {
                Object.keys(reducers).forEach(function(key) {
                    var reducer = reducers[key], initialState = reducer(void 0, {
                        type: _createStore.ActionTypes.INIT
                    });
                    if ("undefined" == typeof initialState) throw new Error('Reducer "' + key + '" returned undefined during initialization. If the state passed to the reducer is undefined, you must explicitly return the initial state. The initial state may not be undefined.');
                    var type = "@@redux/PROBE_UNKNOWN_ACTION_" + Math.random().toString(36).substring(7).split("").join(".");
                    if ("undefined" == typeof reducer(void 0, {
                        type: type
                    })) throw new Error('Reducer "' + key + '" returned undefined when probed with a random type. ' + ("Don't try to handle " + _createStore.ActionTypes.INIT + ' or other actions in "redux/*" ') + "namespace. They are considered private. Instead, you must return the current state for any unknown actions, unless it is undefined, in which case you must return the initial state, regardless of the action type. The initial state may not be undefined.");
                });
            }
            /**
	 * Turns an object whose values are different reducer functions, into a single
	 * reducer function. It will call every child reducer, and gather their results
	 * into a single state object, whose keys correspond to the keys of the passed
	 * reducer functions.
	 *
	 * @param {Object} reducers An object whose values correspond to different
	 * reducer functions that need to be combined into one. One handy way to obtain
	 * it is to use ES6 `import * as reducers` syntax. The reducers may never return
	 * undefined for any action. Instead, they should return their initial state
	 * if the state passed to them was undefined, and the current state for any
	 * unrecognized action.
	 *
	 * @returns {Function} A reducer function that invokes every reducer inside the
	 * passed object, and builds a state object with the same shape.
	 */
            function combineReducers(reducers) {
                for (var reducerKeys = Object.keys(reducers), finalReducers = {}, i = 0; i < reducerKeys.length; i++) {
                    var key = reducerKeys[i];
                    "production" !== process.env.NODE_ENV && "undefined" == typeof reducers[key] && (0, 
                    _warning2.default)('No reducer provided for key "' + key + '"'), "function" == typeof reducers[key] && (finalReducers[key] = reducers[key]);
                }
                var finalReducerKeys = Object.keys(finalReducers);
                if ("production" !== process.env.NODE_ENV) var unexpectedKeyCache = {};
                var sanityError;
                try {
                    assertReducerSanity(finalReducers);
                } catch (e) {
                    sanityError = e;
                }
                return function() {
                    var state = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0], action = arguments[1];
                    if (sanityError) throw sanityError;
                    if ("production" !== process.env.NODE_ENV) {
                        var warningMessage = getUnexpectedStateShapeWarningMessage(state, finalReducers, action, unexpectedKeyCache);
                        warningMessage && (0, _warning2.default)(warningMessage);
                    }
                    for (var hasChanged = !1, nextState = {}, i = 0; i < finalReducerKeys.length; i++) {
                        var key = finalReducerKeys[i], reducer = finalReducers[key], previousStateForKey = state[key], nextStateForKey = reducer(previousStateForKey, action);
                        if ("undefined" == typeof nextStateForKey) {
                            var errorMessage = getUndefinedStateErrorMessage(key, action);
                            throw new Error(errorMessage);
                        }
                        nextState[key] = nextStateForKey, hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
                    }
                    return hasChanged ? nextState : state;
                };
            }
            exports.__esModule = !0, exports.default = combineReducers;
            var _createStore = __webpack_require__(12), _isPlainObject = __webpack_require__(13), _isPlainObject2 = _interopRequireDefault(_isPlainObject), _warning = __webpack_require__(22), _warning2 = _interopRequireDefault(_warning);
        }).call(exports, __webpack_require__(4));
    }, /* 22 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Prints a warning in the console if it exists.
	 *
	 * @param {String} message The warning message.
	 * @returns {void}
	 */
        function warning(message) {
            /* eslint-disable no-console */
            "undefined" != typeof console && "function" == typeof console.error && console.error(message);
            /* eslint-enable no-console */
            try {
                // This error was thrown as a convenience so that if you enable
                // "break on all exceptions" in your console,
                // it would pause the execution at this line.
                throw new Error(message);
            } catch (e) {}
        }
        exports.__esModule = !0, exports.default = warning;
    }, /* 23 */
    /***/
    function(module, exports) {
        "use strict";
        function bindActionCreator(actionCreator, dispatch) {
            return function() {
                return dispatch(actionCreator.apply(void 0, arguments));
            };
        }
        /**
	 * Turns an object whose values are action creators, into an object with the
	 * same keys, but with every function wrapped into a `dispatch` call so they
	 * may be invoked directly. This is just a convenience method, as you can call
	 * `store.dispatch(MyActionCreators.doSomething())` yourself just fine.
	 *
	 * For convenience, you can also pass a single function as the first argument,
	 * and get a function in return.
	 *
	 * @param {Function|Object} actionCreators An object whose values are action
	 * creator functions. One handy way to obtain it is to use ES6 `import * as`
	 * syntax. You may also pass a single function.
	 *
	 * @param {Function} dispatch The `dispatch` function available on your Redux
	 * store.
	 *
	 * @returns {Function|Object} The object mimicking the original object, but with
	 * every action creator wrapped into the `dispatch` call. If you passed a
	 * function as `actionCreators`, the return value will also be a single
	 * function.
	 */
        function bindActionCreators(actionCreators, dispatch) {
            if ("function" == typeof actionCreators) return bindActionCreator(actionCreators, dispatch);
            if ("object" != typeof actionCreators || null === actionCreators) throw new Error("bindActionCreators expected an object or a function, instead received " + (null === actionCreators ? "null" : typeof actionCreators) + '. Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?');
            for (var keys = Object.keys(actionCreators), boundActionCreators = {}, i = 0; i < keys.length; i++) {
                var key = keys[i], actionCreator = actionCreators[key];
                "function" == typeof actionCreator && (boundActionCreators[key] = bindActionCreator(actionCreator, dispatch));
            }
            return boundActionCreators;
        }
        exports.__esModule = !0, exports.default = bindActionCreators;
    }, /* 24 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        /**
	 * Creates a store enhancer that applies middleware to the dispatch method
	 * of the Redux store. This is handy for a variety of tasks, such as expressing
	 * asynchronous actions in a concise manner, or logging every action payload.
	 *
	 * See `redux-thunk` package as an example of the Redux middleware.
	 *
	 * Because middleware is potentially asynchronous, this should be the first
	 * store enhancer in the composition chain.
	 *
	 * Note that each middleware will be given the `dispatch` and `getState` functions
	 * as named arguments.
	 *
	 * @param {...Function} middlewares The middleware chain to be applied.
	 * @returns {Function} A store enhancer applying the middleware.
	 */
        function applyMiddleware() {
            for (var _len = arguments.length, middlewares = Array(_len), _key = 0; _key < _len; _key++) middlewares[_key] = arguments[_key];
            return function(createStore) {
                return function(reducer, preloadedState, enhancer) {
                    var store = createStore(reducer, preloadedState, enhancer), _dispatch = store.dispatch, chain = [], middlewareAPI = {
                        getState: store.getState,
                        dispatch: function(action) {
                            return _dispatch(action);
                        }
                    };
                    return chain = middlewares.map(function(middleware) {
                        return middleware(middlewareAPI);
                    }), _dispatch = _compose2.default.apply(void 0, chain)(store.dispatch), _extends({}, store, {
                        dispatch: _dispatch
                    });
                };
            };
        }
        exports.__esModule = !0;
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        };
        exports.default = applyMiddleware;
        var _compose = __webpack_require__(25), _compose2 = _interopRequireDefault(_compose);
    }, /* 25 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Composes single-argument functions from right to left. The rightmost
	 * function can take multiple arguments as it provides the signature for
	 * the resulting composite function.
	 *
	 * @param {...Function} funcs The functions to compose.
	 * @returns {Function} A function obtained by composing the argument functions
	 * from right to left. For example, compose(f, g, h) is identical to doing
	 * (...args) => f(g(h(...args))).
	 */
        function compose() {
            for (var _len = arguments.length, funcs = Array(_len), _key = 0; _key < _len; _key++) funcs[_key] = arguments[_key];
            if (0 === funcs.length) return function(arg) {
                return arg;
            };
            if (1 === funcs.length) return funcs[0];
            var last = funcs[funcs.length - 1], rest = funcs.slice(0, -1);
            return function() {
                return rest.reduceRight(function(composed, f) {
                    return f(composed);
                }, last.apply(void 0, arguments));
            };
        }
        exports.__esModule = !0, exports.default = compose;
    }, /* 26 */
    /***/
    function(module, exports) {
        /**
	 * Copyright 2015, Yahoo! Inc.
	 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
	 */
        "use strict";
        var REACT_STATICS = {
            childContextTypes: !0,
            contextTypes: !0,
            defaultProps: !0,
            displayName: !0,
            getDefaultProps: !0,
            mixins: !0,
            propTypes: !0,
            type: !0
        }, KNOWN_STATICS = {
            name: !0,
            length: !0,
            prototype: !0,
            caller: !0,
            arguments: !0,
            arity: !0
        }, isGetOwnPropertySymbolsAvailable = "function" == typeof Object.getOwnPropertySymbols;
        module.exports = function(targetComponent, sourceComponent, customStatics) {
            if ("string" != typeof sourceComponent) {
                // don't hoist over string (html) components
                var keys = Object.getOwnPropertyNames(sourceComponent);
                /* istanbul ignore else */
                isGetOwnPropertySymbolsAvailable && (keys = keys.concat(Object.getOwnPropertySymbols(sourceComponent)));
                for (var i = 0; i < keys.length; ++i) if (!(REACT_STATICS[keys[i]] || KNOWN_STATICS[keys[i]] || customStatics && customStatics[keys[i]])) try {
                    targetComponent[keys[i]] = sourceComponent[keys[i]];
                } catch (error) {}
            }
            return targetComponent;
        };
    }, /* 27 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            /**
	 * Copyright 2013-2015, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 */
            "use strict";
            /**
	 * Use invariant() to assert state which your program assumes to be true.
	 *
	 * Provide sprintf-style format (only %s is supported) and arguments
	 * to provide information about what broke and what you were
	 * expecting.
	 *
	 * The invariant message will be stripped in production, but the invariant
	 * will remain to ensure logic does not differ in production.
	 */
            var invariant = function(condition, format, a, b, c, d, e, f) {
                if ("production" !== process.env.NODE_ENV && void 0 === format) throw new Error("invariant requires an error message argument");
                if (!condition) {
                    var error;
                    if (void 0 === format) error = new Error("Minified exception occurred; use the non-minified dev environment for the full error message and additional helpful warnings."); else {
                        var args = [ a, b, c, d, e, f ], argIndex = 0;
                        error = new Error(format.replace(/%s/g, function() {
                            return args[argIndex++];
                        })), error.name = "Invariant Violation";
                    }
                    // we don't care about invariant's own frame
                    throw error.framesToPop = 1, error;
                }
            };
            module.exports = invariant;
        }).call(exports, __webpack_require__(4));
    }, /* 28 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        function getNextKey(current, next) {
            var firstLeafHeader = current.firstHeaderRow[current.firstHeaderRow.length - 1], keys = firstLeafHeader.key.split(_constants.KEY_SEPARATOR), nextKey = "";
            // A data field has been toggled
            return current.fields.length > next.fields.length ? !function() {
                var nextFieldIds = next.fields.map(function(field) {
                    return field.id;
                }), missingFieldPosition = current.fields.findIndex(function(field) {
                    return !nextFieldIds.includes(field.id);
                });
                nextKey = keys.slice(0, missingFieldPosition).join(_constants.KEY_SEPARATOR);
            }() : current.fields.length < next.fields.length ? !function() {
                var previousFieldIds = current.fields.map(function(field) {
                    return field.id;
                }), newFieldPosition = next.fields.findIndex(function(field) {
                    return !previousFieldIds.includes(field.id);
                });
                nextKey = keys.slice(0, newFieldPosition).join(_constants.KEY_SEPARATOR);
            }() : nextKey = current.dataFieldsCount !== next.dataFieldsCount ? keys.slice(0, -1).join(_constants.KEY_SEPARATOR) : "", 
            nextKey;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactVirtualized = __webpack_require__(29), _reactDnd = __webpack_require__(81), _ArrowKeyStepper = __webpack_require__(199), _ArrowKeyStepper2 = _interopRequireDefault(_ArrowKeyStepper), _DataCells = __webpack_require__(200), _DataCells2 = _interopRequireDefault(_DataCells), _DimensionHeaders = __webpack_require__(215), _DimensionHeaders2 = _interopRequireDefault(_DimensionHeaders), _ColumnHeaders = __webpack_require__(222), _ColumnHeaders2 = _interopRequireDefault(_ColumnHeaders), _RowHeaders = __webpack_require__(228), _RowHeaders2 = _interopRequireDefault(_RowHeaders), _DragLayer = __webpack_require__(231), _DragLayer2 = _interopRequireDefault(_DragLayer), _AxisUi = __webpack_require__(205), _constants = __webpack_require__(207), PivotGrid = function(_PureComponent) {
            function PivotGrid(props) {
                _classCallCheck(this, PivotGrid);
                var _this = _possibleConstructorReturn(this, (PivotGrid.__proto__ || Object.getPrototypeOf(PivotGrid)).call(this, props));
                return _this.rowStartIndex = 0, _this.columnStartIndex = 0, _this;
            }
            return _inherits(PivotGrid, _PureComponent), _createClass(PivotGrid, [ {
                key: "componentWillReceiveProps",
                value: function(nextProps) {
                    var nextRowStartIndex = void 0, nextColumnStartIndex = void 0, current = {}, next = {};
                    if (current.dataFieldsCount = this.props.dataFieldsCount, next.dataFieldsCount = nextProps.dataFieldsCount, 
                    this.props.rowHeaders.length !== nextProps.rowHeaders.length) {
                        current.fields = this.props.rowFields, next.fields = nextProps.rowFields, current.firstHeaderRow = this.props.rowHeaders[this.rowStartIndex];
                        var nextFirstHeaderKey = getNextKey(current, next);
                        nextRowStartIndex = (0, _AxisUi.keyToIndex)(nextProps.rowHeaders, nextFirstHeaderKey);
                    }
                    if (this.props.columnHeaders.length !== nextProps.columnHeaders.length) {
                        current.fields = this.props.columnFields, next.fields = nextProps.columnFields, 
                        current.firstHeaderRow = this.props.columnHeaders[this.columnStartIndex];
                        var _nextFirstHeaderKey = getNextKey(current, next);
                        nextColumnStartIndex = (0, _AxisUi.keyToIndex)(nextProps.columnHeaders, _nextFirstHeaderKey);
                    }
                    // If keyToIndex does not find the key in the headers, it returns -1
                    // In this case, do nothing
                    nextRowStartIndex >= 0 && (this.rowStartIndex = nextRowStartIndex), nextColumnStartIndex >= 0 && (this.columnStartIndex = nextColumnStartIndex);
                }
            }, {
                key: "componentDidUpdate",
                value: function(prevProps) {
                    var _props = this.props, height = _props.height, width = _props.width, setSizes = _props.setSizes;
                    height === prevProps.height && width === prevProps.width || setSizes({
                        height: height,
                        width: width
                    });
                }
            }, {
                key: "handleSectionRendered",
                value: function(onSectionRendered) {
                    var _this2 = this;
                    return function(indexes) {
                        var rowStartIndex = indexes.rowStartIndex, columnStartIndex = indexes.columnStartIndex;
                        // When the data cells grid is re rendered, it resets row and column
                        // start indexes, losing the information about the previous position,
                        // this prevents that.
                        // I's a hack until I better understand this behaviour.
                        rowStartIndex && (_this2.rowStartIndex = rowStartIndex), columnStartIndex && (_this2.columnStartIndex = columnStartIndex), 
                        onSectionRendered(indexes);
                    };
                }
            }, {
                key: "render",
                value: function() {
                    var _this3 = this, _props2 = this.props, connectDropTarget = _props2.connectDropTarget, width = _props2.width, layout = _props2.layout, customFunctions = _props2.customFunctions, columnHorizontalCount = layout.columnHorizontalCount, rowVerticalCount = layout.rowVerticalCount;
                    // Width has to be set in order to render correctly in a resizable box
                    return connectDropTarget(_react2.default.createElement("div", {
                        style: {
                            width: width
                        }
                    }, _react2.default.createElement(_DragLayer2.default, null), _react2.default.createElement(_ArrowKeyStepper2.default, {
                        columnCount: columnHorizontalCount,
                        mode: "align:top-left",
                        rowCount: rowVerticalCount,
                        scrollToRow: this.rowStartIndex,
                        scrollToColumn: this.columnStartIndex
                    }, function(_ref) {
                        var onSectionRendered = _ref.onSectionRendered, scrollToColumn = _ref.scrollToColumn, scrollToRow = _ref.scrollToRow;
                        return _react2.default.createElement(_reactVirtualized.ScrollSync, null, function(_ref2) {
                            var onScroll = _ref2.onScroll, scrollLeft = _ref2.scrollLeft, scrollTop = _ref2.scrollTop;
                            return _react2.default.createElement("div", {
                                className: "orb-pivotgrid"
                            }, _react2.default.createElement("div", {
                                style: {
                                    display: "flex"
                                }
                            }, _react2.default.createElement(_DimensionHeaders2.default, null), _react2.default.createElement(_ColumnHeaders2.default, {
                                scrollLeft: scrollLeft,
                                ref: function(_ref3) {
                                    _this3.columnHeaders = _ref3;
                                }
                            })), _react2.default.createElement("div", {
                                style: {
                                    display: "flex"
                                }
                            }, _react2.default.createElement(_RowHeaders2.default, {
                                scrollTop: scrollTop,
                                ref: function(_ref4) {
                                    _this3.rowHeaders = _ref4;
                                }
                            }), _react2.default.createElement(_DataCells2.default, {
                                customFunctions: customFunctions,
                                onSectionRendered: _this3.handleSectionRendered(onSectionRendered),
                                scrollToColumn: scrollToColumn,
                                scrollToRow: scrollToRow,
                                onScroll: onScroll
                            })));
                        });
                    })));
                }
            } ]), PivotGrid;
        }(_react.PureComponent), gridSpec = {
            drop: function(props, monitor, component) {
                var handle = monitor.getItem(), initialOffset = monitor.getInitialClientOffset(), offset = monitor.getClientOffset();
                component.props.updateCellSize({
                    handle: handle,
                    offset: offset,
                    initialOffset: initialOffset
                });
            }
        }, collect = function(connect) {
            return {
                connectDropTarget: connect.dropTarget()
            };
        };
        exports.default = (0, _reactDnd.DropTarget)("cell-resize-handle", gridSpec, collect)(PivotGrid);
    }, /* 29 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _ArrowKeyStepper = __webpack_require__(30);
        Object.defineProperty(exports, "ArrowKeyStepper", {
            enumerable: !0,
            get: function() {
                return _ArrowKeyStepper.ArrowKeyStepper;
            }
        });
        var _AutoSizer = __webpack_require__(35);
        Object.defineProperty(exports, "AutoSizer", {
            enumerable: !0,
            get: function() {
                return _AutoSizer.AutoSizer;
            }
        });
        var _CellMeasurer = __webpack_require__(38);
        Object.defineProperty(exports, "CellMeasurer", {
            enumerable: !0,
            get: function() {
                return _CellMeasurer.CellMeasurer;
            }
        }), Object.defineProperty(exports, "defaultCellMeasurerCellSizeCache", {
            enumerable: !0,
            get: function() {
                return _CellMeasurer.defaultCellSizeCache;
            }
        }), Object.defineProperty(exports, "uniformSizeCellMeasurerCellSizeCache", {
            enumerable: !0,
            get: function() {
                return _CellMeasurer.defaultCellSizeCache;
            }
        });
        var _Collection = __webpack_require__(42);
        Object.defineProperty(exports, "Collection", {
            enumerable: !0,
            get: function() {
                return _Collection.Collection;
            }
        });
        var _ColumnSizer = __webpack_require__(53);
        Object.defineProperty(exports, "ColumnSizer", {
            enumerable: !0,
            get: function() {
                return _ColumnSizer.ColumnSizer;
            }
        });
        var _Table = __webpack_require__(63);
        Object.defineProperty(exports, "defaultTableCellDataGetter", {
            enumerable: !0,
            get: function() {
                return _Table.defaultCellDataGetter;
            }
        }), Object.defineProperty(exports, "defaultTableCellRenderer", {
            enumerable: !0,
            get: function() {
                return _Table.defaultCellRenderer;
            }
        }), Object.defineProperty(exports, "defaultTableHeaderRenderer", {
            enumerable: !0,
            get: function() {
                return _Table.defaultHeaderRenderer;
            }
        }), Object.defineProperty(exports, "defaultTableRowRenderer", {
            enumerable: !0,
            get: function() {
                return _Table.defaultRowRenderer;
            }
        }), Object.defineProperty(exports, "Table", {
            enumerable: !0,
            get: function() {
                return _Table.Table;
            }
        }), Object.defineProperty(exports, "Column", {
            enumerable: !0,
            get: function() {
                return _Table.Column;
            }
        }), Object.defineProperty(exports, "SortDirection", {
            enumerable: !0,
            get: function() {
                return _Table.SortDirection;
            }
        }), Object.defineProperty(exports, "SortIndicator", {
            enumerable: !0,
            get: function() {
                return _Table.SortIndicator;
            }
        });
        var _Grid = __webpack_require__(55);
        Object.defineProperty(exports, "defaultCellRangeRenderer", {
            enumerable: !0,
            get: function() {
                return _Grid.defaultCellRangeRenderer;
            }
        }), Object.defineProperty(exports, "Grid", {
            enumerable: !0,
            get: function() {
                return _Grid.Grid;
            }
        });
        var _InfiniteLoader = __webpack_require__(72);
        Object.defineProperty(exports, "InfiniteLoader", {
            enumerable: !0,
            get: function() {
                return _InfiniteLoader.InfiniteLoader;
            }
        });
        var _ScrollSync = __webpack_require__(74);
        Object.defineProperty(exports, "ScrollSync", {
            enumerable: !0,
            get: function() {
                return _ScrollSync.ScrollSync;
            }
        });
        var _List = __webpack_require__(76);
        Object.defineProperty(exports, "List", {
            enumerable: !0,
            get: function() {
                return _List.List;
            }
        });
        var _WindowScroller = __webpack_require__(78);
        Object.defineProperty(exports, "WindowScroller", {
            enumerable: !0,
            get: function() {
                return _WindowScroller.WindowScroller;
            }
        });
    }, /* 30 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.ArrowKeyStepper = exports.default = void 0;
        var _ArrowKeyStepper2 = __webpack_require__(31), _ArrowKeyStepper3 = _interopRequireDefault(_ArrowKeyStepper2);
        exports.default = _ArrowKeyStepper3.default, exports.ArrowKeyStepper = _ArrowKeyStepper3.default;
    }, /* 31 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), ArrowKeyStepper = function(_Component) {
                function ArrowKeyStepper(props, context) {
                    _classCallCheck(this, ArrowKeyStepper);
                    var _this = _possibleConstructorReturn(this, (ArrowKeyStepper.__proto__ || Object.getPrototypeOf(ArrowKeyStepper)).call(this, props, context));
                    return _this.state = {
                        scrollToColumn: props.scrollToColumn,
                        scrollToRow: props.scrollToRow
                    }, _this._columnStartIndex = 0, _this._columnStopIndex = 0, _this._rowStartIndex = 0, 
                    _this._rowStopIndex = 0, _this._onKeyDown = _this._onKeyDown.bind(_this), _this._onSectionRendered = _this._onSectionRendered.bind(_this), 
                    _this;
                }
                return _inherits(ArrowKeyStepper, _Component), _createClass(ArrowKeyStepper, [ {
                    key: "componentWillUpdate",
                    value: function(nextProps, nextState) {
                        var scrollToColumn = nextProps.scrollToColumn, scrollToRow = nextProps.scrollToRow;
                        this.props.scrollToColumn !== scrollToColumn && this.setState({
                            scrollToColumn: scrollToColumn
                        }), this.props.scrollToRow !== scrollToRow && this.setState({
                            scrollToRow: scrollToRow
                        });
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _props = this.props, className = _props.className, children = _props.children, _state = this.state, scrollToColumn = _state.scrollToColumn, scrollToRow = _state.scrollToRow;
                        return _react2.default.createElement("div", {
                            className: className,
                            onKeyDown: this._onKeyDown
                        }, children({
                            onSectionRendered: this._onSectionRendered,
                            scrollToColumn: scrollToColumn,
                            scrollToRow: scrollToRow
                        }));
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_onKeyDown",
                    value: function(event) {
                        var _props2 = this.props, columnCount = _props2.columnCount, disabled = _props2.disabled, mode = _props2.mode, rowCount = _props2.rowCount;
                        if (!disabled) {
                            var _state2 = this.state, scrollToColumnPrevious = _state2.scrollToColumn, scrollToRowPrevious = _state2.scrollToRow, _state3 = this.state, scrollToColumn = _state3.scrollToColumn, scrollToRow = _state3.scrollToRow;
                            // The above cases all prevent default event event behavior.
                            // This is to keep the grid from scrolling after the snap-to update.
                            switch (event.key) {
                              case "ArrowDown":
                                scrollToRow = "cells" === mode ? Math.min(scrollToRow + 1, rowCount - 1) : Math.min(this._rowStopIndex + 1, rowCount - 1);
                                break;

                              case "ArrowLeft":
                                scrollToColumn = "cells" === mode ? Math.max(scrollToColumn - 1, 0) : Math.max(this._columnStartIndex - 1, 0);
                                break;

                              case "ArrowRight":
                                scrollToColumn = "cells" === mode ? Math.min(scrollToColumn + 1, columnCount - 1) : Math.min(this._columnStopIndex + 1, columnCount - 1);
                                break;

                              case "ArrowUp":
                                scrollToRow = "cells" === mode ? Math.max(scrollToRow - 1, 0) : Math.max(this._rowStartIndex - 1, 0);
                            }
                            scrollToColumn === scrollToColumnPrevious && scrollToRow === scrollToRowPrevious || (event.preventDefault(), 
                            this.setState({
                                scrollToColumn: scrollToColumn,
                                scrollToRow: scrollToRow
                            }));
                        }
                    }
                }, {
                    key: "_onSectionRendered",
                    value: function(_ref) {
                        var columnStartIndex = _ref.columnStartIndex, columnStopIndex = _ref.columnStopIndex, rowStartIndex = _ref.rowStartIndex, rowStopIndex = _ref.rowStopIndex;
                        this._columnStartIndex = columnStartIndex, this._columnStopIndex = columnStopIndex, 
                        this._rowStartIndex = rowStartIndex, this._rowStopIndex = rowStopIndex;
                    }
                } ]), ArrowKeyStepper;
            }(_react.Component);
            ArrowKeyStepper.defaultProps = {
                disabled: !1,
                mode: "edges",
                scrollToColumn: 0,
                scrollToRow: 0
            }, exports.default = ArrowKeyStepper, "production" !== process.env.NODE_ENV ? ArrowKeyStepper.propTypes = {
                children: _react.PropTypes.func.isRequired,
                className: _react.PropTypes.string,
                columnCount: _react.PropTypes.number.isRequired,
                disabled: _react.PropTypes.bool.isRequired,
                mode: _react.PropTypes.oneOf([ "cells", "edges" ]),
                rowCount: _react.PropTypes.number.isRequired,
                scrollToColumn: _react.PropTypes.number.isRequired,
                scrollToRow: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 32 */
    /***/
    function(module, exports, __webpack_require__) {
        module.exports = __webpack_require__(33);
    }, /* 33 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Copyright 2013-present, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 *
	 */
        "use strict";
        /**
	 * Does a shallow comparison for props and state.
	 * See ReactComponentWithPureRenderMixin
	 * See also https://facebook.github.io/react/docs/shallow-compare.html
	 */
        function shallowCompare(instance, nextProps, nextState) {
            return !shallowEqual(instance.props, nextProps) || !shallowEqual(instance.state, nextState);
        }
        var shallowEqual = __webpack_require__(34);
        module.exports = shallowCompare;
    }, /* 34 */
    /***/
    function(module, exports) {
        /**
	 * Copyright (c) 2013-present, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 *
	 * @typechecks
	 * 
	 */
        /*eslint-disable no-self-compare */
        "use strict";
        /**
	 * inlined Object.is polyfill to avoid requiring consumers ship their own
	 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
	 */
        function is(x, y) {
            // SameValue algorithm
            // SameValue algorithm
            return x === y ? 0 !== x || 0 !== y || 1 / x === 1 / y : x !== x && y !== y;
        }
        /**
	 * Performs equality by iterating through keys on an object and returning false
	 * when any key has values which are not strictly equal between the arguments.
	 * Returns true when the values of all keys are strictly equal.
	 */
        function shallowEqual(objA, objB) {
            if (is(objA, objB)) return !0;
            if ("object" != typeof objA || null === objA || "object" != typeof objB || null === objB) return !1;
            var keysA = Object.keys(objA), keysB = Object.keys(objB);
            if (keysA.length !== keysB.length) return !1;
            // Test for A's keys different from B.
            for (var i = 0; i < keysA.length; i++) if (!hasOwnProperty.call(objB, keysA[i]) || !is(objA[keysA[i]], objB[keysA[i]])) return !1;
            return !0;
        }
        var hasOwnProperty = Object.prototype.hasOwnProperty;
        module.exports = shallowEqual;
    }, /* 35 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.AutoSizer = exports.default = void 0;
        var _AutoSizer2 = __webpack_require__(36), _AutoSizer3 = _interopRequireDefault(_AutoSizer2);
        exports.default = _AutoSizer3.default, exports.AutoSizer = _AutoSizer3.default;
    }, /* 36 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _detectElementResize = __webpack_require__(37), _detectElementResize2 = _interopRequireDefault(_detectElementResize), AutoSizer = function(_Component) {
                function AutoSizer(props) {
                    _classCallCheck(this, AutoSizer);
                    var _this = _possibleConstructorReturn(this, (AutoSizer.__proto__ || Object.getPrototypeOf(AutoSizer)).call(this, props));
                    return _this.state = {
                        height: 0,
                        width: 0
                    }, _this._onResize = _this._onResize.bind(_this), _this._setRef = _this._setRef.bind(_this), 
                    _this;
                }
                return _inherits(AutoSizer, _Component), _createClass(AutoSizer, [ {
                    key: "componentDidMount",
                    value: function() {
                        // Delay access of parentNode until mount.
                        // This handles edge-cases where the component has already been unmounted before its ref has been set,
                        // As well as libraries like react-lite which have a slightly different lifecycle.
                        this._parentNode = this._autoSizer.parentNode, // Defer requiring resize handler in order to support server-side rendering.
                        // See issue #41
                        this._detectElementResize = (0, _detectElementResize2.default)(), this._detectElementResize.addResizeListener(this._parentNode, this._onResize), 
                        this._onResize();
                    }
                }, {
                    key: "componentWillUnmount",
                    value: function() {
                        this._detectElementResize && this._detectElementResize.removeResizeListener(this._parentNode, this._onResize);
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _props = this.props, children = _props.children, disableHeight = _props.disableHeight, disableWidth = _props.disableWidth, _state = this.state, height = _state.height, width = _state.width, outerStyle = {
                            overflow: "visible"
                        };
                        return disableHeight || (outerStyle.height = 0), disableWidth || (outerStyle.width = 0), 
                        _react2.default.createElement("div", {
                            ref: this._setRef,
                            style: outerStyle
                        }, children({
                            height: height,
                            width: width
                        }));
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_onResize",
                    value: function() {
                        var onResize = this.props.onResize, boundingRect = this._parentNode.getBoundingClientRect(), height = boundingRect.height || 0, width = boundingRect.width || 0, style = window.getComputedStyle(this._parentNode) || {}, paddingLeft = parseInt(style.paddingLeft, 10) || 0, paddingRight = parseInt(style.paddingRight, 10) || 0, paddingTop = parseInt(style.paddingTop, 10) || 0, paddingBottom = parseInt(style.paddingBottom, 10) || 0;
                        this.setState({
                            height: height - paddingTop - paddingBottom,
                            width: width - paddingLeft - paddingRight
                        }), onResize({
                            height: height,
                            width: width
                        });
                    }
                }, {
                    key: "_setRef",
                    value: function(autoSizer) {
                        this._autoSizer = autoSizer;
                    }
                } ]), AutoSizer;
            }(_react.Component);
            AutoSizer.defaultProps = {
                onResize: function() {}
            }, exports.default = AutoSizer, "production" !== process.env.NODE_ENV ? AutoSizer.propTypes = {
                /**
	  * Function responsible for rendering children.
	  * This function should implement the following signature:
	  * ({ height, width }) => PropTypes.element
	  */
                children: _react.PropTypes.func.isRequired,
                /** Disable dynamic :height property */
                disableHeight: _react.PropTypes.bool,
                /** Disable dynamic :width property */
                disableWidth: _react.PropTypes.bool,
                /** Callback to be invoked on-resize: ({ height, width }) */
                onResize: _react.PropTypes.func.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 37 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Detect Element Resize.
	 * https://github.com/sdecima/javascript-detect-element-resize
	 * Sebastian Decima
	 *
	 * Forked from version 0.5.3; includes the following modifications:
	 * 1) Guard against unsafe 'window' and 'document' references (to support SSR).
	 * 2) Defer initialization code via a top-level function wrapper (to support SSR).
	 * 3) Avoid unnecessary reflows by not measuring size for scroll events bubbling from children.
	 **/
        function createDetectElementResize() {
            // Check `document` and `window` in case of server-side rendering
            var _window;
            _window = "undefined" != typeof window ? window : "undefined" != typeof self ? self : this;
            var attachEvent = "undefined" != typeof document && document.attachEvent, stylesCreated = !1;
            if (!attachEvent) {
                var requestFrame = function() {
                    var raf = _window.requestAnimationFrame || _window.mozRequestAnimationFrame || _window.webkitRequestAnimationFrame || function(fn) {
                        return _window.setTimeout(fn, 20);
                    };
                    return function(fn) {
                        return raf(fn);
                    };
                }(), cancelFrame = function() {
                    var cancel = _window.cancelAnimationFrame || _window.mozCancelAnimationFrame || _window.webkitCancelAnimationFrame || _window.clearTimeout;
                    return function(id) {
                        return cancel(id);
                    };
                }(), resetTriggers = function(element) {
                    var triggers = element.__resizeTriggers__, expand = triggers.firstElementChild, contract = triggers.lastElementChild, expandChild = expand.firstElementChild;
                    contract.scrollLeft = contract.scrollWidth, contract.scrollTop = contract.scrollHeight, 
                    expandChild.style.width = expand.offsetWidth + 1 + "px", expandChild.style.height = expand.offsetHeight + 1 + "px", 
                    expand.scrollLeft = expand.scrollWidth, expand.scrollTop = expand.scrollHeight;
                }, checkTriggers = function(element) {
                    return element.offsetWidth != element.__resizeLast__.width || element.offsetHeight != element.__resizeLast__.height;
                }, scrollListener = function(e) {
                    // Don't measure (which forces) reflow for scrolls that happen inside of children!
                    if (!(e.target.className.indexOf("contract-trigger") < 0 && e.target.className.indexOf("expand-trigger") < 0)) {
                        var element = this;
                        resetTriggers(this), this.__resizeRAF__ && cancelFrame(this.__resizeRAF__), this.__resizeRAF__ = requestFrame(function() {
                            checkTriggers(element) && (element.__resizeLast__.width = element.offsetWidth, element.__resizeLast__.height = element.offsetHeight, 
                            element.__resizeListeners__.forEach(function(fn) {
                                fn.call(element, e);
                            }));
                        });
                    }
                }, animation = !1, animationstring = "animation", keyframeprefix = "", animationstartevent = "animationstart", domPrefixes = "Webkit Moz O ms".split(" "), startEvents = "webkitAnimationStart animationstart oAnimationStart MSAnimationStart".split(" "), pfx = "", elm = document.createElement("fakeelement");
                if (void 0 !== elm.style.animationName && (animation = !0), animation === !1) for (var i = 0; i < domPrefixes.length; i++) if (void 0 !== elm.style[domPrefixes[i] + "AnimationName"]) {
                    pfx = domPrefixes[i], animationstring = pfx + "Animation", keyframeprefix = "-" + pfx.toLowerCase() + "-", 
                    animationstartevent = startEvents[i], animation = !0;
                    break;
                }
                var animationName = "resizeanim", animationKeyframes = "@" + keyframeprefix + "keyframes " + animationName + " { from { opacity: 0; } to { opacity: 0; } } ", animationStyle = keyframeprefix + "animation: 1ms " + animationName + "; ";
            }
            var createStyles = function() {
                if (!stylesCreated) {
                    //opacity:0 works around a chrome bug https://code.google.com/p/chromium/issues/detail?id=286360
                    var css = (animationKeyframes ? animationKeyframes : "") + ".resize-triggers { " + (animationStyle ? animationStyle : "") + 'visibility: hidden; opacity: 0; } .resize-triggers, .resize-triggers > div, .contract-trigger:before { content: " "; display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%; overflow: hidden; z-index: -1; } .resize-triggers > div { background: #eee; overflow: auto; } .contract-trigger:before { width: 200%; height: 200%; }', head = document.head || document.getElementsByTagName("head")[0], style = document.createElement("style");
                    style.type = "text/css", style.styleSheet ? style.styleSheet.cssText = css : style.appendChild(document.createTextNode(css)), 
                    head.appendChild(style), stylesCreated = !0;
                }
            }, addResizeListener = function(element, fn) {
                if (attachEvent) element.attachEvent("onresize", fn); else {
                    if (!element.__resizeTriggers__) {
                        var elementStyle = _window.getComputedStyle(element);
                        elementStyle && "static" == elementStyle.position && (element.style.position = "relative"), 
                        createStyles(), element.__resizeLast__ = {}, element.__resizeListeners__ = [], (element.__resizeTriggers__ = document.createElement("div")).className = "resize-triggers", 
                        element.__resizeTriggers__.innerHTML = '<div class="expand-trigger"><div></div></div><div class="contract-trigger"></div>', 
                        element.appendChild(element.__resizeTriggers__), resetTriggers(element), element.addEventListener("scroll", scrollListener, !0), 
                        /* Listen for a css animation to detect element display/re-attach */
                        animationstartevent && (element.__resizeTriggers__.__animationListener__ = function(e) {
                            e.animationName == animationName && resetTriggers(element);
                        }, element.__resizeTriggers__.addEventListener(animationstartevent, element.__resizeTriggers__.__animationListener__));
                    }
                    element.__resizeListeners__.push(fn);
                }
            }, removeResizeListener = function(element, fn) {
                attachEvent ? element.detachEvent("onresize", fn) : (element.__resizeListeners__.splice(element.__resizeListeners__.indexOf(fn), 1), 
                element.__resizeListeners__.length || (element.removeEventListener("scroll", scrollListener, !0), 
                element.__resizeTriggers__.__animationListener__ && (element.__resizeTriggers__.removeEventListener(animationstartevent, element.__resizeTriggers__.__animationListener__), 
                element.__resizeTriggers__.__animationListener__ = null), element.__resizeTriggers__ = !element.removeChild(element.__resizeTriggers__)));
            };
            return {
                addResizeListener: addResizeListener,
                removeResizeListener: removeResizeListener
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = createDetectElementResize;
    }, /* 38 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.defaultCellSizeCache = exports.CellMeasurer = exports.default = void 0;
        var _CellMeasurer2 = __webpack_require__(39), _CellMeasurer3 = _interopRequireDefault(_CellMeasurer2), _defaultCellSizeCache2 = __webpack_require__(41), _defaultCellSizeCache3 = _interopRequireDefault(_defaultCellSizeCache2);
        exports.default = _CellMeasurer3.default, exports.CellMeasurer = _CellMeasurer3.default, 
        exports.defaultCellSizeCache = _defaultCellSizeCache3.default;
    }, /* 39 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _reactDom = __webpack_require__(40), _reactDom2 = _interopRequireDefault(_reactDom), _defaultCellSizeCache = __webpack_require__(41), _defaultCellSizeCache2 = _interopRequireDefault(_defaultCellSizeCache), CellMeasurer = function(_Component) {
                function CellMeasurer(props, state) {
                    _classCallCheck(this, CellMeasurer);
                    var _this = _possibleConstructorReturn(this, (CellMeasurer.__proto__ || Object.getPrototypeOf(CellMeasurer)).call(this, props, state));
                    return _this._cellSizeCache = props.cellSizeCache || new _defaultCellSizeCache2.default(), 
                    _this.getColumnWidth = _this.getColumnWidth.bind(_this), _this.getRowHeight = _this.getRowHeight.bind(_this), 
                    _this.resetMeasurements = _this.resetMeasurements.bind(_this), _this.resetMeasurementForColumn = _this.resetMeasurementForColumn.bind(_this), 
                    _this.resetMeasurementForRow = _this.resetMeasurementForRow.bind(_this), _this;
                }
                return _inherits(CellMeasurer, _Component), _createClass(CellMeasurer, [ {
                    key: "getColumnWidth",
                    value: function(_ref) {
                        var index = _ref.index;
                        if (this._cellSizeCache.hasColumnWidth(index)) return this._cellSizeCache.getColumnWidth(index);
                        for (var rowCount = this.props.rowCount, maxWidth = 0, rowIndex = 0; rowIndex < rowCount; rowIndex++) {
                            var _measureCell2 = this._measureCell({
                                clientWidth: !0,
                                columnIndex: index,
                                rowIndex: rowIndex
                            }), width = _measureCell2.width;
                            maxWidth = Math.max(maxWidth, width);
                        }
                        return this._cellSizeCache.setColumnWidth(index, maxWidth), maxWidth;
                    }
                }, {
                    key: "getRowHeight",
                    value: function(_ref2) {
                        var index = _ref2.index;
                        if (this._cellSizeCache.hasRowHeight(index)) return this._cellSizeCache.getRowHeight(index);
                        for (var columnCount = this.props.columnCount, maxHeight = 0, columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                            var _measureCell3 = this._measureCell({
                                clientHeight: !0,
                                columnIndex: columnIndex,
                                rowIndex: index
                            }), height = _measureCell3.height;
                            maxHeight = Math.max(maxHeight, height);
                        }
                        return this._cellSizeCache.setRowHeight(index, maxHeight), maxHeight;
                    }
                }, {
                    key: "resetMeasurementForColumn",
                    value: function(columnIndex) {
                        this._cellSizeCache.clearColumnWidth(columnIndex);
                    }
                }, {
                    key: "resetMeasurementForRow",
                    value: function(rowIndex) {
                        this._cellSizeCache.clearRowHeight(rowIndex);
                    }
                }, {
                    key: "resetMeasurements",
                    value: function() {
                        this._cellSizeCache.clearAllColumnWidths(), this._cellSizeCache.clearAllRowHeights();
                    }
                }, {
                    key: "componentDidMount",
                    value: function() {
                        this._renderAndMount();
                    }
                }, {
                    key: "componentWillReceiveProps",
                    value: function(nextProps) {
                        var cellSizeCache = this.props.cellSizeCache;
                        cellSizeCache !== nextProps.cellSizeCache && (this._cellSizeCache = nextProps.cellSizeCache), 
                        this._updateDivDimensions(nextProps);
                    }
                }, {
                    key: "componentWillUnmount",
                    value: function() {
                        this._unmountContainer();
                    }
                }, {
                    key: "render",
                    value: function() {
                        var children = this.props.children;
                        return children({
                            getColumnWidth: this.getColumnWidth,
                            getRowHeight: this.getRowHeight,
                            resetMeasurements: this.resetMeasurements,
                            resetMeasurementForColumn: this.resetMeasurementForColumn,
                            resetMeasurementForRow: this.resetMeasurementForRow
                        });
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_getContainerNode",
                    value: function(props) {
                        var container = props.container;
                        return container ? _reactDom2.default.findDOMNode("function" == typeof container ? container() : container) : document.body;
                    }
                }, {
                    key: "_measureCell",
                    value: function(_ref3) {
                        var _ref3$clientHeight = _ref3.clientHeight, clientHeight = void 0 !== _ref3$clientHeight && _ref3$clientHeight, _ref3$clientWidth = _ref3.clientWidth, clientWidth = void 0 === _ref3$clientWidth || _ref3$clientWidth, columnIndex = _ref3.columnIndex, rowIndex = _ref3.rowIndex, cellRenderer = this.props.cellRenderer, rendered = cellRenderer({
                            columnIndex: columnIndex,
                            rowIndex: rowIndex
                        });
                        // Handle edge case where this method is called before the CellMeasurer has completed its initial render (and mounted).
                        this._renderAndMount(), // @TODO Keep an eye on this for future React updates as the interface may change:
                        // https://twitter.com/soprano/status/737316379712331776
                        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, rendered, this._div);
                        var measurements = {
                            height: clientHeight && this._div.clientHeight,
                            width: clientWidth && this._div.clientWidth
                        };
                        return _reactDom2.default.unmountComponentAtNode(this._div), measurements;
                    }
                }, {
                    key: "_renderAndMount",
                    value: function() {
                        this._div || (this._div = document.createElement("div"), this._div.style.display = "inline-block", 
                        this._div.style.position = "absolute", this._div.style.visibility = "hidden", this._div.style.zIndex = -1, 
                        this._updateDivDimensions(this.props), this._containerNode = this._getContainerNode(this.props), 
                        this._containerNode.appendChild(this._div));
                    }
                }, {
                    key: "_unmountContainer",
                    value: function() {
                        this._div && (this._containerNode.removeChild(this._div), this._div = null), this._containerNode = null;
                    }
                }, {
                    key: "_updateDivDimensions",
                    value: function(props) {
                        var height = props.height, width = props.width;
                        height && height !== this._divHeight && (this._divHeight = height, this._div.style.height = height + "px"), 
                        width && width !== this._divWidth && (this._divWidth = width, this._div.style.width = width + "px");
                    }
                } ]), CellMeasurer;
            }(_react.Component);
            exports.default = CellMeasurer, "production" !== process.env.NODE_ENV ? CellMeasurer.propTypes = {
                /**
	   * Renders a cell given its indices.
	   * Should implement the following interface: ({ columnIndex: number, rowIndex: number }): PropTypes.node
	   */
                cellRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Optional, custom caching strategy for cell sizes.
	   */
                cellSizeCache: _react.PropTypes.object,
                /**
	   * Function responsible for rendering a virtualized component.
	   * This function should implement the following signature:
	   * ({ getColumnWidth, getRowHeight, resetMeasurements }) => PropTypes.element
	   */
                children: _react.PropTypes.func.isRequired,
                /**
	   * Number of columns in grid.
	   */
                columnCount: _react.PropTypes.number.isRequired,
                /**
	   * A Node, Component instance, or function that returns either.
	   * If this property is not specified the document body will be used.
	   */
                container: _react2.default.PropTypes.oneOfType([ _react2.default.PropTypes.func, _react2.default.PropTypes.node ]),
                /**
	   * Assign a fixed :height in order to measure dynamic text :width only.
	   */
                height: _react.PropTypes.number,
                /**
	   * Number of rows in grid.
	   */
                rowCount: _react.PropTypes.number.isRequired,
                /**
	   * Assign a fixed :width in order to measure dynamic text :height only.
	   */
                width: _react.PropTypes.number
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 40 */
    /***/
    function(module, exports) {
        module.exports = __WEBPACK_EXTERNAL_MODULE_40__;
    }, /* 41 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), CellSizeCache = function() {
            function CellSizeCache() {
                var _ref = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, _ref$uniformRowHeight = _ref.uniformRowHeight, uniformRowHeight = void 0 !== _ref$uniformRowHeight && _ref$uniformRowHeight, _ref$uniformColumnWid = _ref.uniformColumnWidth, uniformColumnWidth = void 0 !== _ref$uniformColumnWid && _ref$uniformColumnWid;
                _classCallCheck(this, CellSizeCache), this._uniformRowHeight = uniformRowHeight, 
                this._uniformColumnWidth = uniformColumnWidth, this._cachedColumnWidths = {}, this._cachedRowHeights = {};
            }
            return _createClass(CellSizeCache, [ {
                key: "clearAllColumnWidths",
                value: function() {
                    this._cachedColumnWidth = void 0, this._cachedColumnWidths = {};
                }
            }, {
                key: "clearAllRowHeights",
                value: function() {
                    this._cachedRowHeight = void 0, this._cachedRowHeights = {};
                }
            }, {
                key: "clearColumnWidth",
                value: function(index) {
                    this._cachedColumnWidth = void 0, delete this._cachedColumnWidths[index];
                }
            }, {
                key: "clearRowHeight",
                value: function(index) {
                    this._cachedRowHeight = void 0, delete this._cachedRowHeights[index];
                }
            }, {
                key: "getColumnWidth",
                value: function(index) {
                    return this._uniformColumnWidth ? this._cachedColumnWidth : this._cachedColumnWidths[index];
                }
            }, {
                key: "getRowHeight",
                value: function(index) {
                    return this._uniformRowHeight ? this._cachedRowHeight : this._cachedRowHeights[index];
                }
            }, {
                key: "hasColumnWidth",
                value: function(index) {
                    return this._uniformColumnWidth ? !!this._cachedColumnWidth : !!this._cachedColumnWidths[index];
                }
            }, {
                key: "hasRowHeight",
                value: function(index) {
                    return this._uniformRowHeight ? !!this._cachedRowHeight : !!this._cachedRowHeights[index];
                }
            }, {
                key: "setColumnWidth",
                value: function(index, width) {
                    this._cachedColumnWidth = width, this._cachedColumnWidths[index] = width;
                }
            }, {
                key: "setRowHeight",
                value: function(index, height) {
                    this._cachedRowHeight = height, this._cachedRowHeights[index] = height;
                }
            } ]), CellSizeCache;
        }();
        exports.default = CellSizeCache;
    }, /* 42 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.Collection = exports.default = void 0;
        var _Collection2 = __webpack_require__(43), _Collection3 = _interopRequireDefault(_Collection2);
        exports.default = _Collection3.default, exports.Collection = _Collection3.default;
    }, /* 43 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _objectWithoutProperties(obj, keys) {
                var target = {};
                for (var i in obj) keys.indexOf(i) >= 0 || Object.prototype.hasOwnProperty.call(obj, i) && (target[i] = obj[i]);
                return target;
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            function defaultCellGroupRenderer(_ref5) {
                var cellCache = _ref5.cellCache, cellRenderer = _ref5.cellRenderer, cellSizeAndPositionGetter = _ref5.cellSizeAndPositionGetter, indices = _ref5.indices, isScrolling = _ref5.isScrolling;
                return indices.map(function(index) {
                    var cellMetadata = cellSizeAndPositionGetter({
                        index: index
                    }), cellRendererProps = {
                        index: index,
                        isScrolling: isScrolling,
                        key: index,
                        style: {
                            height: cellMetadata.height,
                            left: cellMetadata.x,
                            position: "absolute",
                            top: cellMetadata.y,
                            width: cellMetadata.width
                        }
                    };
                    // Avoid re-creating cells while scrolling.
                    // This can lead to the same cell being created many times and can cause performance issues for "heavy" cells.
                    // If a scroll is in progress- cache and reuse cells.
                    // This cache will be thrown away once scrolling complets.
                    // Avoid re-creating cells while scrolling.
                    // This can lead to the same cell being created many times and can cause performance issues for "heavy" cells.
                    // If a scroll is in progress- cache and reuse cells.
                    // This cache will be thrown away once scrolling complets.
                    return isScrolling ? (index in cellCache || (cellCache[index] = cellRenderer(cellRendererProps)), 
                    cellCache[index]) : cellRenderer(cellRendererProps);
                }).filter(function(renderedCell) {
                    return !!renderedCell;
                });
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            }, _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _CollectionView = __webpack_require__(44), _CollectionView2 = _interopRequireDefault(_CollectionView), _calculateSizeAndPositionData2 = __webpack_require__(49), _calculateSizeAndPositionData3 = _interopRequireDefault(_calculateSizeAndPositionData2), _getUpdatedOffsetForIndex = __webpack_require__(52), _getUpdatedOffsetForIndex2 = _interopRequireDefault(_getUpdatedOffsetForIndex), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), Collection = function(_Component) {
                function Collection(props, context) {
                    _classCallCheck(this, Collection);
                    var _this = _possibleConstructorReturn(this, (Collection.__proto__ || Object.getPrototypeOf(Collection)).call(this, props, context));
                    // Cell cache during scroll (for perforamnce)
                    return _this._cellMetadata = [], _this._lastRenderedCellIndices = [], _this._cellCache = [], 
                    _this._isScrollingChange = _this._isScrollingChange.bind(_this), _this;
                }
                /** See Collection#recomputeCellSizesAndPositions */
                return _inherits(Collection, _Component), _createClass(Collection, [ {
                    key: "recomputeCellSizesAndPositions",
                    value: function() {
                        this._cellCache = [], this._collectionView.recomputeCellSizesAndPositions();
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _this2 = this, props = _objectWithoutProperties(this.props, []);
                        return _react2.default.createElement(_CollectionView2.default, _extends({
                            cellLayoutManager: this,
                            isScrollingChange: this._isScrollingChange,
                            ref: function(_ref) {
                                _this2._collectionView = _ref;
                            }
                        }, props));
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "calculateSizeAndPositionData",
                    value: function() {
                        var _props = this.props, cellCount = _props.cellCount, cellSizeAndPositionGetter = _props.cellSizeAndPositionGetter, sectionSize = _props.sectionSize, data = (0, 
                        _calculateSizeAndPositionData3.default)({
                            cellCount: cellCount,
                            cellSizeAndPositionGetter: cellSizeAndPositionGetter,
                            sectionSize: sectionSize
                        });
                        this._cellMetadata = data.cellMetadata, this._sectionManager = data.sectionManager, 
                        this._height = data.height, this._width = data.width;
                    }
                }, {
                    key: "getLastRenderedIndices",
                    value: function() {
                        return this._lastRenderedCellIndices;
                    }
                }, {
                    key: "getScrollPositionForCell",
                    value: function(_ref2) {
                        var align = _ref2.align, cellIndex = _ref2.cellIndex, height = _ref2.height, scrollLeft = _ref2.scrollLeft, scrollTop = _ref2.scrollTop, width = _ref2.width, cellCount = this.props.cellCount;
                        if (cellIndex >= 0 && cellIndex < cellCount) {
                            var cellMetadata = this._cellMetadata[cellIndex];
                            scrollLeft = (0, _getUpdatedOffsetForIndex2.default)({
                                align: align,
                                cellOffset: cellMetadata.x,
                                cellSize: cellMetadata.width,
                                containerSize: width,
                                currentOffset: scrollLeft,
                                targetIndex: cellIndex
                            }), scrollTop = (0, _getUpdatedOffsetForIndex2.default)({
                                align: align,
                                cellOffset: cellMetadata.y,
                                cellSize: cellMetadata.height,
                                containerSize: height,
                                currentOffset: scrollTop,
                                targetIndex: cellIndex
                            });
                        }
                        return {
                            scrollLeft: scrollLeft,
                            scrollTop: scrollTop
                        };
                    }
                }, {
                    key: "getTotalSize",
                    value: function() {
                        return {
                            height: this._height,
                            width: this._width
                        };
                    }
                }, {
                    key: "cellRenderers",
                    value: function(_ref3) {
                        var _this3 = this, height = _ref3.height, isScrolling = _ref3.isScrolling, width = _ref3.width, x = _ref3.x, y = _ref3.y, _props2 = this.props, cellGroupRenderer = _props2.cellGroupRenderer, cellRenderer = _props2.cellRenderer;
                        // Store for later calls to getLastRenderedIndices()
                        return this._lastRenderedCellIndices = this._sectionManager.getCellIndices({
                            height: height,
                            width: width,
                            x: x,
                            y: y
                        }), cellGroupRenderer({
                            cellCache: this._cellCache,
                            cellRenderer: cellRenderer,
                            cellSizeAndPositionGetter: function(_ref4) {
                                var index = _ref4.index;
                                return _this3._sectionManager.getCellMetadata({
                                    index: index
                                });
                            },
                            indices: this._lastRenderedCellIndices,
                            isScrolling: isScrolling
                        });
                    }
                }, {
                    key: "_isScrollingChange",
                    value: function(isScrolling) {
                        isScrolling || (this._cellCache = []);
                    }
                } ]), Collection;
            }(_react.Component);
            Collection.defaultProps = {
                "aria-label": "grid",
                cellGroupRenderer: defaultCellGroupRenderer
            }, exports.default = Collection, "production" !== process.env.NODE_ENV ? Collection.propTypes = {
                "aria-label": _react.PropTypes.string,
                /**
	   * Number of cells in Collection.
	   */
                cellCount: _react.PropTypes.number.isRequired,
                /**
	   * Responsible for rendering a group of cells given their indices.
	   * Should implement the following interface: ({
	   *   cellSizeAndPositionGetter:Function,
	   *   indices: Array<number>,
	   *   cellRenderer: Function
	   * }): Array<PropTypes.node>
	   */
                cellGroupRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Responsible for rendering a cell given an row and column index.
	   * Should implement the following interface: ({ index: number, key: string, style: object }): PropTypes.element
	   */
                cellRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Callback responsible for returning size and offset/position information for a given cell (index).
	   * ({ index: number }): { height: number, width: number, x: number, y: number }
	   */
                cellSizeAndPositionGetter: _react.PropTypes.func.isRequired,
                /**
	   * Optionally override the size of the sections a Collection's cells are split into.
	   */
                sectionSize: _react.PropTypes.number
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 44 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            }, _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _classnames = __webpack_require__(45), _classnames2 = _interopRequireDefault(_classnames), _createCallbackMemoizer = __webpack_require__(46), _createCallbackMemoizer2 = _interopRequireDefault(_createCallbackMemoizer), _scrollbarSize = __webpack_require__(47), _scrollbarSize2 = _interopRequireDefault(_scrollbarSize), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), IS_SCROLLING_TIMEOUT = 150, SCROLL_POSITION_CHANGE_REASONS = {
                OBSERVED: "observed",
                REQUESTED: "requested"
            }, CollectionView = function(_Component) {
                function CollectionView(props, context) {
                    _classCallCheck(this, CollectionView);
                    var _this = _possibleConstructorReturn(this, (CollectionView.__proto__ || Object.getPrototypeOf(CollectionView)).call(this, props, context));
                    // Invokes callbacks only when their values have changed.
                    // Bind functions to instance so they don't lose context when passed around.
                    return _this.state = {
                        calculateSizeAndPositionDataOnNextUpdate: !1,
                        isScrolling: !1,
                        scrollLeft: 0,
                        scrollTop: 0
                    }, _this._onSectionRenderedMemoizer = (0, _createCallbackMemoizer2.default)(), _this._onScrollMemoizer = (0, 
                    _createCallbackMemoizer2.default)(!1), _this._invokeOnSectionRenderedHelper = _this._invokeOnSectionRenderedHelper.bind(_this), 
                    _this._onScroll = _this._onScroll.bind(_this), _this._updateScrollPositionForScrollToCell = _this._updateScrollPositionForScrollToCell.bind(_this), 
                    _this;
                }
                /**
	   * Forced recompute of cell sizes and positions.
	   * This function should be called if cell sizes have changed but nothing else has.
	   * Since cell positions are calculated by callbacks, the collection view has no way of detecting when the underlying data has changed.
	   */
                return _inherits(CollectionView, _Component), _createClass(CollectionView, [ {
                    key: "recomputeCellSizesAndPositions",
                    value: function() {
                        this.setState({
                            calculateSizeAndPositionDataOnNextUpdate: !0
                        });
                    }
                }, {
                    key: "componentDidMount",
                    value: function() {
                        var _props = this.props, cellLayoutManager = _props.cellLayoutManager, scrollLeft = _props.scrollLeft, scrollToCell = _props.scrollToCell, scrollTop = _props.scrollTop;
                        // If this component was first rendered server-side, scrollbar size will be undefined.
                        // In that event we need to remeasure.
                        this._scrollbarSizeMeasured || (this._scrollbarSize = (0, _scrollbarSize2.default)(), 
                        this._scrollbarSizeMeasured = !0, this.setState({})), scrollToCell >= 0 ? this._updateScrollPositionForScrollToCell() : (scrollLeft >= 0 || scrollTop >= 0) && this._setScrollPosition({
                            scrollLeft: scrollLeft,
                            scrollTop: scrollTop
                        }), // Update onSectionRendered callback.
                        this._invokeOnSectionRenderedHelper();
                        var _cellLayoutManager$ge = cellLayoutManager.getTotalSize(), totalHeight = _cellLayoutManager$ge.height, totalWidth = _cellLayoutManager$ge.width;
                        // Initialize onScroll callback.
                        this._invokeOnScrollMemoizer({
                            scrollLeft: scrollLeft || 0,
                            scrollTop: scrollTop || 0,
                            totalHeight: totalHeight,
                            totalWidth: totalWidth
                        });
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function(prevProps, prevState) {
                        var _props2 = this.props, height = _props2.height, scrollToAlignment = _props2.scrollToAlignment, scrollToCell = _props2.scrollToCell, width = _props2.width, _state = this.state, scrollLeft = _state.scrollLeft, scrollPositionChangeReason = _state.scrollPositionChangeReason, scrollTop = _state.scrollTop;
                        // Make sure requested changes to :scrollLeft or :scrollTop get applied.
                        // Assigning to scrollLeft/scrollTop tells the browser to interrupt any running scroll animations,
                        // And to discard any pending async changes to the scroll position that may have happened in the meantime (e.g. on a separate scrolling thread).
                        // So we only set these when we require an adjustment of the scroll position.
                        // See issue #2 for more information.
                        scrollPositionChangeReason === SCROLL_POSITION_CHANGE_REASONS.REQUESTED && (scrollLeft >= 0 && scrollLeft !== prevState.scrollLeft && scrollLeft !== this._scrollingContainer.scrollLeft && (this._scrollingContainer.scrollLeft = scrollLeft), 
                        scrollTop >= 0 && scrollTop !== prevState.scrollTop && scrollTop !== this._scrollingContainer.scrollTop && (this._scrollingContainer.scrollTop = scrollTop)), 
                        // Update scroll offsets if the current :scrollToCell values requires it
                        height === prevProps.height && scrollToAlignment === prevProps.scrollToAlignment && scrollToCell === prevProps.scrollToCell && width === prevProps.width || this._updateScrollPositionForScrollToCell(), 
                        // Update onRowsRendered callback if start/stop indices have changed
                        this._invokeOnSectionRenderedHelper();
                    }
                }, {
                    key: "componentWillMount",
                    value: function() {
                        var cellLayoutManager = this.props.cellLayoutManager;
                        cellLayoutManager.calculateSizeAndPositionData(), // If this component is being rendered server-side, getScrollbarSize() will return undefined.
                        // We handle this case in componentDidMount()
                        this._scrollbarSize = (0, _scrollbarSize2.default)(), void 0 === this._scrollbarSize ? (this._scrollbarSizeMeasured = !1, 
                        this._scrollbarSize = 0) : this._scrollbarSizeMeasured = !0;
                    }
                }, {
                    key: "componentWillUnmount",
                    value: function() {
                        this._disablePointerEventsTimeoutId && clearTimeout(this._disablePointerEventsTimeoutId);
                    }
                }, {
                    key: "componentWillUpdate",
                    value: function(nextProps, nextState) {
                        0 !== nextProps.cellCount || 0 === nextState.scrollLeft && 0 === nextState.scrollTop ? nextProps.scrollLeft === this.props.scrollLeft && nextProps.scrollTop === this.props.scrollTop || this._setScrollPosition({
                            scrollLeft: nextProps.scrollLeft,
                            scrollTop: nextProps.scrollTop
                        }) : this._setScrollPosition({
                            scrollLeft: 0,
                            scrollTop: 0
                        }), (nextProps.cellCount !== this.props.cellCount || nextProps.cellLayoutManager !== this.props.cellLayoutManager || nextState.calculateSizeAndPositionDataOnNextUpdate) && nextProps.cellLayoutManager.calculateSizeAndPositionData(), 
                        nextState.calculateSizeAndPositionDataOnNextUpdate && this.setState({
                            calculateSizeAndPositionDataOnNextUpdate: !1
                        });
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _this2 = this, _props3 = this.props, autoHeight = _props3.autoHeight, cellCount = _props3.cellCount, cellLayoutManager = _props3.cellLayoutManager, className = _props3.className, height = _props3.height, horizontalOverscanSize = _props3.horizontalOverscanSize, id = _props3.id, noContentRenderer = _props3.noContentRenderer, style = _props3.style, verticalOverscanSize = _props3.verticalOverscanSize, width = _props3.width, _state2 = this.state, isScrolling = _state2.isScrolling, scrollLeft = _state2.scrollLeft, scrollTop = _state2.scrollTop, _cellLayoutManager$ge2 = cellLayoutManager.getTotalSize(), totalHeight = _cellLayoutManager$ge2.height, totalWidth = _cellLayoutManager$ge2.width, left = Math.max(0, scrollLeft - horizontalOverscanSize), top = Math.max(0, scrollTop - verticalOverscanSize), right = Math.min(totalWidth, scrollLeft + width + horizontalOverscanSize), bottom = Math.min(totalHeight, scrollTop + height + verticalOverscanSize), childrenToDisplay = height > 0 && width > 0 ? cellLayoutManager.cellRenderers({
                            height: bottom - top,
                            isScrolling: isScrolling,
                            width: right - left,
                            x: left,
                            y: top
                        }) : [], collectionStyle = {
                            boxSizing: "border-box",
                            direction: "ltr",
                            height: autoHeight ? "auto" : height,
                            overflow: "auto",
                            position: "relative",
                            WebkitOverflowScrolling: "touch",
                            width: width,
                            willChange: "transform"
                        }, verticalScrollBarSize = totalHeight > height ? this._scrollbarSize : 0, horizontalScrollBarSize = totalWidth > width ? this._scrollbarSize : 0;
                        return totalWidth + verticalScrollBarSize <= width && (collectionStyle.overflowX = "hidden"), 
                        totalHeight + horizontalScrollBarSize <= height && (collectionStyle.overflowY = "hidden"), 
                        _react2.default.createElement("div", {
                            ref: function(_ref) {
                                _this2._scrollingContainer = _ref;
                            },
                            "aria-label": this.props["aria-label"],
                            className: (0, _classnames2.default)("ReactVirtualized__Collection", className),
                            id: id,
                            onScroll: this._onScroll,
                            role: "grid",
                            style: _extends({}, collectionStyle, style),
                            tabIndex: 0
                        }, cellCount > 0 && _react2.default.createElement("div", {
                            className: "ReactVirtualized__Collection__innerScrollContainer",
                            style: {
                                height: totalHeight,
                                maxHeight: totalHeight,
                                maxWidth: totalWidth,
                                overflow: "hidden",
                                pointerEvents: isScrolling ? "none" : "",
                                width: totalWidth
                            }
                        }, childrenToDisplay), 0 === cellCount && noContentRenderer());
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_enablePointerEventsAfterDelay",
                    value: function() {
                        var _this3 = this;
                        this._disablePointerEventsTimeoutId && clearTimeout(this._disablePointerEventsTimeoutId), 
                        this._disablePointerEventsTimeoutId = setTimeout(function() {
                            var isScrollingChange = _this3.props.isScrollingChange;
                            isScrollingChange(!1), _this3._disablePointerEventsTimeoutId = null, _this3.setState({
                                isScrolling: !1
                            });
                        }, IS_SCROLLING_TIMEOUT);
                    }
                }, {
                    key: "_invokeOnSectionRenderedHelper",
                    value: function() {
                        var _props4 = this.props, cellLayoutManager = _props4.cellLayoutManager, onSectionRendered = _props4.onSectionRendered;
                        this._onSectionRenderedMemoizer({
                            callback: onSectionRendered,
                            indices: {
                                indices: cellLayoutManager.getLastRenderedIndices()
                            }
                        });
                    }
                }, {
                    key: "_invokeOnScrollMemoizer",
                    value: function(_ref2) {
                        var _this4 = this, scrollLeft = _ref2.scrollLeft, scrollTop = _ref2.scrollTop, totalHeight = _ref2.totalHeight, totalWidth = _ref2.totalWidth;
                        this._onScrollMemoizer({
                            callback: function(_ref3) {
                                var scrollLeft = _ref3.scrollLeft, scrollTop = _ref3.scrollTop, _props5 = _this4.props, height = _props5.height, onScroll = _props5.onScroll, width = _props5.width;
                                onScroll({
                                    clientHeight: height,
                                    clientWidth: width,
                                    scrollHeight: totalHeight,
                                    scrollLeft: scrollLeft,
                                    scrollTop: scrollTop,
                                    scrollWidth: totalWidth
                                });
                            },
                            indices: {
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop
                            }
                        });
                    }
                }, {
                    key: "_setScrollPosition",
                    value: function(_ref4) {
                        var scrollLeft = _ref4.scrollLeft, scrollTop = _ref4.scrollTop, newState = {
                            scrollPositionChangeReason: SCROLL_POSITION_CHANGE_REASONS.REQUESTED
                        };
                        scrollLeft >= 0 && (newState.scrollLeft = scrollLeft), scrollTop >= 0 && (newState.scrollTop = scrollTop), 
                        (scrollLeft >= 0 && scrollLeft !== this.state.scrollLeft || scrollTop >= 0 && scrollTop !== this.state.scrollTop) && this.setState(newState);
                    }
                }, {
                    key: "_updateScrollPositionForScrollToCell",
                    value: function() {
                        var _props6 = this.props, cellLayoutManager = _props6.cellLayoutManager, height = _props6.height, scrollToAlignment = _props6.scrollToAlignment, scrollToCell = _props6.scrollToCell, width = _props6.width, _state3 = this.state, scrollLeft = _state3.scrollLeft, scrollTop = _state3.scrollTop;
                        if (scrollToCell >= 0) {
                            var scrollPosition = cellLayoutManager.getScrollPositionForCell({
                                align: scrollToAlignment,
                                cellIndex: scrollToCell,
                                height: height,
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop,
                                width: width
                            });
                            scrollPosition.scrollLeft === scrollLeft && scrollPosition.scrollTop === scrollTop || this._setScrollPosition(scrollPosition);
                        }
                    }
                }, {
                    key: "_onScroll",
                    value: function(event) {
                        // In certain edge-cases React dispatches an onScroll event with an invalid target.scrollLeft / target.scrollTop.
                        // This invalid event can be detected by comparing event.target to this component's scrollable DOM element.
                        // See issue #404 for more information.
                        if (event.target === this._scrollingContainer) {
                            // Prevent pointer events from interrupting a smooth scroll
                            this._enablePointerEventsAfterDelay();
                            // When this component is shrunk drastically, React dispatches a series of back-to-back scroll events,
                            // Gradually converging on a scrollTop that is within the bounds of the new, smaller height.
                            // This causes a series of rapid renders that is slow for long lists.
                            // We can avoid that by doing some simple bounds checking to ensure that scrollTop never exceeds the total height.
                            var _props7 = this.props, cellLayoutManager = _props7.cellLayoutManager, height = _props7.height, isScrollingChange = _props7.isScrollingChange, width = _props7.width, scrollbarSize = this._scrollbarSize, _cellLayoutManager$ge3 = cellLayoutManager.getTotalSize(), totalHeight = _cellLayoutManager$ge3.height, totalWidth = _cellLayoutManager$ge3.width, scrollLeft = Math.max(0, Math.min(totalWidth - width + scrollbarSize, event.target.scrollLeft)), scrollTop = Math.max(0, Math.min(totalHeight - height + scrollbarSize, event.target.scrollTop));
                            // Certain devices (like Apple touchpad) rapid-fire duplicate events.
                            // Don't force a re-render if this is the case.
                            // The mouse may move faster then the animation frame does.
                            // Use requestAnimationFrame to avoid over-updating.
                            if (this.state.scrollLeft !== scrollLeft || this.state.scrollTop !== scrollTop) {
                                // Browsers with cancelable scroll events (eg. Firefox) interrupt scrolling animations if scrollTop/scrollLeft is set.
                                // Other browsers (eg. Safari) don't scroll as well without the help under certain conditions (DOM or style changes during scrolling).
                                // All things considered, this seems to be the best current work around that I'm aware of.
                                // For more information see https://github.com/bvaughn/react-virtualized/pull/124
                                var scrollPositionChangeReason = event.cancelable ? SCROLL_POSITION_CHANGE_REASONS.OBSERVED : SCROLL_POSITION_CHANGE_REASONS.REQUESTED;
                                // Synchronously set :isScrolling the first time (since _setNextState will reschedule its animation frame each time it's called)
                                this.state.isScrolling || isScrollingChange(!0), this.setState({
                                    isScrolling: !0,
                                    scrollLeft: scrollLeft,
                                    scrollPositionChangeReason: scrollPositionChangeReason,
                                    scrollTop: scrollTop
                                });
                            }
                            this._invokeOnScrollMemoizer({
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop,
                                totalWidth: totalWidth,
                                totalHeight: totalHeight
                            });
                        }
                    }
                } ]), CollectionView;
            }(_react.Component);
            CollectionView.defaultProps = {
                "aria-label": "grid",
                horizontalOverscanSize: 0,
                noContentRenderer: function() {
                    return null;
                },
                onScroll: function() {
                    return null;
                },
                onSectionRendered: function() {
                    return null;
                },
                scrollToAlignment: "auto",
                style: {},
                verticalOverscanSize: 0
            }, exports.default = CollectionView, "production" !== process.env.NODE_ENV ? CollectionView.propTypes = {
                "aria-label": _react.PropTypes.string,
                /**
	   * Removes fixed height from the scrollingContainer so that the total height
	   * of rows can stretch the window. Intended for use with WindowScroller
	   */
                autoHeight: _react.PropTypes.bool,
                /**
	   * Number of cells in collection.
	   */
                cellCount: _react.PropTypes.number.isRequired,
                /**
	   * Calculates cell sizes and positions and manages rendering the appropriate cells given a specified window.
	   */
                cellLayoutManager: _react.PropTypes.object.isRequired,
                /**
	   * Optional custom CSS class name to attach to root Collection element.
	   */
                className: _react.PropTypes.string,
                /**
	   * Height of Collection; this property determines the number of visible (vs virtualized) rows.
	   */
                height: _react.PropTypes.number.isRequired,
                /**
	   * Optional custom id to attach to root Collection element.
	   */
                id: _react.PropTypes.string,
                /**
	   * Enables the `Collection` to horiontally "overscan" its content similar to how `Grid` does.
	   * This can reduce flicker around the edges when a user scrolls quickly.
	   */
                horizontalOverscanSize: _react.PropTypes.number.isRequired,
                isScrollingChange: _react.PropTypes.func,
                /**
	   * Optional renderer to be used in place of rows when either :rowCount or :cellCount is 0.
	   */
                noContentRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Callback invoked whenever the scroll offset changes within the inner scrollable region.
	   * This callback can be used to sync scrolling between lists, tables, or grids.
	   * ({ clientHeight, clientWidth, scrollHeight, scrollLeft, scrollTop, scrollWidth }): void
	   */
                onScroll: _react.PropTypes.func.isRequired,
                /**
	   * Callback invoked with information about the section of the Collection that was just rendered.
	   * This callback is passed a named :indices parameter which is an Array of the most recently rendered section indices.
	   */
                onSectionRendered: _react.PropTypes.func.isRequired,
                /**
	   * Horizontal offset.
	   */
                scrollLeft: _react.PropTypes.number,
                /**
	   * Controls scroll-to-cell behavior of the Grid.
	   * The default ("auto") scrolls the least amount possible to ensure that the specified cell is fully visible.
	   * Use "start" to align cells to the top/left of the Grid and "end" to align bottom/right.
	   */
                scrollToAlignment: _react.PropTypes.oneOf([ "auto", "end", "start", "center" ]).isRequired,
                /**
	   * Cell index to ensure visible (by forcefully scrolling if necessary).
	   */
                scrollToCell: _react.PropTypes.number,
                /**
	   * Vertical offset.
	   */
                scrollTop: _react.PropTypes.number,
                /**
	   * Optional custom inline style to attach to root Collection element.
	   */
                style: _react.PropTypes.object,
                /**
	   * Enables the `Collection` to vertically "overscan" its content similar to how `Grid` does.
	   * This can reduce flicker around the edges when a user scrolls quickly.
	   */
                verticalOverscanSize: _react.PropTypes.number.isRequired,
                /**
	   * Width of Collection; this property determines the number of visible (vs virtualized) columns.
	   */
                width: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 45 */
    /***/
    function(module, exports, __webpack_require__) {
        var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
        /*!
	  Copyright (c) 2016 Jed Watson.
	  Licensed under the MIT License (MIT), see
	  http://jedwatson.github.io/classnames
	*/
        /* global define */
        !function() {
            "use strict";
            function classNames() {
                for (var classes = [], i = 0; i < arguments.length; i++) {
                    var arg = arguments[i];
                    if (arg) {
                        var argType = typeof arg;
                        if ("string" === argType || "number" === argType) classes.push(arg); else if (Array.isArray(arg)) classes.push(classNames.apply(null, arg)); else if ("object" === argType) for (var key in arg) hasOwn.call(arg, key) && arg[key] && classes.push(key);
                    }
                }
                return classes.join(" ");
            }
            var hasOwn = {}.hasOwnProperty;
            "undefined" != typeof module && module.exports ? module.exports = classNames : (__WEBPACK_AMD_DEFINE_ARRAY__ = [], 
            __WEBPACK_AMD_DEFINE_RESULT__ = function() {
                return classNames;
            }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), // register as 'classnames', consistent with npm package name
            !(void 0 !== __WEBPACK_AMD_DEFINE_RESULT__ && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)));
        }();
    }, /* 46 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Helper utility that updates the specified callback whenever any of the specified indices have changed.
	 */
        function createCallbackMemoizer() {
            var requireAllKeys = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], cachedIndices = {};
            return function(_ref) {
                var callback = _ref.callback, indices = _ref.indices, keys = Object.keys(indices), allInitialized = !requireAllKeys || keys.every(function(key) {
                    var value = indices[key];
                    return Array.isArray(value) ? value.length > 0 : value >= 0;
                }), indexChanged = keys.length !== Object.keys(cachedIndices).length || keys.some(function(key) {
                    var cachedValue = cachedIndices[key], value = indices[key];
                    return Array.isArray(value) ? cachedValue.join(",") !== value.join(",") : cachedValue !== value;
                });
                cachedIndices = indices, allInitialized && indexChanged && callback(indices);
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = createCallbackMemoizer;
    }, /* 47 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        var size, canUseDOM = __webpack_require__(48);
        module.exports = function(recalc) {
            if ((!size || recalc) && canUseDOM) {
                var scrollDiv = document.createElement("div");
                scrollDiv.style.position = "absolute", scrollDiv.style.top = "-9999px", scrollDiv.style.width = "50px", 
                scrollDiv.style.height = "50px", scrollDiv.style.overflow = "scroll", document.body.appendChild(scrollDiv), 
                size = scrollDiv.offsetWidth - scrollDiv.clientWidth, document.body.removeChild(scrollDiv);
            }
            return size;
        };
    }, /* 48 */
    /***/
    function(module, exports) {
        "use strict";
        module.exports = !("undefined" == typeof window || !window.document || !window.document.createElement);
    }, /* 49 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function calculateSizeAndPositionData(_ref) {
            for (var cellCount = _ref.cellCount, cellSizeAndPositionGetter = _ref.cellSizeAndPositionGetter, sectionSize = _ref.sectionSize, cellMetadata = [], sectionManager = new _SectionManager2.default(sectionSize), height = 0, width = 0, index = 0; index < cellCount; index++) {
                var cellMetadatum = cellSizeAndPositionGetter({
                    index: index
                });
                if (null == cellMetadatum.height || isNaN(cellMetadatum.height) || null == cellMetadatum.width || isNaN(cellMetadatum.width) || null == cellMetadatum.x || isNaN(cellMetadatum.x) || null == cellMetadatum.y || isNaN(cellMetadatum.y)) throw Error("Invalid metadata returned for cell " + index + ":\n        x:" + cellMetadatum.x + ", y:" + cellMetadatum.y + ", width:" + cellMetadatum.width + ", height:" + cellMetadatum.height);
                height = Math.max(height, cellMetadatum.y + cellMetadatum.height), width = Math.max(width, cellMetadatum.x + cellMetadatum.width), 
                cellMetadata[index] = cellMetadatum, sectionManager.registerCell({
                    cellMetadatum: cellMetadatum,
                    index: index
                });
            }
            return {
                cellMetadata: cellMetadata,
                height: height,
                sectionManager: sectionManager,
                width: width
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = calculateSizeAndPositionData;
        var _SectionManager = __webpack_require__(50), _SectionManager2 = _interopRequireDefault(_SectionManager);
    }, /* 50 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _Section = __webpack_require__(51), _Section2 = _interopRequireDefault(_Section), SECTION_SIZE = 100, SectionManager = function() {
            function SectionManager() {
                var sectionSize = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : SECTION_SIZE;
                _classCallCheck(this, SectionManager), this._sectionSize = sectionSize, this._cellMetadata = [], 
                this._sections = {};
            }
            /**
	   * Gets all cell indices contained in the specified region.
	   * A region may encompass 1 or more Sections.
	   */
            return _createClass(SectionManager, [ {
                key: "getCellIndices",
                value: function(_ref) {
                    var height = _ref.height, width = _ref.width, x = _ref.x, y = _ref.y, indices = {};
                    // Object keys are strings; this function returns numbers
                    return this.getSections({
                        height: height,
                        width: width,
                        x: x,
                        y: y
                    }).forEach(function(section) {
                        return section.getCellIndices().forEach(function(index) {
                            indices[index] = index;
                        });
                    }), Object.keys(indices).map(function(index) {
                        return indices[index];
                    });
                }
            }, {
                key: "getCellMetadata",
                value: function(_ref2) {
                    var index = _ref2.index;
                    return this._cellMetadata[index];
                }
            }, {
                key: "getSections",
                value: function(_ref3) {
                    for (var height = _ref3.height, width = _ref3.width, x = _ref3.x, y = _ref3.y, sectionXStart = Math.floor(x / this._sectionSize), sectionXStop = Math.floor((x + width - 1) / this._sectionSize), sectionYStart = Math.floor(y / this._sectionSize), sectionYStop = Math.floor((y + height - 1) / this._sectionSize), sections = [], sectionX = sectionXStart; sectionX <= sectionXStop; sectionX++) for (var sectionY = sectionYStart; sectionY <= sectionYStop; sectionY++) {
                        var key = sectionX + "." + sectionY;
                        this._sections[key] || (this._sections[key] = new _Section2.default({
                            height: this._sectionSize,
                            width: this._sectionSize,
                            x: sectionX * this._sectionSize,
                            y: sectionY * this._sectionSize
                        })), sections.push(this._sections[key]);
                    }
                    return sections;
                }
            }, {
                key: "getTotalSectionCount",
                value: function() {
                    return Object.keys(this._sections).length;
                }
            }, {
                key: "toString",
                value: function() {
                    var _this = this;
                    return Object.keys(this._sections).map(function(index) {
                        return _this._sections[index].toString();
                    });
                }
            }, {
                key: "registerCell",
                value: function(_ref4) {
                    var cellMetadatum = _ref4.cellMetadatum, index = _ref4.index;
                    this._cellMetadata[index] = cellMetadatum, this.getSections(cellMetadatum).forEach(function(section) {
                        return section.addCellIndex({
                            index: index
                        });
                    });
                }
            } ]), SectionManager;
        }();
        exports.default = SectionManager;
    }, /* 51 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), Section = function() {
            function Section(_ref) {
                var height = _ref.height, width = _ref.width, x = _ref.x, y = _ref.y;
                _classCallCheck(this, Section), this.height = height, this.width = width, this.x = x, 
                this.y = y, this._indexMap = {}, this._indices = [];
            }
            /** Add a cell to this section. */
            return _createClass(Section, [ {
                key: "addCellIndex",
                value: function(_ref2) {
                    var index = _ref2.index;
                    this._indexMap[index] || (this._indexMap[index] = !0, this._indices.push(index));
                }
            }, {
                key: "getCellIndices",
                value: function() {
                    return this._indices;
                }
            }, {
                key: "toString",
                value: function() {
                    return this.x + "," + this.y + " " + this.width + "x" + this.height;
                }
            } ]), Section;
        }();
        /** @rlow */
        exports.default = Section;
    }, /* 52 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Determines a new offset that ensures a certain cell is visible, given the current offset.
	 * If the cell is already visible then the current offset will be returned.
	 * If the current offset is too great or small, it will be adjusted just enough to ensure the specified index is visible.
	 *
	 * @param align Desired alignment within container; one of "auto" (default), "start", or "end"
	 * @param cellOffset Offset (x or y) position for cell
	 * @param cellSize Size (width or height) of cell
	 * @param containerSize Total size (width or height) of the container
	 * @param currentOffset Container's current (x or y) offset
	 * @return Offset to use to ensure the specified cell is visible
	 */
        function getUpdatedOffsetForIndex(_ref) {
            var _ref$align = _ref.align, align = void 0 === _ref$align ? "auto" : _ref$align, cellOffset = _ref.cellOffset, cellSize = _ref.cellSize, containerSize = _ref.containerSize, currentOffset = _ref.currentOffset, maxOffset = cellOffset, minOffset = maxOffset - containerSize + cellSize;
            switch (align) {
              case "start":
                return maxOffset;

              case "end":
                return minOffset;

              case "center":
                return maxOffset - (containerSize - cellSize) / 2;

              default:
                return Math.max(minOffset, Math.min(maxOffset, currentOffset));
            }
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = getUpdatedOffsetForIndex;
    }, /* 53 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.ColumnSizer = exports.default = void 0;
        var _ColumnSizer2 = __webpack_require__(54), _ColumnSizer3 = _interopRequireDefault(_ColumnSizer2);
        exports.default = _ColumnSizer3.default, exports.ColumnSizer = _ColumnSizer3.default;
    }, /* 54 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _Grid = __webpack_require__(55), _Grid2 = _interopRequireDefault(_Grid), ColumnSizer = function(_Component) {
                function ColumnSizer(props, context) {
                    _classCallCheck(this, ColumnSizer);
                    var _this = _possibleConstructorReturn(this, (ColumnSizer.__proto__ || Object.getPrototypeOf(ColumnSizer)).call(this, props, context));
                    return _this._registerChild = _this._registerChild.bind(_this), _this;
                }
                return _inherits(ColumnSizer, _Component), _createClass(ColumnSizer, [ {
                    key: "componentDidUpdate",
                    value: function(prevProps, prevState) {
                        var _props = this.props, columnMaxWidth = _props.columnMaxWidth, columnMinWidth = _props.columnMinWidth, columnCount = _props.columnCount, width = _props.width;
                        columnMaxWidth === prevProps.columnMaxWidth && columnMinWidth === prevProps.columnMinWidth && columnCount === prevProps.columnCount && width === prevProps.width || this._registeredChild && this._registeredChild.recomputeGridSize();
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _props2 = this.props, children = _props2.children, columnMaxWidth = _props2.columnMaxWidth, columnMinWidth = _props2.columnMinWidth, columnCount = _props2.columnCount, width = _props2.width, safeColumnMinWidth = columnMinWidth || 1, safeColumnMaxWidth = columnMaxWidth ? Math.min(columnMaxWidth, width) : width, columnWidth = width / columnCount;
                        columnWidth = Math.max(safeColumnMinWidth, columnWidth), columnWidth = Math.min(safeColumnMaxWidth, columnWidth), 
                        columnWidth = Math.floor(columnWidth);
                        var adjustedWidth = Math.min(width, columnWidth * columnCount);
                        return children({
                            adjustedWidth: adjustedWidth,
                            getColumnWidth: function() {
                                return columnWidth;
                            },
                            registerChild: this._registerChild
                        });
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_registerChild",
                    value: function(child) {
                        if (null !== child && !(child instanceof _Grid2.default)) throw Error("Unexpected child type registered; only Grid children are supported.");
                        this._registeredChild = child, this._registeredChild && this._registeredChild.recomputeGridSize();
                    }
                } ]), ColumnSizer;
            }(_react.Component);
            exports.default = ColumnSizer, "production" !== process.env.NODE_ENV ? ColumnSizer.propTypes = {
                /**
	   * Function responsible for rendering a virtualized Grid.
	   * This function should implement the following signature:
	   * ({ adjustedWidth, getColumnWidth, registerChild }) => PropTypes.element
	   *
	   * The specified :getColumnWidth function should be passed to the Grid's :columnWidth property.
	   * The :registerChild should be passed to the Grid's :ref property.
	   * The :adjustedWidth property is optional; it reflects the lesser of the overall width or the width of all columns.
	   */
                children: _react.PropTypes.func.isRequired,
                /** Optional maximum allowed column width */
                columnMaxWidth: _react.PropTypes.number,
                /** Optional minimum allowed column width */
                columnMinWidth: _react.PropTypes.number,
                /** Number of columns in Grid or Table child */
                columnCount: _react.PropTypes.number.isRequired,
                /** Width of Grid or Table child */
                width: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 55 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.defaultCellRangeRenderer = exports.Grid = exports.default = void 0;
        var _Grid2 = __webpack_require__(56), _Grid3 = _interopRequireDefault(_Grid2), _defaultCellRangeRenderer2 = __webpack_require__(62), _defaultCellRangeRenderer3 = _interopRequireDefault(_defaultCellRangeRenderer2);
        exports.default = _Grid3.default, exports.Grid = _Grid3.default, exports.defaultCellRangeRenderer = _defaultCellRangeRenderer3.default;
    }, /* 56 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            }), exports.DEFAULT_SCROLLING_RESET_TIME_INTERVAL = void 0;
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            }, _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _classnames = __webpack_require__(45), _classnames2 = _interopRequireDefault(_classnames), _calculateSizeAndPositionDataAndUpdateScrollOffset = __webpack_require__(57), _calculateSizeAndPositionDataAndUpdateScrollOffset2 = _interopRequireDefault(_calculateSizeAndPositionDataAndUpdateScrollOffset), _ScalingCellSizeAndPositionManager = __webpack_require__(58), _ScalingCellSizeAndPositionManager2 = _interopRequireDefault(_ScalingCellSizeAndPositionManager), _createCallbackMemoizer = __webpack_require__(46), _createCallbackMemoizer2 = _interopRequireDefault(_createCallbackMemoizer), _getOverscanIndices = __webpack_require__(60), _getOverscanIndices2 = _interopRequireDefault(_getOverscanIndices), _scrollbarSize = __webpack_require__(47), _scrollbarSize2 = _interopRequireDefault(_scrollbarSize), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _updateScrollIndexHelper = __webpack_require__(61), _updateScrollIndexHelper2 = _interopRequireDefault(_updateScrollIndexHelper), _defaultCellRangeRenderer = __webpack_require__(62), _defaultCellRangeRenderer2 = _interopRequireDefault(_defaultCellRangeRenderer), DEFAULT_SCROLLING_RESET_TIME_INTERVAL = exports.DEFAULT_SCROLLING_RESET_TIME_INTERVAL = 150, SCROLL_POSITION_CHANGE_REASONS = {
                OBSERVED: "observed",
                REQUESTED: "requested"
            }, Grid = function(_Component) {
                function Grid(props, context) {
                    _classCallCheck(this, Grid);
                    var _this = _possibleConstructorReturn(this, (Grid.__proto__ || Object.getPrototypeOf(Grid)).call(this, props, context));
                    // Invokes onSectionRendered callback only when start/stop row or column indices change
                    // Bind functions to instance so they don't lose context when passed around
                    // See defaultCellRangeRenderer() for more information on the usage of this cache
                    return _this.state = {
                        isScrolling: !1,
                        scrollDirectionHorizontal: _getOverscanIndices.SCROLL_DIRECTION_FORWARD,
                        scrollDirectionVertical: _getOverscanIndices.SCROLL_DIRECTION_FORWARD,
                        scrollLeft: 0,
                        scrollTop: 0
                    }, _this._onGridRenderedMemoizer = (0, _createCallbackMemoizer2.default)(), _this._onScrollMemoizer = (0, 
                    _createCallbackMemoizer2.default)(!1), _this._enablePointerEventsAfterDelayCallback = _this._enablePointerEventsAfterDelayCallback.bind(_this), 
                    _this._invokeOnGridRenderedHelper = _this._invokeOnGridRenderedHelper.bind(_this), 
                    _this._onScroll = _this._onScroll.bind(_this), _this._updateScrollLeftForScrollToColumn = _this._updateScrollLeftForScrollToColumn.bind(_this), 
                    _this._updateScrollTopForScrollToRow = _this._updateScrollTopForScrollToRow.bind(_this), 
                    _this._columnWidthGetter = _this._wrapSizeGetter(props.columnWidth), _this._rowHeightGetter = _this._wrapSizeGetter(props.rowHeight), 
                    _this._columnSizeAndPositionManager = new _ScalingCellSizeAndPositionManager2.default({
                        cellCount: props.columnCount,
                        cellSizeGetter: function(index) {
                            return _this._columnWidthGetter(index);
                        },
                        estimatedCellSize: _this._getEstimatedColumnSize(props)
                    }), _this._rowSizeAndPositionManager = new _ScalingCellSizeAndPositionManager2.default({
                        cellCount: props.rowCount,
                        cellSizeGetter: function(index) {
                            return _this._rowHeightGetter(index);
                        },
                        estimatedCellSize: _this._getEstimatedRowSize(props)
                    }), _this._cellCache = {}, _this;
                }
                /**
	   * Pre-measure all columns and rows in a Grid.
	   * Typically cells are only measured as needed and estimated sizes are used for cells that have not yet been measured.
	   * This method ensures that the next call to getTotalSize() returns an exact size (as opposed to just an estimated one).
	   */
                return _inherits(Grid, _Component), _createClass(Grid, [ {
                    key: "measureAllCells",
                    value: function() {
                        var _props = this.props, columnCount = _props.columnCount, rowCount = _props.rowCount;
                        this._columnSizeAndPositionManager.getSizeAndPositionOfCell(columnCount - 1), this._rowSizeAndPositionManager.getSizeAndPositionOfCell(rowCount - 1);
                    }
                }, {
                    key: "recomputeGridSize",
                    value: function() {
                        var _ref = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, _ref$columnIndex = _ref.columnIndex, columnIndex = void 0 === _ref$columnIndex ? 0 : _ref$columnIndex, _ref$rowIndex = _ref.rowIndex, rowIndex = void 0 === _ref$rowIndex ? 0 : _ref$rowIndex;
                        this._columnSizeAndPositionManager.resetCell(columnIndex), this._rowSizeAndPositionManager.resetCell(rowIndex), 
                        // Clear cell cache in case we are scrolling;
                        // Invalid row heights likely mean invalid cached content as well.
                        this._cellCache = {}, this.forceUpdate();
                    }
                }, {
                    key: "componentDidMount",
                    value: function() {
                        var _props2 = this.props, scrollLeft = _props2.scrollLeft, scrollToColumn = _props2.scrollToColumn, scrollTop = _props2.scrollTop, scrollToRow = _props2.scrollToRow;
                        // If this component was first rendered server-side, scrollbar size will be undefined.
                        // In that event we need to remeasure.
                        this._scrollbarSizeMeasured || (this._scrollbarSize = (0, _scrollbarSize2.default)(), 
                        this._scrollbarSizeMeasured = !0, this.setState({})), (scrollLeft >= 0 || scrollTop >= 0) && this._setScrollPosition({
                            scrollLeft: scrollLeft,
                            scrollTop: scrollTop
                        }), (scrollToColumn >= 0 || scrollToRow >= 0) && (this._updateScrollLeftForScrollToColumn(), 
                        this._updateScrollTopForScrollToRow()), // Update onRowsRendered callback
                        this._invokeOnGridRenderedHelper(), // Initialize onScroll callback
                        this._invokeOnScrollMemoizer({
                            scrollLeft: scrollLeft || 0,
                            scrollTop: scrollTop || 0,
                            totalColumnsWidth: this._columnSizeAndPositionManager.getTotalSize(),
                            totalRowsHeight: this._rowSizeAndPositionManager.getTotalSize()
                        });
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function(prevProps, prevState) {
                        var _this2 = this, _props3 = this.props, autoHeight = _props3.autoHeight, columnCount = _props3.columnCount, height = _props3.height, rowCount = _props3.rowCount, scrollToAlignment = _props3.scrollToAlignment, scrollToColumn = _props3.scrollToColumn, scrollToRow = _props3.scrollToRow, width = _props3.width, _state = this.state, scrollLeft = _state.scrollLeft, scrollPositionChangeReason = _state.scrollPositionChangeReason, scrollTop = _state.scrollTop, columnOrRowCountJustIncreasedFromZero = columnCount > 0 && 0 === prevProps.columnCount || rowCount > 0 && 0 === prevProps.rowCount;
                        // Changes to :scrollLeft or :scrollTop should also notify :onScroll listeners
                        if (// Make sure requested changes to :scrollLeft or :scrollTop get applied.
                        // Assigning to scrollLeft/scrollTop tells the browser to interrupt any running scroll animations,
                        // And to discard any pending async changes to the scroll position that may have happened in the meantime (e.g. on a separate scrolling thread).
                        // So we only set these when we require an adjustment of the scroll position.
                        // See issue #2 for more information.
                        scrollPositionChangeReason === SCROLL_POSITION_CHANGE_REASONS.REQUESTED && (scrollLeft >= 0 && (scrollLeft !== prevState.scrollLeft && scrollLeft !== this._scrollingContainer.scrollLeft || columnOrRowCountJustIncreasedFromZero) && (this._scrollingContainer.scrollLeft = scrollLeft), 
                        // @TRICKY :autoHeight property instructs Grid to leave :scrollTop management to an external HOC (eg WindowScroller).
                        // In this case we should avoid checking scrollingContainer.scrollTop since it forces layout/flow.
                        !autoHeight && scrollTop >= 0 && (scrollTop !== prevState.scrollTop && scrollTop !== this._scrollingContainer.scrollTop || columnOrRowCountJustIncreasedFromZero) && (this._scrollingContainer.scrollTop = scrollTop)), 
                        // Update scroll offsets if the current :scrollToColumn or :scrollToRow values requires it
                        // @TODO Do we also need this check or can the one in componentWillUpdate() suffice?
                        (0, _updateScrollIndexHelper2.default)({
                            cellSizeAndPositionManager: this._columnSizeAndPositionManager,
                            previousCellsCount: prevProps.columnCount,
                            previousCellSize: prevProps.columnWidth,
                            previousScrollToAlignment: prevProps.scrollToAlignment,
                            previousScrollToIndex: prevProps.scrollToColumn,
                            previousSize: prevProps.width,
                            scrollOffset: scrollLeft,
                            scrollToAlignment: scrollToAlignment,
                            scrollToIndex: scrollToColumn,
                            size: width,
                            updateScrollIndexCallback: function(scrollToColumn) {
                                return _this2._updateScrollLeftForScrollToColumn(_extends({}, _this2.props, {
                                    scrollToColumn: scrollToColumn
                                }));
                            }
                        }), (0, _updateScrollIndexHelper2.default)({
                            cellSizeAndPositionManager: this._rowSizeAndPositionManager,
                            previousCellsCount: prevProps.rowCount,
                            previousCellSize: prevProps.rowHeight,
                            previousScrollToAlignment: prevProps.scrollToAlignment,
                            previousScrollToIndex: prevProps.scrollToRow,
                            previousSize: prevProps.height,
                            scrollOffset: scrollTop,
                            scrollToAlignment: scrollToAlignment,
                            scrollToIndex: scrollToRow,
                            size: height,
                            updateScrollIndexCallback: function(scrollToRow) {
                                return _this2._updateScrollTopForScrollToRow(_extends({}, _this2.props, {
                                    scrollToRow: scrollToRow
                                }));
                            }
                        }), // Update onRowsRendered callback if start/stop indices have changed
                        this._invokeOnGridRenderedHelper(), scrollLeft !== prevState.scrollLeft || scrollTop !== prevState.scrollTop) {
                            var totalRowsHeight = this._rowSizeAndPositionManager.getTotalSize(), totalColumnsWidth = this._columnSizeAndPositionManager.getTotalSize();
                            this._invokeOnScrollMemoizer({
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop,
                                totalColumnsWidth: totalColumnsWidth,
                                totalRowsHeight: totalRowsHeight
                            });
                        }
                    }
                }, {
                    key: "componentWillMount",
                    value: function() {
                        // If this component is being rendered server-side, getScrollbarSize() will return undefined.
                        // We handle this case in componentDidMount()
                        this._scrollbarSize = (0, _scrollbarSize2.default)(), void 0 === this._scrollbarSize ? (this._scrollbarSizeMeasured = !1, 
                        this._scrollbarSize = 0) : this._scrollbarSizeMeasured = !0, this._calculateChildrenToRender();
                    }
                }, {
                    key: "componentWillUnmount",
                    value: function() {
                        this._disablePointerEventsTimeoutId && clearTimeout(this._disablePointerEventsTimeoutId);
                    }
                }, {
                    key: "componentWillUpdate",
                    value: function(nextProps, nextState) {
                        var _this3 = this;
                        0 === nextProps.columnCount && 0 !== nextState.scrollLeft || 0 === nextProps.rowCount && 0 !== nextState.scrollTop ? this._setScrollPosition({
                            scrollLeft: 0,
                            scrollTop: 0
                        }) : nextProps.scrollLeft === this.props.scrollLeft && nextProps.scrollTop === this.props.scrollTop || this._setScrollPosition({
                            scrollLeft: nextProps.scrollLeft,
                            scrollTop: nextProps.scrollTop
                        }), this._columnWidthGetter = this._wrapSizeGetter(nextProps.columnWidth), this._rowHeightGetter = this._wrapSizeGetter(nextProps.rowHeight), 
                        this._columnSizeAndPositionManager.configure({
                            cellCount: nextProps.columnCount,
                            estimatedCellSize: this._getEstimatedColumnSize(nextProps)
                        }), this._rowSizeAndPositionManager.configure({
                            cellCount: nextProps.rowCount,
                            estimatedCellSize: this._getEstimatedRowSize(nextProps)
                        }), // Update scroll offsets if the size or number of cells have changed, invalidating the previous value
                        (0, _calculateSizeAndPositionDataAndUpdateScrollOffset2.default)({
                            cellCount: this.props.columnCount,
                            cellSize: this.props.columnWidth,
                            computeMetadataCallback: function() {
                                return _this3._columnSizeAndPositionManager.resetCell(0);
                            },
                            computeMetadataCallbackProps: nextProps,
                            nextCellsCount: nextProps.columnCount,
                            nextCellSize: nextProps.columnWidth,
                            nextScrollToIndex: nextProps.scrollToColumn,
                            scrollToIndex: this.props.scrollToColumn,
                            updateScrollOffsetForScrollToIndex: function() {
                                return _this3._updateScrollLeftForScrollToColumn(nextProps, nextState);
                            }
                        }), (0, _calculateSizeAndPositionDataAndUpdateScrollOffset2.default)({
                            cellCount: this.props.rowCount,
                            cellSize: this.props.rowHeight,
                            computeMetadataCallback: function() {
                                return _this3._rowSizeAndPositionManager.resetCell(0);
                            },
                            computeMetadataCallbackProps: nextProps,
                            nextCellsCount: nextProps.rowCount,
                            nextCellSize: nextProps.rowHeight,
                            nextScrollToIndex: nextProps.scrollToRow,
                            scrollToIndex: this.props.scrollToRow,
                            updateScrollOffsetForScrollToIndex: function() {
                                return _this3._updateScrollTopForScrollToRow(nextProps, nextState);
                            }
                        }), this._calculateChildrenToRender(nextProps, nextState);
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _this4 = this, _props4 = this.props, autoContainerWidth = _props4.autoContainerWidth, autoHeight = _props4.autoHeight, className = _props4.className, containerStyle = _props4.containerStyle, height = _props4.height, id = _props4.id, noContentRenderer = _props4.noContentRenderer, style = _props4.style, tabIndex = _props4.tabIndex, width = _props4.width, isScrolling = this.state.isScrolling, gridStyle = {
                            boxSizing: "border-box",
                            direction: "ltr",
                            height: autoHeight ? "auto" : height,
                            position: "relative",
                            width: width,
                            WebkitOverflowScrolling: "touch",
                            willChange: "transform"
                        }, totalColumnsWidth = this._columnSizeAndPositionManager.getTotalSize(), totalRowsHeight = this._rowSizeAndPositionManager.getTotalSize(), verticalScrollBarSize = totalRowsHeight > height ? this._scrollbarSize : 0, horizontalScrollBarSize = totalColumnsWidth > width ? this._scrollbarSize : 0;
                        // Also explicitly init styles to 'auto' if scrollbars are required.
                        // This works around an obscure edge case where external CSS styles have not yet been loaded,
                        // But an initial scroll index of offset is set as an external prop.
                        // Without this style, Grid would render the correct range of cells but would NOT update its internal offset.
                        // This was originally reported via clauderic/react-infinite-calendar/issues/23
                        gridStyle.overflowX = totalColumnsWidth + verticalScrollBarSize <= width ? "hidden" : "auto", 
                        gridStyle.overflowY = totalRowsHeight + horizontalScrollBarSize <= height ? "hidden" : "auto";
                        var childrenToDisplay = this._childrenToDisplay, showNoContentRenderer = 0 === childrenToDisplay.length && height > 0 && width > 0;
                        return _react2.default.createElement("div", {
                            ref: function(_ref2) {
                                _this4._scrollingContainer = _ref2;
                            },
                            "aria-label": this.props["aria-label"],
                            className: (0, _classnames2.default)("ReactVirtualized__Grid", className),
                            id: id,
                            onScroll: this._onScroll,
                            role: "grid",
                            style: _extends({}, gridStyle, style),
                            tabIndex: tabIndex
                        }, childrenToDisplay.length > 0 && _react2.default.createElement("div", {
                            className: "ReactVirtualized__Grid__innerScrollContainer",
                            style: _extends({
                                width: autoContainerWidth ? "auto" : totalColumnsWidth,
                                height: totalRowsHeight,
                                maxWidth: totalColumnsWidth,
                                maxHeight: totalRowsHeight,
                                overflow: "hidden",
                                pointerEvents: isScrolling ? "none" : ""
                            }, containerStyle)
                        }, childrenToDisplay), showNoContentRenderer && noContentRenderer());
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_calculateChildrenToRender",
                    value: function() {
                        var props = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.props, state = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.state, cellRenderer = props.cellRenderer, cellRangeRenderer = props.cellRangeRenderer, columnCount = props.columnCount, height = props.height, overscanColumnCount = props.overscanColumnCount, overscanRowCount = props.overscanRowCount, rowCount = props.rowCount, width = props.width, isScrolling = state.isScrolling, scrollDirectionHorizontal = state.scrollDirectionHorizontal, scrollDirectionVertical = state.scrollDirectionVertical, scrollLeft = state.scrollLeft, scrollTop = state.scrollTop;
                        // Render only enough columns and rows to cover the visible area of the grid.
                        if (this._childrenToDisplay = [], height > 0 && width > 0) {
                            var visibleColumnIndices = this._columnSizeAndPositionManager.getVisibleCellRange({
                                containerSize: width,
                                offset: scrollLeft
                            }), visibleRowIndices = this._rowSizeAndPositionManager.getVisibleCellRange({
                                containerSize: height,
                                offset: scrollTop
                            }), horizontalOffsetAdjustment = this._columnSizeAndPositionManager.getOffsetAdjustment({
                                containerSize: width,
                                offset: scrollLeft
                            }), verticalOffsetAdjustment = this._rowSizeAndPositionManager.getOffsetAdjustment({
                                containerSize: height,
                                offset: scrollTop
                            });
                            // Store for _invokeOnGridRenderedHelper()
                            this._renderedColumnStartIndex = visibleColumnIndices.start, this._renderedColumnStopIndex = visibleColumnIndices.stop, 
                            this._renderedRowStartIndex = visibleRowIndices.start, this._renderedRowStopIndex = visibleRowIndices.stop;
                            var overscanColumnIndices = (0, _getOverscanIndices2.default)({
                                cellCount: columnCount,
                                overscanCellsCount: overscanColumnCount,
                                scrollDirection: scrollDirectionHorizontal,
                                startIndex: this._renderedColumnStartIndex,
                                stopIndex: this._renderedColumnStopIndex
                            }), overscanRowIndices = (0, _getOverscanIndices2.default)({
                                cellCount: rowCount,
                                overscanCellsCount: overscanRowCount,
                                scrollDirection: scrollDirectionVertical,
                                startIndex: this._renderedRowStartIndex,
                                stopIndex: this._renderedRowStopIndex
                            });
                            // Store for _invokeOnGridRenderedHelper()
                            this._columnStartIndex = overscanColumnIndices.overscanStartIndex, this._columnStopIndex = overscanColumnIndices.overscanStopIndex, 
                            this._rowStartIndex = overscanRowIndices.overscanStartIndex, this._rowStopIndex = overscanRowIndices.overscanStopIndex, 
                            this._childrenToDisplay = cellRangeRenderer({
                                cellCache: this._cellCache,
                                cellRenderer: cellRenderer,
                                columnSizeAndPositionManager: this._columnSizeAndPositionManager,
                                columnStartIndex: this._columnStartIndex,
                                columnStopIndex: this._columnStopIndex,
                                horizontalOffsetAdjustment: horizontalOffsetAdjustment,
                                isScrolling: isScrolling,
                                rowSizeAndPositionManager: this._rowSizeAndPositionManager,
                                rowStartIndex: this._rowStartIndex,
                                rowStopIndex: this._rowStopIndex,
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop,
                                verticalOffsetAdjustment: verticalOffsetAdjustment,
                                visibleColumnIndices: visibleColumnIndices,
                                visibleRowIndices: visibleRowIndices
                            });
                        }
                    }
                }, {
                    key: "_enablePointerEventsAfterDelay",
                    value: function() {
                        var scrollingResetTimeInterval = this.props.scrollingResetTimeInterval;
                        this._disablePointerEventsTimeoutId && clearTimeout(this._disablePointerEventsTimeoutId), 
                        this._disablePointerEventsTimeoutId = setTimeout(this._enablePointerEventsAfterDelayCallback, scrollingResetTimeInterval);
                    }
                }, {
                    key: "_enablePointerEventsAfterDelayCallback",
                    value: function() {
                        this._disablePointerEventsTimeoutId = null, // Throw away cell cache once scrolling is complete
                        this._cellCache = {}, this.setState({
                            isScrolling: !1
                        });
                    }
                }, {
                    key: "_getEstimatedColumnSize",
                    value: function(props) {
                        return "number" == typeof props.columnWidth ? props.columnWidth : props.estimatedColumnSize;
                    }
                }, {
                    key: "_getEstimatedRowSize",
                    value: function(props) {
                        return "number" == typeof props.rowHeight ? props.rowHeight : props.estimatedRowSize;
                    }
                }, {
                    key: "_invokeOnGridRenderedHelper",
                    value: function() {
                        var onSectionRendered = this.props.onSectionRendered;
                        this._onGridRenderedMemoizer({
                            callback: onSectionRendered,
                            indices: {
                                columnOverscanStartIndex: this._columnStartIndex,
                                columnOverscanStopIndex: this._columnStopIndex,
                                columnStartIndex: this._renderedColumnStartIndex,
                                columnStopIndex: this._renderedColumnStopIndex,
                                rowOverscanStartIndex: this._rowStartIndex,
                                rowOverscanStopIndex: this._rowStopIndex,
                                rowStartIndex: this._renderedRowStartIndex,
                                rowStopIndex: this._renderedRowStopIndex
                            }
                        });
                    }
                }, {
                    key: "_invokeOnScrollMemoizer",
                    value: function(_ref3) {
                        var _this5 = this, scrollLeft = _ref3.scrollLeft, scrollTop = _ref3.scrollTop, totalColumnsWidth = _ref3.totalColumnsWidth, totalRowsHeight = _ref3.totalRowsHeight;
                        this._onScrollMemoizer({
                            callback: function(_ref4) {
                                var scrollLeft = _ref4.scrollLeft, scrollTop = _ref4.scrollTop, _props5 = _this5.props, height = _props5.height, onScroll = _props5.onScroll, width = _props5.width;
                                onScroll({
                                    clientHeight: height,
                                    clientWidth: width,
                                    scrollHeight: totalRowsHeight,
                                    scrollLeft: scrollLeft,
                                    scrollTop: scrollTop,
                                    scrollWidth: totalColumnsWidth
                                });
                            },
                            indices: {
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop
                            }
                        });
                    }
                }, {
                    key: "_setScrollPosition",
                    value: function(_ref5) {
                        var scrollLeft = _ref5.scrollLeft, scrollTop = _ref5.scrollTop, newState = {
                            scrollPositionChangeReason: SCROLL_POSITION_CHANGE_REASONS.REQUESTED
                        };
                        scrollLeft >= 0 && (newState.scrollLeft = scrollLeft), scrollTop >= 0 && (newState.scrollTop = scrollTop), 
                        (scrollLeft >= 0 && scrollLeft !== this.state.scrollLeft || scrollTop >= 0 && scrollTop !== this.state.scrollTop) && this.setState(newState);
                    }
                }, {
                    key: "_wrapPropertyGetter",
                    value: function(value) {
                        return value instanceof Function ? value : function() {
                            return value;
                        };
                    }
                }, {
                    key: "_wrapSizeGetter",
                    value: function(size) {
                        return this._wrapPropertyGetter(size);
                    }
                }, {
                    key: "_updateScrollLeftForScrollToColumn",
                    value: function() {
                        var props = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.props, state = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.state, columnCount = props.columnCount, scrollToAlignment = props.scrollToAlignment, scrollToColumn = props.scrollToColumn, width = props.width, scrollLeft = state.scrollLeft;
                        if (scrollToColumn >= 0 && columnCount > 0) {
                            var targetIndex = Math.max(0, Math.min(columnCount - 1, scrollToColumn)), calculatedScrollLeft = this._columnSizeAndPositionManager.getUpdatedOffsetForIndex({
                                align: scrollToAlignment,
                                containerSize: width,
                                currentOffset: scrollLeft,
                                targetIndex: targetIndex
                            });
                            scrollLeft !== calculatedScrollLeft && this._setScrollPosition({
                                scrollLeft: calculatedScrollLeft
                            });
                        }
                    }
                }, {
                    key: "_updateScrollTopForScrollToRow",
                    value: function() {
                        var props = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.props, state = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.state, height = props.height, rowCount = props.rowCount, scrollToAlignment = props.scrollToAlignment, scrollToRow = props.scrollToRow, scrollTop = state.scrollTop;
                        if (scrollToRow >= 0 && rowCount > 0) {
                            var targetIndex = Math.max(0, Math.min(rowCount - 1, scrollToRow)), calculatedScrollTop = this._rowSizeAndPositionManager.getUpdatedOffsetForIndex({
                                align: scrollToAlignment,
                                containerSize: height,
                                currentOffset: scrollTop,
                                targetIndex: targetIndex
                            });
                            scrollTop !== calculatedScrollTop && this._setScrollPosition({
                                scrollTop: calculatedScrollTop
                            });
                        }
                    }
                }, {
                    key: "_onScroll",
                    value: function(event) {
                        // In certain edge-cases React dispatches an onScroll event with an invalid target.scrollLeft / target.scrollTop.
                        // This invalid event can be detected by comparing event.target to this component's scrollable DOM element.
                        // See issue #404 for more information.
                        if (event.target === this._scrollingContainer) {
                            // Prevent pointer events from interrupting a smooth scroll
                            this._enablePointerEventsAfterDelay();
                            // When this component is shrunk drastically, React dispatches a series of back-to-back scroll events,
                            // Gradually converging on a scrollTop that is within the bounds of the new, smaller height.
                            // This causes a series of rapid renders that is slow for long lists.
                            // We can avoid that by doing some simple bounds checking to ensure that scrollTop never exceeds the total height.
                            var _props6 = this.props, height = _props6.height, width = _props6.width, scrollbarSize = this._scrollbarSize, totalRowsHeight = this._rowSizeAndPositionManager.getTotalSize(), totalColumnsWidth = this._columnSizeAndPositionManager.getTotalSize(), scrollLeft = Math.min(Math.max(0, totalColumnsWidth - width + scrollbarSize), event.target.scrollLeft), scrollTop = Math.min(Math.max(0, totalRowsHeight - height + scrollbarSize), event.target.scrollTop);
                            // Certain devices (like Apple touchpad) rapid-fire duplicate events.
                            // Don't force a re-render if this is the case.
                            // The mouse may move faster then the animation frame does.
                            // Use requestAnimationFrame to avoid over-updating.
                            if (this.state.scrollLeft !== scrollLeft || this.state.scrollTop !== scrollTop) {
                                // Track scrolling direction so we can more efficiently overscan rows to reduce empty space around the edges while scrolling.
                                var scrollDirectionVertical = scrollTop > this.state.scrollTop ? _getOverscanIndices.SCROLL_DIRECTION_FORWARD : _getOverscanIndices.SCROLL_DIRECTION_BACKWARD, scrollDirectionHorizontal = scrollLeft > this.state.scrollLeft ? _getOverscanIndices.SCROLL_DIRECTION_FORWARD : _getOverscanIndices.SCROLL_DIRECTION_BACKWARD;
                                this.setState({
                                    isScrolling: !0,
                                    scrollDirectionHorizontal: scrollDirectionHorizontal,
                                    scrollDirectionVertical: scrollDirectionVertical,
                                    scrollLeft: scrollLeft,
                                    scrollPositionChangeReason: SCROLL_POSITION_CHANGE_REASONS.OBSERVED,
                                    scrollTop: scrollTop
                                });
                            }
                            this._invokeOnScrollMemoizer({
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop,
                                totalColumnsWidth: totalColumnsWidth,
                                totalRowsHeight: totalRowsHeight
                            });
                        }
                    }
                } ]), Grid;
            }(_react.Component);
            Grid.defaultProps = {
                "aria-label": "grid",
                cellRangeRenderer: _defaultCellRangeRenderer2.default,
                estimatedColumnSize: 100,
                estimatedRowSize: 30,
                noContentRenderer: function() {
                    return null;
                },
                onScroll: function() {
                    return null;
                },
                onSectionRendered: function() {
                    return null;
                },
                overscanColumnCount: 0,
                overscanRowCount: 10,
                scrollingResetTimeInterval: DEFAULT_SCROLLING_RESET_TIME_INTERVAL,
                scrollToAlignment: "auto",
                style: {},
                tabIndex: 0
            }, exports.default = Grid, "production" !== process.env.NODE_ENV ? Grid.propTypes = {
                "aria-label": _react.PropTypes.string,
                /**
	   * Set the width of the inner scrollable container to 'auto'.
	   * This is useful for single-column Grids to ensure that the column doesn't extend below a vertical scrollbar.
	   */
                autoContainerWidth: _react.PropTypes.bool,
                /**
	   * Removes fixed height from the scrollingContainer so that the total height
	   * of rows can stretch the window. Intended for use with WindowScroller
	   */
                autoHeight: _react.PropTypes.bool,
                /**
	   * Responsible for rendering a cell given an row and column index.
	   * Should implement the following interface: ({ columnIndex: number, rowIndex: number }): PropTypes.node
	   */
                cellRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Responsible for rendering a group of cells given their index ranges.
	   * Should implement the following interface: ({
	   *   cellCache: Map,
	   *   cellRenderer: Function,
	   *   columnSizeAndPositionManager: CellSizeAndPositionManager,
	   *   columnStartIndex: number,
	   *   columnStopIndex: number,
	   *   isScrolling: boolean,
	   *   rowSizeAndPositionManager: CellSizeAndPositionManager,
	   *   rowStartIndex: number,
	   *   rowStopIndex: number,
	   *   scrollLeft: number,
	   *   scrollTop: number
	   * }): Array<PropTypes.node>
	   */
                cellRangeRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Optional custom CSS class name to attach to root Grid element.
	   */
                className: _react.PropTypes.string,
                /**
	   * Number of columns in grid.
	   */
                columnCount: _react.PropTypes.number.isRequired,
                /**
	   * Either a fixed column width (number) or a function that returns the width of a column given its index.
	   * Should implement the following interface: (index: number): number
	   */
                columnWidth: _react.PropTypes.oneOfType([ _react.PropTypes.number, _react.PropTypes.func ]).isRequired,
                /** Optional inline style applied to inner cell-container */
                containerStyle: _react.PropTypes.object,
                /**
	   * Used to estimate the total width of a Grid before all of its columns have actually been measured.
	   * The estimated total width is adjusted as columns are rendered.
	   */
                estimatedColumnSize: _react.PropTypes.number.isRequired,
                /**
	   * Used to estimate the total height of a Grid before all of its rows have actually been measured.
	   * The estimated total height is adjusted as rows are rendered.
	   */
                estimatedRowSize: _react.PropTypes.number.isRequired,
                /**
	   * Height of Grid; this property determines the number of visible (vs virtualized) rows.
	   */
                height: _react.PropTypes.number.isRequired,
                /**
	   * Optional custom id to attach to root Grid element.
	   */
                id: _react.PropTypes.string,
                /**
	   * Optional renderer to be used in place of rows when either :rowCount or :columnCount is 0.
	   */
                noContentRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Callback invoked whenever the scroll offset changes within the inner scrollable region.
	   * This callback can be used to sync scrolling between lists, tables, or grids.
	   * ({ clientHeight, clientWidth, scrollHeight, scrollLeft, scrollTop, scrollWidth }): void
	   */
                onScroll: _react.PropTypes.func.isRequired,
                /**
	   * Callback invoked with information about the section of the Grid that was just rendered.
	   * ({ columnStartIndex, columnStopIndex, rowStartIndex, rowStopIndex }): void
	   */
                onSectionRendered: _react.PropTypes.func.isRequired,
                /**
	   * Number of columns to render before/after the visible section of the grid.
	   * These columns can help for smoother scrolling on touch devices or browsers that send scroll events infrequently.
	   */
                overscanColumnCount: _react.PropTypes.number.isRequired,
                /**
	   * Number of rows to render above/below the visible section of the grid.
	   * These rows can help for smoother scrolling on touch devices or browsers that send scroll events infrequently.
	   */
                overscanRowCount: _react.PropTypes.number.isRequired,
                /**
	   * Either a fixed row height (number) or a function that returns the height of a row given its index.
	   * Should implement the following interface: ({ index: number }): number
	   */
                rowHeight: _react.PropTypes.oneOfType([ _react.PropTypes.number, _react.PropTypes.func ]).isRequired,
                /**
	   * Number of rows in grid.
	   */
                rowCount: _react.PropTypes.number.isRequired,
                /** Wait this amount of time after the last scroll event before resetting Grid `pointer-events`. */
                scrollingResetTimeInterval: _react.PropTypes.number,
                /** Horizontal offset. */
                scrollLeft: _react.PropTypes.number,
                /**
	   * Controls scroll-to-cell behavior of the Grid.
	   * The default ("auto") scrolls the least amount possible to ensure that the specified cell is fully visible.
	   * Use "start" to align cells to the top/left of the Grid and "end" to align bottom/right.
	   */
                scrollToAlignment: _react.PropTypes.oneOf([ "auto", "end", "start", "center" ]).isRequired,
                /**
	   * Column index to ensure visible (by forcefully scrolling if necessary)
	   */
                scrollToColumn: _react.PropTypes.number,
                /** Vertical offset. */
                scrollTop: _react.PropTypes.number,
                /**
	   * Row index to ensure visible (by forcefully scrolling if necessary)
	   */
                scrollToRow: _react.PropTypes.number,
                /** Optional inline style */
                style: _react.PropTypes.object,
                /** Tab index for focus */
                tabIndex: _react.PropTypes.number,
                /**
	   * Width of Grid; this property determines the number of visible (vs virtualized) columns.
	   */
                width: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 57 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Helper method that determines when to recalculate row or column metadata.
	 *
	 * @param cellCount Number of rows or columns in the current axis
	 * @param cellsSize Width or height of cells for the current axis
	 * @param computeMetadataCallback Method to invoke if cell metadata should be recalculated
	 * @param computeMetadataCallbackProps Parameters to pass to :computeMetadataCallback
	 * @param nextCellsCount Newly updated number of rows or columns in the current axis
	 * @param nextCellsSize Newly updated width or height of cells for the current axis
	 * @param nextScrollToIndex Newly updated scroll-to-index
	 * @param scrollToIndex Scroll-to-index
	 * @param updateScrollOffsetForScrollToIndex Callback to invoke if the scroll position should be recalculated
	 */
        function calculateSizeAndPositionDataAndUpdateScrollOffset(_ref) {
            var cellCount = _ref.cellCount, cellSize = _ref.cellSize, computeMetadataCallback = _ref.computeMetadataCallback, computeMetadataCallbackProps = _ref.computeMetadataCallbackProps, nextCellsCount = _ref.nextCellsCount, nextCellSize = _ref.nextCellSize, nextScrollToIndex = _ref.nextScrollToIndex, scrollToIndex = _ref.scrollToIndex, updateScrollOffsetForScrollToIndex = _ref.updateScrollOffsetForScrollToIndex;
            // Don't compare cell sizes if they are functions because inline functions would cause infinite loops.
            // In that event users should use the manual recompute methods to inform of changes.
            cellCount === nextCellsCount && ("number" != typeof cellSize && "number" != typeof nextCellSize || cellSize === nextCellSize) || (computeMetadataCallback(computeMetadataCallbackProps), 
            // Updated cell metadata may have hidden the previous scrolled-to item.
            // In this case we should also update the scrollTop to ensure it stays visible.
            scrollToIndex >= 0 && scrollToIndex === nextScrollToIndex && updateScrollOffsetForScrollToIndex());
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = calculateSizeAndPositionDataAndUpdateScrollOffset;
    }, /* 58 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _objectWithoutProperties(obj, keys) {
            var target = {};
            for (var i in obj) keys.indexOf(i) >= 0 || Object.prototype.hasOwnProperty.call(obj, i) && (target[i] = obj[i]);
            return target;
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.DEFAULT_MAX_SCROLL_SIZE = void 0;
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _CellSizeAndPositionManager = __webpack_require__(59), _CellSizeAndPositionManager2 = _interopRequireDefault(_CellSizeAndPositionManager), DEFAULT_MAX_SCROLL_SIZE = exports.DEFAULT_MAX_SCROLL_SIZE = 15e5, ScalingCellSizeAndPositionManager = function() {
            function ScalingCellSizeAndPositionManager(_ref) {
                var _ref$maxScrollSize = _ref.maxScrollSize, maxScrollSize = void 0 === _ref$maxScrollSize ? DEFAULT_MAX_SCROLL_SIZE : _ref$maxScrollSize, params = _objectWithoutProperties(_ref, [ "maxScrollSize" ]);
                _classCallCheck(this, ScalingCellSizeAndPositionManager), // Favor composition over inheritance to simplify IE10 support
                this._cellSizeAndPositionManager = new _CellSizeAndPositionManager2.default(params), 
                this._maxScrollSize = maxScrollSize;
            }
            return _createClass(ScalingCellSizeAndPositionManager, [ {
                key: "configure",
                value: function(params) {
                    this._cellSizeAndPositionManager.configure(params);
                }
            }, {
                key: "getCellCount",
                value: function() {
                    return this._cellSizeAndPositionManager.getCellCount();
                }
            }, {
                key: "getEstimatedCellSize",
                value: function() {
                    return this._cellSizeAndPositionManager.getEstimatedCellSize();
                }
            }, {
                key: "getLastMeasuredIndex",
                value: function() {
                    return this._cellSizeAndPositionManager.getLastMeasuredIndex();
                }
            }, {
                key: "getOffsetAdjustment",
                value: function(_ref2) {
                    var containerSize = _ref2.containerSize, offset = _ref2.offset, totalSize = this._cellSizeAndPositionManager.getTotalSize(), safeTotalSize = this.getTotalSize(), offsetPercentage = this._getOffsetPercentage({
                        containerSize: containerSize,
                        offset: offset,
                        totalSize: safeTotalSize
                    });
                    return Math.round(offsetPercentage * (safeTotalSize - totalSize));
                }
            }, {
                key: "getSizeAndPositionOfCell",
                value: function(index) {
                    return this._cellSizeAndPositionManager.getSizeAndPositionOfCell(index);
                }
            }, {
                key: "getSizeAndPositionOfLastMeasuredCell",
                value: function() {
                    return this._cellSizeAndPositionManager.getSizeAndPositionOfLastMeasuredCell();
                }
            }, {
                key: "getTotalSize",
                value: function() {
                    return Math.min(this._maxScrollSize, this._cellSizeAndPositionManager.getTotalSize());
                }
            }, {
                key: "getUpdatedOffsetForIndex",
                value: function(_ref3) {
                    var _ref3$align = _ref3.align, align = void 0 === _ref3$align ? "auto" : _ref3$align, containerSize = _ref3.containerSize, currentOffset = _ref3.currentOffset, targetIndex = _ref3.targetIndex, totalSize = _ref3.totalSize;
                    currentOffset = this._safeOffsetToOffset({
                        containerSize: containerSize,
                        offset: currentOffset
                    });
                    var offset = this._cellSizeAndPositionManager.getUpdatedOffsetForIndex({
                        align: align,
                        containerSize: containerSize,
                        currentOffset: currentOffset,
                        targetIndex: targetIndex,
                        totalSize: totalSize
                    });
                    return this._offsetToSafeOffset({
                        containerSize: containerSize,
                        offset: offset
                    });
                }
            }, {
                key: "getVisibleCellRange",
                value: function(_ref4) {
                    var containerSize = _ref4.containerSize, offset = _ref4.offset;
                    return offset = this._safeOffsetToOffset({
                        containerSize: containerSize,
                        offset: offset
                    }), this._cellSizeAndPositionManager.getVisibleCellRange({
                        containerSize: containerSize,
                        offset: offset
                    });
                }
            }, {
                key: "resetCell",
                value: function(index) {
                    this._cellSizeAndPositionManager.resetCell(index);
                }
            }, {
                key: "_getOffsetPercentage",
                value: function(_ref5) {
                    var containerSize = _ref5.containerSize, offset = _ref5.offset, totalSize = _ref5.totalSize;
                    return totalSize <= containerSize ? 0 : offset / (totalSize - containerSize);
                }
            }, {
                key: "_offsetToSafeOffset",
                value: function(_ref6) {
                    var containerSize = _ref6.containerSize, offset = _ref6.offset, totalSize = this._cellSizeAndPositionManager.getTotalSize(), safeTotalSize = this.getTotalSize();
                    if (totalSize === safeTotalSize) return offset;
                    var offsetPercentage = this._getOffsetPercentage({
                        containerSize: containerSize,
                        offset: offset,
                        totalSize: totalSize
                    });
                    return Math.round(offsetPercentage * (safeTotalSize - containerSize));
                }
            }, {
                key: "_safeOffsetToOffset",
                value: function(_ref7) {
                    var containerSize = _ref7.containerSize, offset = _ref7.offset, totalSize = this._cellSizeAndPositionManager.getTotalSize(), safeTotalSize = this.getTotalSize();
                    if (totalSize === safeTotalSize) return offset;
                    var offsetPercentage = this._getOffsetPercentage({
                        containerSize: containerSize,
                        offset: offset,
                        totalSize: safeTotalSize
                    });
                    return Math.round(offsetPercentage * (totalSize - containerSize));
                }
            } ]), ScalingCellSizeAndPositionManager;
        }();
        exports.default = ScalingCellSizeAndPositionManager;
    }, /* 59 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), CellSizeAndPositionManager = function() {
            function CellSizeAndPositionManager(_ref) {
                var cellCount = _ref.cellCount, cellSizeGetter = _ref.cellSizeGetter, estimatedCellSize = _ref.estimatedCellSize;
                _classCallCheck(this, CellSizeAndPositionManager), this._cellSizeGetter = cellSizeGetter, 
                this._cellCount = cellCount, this._estimatedCellSize = estimatedCellSize, // Cache of size and position data for cells, mapped by cell index.
                // Note that invalid values may exist in this map so only rely on cells up to this._lastMeasuredIndex
                this._cellSizeAndPositionData = {}, // Measurements for cells up to this index can be trusted; cells afterward should be estimated.
                this._lastMeasuredIndex = -1;
            }
            return _createClass(CellSizeAndPositionManager, [ {
                key: "configure",
                value: function(_ref2) {
                    var cellCount = _ref2.cellCount, estimatedCellSize = _ref2.estimatedCellSize;
                    this._cellCount = cellCount, this._estimatedCellSize = estimatedCellSize;
                }
            }, {
                key: "getCellCount",
                value: function() {
                    return this._cellCount;
                }
            }, {
                key: "getEstimatedCellSize",
                value: function() {
                    return this._estimatedCellSize;
                }
            }, {
                key: "getLastMeasuredIndex",
                value: function() {
                    return this._lastMeasuredIndex;
                }
            }, {
                key: "getSizeAndPositionOfCell",
                value: function(index) {
                    if (index < 0 || index >= this._cellCount) throw Error("Requested index " + index + " is outside of range 0.." + this._cellCount);
                    if (index > this._lastMeasuredIndex) {
                        for (var lastMeasuredCellSizeAndPosition = this.getSizeAndPositionOfLastMeasuredCell(), _offset = lastMeasuredCellSizeAndPosition.offset + lastMeasuredCellSizeAndPosition.size, i = this._lastMeasuredIndex + 1; i <= index; i++) {
                            var _size = this._cellSizeGetter({
                                index: i
                            });
                            if (null == _size || isNaN(_size)) throw Error("Invalid size returned for cell " + i + " of value " + _size);
                            this._cellSizeAndPositionData[i] = {
                                offset: _offset,
                                size: _size
                            }, _offset += _size;
                        }
                        this._lastMeasuredIndex = index;
                    }
                    return this._cellSizeAndPositionData[index];
                }
            }, {
                key: "getSizeAndPositionOfLastMeasuredCell",
                value: function() {
                    return this._lastMeasuredIndex >= 0 ? this._cellSizeAndPositionData[this._lastMeasuredIndex] : {
                        offset: 0,
                        size: 0
                    };
                }
            }, {
                key: "getTotalSize",
                value: function() {
                    var lastMeasuredCellSizeAndPosition = this.getSizeAndPositionOfLastMeasuredCell();
                    return lastMeasuredCellSizeAndPosition.offset + lastMeasuredCellSizeAndPosition.size + (this._cellCount - this._lastMeasuredIndex - 1) * this._estimatedCellSize;
                }
            }, {
                key: "getUpdatedOffsetForIndex",
                value: function(_ref3) {
                    var _ref3$align = _ref3.align, align = void 0 === _ref3$align ? "auto" : _ref3$align, containerSize = _ref3.containerSize, currentOffset = _ref3.currentOffset, targetIndex = _ref3.targetIndex;
                    if (containerSize <= 0) return 0;
                    var datum = this.getSizeAndPositionOfCell(targetIndex), maxOffset = datum.offset, minOffset = maxOffset - containerSize + datum.size, idealOffset = void 0;
                    switch (align) {
                      case "start":
                        idealOffset = maxOffset;
                        break;

                      case "end":
                        idealOffset = minOffset;
                        break;

                      case "center":
                        idealOffset = maxOffset - (containerSize - datum.size) / 2;
                        break;

                      default:
                        idealOffset = Math.max(minOffset, Math.min(maxOffset, currentOffset));
                    }
                    var totalSize = this.getTotalSize();
                    return Math.max(0, Math.min(totalSize - containerSize, idealOffset));
                }
            }, {
                key: "getVisibleCellRange",
                value: function(_ref4) {
                    var containerSize = _ref4.containerSize, offset = _ref4.offset, totalSize = this.getTotalSize();
                    if (0 === totalSize) return {};
                    var maxOffset = offset + containerSize, start = this._findNearestCell(offset), datum = this.getSizeAndPositionOfCell(start);
                    offset = datum.offset + datum.size;
                    for (var stop = start; offset < maxOffset && stop < this._cellCount - 1; ) stop++, 
                    offset += this.getSizeAndPositionOfCell(stop).size;
                    return {
                        start: start,
                        stop: stop
                    };
                }
            }, {
                key: "resetCell",
                value: function(index) {
                    this._lastMeasuredIndex = Math.min(this._lastMeasuredIndex, index - 1);
                }
            }, {
                key: "_binarySearch",
                value: function(_ref5) {
                    for (var high = _ref5.high, low = _ref5.low, offset = _ref5.offset, middle = void 0, currentOffset = void 0; low <= high; ) {
                        if (middle = low + Math.floor((high - low) / 2), currentOffset = this.getSizeAndPositionOfCell(middle).offset, 
                        currentOffset === offset) return middle;
                        currentOffset < offset ? low = middle + 1 : currentOffset > offset && (high = middle - 1);
                    }
                    if (low > 0) return low - 1;
                }
            }, {
                key: "_exponentialSearch",
                value: function(_ref6) {
                    for (var index = _ref6.index, offset = _ref6.offset, interval = 1; index < this._cellCount && this.getSizeAndPositionOfCell(index).offset < offset; ) index += interval, 
                    interval *= 2;
                    return this._binarySearch({
                        high: Math.min(index, this._cellCount - 1),
                        low: Math.floor(index / 2),
                        offset: offset
                    });
                }
            }, {
                key: "_findNearestCell",
                value: function(offset) {
                    if (isNaN(offset)) throw Error("Invalid offset " + offset + " specified");
                    // Our search algorithms find the nearest match at or below the specified offset.
                    // So make sure the offset is at least 0 or no match will be found.
                    offset = Math.max(0, offset);
                    var lastMeasuredCellSizeAndPosition = this.getSizeAndPositionOfLastMeasuredCell(), lastMeasuredIndex = Math.max(0, this._lastMeasuredIndex);
                    return lastMeasuredCellSizeAndPosition.offset >= offset ? this._binarySearch({
                        high: lastMeasuredIndex,
                        low: 0,
                        offset: offset
                    }) : this._exponentialSearch({
                        index: lastMeasuredIndex,
                        offset: offset
                    });
                }
            } ]), CellSizeAndPositionManager;
        }();
        exports.default = CellSizeAndPositionManager;
    }, /* 60 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Calculates the number of cells to overscan before and after a specified range.
	 * This function ensures that overscanning doesn't exceed the available cells.
	 *
	 * @param cellCount Number of rows or columns in the current axis
	 * @param scrollDirection One of SCROLL_DIRECTION_BACKWARD
	 * @param overscanCellsCount Maximum number of cells to over-render in either direction
	 * @param startIndex Begin of range of visible cells
	 * @param stopIndex End of range of visible cells
	 */
        function getOverscanIndices(_ref) {
            var cellCount = _ref.cellCount, overscanCellsCount = _ref.overscanCellsCount, scrollDirection = _ref.scrollDirection, startIndex = _ref.startIndex, stopIndex = _ref.stopIndex, overscanStartIndex = void 0, overscanStopIndex = void 0;
            switch (scrollDirection) {
              case SCROLL_DIRECTION_FORWARD:
                overscanStartIndex = startIndex, overscanStopIndex = stopIndex + overscanCellsCount;
                break;

              case SCROLL_DIRECTION_BACKWARD:
                overscanStartIndex = startIndex - overscanCellsCount, overscanStopIndex = stopIndex;
            }
            return {
                overscanStartIndex: Math.max(0, overscanStartIndex),
                overscanStopIndex: Math.min(cellCount - 1, overscanStopIndex)
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = getOverscanIndices;
        var SCROLL_DIRECTION_BACKWARD = exports.SCROLL_DIRECTION_BACKWARD = -1, SCROLL_DIRECTION_FORWARD = exports.SCROLL_DIRECTION_FORWARD = 1;
    }, /* 61 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Helper function that determines when to update scroll offsets to ensure that a scroll-to-index remains visible.
	 * This function also ensures that the scroll ofset isn't past the last column/row of cells.
	 *
	 * @param cellsSize Width or height of cells for the current axis
	 * @param cellSizeAndPositionManager Manages size and position metadata of cells
	 * @param previousCellsCount Previous number of rows or columns
	 * @param previousCellsSize Previous width or height of cells
	 * @param previousScrollToIndex Previous scroll-to-index
	 * @param previousSize Previous width or height of the virtualized container
	 * @param scrollOffset Current scrollLeft or scrollTop
	 * @param scrollToIndex Scroll-to-index
	 * @param size Width or height of the virtualized container
	 * @param updateScrollIndexCallback Callback to invoke with an scroll-to-index value
	 */
        function updateScrollIndexHelper(_ref) {
            var cellSize = _ref.cellSize, cellSizeAndPositionManager = _ref.cellSizeAndPositionManager, previousCellsCount = _ref.previousCellsCount, previousCellSize = _ref.previousCellSize, previousScrollToAlignment = _ref.previousScrollToAlignment, previousScrollToIndex = _ref.previousScrollToIndex, previousSize = _ref.previousSize, scrollOffset = _ref.scrollOffset, scrollToAlignment = _ref.scrollToAlignment, scrollToIndex = _ref.scrollToIndex, size = _ref.size, updateScrollIndexCallback = _ref.updateScrollIndexCallback, cellCount = cellSizeAndPositionManager.getCellCount(), hasScrollToIndex = scrollToIndex >= 0 && scrollToIndex < cellCount, sizeHasChanged = size !== previousSize || !previousCellSize || "number" == typeof cellSize && cellSize !== previousCellSize;
            // If we have a new scroll target OR if height/row-height has changed,
            // We should ensure that the scroll target is visible.
            hasScrollToIndex && (sizeHasChanged || scrollToAlignment !== previousScrollToAlignment || scrollToIndex !== previousScrollToIndex) ? updateScrollIndexCallback(scrollToIndex) : !hasScrollToIndex && cellCount > 0 && (size < previousSize || cellCount < previousCellsCount) && scrollOffset > cellSizeAndPositionManager.getTotalSize() - size && updateScrollIndexCallback(cellCount - 1);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = updateScrollIndexHelper;
    }, /* 62 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Default implementation of cellRangeRenderer used by Grid.
	 * This renderer supports cell-caching while the user is scrolling.
	 */
        function defaultCellRangeRenderer(_ref) {
            for (var cellCache = _ref.cellCache, cellRenderer = _ref.cellRenderer, columnSizeAndPositionManager = _ref.columnSizeAndPositionManager, columnStartIndex = _ref.columnStartIndex, columnStopIndex = _ref.columnStopIndex, horizontalOffsetAdjustment = _ref.horizontalOffsetAdjustment, isScrolling = _ref.isScrolling, rowSizeAndPositionManager = _ref.rowSizeAndPositionManager, rowStartIndex = _ref.rowStartIndex, rowStopIndex = _ref.rowStopIndex, verticalOffsetAdjustment = (_ref.scrollLeft, 
            _ref.scrollTop, _ref.verticalOffsetAdjustment), visibleColumnIndices = _ref.visibleColumnIndices, visibleRowIndices = _ref.visibleRowIndices, renderedCells = [], rowIndex = rowStartIndex; rowIndex <= rowStopIndex; rowIndex++) for (var rowDatum = rowSizeAndPositionManager.getSizeAndPositionOfCell(rowIndex), columnIndex = columnStartIndex; columnIndex <= columnStopIndex; columnIndex++) {
                var columnDatum = columnSizeAndPositionManager.getSizeAndPositionOfCell(columnIndex), isVisible = columnIndex >= visibleColumnIndices.start && columnIndex <= visibleColumnIndices.stop && rowIndex >= visibleRowIndices.start && rowIndex <= visibleRowIndices.stop, key = rowIndex + "-" + columnIndex, style = {
                    height: rowDatum.size,
                    left: columnDatum.offset + horizontalOffsetAdjustment,
                    position: "absolute",
                    top: rowDatum.offset + verticalOffsetAdjustment,
                    width: columnDatum.size
                }, cellRendererParams = {
                    columnIndex: columnIndex,
                    isScrolling: isScrolling,
                    isVisible: isVisible,
                    key: key,
                    rowIndex: rowIndex,
                    style: style
                }, renderedCell = void 0;
                // Avoid re-creating cells while scrolling.
                // This can lead to the same cell being created many times and can cause performance issues for "heavy" cells.
                // If a scroll is in progress- cache and reuse cells.
                // This cache will be thrown away once scrolling completes.
                // However if we are scaling scroll positions and sizes, we should also avoid caching.
                // This is because the offset changes slightly as scroll position changes and caching leads to stale values.
                // For more info refer to issue #395
                !isScrolling || horizontalOffsetAdjustment || verticalOffsetAdjustment ? renderedCell = cellRenderer(cellRendererParams) : (cellCache[key] || (cellCache[key] = cellRenderer(cellRendererParams)), 
                renderedCell = cellCache[key]), null != renderedCell && renderedCell !== !1 && renderedCells.push(renderedCell);
            }
            return renderedCells;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = defaultCellRangeRenderer;
    }, /* 63 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.SortIndicator = exports.SortDirection = exports.Column = exports.Table = exports.defaultRowRenderer = exports.defaultHeaderRenderer = exports.defaultCellRenderer = exports.defaultCellDataGetter = exports.default = void 0;
        var _Table2 = __webpack_require__(64), _Table3 = _interopRequireDefault(_Table2), _defaultCellDataGetter2 = __webpack_require__(70), _defaultCellDataGetter3 = _interopRequireDefault(_defaultCellDataGetter2), _defaultCellRenderer2 = __webpack_require__(69), _defaultCellRenderer3 = _interopRequireDefault(_defaultCellRenderer2), _defaultHeaderRenderer2 = __webpack_require__(66), _defaultHeaderRenderer3 = _interopRequireDefault(_defaultHeaderRenderer2), _defaultRowRenderer2 = __webpack_require__(71), _defaultRowRenderer3 = _interopRequireDefault(_defaultRowRenderer2), _Column2 = __webpack_require__(65), _Column3 = _interopRequireDefault(_Column2), _SortDirection2 = __webpack_require__(68), _SortDirection3 = _interopRequireDefault(_SortDirection2), _SortIndicator2 = __webpack_require__(67), _SortIndicator3 = _interopRequireDefault(_SortIndicator2);
        exports.default = _Table3.default, exports.defaultCellDataGetter = _defaultCellDataGetter3.default, 
        exports.defaultCellRenderer = _defaultCellRenderer3.default, exports.defaultHeaderRenderer = _defaultHeaderRenderer3.default, 
        exports.defaultRowRenderer = _defaultRowRenderer3.default, exports.Table = _Table3.default, 
        exports.Column = _Column3.default, exports.SortDirection = _SortDirection3.default, 
        exports.SortIndicator = _SortIndicator3.default;
    }, /* 64 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            }, _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _classnames = __webpack_require__(45), _classnames2 = _interopRequireDefault(_classnames), _Column = __webpack_require__(65), _Column2 = _interopRequireDefault(_Column), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactDom = __webpack_require__(40), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _Grid = __webpack_require__(55), _Grid2 = _interopRequireDefault(_Grid), _defaultRowRenderer = __webpack_require__(71), _defaultRowRenderer2 = _interopRequireDefault(_defaultRowRenderer), _SortDirection = __webpack_require__(68), _SortDirection2 = _interopRequireDefault(_SortDirection), Table = function(_Component) {
                function Table(props) {
                    _classCallCheck(this, Table);
                    var _this = _possibleConstructorReturn(this, (Table.__proto__ || Object.getPrototypeOf(Table)).call(this, props));
                    return _this.state = {
                        scrollbarWidth: 0
                    }, _this._createColumn = _this._createColumn.bind(_this), _this._createRow = _this._createRow.bind(_this), 
                    _this._onScroll = _this._onScroll.bind(_this), _this._onSectionRendered = _this._onSectionRendered.bind(_this), 
                    _this;
                }
                return _inherits(Table, _Component), _createClass(Table, [ {
                    key: "forceUpdateGrid",
                    value: function() {
                        this.Grid.forceUpdate();
                    }
                }, {
                    key: "measureAllRows",
                    value: function() {
                        this.Grid.measureAllCells();
                    }
                }, {
                    key: "recomputeRowHeights",
                    value: function() {
                        var index = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        this.Grid.recomputeGridSize({
                            rowIndex: index
                        }), this.forceUpdateGrid();
                    }
                }, {
                    key: "componentDidMount",
                    value: function() {
                        this._setScrollbarWidth();
                    }
                }, {
                    key: "componentDidUpdate",
                    value: function() {
                        this._setScrollbarWidth();
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _this2 = this, _props = this.props, children = _props.children, className = _props.className, disableHeader = _props.disableHeader, gridClassName = _props.gridClassName, gridStyle = _props.gridStyle, headerHeight = _props.headerHeight, height = _props.height, id = _props.id, noRowsRenderer = _props.noRowsRenderer, rowClassName = _props.rowClassName, rowStyle = _props.rowStyle, scrollToIndex = _props.scrollToIndex, style = _props.style, width = _props.width, scrollbarWidth = this.state.scrollbarWidth, availableRowsHeight = disableHeader ? height : height - headerHeight, rowClass = rowClassName instanceof Function ? rowClassName({
                            index: -1
                        }) : rowClassName, rowStyleObject = rowStyle instanceof Function ? rowStyle({
                            index: -1
                        }) : rowStyle;
                        // Note that we specify :rowCount, :scrollbarWidth, :sortBy, and :sortDirection as properties on Grid even though these have nothing to do with Grid.
                        // This is done because Grid is a pure component and won't update unless its properties or state has changed.
                        // Any property that should trigger a re-render of Grid then is specified here to avoid a stale display.
                        // Precompute and cache column styles before rendering rows and columns to speed things up
                        return this._cachedColumnStyles = [], _react2.default.Children.toArray(children).forEach(function(column, index) {
                            var flexStyles = _this2._getFlexStyleForColumn(column, column.props.style);
                            _this2._cachedColumnStyles[index] = _extends({}, flexStyles, {
                                overflow: "hidden"
                            });
                        }), _react2.default.createElement("div", {
                            className: (0, _classnames2.default)("ReactVirtualized__Table", className),
                            id: id,
                            style: style
                        }, !disableHeader && _react2.default.createElement("div", {
                            className: (0, _classnames2.default)("ReactVirtualized__Table__headerRow", rowClass),
                            style: _extends({}, rowStyleObject, {
                                height: headerHeight,
                                overflow: "hidden",
                                paddingRight: scrollbarWidth,
                                width: width
                            })
                        }, this._getRenderedHeaderRow()), _react2.default.createElement(_Grid2.default, _extends({}, this.props, {
                            autoContainerWidth: !0,
                            className: (0, _classnames2.default)("ReactVirtualized__Table__Grid", gridClassName),
                            cellRenderer: this._createRow,
                            columnWidth: width,
                            columnCount: 1,
                            height: availableRowsHeight,
                            id: void 0,
                            noContentRenderer: noRowsRenderer,
                            onScroll: this._onScroll,
                            onSectionRendered: this._onSectionRendered,
                            ref: function(_ref) {
                                _this2.Grid = _ref;
                            },
                            scrollbarWidth: scrollbarWidth,
                            scrollToRow: scrollToIndex,
                            style: _extends({}, gridStyle, {
                                overflowX: "hidden"
                            })
                        })));
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_createColumn",
                    value: function(_ref2) {
                        var column = _ref2.column, columnIndex = _ref2.columnIndex, isScrolling = _ref2.isScrolling, rowData = _ref2.rowData, rowIndex = _ref2.rowIndex, _column$props = column.props, cellDataGetter = _column$props.cellDataGetter, cellRenderer = _column$props.cellRenderer, className = _column$props.className, columnData = _column$props.columnData, dataKey = _column$props.dataKey, cellData = cellDataGetter({
                            columnData: columnData,
                            dataKey: dataKey,
                            rowData: rowData
                        }), renderedCell = cellRenderer({
                            cellData: cellData,
                            columnData: columnData,
                            dataKey: dataKey,
                            isScrolling: isScrolling,
                            rowData: rowData,
                            rowIndex: rowIndex
                        }), style = this._cachedColumnStyles[columnIndex], title = "string" == typeof renderedCell ? renderedCell : null;
                        return _react2.default.createElement("div", {
                            key: "Row" + rowIndex + "-Col" + columnIndex,
                            className: (0, _classnames2.default)("ReactVirtualized__Table__rowColumn", className),
                            style: style,
                            title: title
                        }, renderedCell);
                    }
                }, {
                    key: "_createHeader",
                    value: function(_ref3) {
                        var column = _ref3.column, index = _ref3.index, _props2 = this.props, headerClassName = _props2.headerClassName, headerStyle = _props2.headerStyle, onHeaderClick = _props2.onHeaderClick, sort = _props2.sort, sortBy = _props2.sortBy, sortDirection = _props2.sortDirection, _column$props2 = column.props, dataKey = _column$props2.dataKey, disableSort = _column$props2.disableSort, headerRenderer = _column$props2.headerRenderer, label = _column$props2.label, columnData = _column$props2.columnData, sortEnabled = !disableSort && sort, classNames = (0, 
                        _classnames2.default)("ReactVirtualized__Table__headerColumn", headerClassName, column.props.headerClassName, {
                            ReactVirtualized__Table__sortableHeaderColumn: sortEnabled
                        }), style = this._getFlexStyleForColumn(column, headerStyle), renderedHeader = headerRenderer({
                            columnData: columnData,
                            dataKey: dataKey,
                            disableSort: disableSort,
                            label: label,
                            sortBy: sortBy,
                            sortDirection: sortDirection
                        }), a11yProps = {};
                        return (sortEnabled || onHeaderClick) && !function() {
                            // If this is a sortable header, clicking it should update the table data's sorting.
                            var newSortDirection = sortBy !== dataKey || sortDirection === _SortDirection2.default.DESC ? _SortDirection2.default.ASC : _SortDirection2.default.DESC, onClick = function() {
                                sortEnabled && sort({
                                    sortBy: dataKey,
                                    sortDirection: newSortDirection
                                }), onHeaderClick && onHeaderClick({
                                    columnData: columnData,
                                    dataKey: dataKey
                                });
                            }, onKeyDown = function(event) {
                                "Enter" !== event.key && " " !== event.key || onClick();
                            };
                            a11yProps["aria-label"] = column.props["aria-label"] || label || dataKey, a11yProps.role = "rowheader", 
                            a11yProps.tabIndex = 0, a11yProps.onClick = onClick, a11yProps.onKeyDown = onKeyDown;
                        }(), _react2.default.createElement("div", _extends({}, a11yProps, {
                            key: "Header-Col" + index,
                            className: classNames,
                            style: style
                        }), renderedHeader);
                    }
                }, {
                    key: "_createRow",
                    value: function(_ref4) {
                        var _this3 = this, index = _ref4.rowIndex, isScrolling = _ref4.isScrolling, key = _ref4.key, style = _ref4.style, _props3 = this.props, children = _props3.children, onRowClick = _props3.onRowClick, onRowDoubleClick = _props3.onRowDoubleClick, onRowMouseOver = _props3.onRowMouseOver, onRowMouseOut = _props3.onRowMouseOut, rowClassName = _props3.rowClassName, rowGetter = _props3.rowGetter, rowRenderer = _props3.rowRenderer, rowStyle = _props3.rowStyle, scrollbarWidth = this.state.scrollbarWidth, rowClass = rowClassName instanceof Function ? rowClassName({
                            index: index
                        }) : rowClassName, rowStyleObject = rowStyle instanceof Function ? rowStyle({
                            index: index
                        }) : rowStyle, rowData = rowGetter({
                            index: index
                        }), columns = _react2.default.Children.toArray(children).map(function(column, columnIndex) {
                            return _this3._createColumn({
                                column: column,
                                columnIndex: columnIndex,
                                isScrolling: isScrolling,
                                rowData: rowData,
                                rowIndex: index,
                                scrollbarWidth: scrollbarWidth
                            });
                        }), className = (0, _classnames2.default)("ReactVirtualized__Table__row", rowClass), flattenedStyle = _extends({}, style, rowStyleObject, {
                            height: this._getRowHeight(index),
                            overflow: "hidden",
                            paddingRight: scrollbarWidth
                        });
                        return rowRenderer({
                            className: className,
                            columns: columns,
                            index: index,
                            isScrolling: isScrolling,
                            key: key,
                            onRowClick: onRowClick,
                            onRowDoubleClick: onRowDoubleClick,
                            onRowMouseOver: onRowMouseOver,
                            onRowMouseOut: onRowMouseOut,
                            rowData: rowData,
                            style: flattenedStyle
                        });
                    }
                }, {
                    key: "_getFlexStyleForColumn",
                    value: function(column) {
                        var customStyle = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, flexValue = column.props.flexGrow + " " + column.props.flexShrink + " " + column.props.width + "px", style = _extends({}, customStyle, {
                            flex: flexValue,
                            msFlex: flexValue,
                            WebkitFlex: flexValue
                        });
                        return column.props.maxWidth && (style.maxWidth = column.props.maxWidth), column.props.minWidth && (style.minWidth = column.props.minWidth), 
                        style;
                    }
                }, {
                    key: "_getRenderedHeaderRow",
                    value: function() {
                        var _this4 = this, _props4 = this.props, children = _props4.children, disableHeader = _props4.disableHeader, items = disableHeader ? [] : _react2.default.Children.toArray(children);
                        return items.map(function(column, index) {
                            return _this4._createHeader({
                                column: column,
                                index: index
                            });
                        });
                    }
                }, {
                    key: "_getRowHeight",
                    value: function(rowIndex) {
                        var rowHeight = this.props.rowHeight;
                        return rowHeight instanceof Function ? rowHeight({
                            index: rowIndex
                        }) : rowHeight;
                    }
                }, {
                    key: "_onScroll",
                    value: function(_ref5) {
                        var clientHeight = _ref5.clientHeight, scrollHeight = _ref5.scrollHeight, scrollTop = _ref5.scrollTop, onScroll = this.props.onScroll;
                        onScroll({
                            clientHeight: clientHeight,
                            scrollHeight: scrollHeight,
                            scrollTop: scrollTop
                        });
                    }
                }, {
                    key: "_onSectionRendered",
                    value: function(_ref6) {
                        var rowOverscanStartIndex = _ref6.rowOverscanStartIndex, rowOverscanStopIndex = _ref6.rowOverscanStopIndex, rowStartIndex = _ref6.rowStartIndex, rowStopIndex = _ref6.rowStopIndex, onRowsRendered = this.props.onRowsRendered;
                        onRowsRendered({
                            overscanStartIndex: rowOverscanStartIndex,
                            overscanStopIndex: rowOverscanStopIndex,
                            startIndex: rowStartIndex,
                            stopIndex: rowStopIndex
                        });
                    }
                }, {
                    key: "_setScrollbarWidth",
                    value: function() {
                        var Grid = (0, _reactDom.findDOMNode)(this.Grid), clientWidth = Grid.clientWidth || 0, offsetWidth = Grid.offsetWidth || 0, scrollbarWidth = offsetWidth - clientWidth;
                        this.setState({
                            scrollbarWidth: scrollbarWidth
                        });
                    }
                } ]), Table;
            }(_react.Component);
            Table.defaultProps = {
                disableHeader: !1,
                estimatedRowSize: 30,
                headerHeight: 0,
                headerStyle: {},
                noRowsRenderer: function() {
                    return null;
                },
                onRowsRendered: function() {
                    return null;
                },
                onScroll: function() {
                    return null;
                },
                overscanRowCount: 10,
                rowRenderer: _defaultRowRenderer2.default,
                rowStyle: {},
                scrollToAlignment: "auto",
                style: {}
            }, exports.default = Table, "production" !== process.env.NODE_ENV ? Table.propTypes = {
                "aria-label": _react.PropTypes.string,
                /**
	   * Removes fixed height from the scrollingContainer so that the total height
	   * of rows can stretch the window. Intended for use with WindowScroller
	   */
                autoHeight: _react.PropTypes.bool,
                /** One or more Columns describing the data displayed in this row */
                children: function children(props, propName, componentName) {
                    for (var children = _react2.default.Children.toArray(props.children), i = 0; i < children.length; i++) if (children[i].type !== _Column2.default) return new Error("Table only accepts children of type Column");
                },
                /** Optional CSS class name */
                className: _react.PropTypes.string,
                /** Disable rendering the header at all */
                disableHeader: _react.PropTypes.bool,
                /**
	   * Used to estimate the total height of a Table before all of its rows have actually been measured.
	   * The estimated total height is adjusted as rows are rendered.
	   */
                estimatedRowSize: _react.PropTypes.number.isRequired,
                /** Optional custom CSS class name to attach to inner Grid element. */
                gridClassName: _react.PropTypes.string,
                /** Optional inline style to attach to inner Grid element. */
                gridStyle: _react.PropTypes.object,
                /** Optional CSS class to apply to all column headers */
                headerClassName: _react.PropTypes.string,
                /** Fixed height of header row */
                headerHeight: _react.PropTypes.number.isRequired,
                /** Fixed/available height for out DOM element */
                height: _react.PropTypes.number.isRequired,
                /** Optional id */
                id: _react.PropTypes.string,
                /** Optional renderer to be used in place of table body rows when rowCount is 0 */
                noRowsRenderer: _react.PropTypes.func,
                /**
	  * Optional callback when a column's header is clicked.
	  * ({ columnData: any, dataKey: string }): void
	  */
                onHeaderClick: _react.PropTypes.func,
                /** Optional custom inline style to attach to table header columns. */
                headerStyle: _react.PropTypes.object,
                /**
	   * Callback invoked when a user clicks on a table row.
	   * ({ index: number }): void
	   */
                onRowClick: _react.PropTypes.func,
                /**
	   * Callback invoked when a user double-clicks on a table row.
	   * ({ index: number }): void
	   */
                onRowDoubleClick: _react.PropTypes.func,
                /**
	   * Callback invoked when the mouse leaves a table row.
	   * ({ index: number }): void
	   */
                onRowMouseOut: _react.PropTypes.func,
                /**
	   * Callback invoked when a user moves the mouse over a table row.
	   * ({ index: number }): void
	   */
                onRowMouseOver: _react.PropTypes.func,
                /**
	   * Callback invoked with information about the slice of rows that were just rendered.
	   * ({ startIndex, stopIndex }): void
	   */
                onRowsRendered: _react.PropTypes.func,
                /**
	   * Callback invoked whenever the scroll offset changes within the inner scrollable region.
	   * This callback can be used to sync scrolling between lists, tables, or grids.
	   * ({ clientHeight, scrollHeight, scrollTop }): void
	   */
                onScroll: _react.PropTypes.func.isRequired,
                /**
	   * Number of rows to render above/below the visible bounds of the list.
	   * These rows can help for smoother scrolling on touch devices.
	   */
                overscanRowCount: _react.PropTypes.number.isRequired,
                /**
	   * Optional CSS class to apply to all table rows (including the header row).
	   * This property can be a CSS class name (string) or a function that returns a class name.
	   * If a function is provided its signature should be: ({ index: number }): string
	   */
                rowClassName: _react.PropTypes.oneOfType([ _react.PropTypes.string, _react.PropTypes.func ]),
                /**
	   * Callback responsible for returning a data row given an index.
	   * ({ index: number }): any
	   */
                rowGetter: _react.PropTypes.func.isRequired,
                /**
	   * Either a fixed row height (number) or a function that returns the height of a row given its index.
	   * ({ index: number }): number
	   */
                rowHeight: _react.PropTypes.oneOfType([ _react.PropTypes.number, _react.PropTypes.func ]).isRequired,
                /** Number of rows in table. */
                rowCount: _react.PropTypes.number.isRequired,
                /**
	   * Responsible for rendering a table row given an array of columns:
	   * Should implement the following interface: ({
	   *   className: string,
	   *   columns: Array,
	   *   index: number,
	   *   isScrolling: boolean,
	   *   onRowClick: ?Function,
	   *   onRowDoubleClick: ?Function,
	   *   onRowMouseOver: ?Function,
	   *   onRowMouseOut: ?Function,
	   *   rowData: any,
	   *   style: any
	   * }): PropTypes.node
	   */
                rowRenderer: _react.PropTypes.func,
                /** Optional custom inline style to attach to table rows. */
                rowStyle: _react.PropTypes.oneOfType([ _react.PropTypes.object, _react.PropTypes.func ]).isRequired,
                /** See Grid#scrollToAlignment */
                scrollToAlignment: _react.PropTypes.oneOf([ "auto", "end", "start", "center" ]).isRequired,
                /** Row index to ensure visible (by forcefully scrolling if necessary) */
                scrollToIndex: _react.PropTypes.number,
                /** Vertical offset. */
                scrollTop: _react.PropTypes.number,
                /**
	   * Sort function to be called if a sortable header is clicked.
	   * ({ sortBy: string, sortDirection: SortDirection }): void
	   */
                sort: _react.PropTypes.func,
                /** Table data is currently sorted by this :dataKey (if it is sorted at all) */
                sortBy: _react.PropTypes.string,
                /** Table data is currently sorted in this direction (if it is sorted at all) */
                sortDirection: _react.PropTypes.oneOf([ _SortDirection2.default.ASC, _SortDirection2.default.DESC ]),
                /** Optional inline style */
                style: _react.PropTypes.object,
                /** Tab index for focus */
                tabIndex: _react.PropTypes.number,
                /** Width of list */
                width: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 65 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _react = __webpack_require__(5), _defaultHeaderRenderer = __webpack_require__(66), _defaultHeaderRenderer2 = _interopRequireDefault(_defaultHeaderRenderer), _defaultCellRenderer = __webpack_require__(69), _defaultCellRenderer2 = _interopRequireDefault(_defaultCellRenderer), _defaultCellDataGetter = __webpack_require__(70), _defaultCellDataGetter2 = _interopRequireDefault(_defaultCellDataGetter), Column = function(_Component) {
                function Column() {
                    return _classCallCheck(this, Column), _possibleConstructorReturn(this, (Column.__proto__ || Object.getPrototypeOf(Column)).apply(this, arguments));
                }
                return _inherits(Column, _Component), Column;
            }(_react.Component);
            Column.defaultProps = {
                cellDataGetter: _defaultCellDataGetter2.default,
                cellRenderer: _defaultCellRenderer2.default,
                flexGrow: 0,
                flexShrink: 1,
                headerRenderer: _defaultHeaderRenderer2.default,
                style: {}
            }, exports.default = Column, "production" !== process.env.NODE_ENV ? Column.propTypes = {
                /** Optional aria-label value to set on the column header */
                "aria-label": _react.PropTypes.string,
                /**
	   * Callback responsible for returning a cell's data, given its :dataKey
	   * ({ columnData: any, dataKey: string, rowData: any }): any
	   */
                cellDataGetter: _react.PropTypes.func,
                /**
	   * Callback responsible for rendering a cell's contents.
	   * ({ cellData: any, columnData: any, dataKey: string, rowData: any, rowIndex: number }): node
	   */
                cellRenderer: _react.PropTypes.func,
                /** Optional CSS class to apply to cell */
                className: _react.PropTypes.string,
                /** Optional additional data passed to this column's :cellDataGetter */
                columnData: _react.PropTypes.object,
                /** Uniquely identifies the row-data attribute correspnding to this cell */
                dataKey: _react.PropTypes.any.isRequired,
                /** If sort is enabled for the table at large, disable it for this column */
                disableSort: _react.PropTypes.bool,
                /** Flex grow style; defaults to 0 */
                flexGrow: _react.PropTypes.number,
                /** Flex shrink style; defaults to 1 */
                flexShrink: _react.PropTypes.number,
                /** Optional CSS class to apply to this column's header */
                headerClassName: _react.PropTypes.string,
                /**
	   * Optional callback responsible for rendering a column header contents.
	   * ({ columnData: object, dataKey: string, disableSort: boolean, label: string, sortBy: string, sortDirection: string }): PropTypes.node
	   */
                headerRenderer: _react.PropTypes.func.isRequired,
                /** Header label for this column */
                label: _react.PropTypes.string,
                /** Maximum width of column; this property will only be used if :flexGrow is > 0. */
                maxWidth: _react.PropTypes.number,
                /** Minimum width of column. */
                minWidth: _react.PropTypes.number,
                /** Optional inline style to apply to cell */
                style: _react.PropTypes.object,
                /** Flex basis (width) for this column; This value can grow or shrink based on :flexGrow and :flexShrink properties. */
                width: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 66 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        /**
	 * Default table header renderer.
	 */
        function defaultHeaderRenderer(_ref) {
            var dataKey = (_ref.columnData, _ref.dataKey), label = (_ref.disableSort, _ref.label), sortBy = _ref.sortBy, sortDirection = _ref.sortDirection, showSortIndicator = sortBy === dataKey, children = [ _react2.default.createElement("span", {
                className: "ReactVirtualized__Table__headerTruncatedText",
                key: "label",
                title: label
            }, label) ];
            return showSortIndicator && children.push(_react2.default.createElement(_SortIndicator2.default, {
                key: "SortIndicator",
                sortDirection: sortDirection
            })), children;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = defaultHeaderRenderer;
        var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _SortIndicator = __webpack_require__(67), _SortIndicator2 = _interopRequireDefault(_SortIndicator);
    }, /* 67 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            /**
	 * Displayed beside a header to indicate that a Table is currently sorted by this column.
	 */
            function SortIndicator(_ref) {
                var sortDirection = _ref.sortDirection, classNames = (0, _classnames2.default)("ReactVirtualized__Table__sortableHeaderIcon", {
                    "ReactVirtualized__Table__sortableHeaderIcon--ASC": sortDirection === _SortDirection2.default.ASC,
                    "ReactVirtualized__Table__sortableHeaderIcon--DESC": sortDirection === _SortDirection2.default.DESC
                });
                return _react2.default.createElement("svg", {
                    className: classNames,
                    width: 18,
                    height: 18,
                    viewBox: "0 0 24 24"
                }, sortDirection === _SortDirection2.default.ASC ? _react2.default.createElement("path", {
                    d: "M7 14l5-5 5 5z"
                }) : _react2.default.createElement("path", {
                    d: "M7 10l5 5 5-5z"
                }), _react2.default.createElement("path", {
                    d: "M0 0h24v24H0z",
                    fill: "none"
                }));
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            }), exports.default = SortIndicator;
            var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _classnames = __webpack_require__(45), _classnames2 = _interopRequireDefault(_classnames), _SortDirection = __webpack_require__(68), _SortDirection2 = _interopRequireDefault(_SortDirection);
            "production" !== process.env.NODE_ENV ? SortIndicator.propTypes = {
                sortDirection: _react.PropTypes.oneOf([ _SortDirection2.default.ASC, _SortDirection2.default.DESC ])
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 68 */
    /***/
    function(module, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var SortDirection = {
            /**
	   * Sort items in ascending order.
	   * This means arranging from the lowest value to the highest (e.g. a-z, 0-9).
	   */
            ASC: "ASC",
            /**
	   * Sort items in descending order.
	   * This means arranging from the highest value to the lowest (e.g. z-a, 9-0).
	   */
            DESC: "DESC"
        };
        exports.default = SortDirection;
    }, /* 69 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Default cell renderer that displays an attribute as a simple string
	 * You should override the column's cellRenderer if your data is some other type of object.
	 */
        function defaultCellRenderer(_ref) {
            var cellData = _ref.cellData;
            _ref.cellDataKey, _ref.columnData, _ref.rowData, _ref.rowIndex;
            return null == cellData ? "" : String(cellData);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = defaultCellRenderer;
    }, /* 70 */
    /***/
    function(module, exports) {
        "use strict";
        /**
	 * Default accessor for returning a cell value for a given attribute.
	 * This function expects to operate on either a vanilla Object or an Immutable Map.
	 * You should override the column's cellDataGetter if your data is some other type of object.
	 */
        function defaultCellDataGetter(_ref) {
            var dataKey = (_ref.columnData, _ref.dataKey), rowData = _ref.rowData;
            return rowData.get instanceof Function ? rowData.get(dataKey) : rowData[dataKey];
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = defaultCellDataGetter;
    }, /* 71 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        /**
	 * Default row renderer for Table.
	 */
        function defaultRowRenderer(_ref) {
            var className = _ref.className, columns = _ref.columns, index = _ref.index, key = (_ref.isScrolling, 
            _ref.key), onRowClick = _ref.onRowClick, onRowDoubleClick = _ref.onRowDoubleClick, onRowMouseOver = _ref.onRowMouseOver, onRowMouseOut = _ref.onRowMouseOut, style = (_ref.rowData, 
            _ref.style), a11yProps = {};
            return (onRowClick || onRowDoubleClick || onRowMouseOver || onRowMouseOut) && (a11yProps["aria-label"] = "row", 
            a11yProps.role = "row", a11yProps.tabIndex = 0, onRowClick && (a11yProps.onClick = function() {
                return onRowClick({
                    index: index
                });
            }), onRowDoubleClick && (a11yProps.onDoubleClick = function() {
                return onRowDoubleClick({
                    index: index
                });
            }), onRowMouseOut && (a11yProps.onMouseOut = function() {
                return onRowMouseOut({
                    index: index
                });
            }), onRowMouseOver && (a11yProps.onMouseOver = function() {
                return onRowMouseOver({
                    index: index
                });
            })), _react2.default.createElement("div", _extends({}, a11yProps, {
                className: className,
                key: key,
                style: style
            }), columns);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        };
        exports.default = defaultRowRenderer;
        var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react);
    }, /* 72 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.InfiniteLoader = exports.default = void 0;
        var _InfiniteLoader2 = __webpack_require__(73), _InfiniteLoader3 = _interopRequireDefault(_InfiniteLoader2);
        exports.default = _InfiniteLoader3.default, exports.InfiniteLoader = _InfiniteLoader3.default;
    }, /* 73 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            function isRangeVisible(_ref2) {
                var lastRenderedStartIndex = _ref2.lastRenderedStartIndex, lastRenderedStopIndex = _ref2.lastRenderedStopIndex, startIndex = _ref2.startIndex, stopIndex = _ref2.stopIndex;
                return !(startIndex > lastRenderedStopIndex || stopIndex < lastRenderedStartIndex);
            }
            /**
	 * Returns all of the ranges within a larger range that contain unloaded rows.
	 */
            function scanForUnloadedRanges(_ref3) {
                for (var isRowLoaded = _ref3.isRowLoaded, minimumBatchSize = _ref3.minimumBatchSize, rowCount = _ref3.rowCount, startIndex = _ref3.startIndex, stopIndex = _ref3.stopIndex, unloadedRanges = [], rangeStartIndex = null, rangeStopIndex = null, index = startIndex; index <= stopIndex; index++) {
                    var loaded = isRowLoaded({
                        index: index
                    });
                    loaded ? null !== rangeStopIndex && (unloadedRanges.push({
                        startIndex: rangeStartIndex,
                        stopIndex: rangeStopIndex
                    }), rangeStartIndex = rangeStopIndex = null) : (rangeStopIndex = index, null === rangeStartIndex && (rangeStartIndex = index));
                }
                // If :rangeStopIndex is not null it means we haven't ran out of unloaded rows.
                // Scan forward to try filling our :minimumBatchSize.
                if (null !== rangeStopIndex) {
                    for (var potentialStopIndex = Math.min(Math.max(rangeStopIndex, rangeStartIndex + minimumBatchSize - 1), rowCount - 1), _index = rangeStopIndex + 1; _index <= potentialStopIndex && !isRowLoaded({
                        index: _index
                    }); _index++) rangeStopIndex = _index;
                    unloadedRanges.push({
                        startIndex: rangeStartIndex,
                        stopIndex: rangeStopIndex
                    });
                }
                // Check to see if our first range ended prematurely.
                // In this case we should scan backwards to try filling our :minimumBatchSize.
                if (unloadedRanges.length) for (var firstUnloadedRange = unloadedRanges[0]; firstUnloadedRange.stopIndex - firstUnloadedRange.startIndex + 1 < minimumBatchSize && firstUnloadedRange.startIndex > 0; ) {
                    var _index2 = firstUnloadedRange.startIndex - 1;
                    if (isRowLoaded({
                        index: _index2
                    })) break;
                    firstUnloadedRange.startIndex = _index2;
                }
                return unloadedRanges;
            }
            /**
	 * Since RV components use shallowCompare we need to force a render (even though props haven't changed).
	 * However InfiniteLoader may wrap a Grid or it may wrap a Table or List.
	 * In the first case the built-in React forceUpdate() method is sufficient to force a re-render,
	 * But in the latter cases we need to use the RV-specific forceUpdateGrid() method.
	 * Else the inner Grid will not be re-rendered and visuals may be stale.
	 */
            function forceUpdateReactVirtualizedComponent(component) {
                "function" == typeof component.forceUpdateGrid ? component.forceUpdateGrid() : component.forceUpdate();
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }();
            exports.isRangeVisible = isRangeVisible, exports.scanForUnloadedRanges = scanForUnloadedRanges, 
            exports.forceUpdateReactVirtualizedComponent = forceUpdateReactVirtualizedComponent;
            var _react = __webpack_require__(5), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _createCallbackMemoizer = __webpack_require__(46), _createCallbackMemoizer2 = _interopRequireDefault(_createCallbackMemoizer), InfiniteLoader = function(_Component) {
                function InfiniteLoader(props, context) {
                    _classCallCheck(this, InfiniteLoader);
                    var _this = _possibleConstructorReturn(this, (InfiniteLoader.__proto__ || Object.getPrototypeOf(InfiniteLoader)).call(this, props, context));
                    return _this._loadMoreRowsMemoizer = (0, _createCallbackMemoizer2.default)(), _this._onRowsRendered = _this._onRowsRendered.bind(_this), 
                    _this._registerChild = _this._registerChild.bind(_this), _this;
                }
                return _inherits(InfiniteLoader, _Component), _createClass(InfiniteLoader, [ {
                    key: "render",
                    value: function() {
                        var children = this.props.children;
                        return children({
                            onRowsRendered: this._onRowsRendered,
                            registerChild: this._registerChild
                        });
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_loadUnloadedRanges",
                    value: function(unloadedRanges) {
                        var _this2 = this, loadMoreRows = this.props.loadMoreRows;
                        unloadedRanges.forEach(function(unloadedRange) {
                            var promise = loadMoreRows(unloadedRange);
                            promise && promise.then(function() {
                                // Refresh the visible rows if any of them have just been loaded.
                                // Otherwise they will remain in their unloaded visual state.
                                isRangeVisible({
                                    lastRenderedStartIndex: _this2._lastRenderedStartIndex,
                                    lastRenderedStopIndex: _this2._lastRenderedStopIndex,
                                    startIndex: unloadedRange.startIndex,
                                    stopIndex: unloadedRange.stopIndex
                                }) && _this2._registeredChild && forceUpdateReactVirtualizedComponent(_this2._registeredChild);
                            });
                        });
                    }
                }, {
                    key: "_onRowsRendered",
                    value: function(_ref) {
                        var _this3 = this, startIndex = _ref.startIndex, stopIndex = _ref.stopIndex, _props = this.props, isRowLoaded = _props.isRowLoaded, minimumBatchSize = _props.minimumBatchSize, rowCount = _props.rowCount, threshold = _props.threshold;
                        this._lastRenderedStartIndex = startIndex, this._lastRenderedStopIndex = stopIndex;
                        var unloadedRanges = scanForUnloadedRanges({
                            isRowLoaded: isRowLoaded,
                            minimumBatchSize: minimumBatchSize,
                            rowCount: rowCount,
                            startIndex: Math.max(0, startIndex - threshold),
                            stopIndex: Math.min(rowCount - 1, stopIndex + threshold)
                        }), squashedUnloadedRanges = unloadedRanges.reduce(function(reduced, unloadedRange) {
                            return reduced.concat([ unloadedRange.startIndex, unloadedRange.stopIndex ]);
                        }, []);
                        this._loadMoreRowsMemoizer({
                            callback: function() {
                                _this3._loadUnloadedRanges(unloadedRanges);
                            },
                            indices: {
                                squashedUnloadedRanges: squashedUnloadedRanges
                            }
                        });
                    }
                }, {
                    key: "_registerChild",
                    value: function(registeredChild) {
                        this._registeredChild = registeredChild;
                    }
                } ]), InfiniteLoader;
            }(_react.Component);
            /**
	 * Determines if the specified start/stop range is visible based on the most recently rendered range.
	 */
            InfiniteLoader.defaultProps = {
                minimumBatchSize: 10,
                rowCount: 0,
                threshold: 15
            }, exports.default = InfiniteLoader, "production" !== process.env.NODE_ENV ? InfiniteLoader.propTypes = {
                /**
	   * Function responsible for rendering a virtualized component.
	   * This function should implement the following signature:
	   * ({ onRowsRendered, registerChild }) => PropTypes.element
	   *
	   * The specified :onRowsRendered function should be passed through to the child's :onRowsRendered property.
	   * The :registerChild callback should be set as the virtualized component's :ref.
	   */
                children: _react.PropTypes.func.isRequired,
                /**
	   * Function responsible for tracking the loaded state of each row.
	   * It should implement the following signature: ({ index: number }): boolean
	   */
                isRowLoaded: _react.PropTypes.func.isRequired,
                /**
	   * Callback to be invoked when more rows must be loaded.
	   * It should implement the following signature: ({ startIndex, stopIndex }): Promise
	   * The returned Promise should be resolved once row data has finished loading.
	   * It will be used to determine when to refresh the list with the newly-loaded data.
	   * This callback may be called multiple times in reaction to a single scroll event.
	   */
                loadMoreRows: _react.PropTypes.func.isRequired,
                /**
	   * Minimum number of rows to be loaded at a time.
	   * This property can be used to batch requests to reduce HTTP requests.
	   */
                minimumBatchSize: _react.PropTypes.number.isRequired,
                /**
	   * Number of rows in list; can be arbitrary high number if actual number is unknown.
	   */
                rowCount: _react.PropTypes.number.isRequired,
                /**
	   * Threshold at which to pre-fetch data.
	   * A threshold X means that data will start loading when a user scrolls within X rows.
	   * This value defaults to 15.
	   */
                threshold: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 74 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.ScrollSync = exports.default = void 0;
        var _ScrollSync2 = __webpack_require__(75), _ScrollSync3 = _interopRequireDefault(_ScrollSync2);
        exports.default = _ScrollSync3.default, exports.ScrollSync = _ScrollSync3.default;
    }, /* 75 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), ScrollSync = function(_Component) {
                function ScrollSync(props, context) {
                    _classCallCheck(this, ScrollSync);
                    var _this = _possibleConstructorReturn(this, (ScrollSync.__proto__ || Object.getPrototypeOf(ScrollSync)).call(this, props, context));
                    return _this.state = {
                        clientHeight: 0,
                        clientWidth: 0,
                        scrollHeight: 0,
                        scrollLeft: 0,
                        scrollTop: 0,
                        scrollWidth: 0
                    }, _this._onScroll = _this._onScroll.bind(_this), _this;
                }
                return _inherits(ScrollSync, _Component), _createClass(ScrollSync, [ {
                    key: "render",
                    value: function() {
                        var children = this.props.children, _state = this.state, clientHeight = _state.clientHeight, clientWidth = _state.clientWidth, scrollHeight = _state.scrollHeight, scrollLeft = _state.scrollLeft, scrollTop = _state.scrollTop, scrollWidth = _state.scrollWidth;
                        return children({
                            clientHeight: clientHeight,
                            clientWidth: clientWidth,
                            onScroll: this._onScroll,
                            scrollHeight: scrollHeight,
                            scrollLeft: scrollLeft,
                            scrollTop: scrollTop,
                            scrollWidth: scrollWidth
                        });
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_onScroll",
                    value: function(_ref) {
                        var clientHeight = _ref.clientHeight, clientWidth = _ref.clientWidth, scrollHeight = _ref.scrollHeight, scrollLeft = _ref.scrollLeft, scrollTop = _ref.scrollTop, scrollWidth = _ref.scrollWidth;
                        this.setState({
                            clientHeight: clientHeight,
                            clientWidth: clientWidth,
                            scrollHeight: scrollHeight,
                            scrollLeft: scrollLeft,
                            scrollTop: scrollTop,
                            scrollWidth: scrollWidth
                        });
                    }
                } ]), ScrollSync;
            }(_react.Component);
            exports.default = ScrollSync, "production" !== process.env.NODE_ENV ? ScrollSync.propTypes = {
                /**
	   * Function responsible for rendering 2 or more virtualized components.
	   * This function should implement the following signature:
	   * ({ onScroll, scrollLeft, scrollTop }) => PropTypes.element
	   */
                children: _react.PropTypes.func.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 76 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.List = exports.default = void 0;
        var _List2 = __webpack_require__(77), _List3 = _interopRequireDefault(_List2);
        exports.default = _List3.default, exports.List = _List3.default;
    }, /* 77 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _objectWithoutProperties(obj, keys) {
                var target = {};
                for (var i in obj) keys.indexOf(i) >= 0 || Object.prototype.hasOwnProperty.call(obj, i) && (target[i] = obj[i]);
                return target;
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            }, _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _Grid = __webpack_require__(55), _Grid2 = _interopRequireDefault(_Grid), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _classnames = __webpack_require__(45), _classnames2 = _interopRequireDefault(_classnames), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), List = function(_Component) {
                function List(props, context) {
                    _classCallCheck(this, List);
                    var _this = _possibleConstructorReturn(this, (List.__proto__ || Object.getPrototypeOf(List)).call(this, props, context));
                    return _this._cellRenderer = _this._cellRenderer.bind(_this), _this._onScroll = _this._onScroll.bind(_this), 
                    _this._onSectionRendered = _this._onSectionRendered.bind(_this), _this;
                }
                return _inherits(List, _Component), _createClass(List, [ {
                    key: "forceUpdateGrid",
                    value: function() {
                        this.Grid.forceUpdate();
                    }
                }, {
                    key: "measureAllRows",
                    value: function() {
                        this.Grid.measureAllCells();
                    }
                }, {
                    key: "recomputeRowHeights",
                    value: function() {
                        var index = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        this.Grid.recomputeGridSize({
                            rowIndex: index
                        }), this.forceUpdateGrid();
                    }
                }, {
                    key: "render",
                    value: function() {
                        var _this2 = this, _props = this.props, className = _props.className, noRowsRenderer = _props.noRowsRenderer, scrollToIndex = _props.scrollToIndex, width = _props.width, classNames = (0, 
                        _classnames2.default)("ReactVirtualized__List", className);
                        return _react2.default.createElement(_Grid2.default, _extends({}, this.props, {
                            autoContainerWidth: !0,
                            cellRenderer: this._cellRenderer,
                            className: classNames,
                            columnWidth: width,
                            columnCount: 1,
                            noContentRenderer: noRowsRenderer,
                            onScroll: this._onScroll,
                            onSectionRendered: this._onSectionRendered,
                            ref: function(_ref) {
                                _this2.Grid = _ref;
                            },
                            scrollToRow: scrollToIndex
                        }));
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_cellRenderer",
                    value: function(_ref2) {
                        var rowIndex = _ref2.rowIndex, style = _ref2.style, rest = _objectWithoutProperties(_ref2, [ "rowIndex", "style" ]), rowRenderer = this.props.rowRenderer;
                        // By default, List cells should be 100% width.
                        // This prevents them from flowing under a scrollbar (if present).
                        return style.width = "100%", rowRenderer(_extends({
                            index: rowIndex,
                            style: style
                        }, rest));
                    }
                }, {
                    key: "_onScroll",
                    value: function(_ref3) {
                        var clientHeight = _ref3.clientHeight, scrollHeight = _ref3.scrollHeight, scrollTop = _ref3.scrollTop, onScroll = this.props.onScroll;
                        onScroll({
                            clientHeight: clientHeight,
                            scrollHeight: scrollHeight,
                            scrollTop: scrollTop
                        });
                    }
                }, {
                    key: "_onSectionRendered",
                    value: function(_ref4) {
                        var rowOverscanStartIndex = _ref4.rowOverscanStartIndex, rowOverscanStopIndex = _ref4.rowOverscanStopIndex, rowStartIndex = _ref4.rowStartIndex, rowStopIndex = _ref4.rowStopIndex, onRowsRendered = this.props.onRowsRendered;
                        onRowsRendered({
                            overscanStartIndex: rowOverscanStartIndex,
                            overscanStopIndex: rowOverscanStopIndex,
                            startIndex: rowStartIndex,
                            stopIndex: rowStopIndex
                        });
                    }
                } ]), List;
            }(_react.Component);
            List.defaultProps = {
                estimatedRowSize: 30,
                noRowsRenderer: function() {
                    return null;
                },
                onRowsRendered: function() {
                    return null;
                },
                onScroll: function() {
                    return null;
                },
                overscanRowCount: 10,
                scrollToAlignment: "auto",
                style: {}
            }, exports.default = List, "production" !== process.env.NODE_ENV ? List.propTypes = {
                "aria-label": _react.PropTypes.string,
                /**
	   * Removes fixed height from the scrollingContainer so that the total height
	   * of rows can stretch the window. Intended for use with WindowScroller
	   */
                autoHeight: _react.PropTypes.bool,
                /** Optional CSS class name */
                className: _react.PropTypes.string,
                /**
	   * Used to estimate the total height of a List before all of its rows have actually been measured.
	   * The estimated total height is adjusted as rows are rendered.
	   */
                estimatedRowSize: _react.PropTypes.number.isRequired,
                /** Height constraint for list (determines how many actual rows are rendered) */
                height: _react.PropTypes.number.isRequired,
                /** Optional renderer to be used in place of rows when rowCount is 0 */
                noRowsRenderer: _react.PropTypes.func.isRequired,
                /**
	   * Callback invoked with information about the slice of rows that were just rendered.
	   * ({ startIndex, stopIndex }): void
	   */
                onRowsRendered: _react.PropTypes.func.isRequired,
                /**
	   * Number of rows to render above/below the visible bounds of the list.
	   * These rows can help for smoother scrolling on touch devices.
	   */
                overscanRowCount: _react.PropTypes.number.isRequired,
                /**
	   * Callback invoked whenever the scroll offset changes within the inner scrollable region.
	   * This callback can be used to sync scrolling between lists, tables, or grids.
	   * ({ clientHeight, scrollHeight, scrollTop }): void
	   */
                onScroll: _react.PropTypes.func.isRequired,
                /**
	   * Either a fixed row height (number) or a function that returns the height of a row given its index.
	   * ({ index: number }): number
	   */
                rowHeight: _react.PropTypes.oneOfType([ _react.PropTypes.number, _react.PropTypes.func ]).isRequired,
                /** Responsible for rendering a row given an index; ({ index: number }): node */
                rowRenderer: _react.PropTypes.func.isRequired,
                /** Number of rows in list. */
                rowCount: _react.PropTypes.number.isRequired,
                /** See Grid#scrollToAlignment */
                scrollToAlignment: _react.PropTypes.oneOf([ "auto", "end", "start", "center" ]).isRequired,
                /** Row index to ensure visible (by forcefully scrolling if necessary) */
                scrollToIndex: _react.PropTypes.number,
                /** Vertical offset. */
                scrollTop: _react.PropTypes.number,
                /** Optional inline style */
                style: _react.PropTypes.object,
                /** Tab index for focus */
                tabIndex: _react.PropTypes.number,
                /** Width of list */
                width: _react.PropTypes.number.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 78 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.IS_SCROLLING_TIMEOUT = exports.WindowScroller = exports.default = void 0;
        var _onScroll = __webpack_require__(79);
        Object.defineProperty(exports, "IS_SCROLLING_TIMEOUT", {
            enumerable: !0,
            get: function() {
                return _onScroll.IS_SCROLLING_TIMEOUT;
            }
        });
        var _WindowScroller2 = __webpack_require__(80), _WindowScroller3 = _interopRequireDefault(_WindowScroller2);
        exports.default = _WindowScroller3.default, exports.WindowScroller = _WindowScroller3.default;
    }, /* 79 */
    /***/
    function(module, exports) {
        "use strict";
        function enablePointerEventsIfDisabled() {
            disablePointerEventsTimeoutId && (disablePointerEventsTimeoutId = null, document.body.style.pointerEvents = originalBodyPointerEvents, 
            originalBodyPointerEvents = null);
        }
        function enablePointerEventsAfterDelayCallback() {
            enablePointerEventsIfDisabled(), mountedInstances.forEach(function(component) {
                return component._enablePointerEventsAfterDelayCallback();
            });
        }
        function enablePointerEventsAfterDelay() {
            disablePointerEventsTimeoutId && clearTimeout(disablePointerEventsTimeoutId), disablePointerEventsTimeoutId = setTimeout(enablePointerEventsAfterDelayCallback, IS_SCROLLING_TIMEOUT);
        }
        function onScrollWindow(event) {
            null == originalBodyPointerEvents && (originalBodyPointerEvents = document.body.style.pointerEvents, 
            document.body.style.pointerEvents = "none", enablePointerEventsAfterDelay()), mountedInstances.forEach(function(component) {
                return component._onScrollWindow(event);
            });
        }
        function registerScrollListener(component) {
            mountedInstances.length || window.addEventListener("scroll", onScrollWindow), mountedInstances.push(component);
        }
        function unregisterScrollListener(component) {
            mountedInstances = mountedInstances.filter(function(c) {
                return c !== component;
            }), mountedInstances.length || (window.removeEventListener("scroll", onScrollWindow), 
            disablePointerEventsTimeoutId && (clearTimeout(disablePointerEventsTimeoutId), enablePointerEventsIfDisabled()));
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.registerScrollListener = registerScrollListener, exports.unregisterScrollListener = unregisterScrollListener;
        var mountedInstances = [], originalBodyPointerEvents = null, disablePointerEventsTimeoutId = null, IS_SCROLLING_TIMEOUT = exports.IS_SCROLLING_TIMEOUT = 150;
    }, /* 80 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _possibleConstructorReturn(self, call) {
                if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !call || "object" != typeof call && "function" != typeof call ? self : call;
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            Object.defineProperty(exports, "__esModule", {
                value: !0
            });
            var _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }(), _react = __webpack_require__(5), _reactDom = __webpack_require__(40), _reactDom2 = _interopRequireDefault(_reactDom), _reactAddonsShallowCompare = __webpack_require__(32), _reactAddonsShallowCompare2 = _interopRequireDefault(_reactAddonsShallowCompare), _onScroll = __webpack_require__(79), WindowScroller = function(_Component) {
                function WindowScroller(props) {
                    _classCallCheck(this, WindowScroller);
                    var _this = _possibleConstructorReturn(this, (WindowScroller.__proto__ || Object.getPrototypeOf(WindowScroller)).call(this, props)), height = "undefined" != typeof window ? window.innerHeight : 0;
                    return _this.state = {
                        isScrolling: !1,
                        height: height,
                        scrollTop: 0
                    }, _this._onScrollWindow = _this._onScrollWindow.bind(_this), _this._onResizeWindow = _this._onResizeWindow.bind(_this), 
                    _this._enablePointerEventsAfterDelayCallback = _this._enablePointerEventsAfterDelayCallback.bind(_this), 
                    _this;
                }
                return _inherits(WindowScroller, _Component), _createClass(WindowScroller, [ {
                    key: "componentDidMount",
                    value: function() {
                        var height = this.state.height;
                        // Subtract documentElement top to handle edge-case where a user is navigating back (history) from an already-scrolled bage.
                        // In this case the body's top position will be a negative number and this element's top will be increased (by that amount).
                        this._positionFromTop = _reactDom2.default.findDOMNode(this).getBoundingClientRect().top - document.documentElement.getBoundingClientRect().top, 
                        height !== window.innerHeight && this.setState({
                            height: window.innerHeight
                        }), (0, _onScroll.registerScrollListener)(this), window.addEventListener("resize", this._onResizeWindow, !1);
                    }
                }, {
                    key: "componentWillUnmount",
                    value: function() {
                        (0, _onScroll.unregisterScrollListener)(this), window.removeEventListener("resize", this._onResizeWindow, !1);
                    }
                }, {
                    key: "render",
                    value: function() {
                        var children = this.props.children, _state = this.state, isScrolling = _state.isScrolling, scrollTop = _state.scrollTop, height = _state.height;
                        return children({
                            height: height,
                            isScrolling: isScrolling,
                            scrollTop: scrollTop
                        });
                    }
                }, {
                    key: "shouldComponentUpdate",
                    value: function(nextProps, nextState) {
                        return (0, _reactAddonsShallowCompare2.default)(this, nextProps, nextState);
                    }
                }, {
                    key: "_enablePointerEventsAfterDelayCallback",
                    value: function() {
                        this.setState({
                            isScrolling: !1
                        });
                    }
                }, {
                    key: "_onResizeWindow",
                    value: function(event) {
                        var onResize = this.props.onResize, height = window.innerHeight || 0;
                        this.setState({
                            height: height
                        }), onResize({
                            height: height
                        });
                    }
                }, {
                    key: "_onScrollWindow",
                    value: function(event) {
                        var onScroll = this.props.onScroll, scrollY = "scrollY" in window ? window.scrollY : document.documentElement.scrollTop, scrollTop = Math.max(0, scrollY - this._positionFromTop);
                        this.setState({
                            isScrolling: !0,
                            scrollTop: scrollTop
                        }), onScroll({
                            scrollTop: scrollTop
                        });
                    }
                } ]), WindowScroller;
            }(_react.Component);
            WindowScroller.defaultProps = {
                onResize: function() {},
                onScroll: function() {}
            }, exports.default = WindowScroller, "production" !== process.env.NODE_ENV ? WindowScroller.propTypes = {
                /**
	   * Function responsible for rendering children.
	   * This function should implement the following signature:
	   * ({ height, scrollTop }) => PropTypes.element
	   */
                children: _react.PropTypes.func.isRequired,
                /** Callback to be invoked on-resize: ({ height }) */
                onResize: _react.PropTypes.func.isRequired,
                /** Callback to be invoked on-scroll: ({ scrollTop }) */
                onScroll: _react.PropTypes.func.isRequired
            } : void 0;
        }).call(exports, __webpack_require__(4));
    }, /* 81 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequire(obj) {
            return obj && obj.__esModule ? obj.default : obj;
        }
        exports.__esModule = !0;
        var _DragDropContext = __webpack_require__(82);
        exports.DragDropContext = _interopRequire(_DragDropContext);
        var _DragLayer = __webpack_require__(176);
        exports.DragLayer = _interopRequire(_DragLayer);
        var _DragSource = __webpack_require__(179);
        exports.DragSource = _interopRequire(_DragSource);
        var _DropTarget = __webpack_require__(194);
        exports.DropTarget = _interopRequire(_DropTarget);
    }, /* 82 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        function DragDropContext(backendOrModule) {
            _utilsCheckDecoratorArguments2.default.apply(void 0, [ "DragDropContext", "backend" ].concat(_slice.call(arguments)));
            // Auto-detect ES6 default export for people still using ES5
            var backend = void 0;
            backend = "object" == typeof backendOrModule && "function" == typeof backendOrModule.default ? backendOrModule.default : backendOrModule, 
            _invariant2.default("function" == typeof backend, "Expected the backend to be a function or an ES6 module exporting a default function. Read more: http://gaearon.github.io/react-dnd/docs-drag-drop-context.html");
            var childContext = {
                dragDropManager: new _dndCore.DragDropManager(backend)
            };
            return function(DecoratedComponent) {
                var displayName = DecoratedComponent.displayName || DecoratedComponent.name || "Component";
                return function(_Component) {
                    function DragDropContextContainer() {
                        _classCallCheck(this, DragDropContextContainer), _Component.apply(this, arguments);
                    }
                    return _inherits(DragDropContextContainer, _Component), DragDropContextContainer.prototype.getDecoratedComponentInstance = function() {
                        return this.refs.child;
                    }, DragDropContextContainer.prototype.getManager = function() {
                        return childContext.dragDropManager;
                    }, DragDropContextContainer.prototype.getChildContext = function() {
                        return childContext;
                    }, DragDropContextContainer.prototype.render = function() {
                        return _react2.default.createElement(DecoratedComponent, _extends({}, this.props, {
                            ref: "child"
                        }));
                    }, _createClass(DragDropContextContainer, null, [ {
                        key: "DecoratedComponent",
                        value: DecoratedComponent,
                        enumerable: !0
                    }, {
                        key: "displayName",
                        value: "DragDropContext(" + displayName + ")",
                        enumerable: !0
                    }, {
                        key: "childContextTypes",
                        value: {
                            dragDropManager: _react.PropTypes.object.isRequired
                        },
                        enumerable: !0
                    } ]), DragDropContextContainer;
                }(_react.Component);
            };
        }
        exports.__esModule = !0;
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _slice = Array.prototype.slice, _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }();
        exports.default = DragDropContext;
        var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _dndCore = __webpack_require__(83), _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _utilsCheckDecoratorArguments = __webpack_require__(175), _utilsCheckDecoratorArguments2 = _interopRequireDefault(_utilsCheckDecoratorArguments);
        module.exports = exports.default;
    }, /* 83 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequire(obj) {
            return obj && obj.__esModule ? obj.default : obj;
        }
        exports.__esModule = !0;
        var _DragDropManager = __webpack_require__(84);
        exports.DragDropManager = _interopRequire(_DragDropManager);
        var _DragSource = __webpack_require__(172);
        exports.DragSource = _interopRequire(_DragSource);
        var _DropTarget = __webpack_require__(173);
        exports.DropTarget = _interopRequire(_DropTarget);
        var _backendsCreateTestBackend = __webpack_require__(174);
        exports.createTestBackend = _interopRequire(_backendsCreateTestBackend);
    }, /* 84 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireWildcard(obj) {
            if (obj && obj.__esModule) return obj;
            var newObj = {};
            if (null != obj) for (var key in obj) Object.prototype.hasOwnProperty.call(obj, key) && (newObj[key] = obj[key]);
            return newObj.default = obj, newObj;
        }
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var _reduxLibCreateStore = __webpack_require__(12), _reduxLibCreateStore2 = _interopRequireDefault(_reduxLibCreateStore), _reducers = __webpack_require__(85), _reducers2 = _interopRequireDefault(_reducers), _actionsDragDrop = __webpack_require__(87), dragDropActions = _interopRequireWildcard(_actionsDragDrop), _DragDropMonitor = __webpack_require__(167), _DragDropMonitor2 = _interopRequireDefault(_DragDropMonitor), _HandlerRegistry = __webpack_require__(168), DragDropManager = (_interopRequireDefault(_HandlerRegistry), 
        function() {
            function DragDropManager(createBackend) {
                _classCallCheck(this, DragDropManager);
                var store = _reduxLibCreateStore2.default(_reducers2.default);
                this.store = store, this.monitor = new _DragDropMonitor2.default(store), this.registry = this.monitor.registry, 
                this.backend = createBackend(this), store.subscribe(this.handleRefCountChange.bind(this));
            }
            return DragDropManager.prototype.handleRefCountChange = function() {
                var shouldSetUp = this.store.getState().refCount > 0;
                shouldSetUp && !this.isSetUp ? (this.backend.setup(), this.isSetUp = !0) : !shouldSetUp && this.isSetUp && (this.backend.teardown(), 
                this.isSetUp = !1);
            }, DragDropManager.prototype.getMonitor = function() {
                return this.monitor;
            }, DragDropManager.prototype.getBackend = function() {
                return this.backend;
            }, DragDropManager.prototype.getRegistry = function() {
                return this.registry;
            }, DragDropManager.prototype.getActions = function() {
                function bindActionCreator(actionCreator) {
                    return function() {
                        var action = actionCreator.apply(manager, arguments);
                        "undefined" != typeof action && dispatch(action);
                    };
                }
                var manager = this, dispatch = this.store.dispatch;
                return Object.keys(dragDropActions).filter(function(key) {
                    return "function" == typeof dragDropActions[key];
                }).reduce(function(boundActions, key) {
                    return boundActions[key] = bindActionCreator(dragDropActions[key]), boundActions;
                }, {});
            }, DragDropManager;
        }());
        exports.default = DragDropManager, module.exports = exports.default;
    }, /* 85 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        exports.__esModule = !0;
        var _dragOffset = __webpack_require__(86), _dragOffset2 = _interopRequireDefault(_dragOffset), _dragOperation = __webpack_require__(91), _dragOperation2 = _interopRequireDefault(_dragOperation), _refCount = __webpack_require__(152), _refCount2 = _interopRequireDefault(_refCount), _dirtyHandlerIds = __webpack_require__(153), _dirtyHandlerIds2 = _interopRequireDefault(_dirtyHandlerIds), _stateId = __webpack_require__(166), _stateId2 = _interopRequireDefault(_stateId);
        exports.default = function(state, action) {
            return void 0 === state && (state = {}), {
                dirtyHandlerIds: _dirtyHandlerIds2.default(state.dirtyHandlerIds, action, state.dragOperation),
                dragOffset: _dragOffset2.default(state.dragOffset, action),
                refCount: _refCount2.default(state.refCount, action),
                dragOperation: _dragOperation2.default(state.dragOperation, action),
                stateId: _stateId2.default(state.stateId)
            };
        }, module.exports = exports.default;
    }, /* 86 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function areOffsetsEqual(offsetA, offsetB) {
            return offsetA === offsetB || offsetA && offsetB && offsetA.x === offsetB.x && offsetA.y === offsetB.y;
        }
        function dragOffset(state, action) {
            switch (void 0 === state && (state = initialState), action.type) {
              case _actionsDragDrop.BEGIN_DRAG:
                return {
                    initialSourceClientOffset: action.sourceClientOffset,
                    initialClientOffset: action.clientOffset,
                    clientOffset: action.clientOffset
                };

              case _actionsDragDrop.HOVER:
                return areOffsetsEqual(state.clientOffset, action.clientOffset) ? state : _extends({}, state, {
                    clientOffset: action.clientOffset
                });

              case _actionsDragDrop.END_DRAG:
              case _actionsDragDrop.DROP:
                return initialState;

              default:
                return state;
            }
        }
        function getSourceClientOffset(state) {
            var clientOffset = state.clientOffset, initialClientOffset = state.initialClientOffset, initialSourceClientOffset = state.initialSourceClientOffset;
            return clientOffset && initialClientOffset && initialSourceClientOffset ? {
                x: clientOffset.x + initialSourceClientOffset.x - initialClientOffset.x,
                y: clientOffset.y + initialSourceClientOffset.y - initialClientOffset.y
            } : null;
        }
        function getDifferenceFromInitialOffset(state) {
            var clientOffset = state.clientOffset, initialClientOffset = state.initialClientOffset;
            return clientOffset && initialClientOffset ? {
                x: clientOffset.x - initialClientOffset.x,
                y: clientOffset.y - initialClientOffset.y
            } : null;
        }
        exports.__esModule = !0;
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        };
        exports.default = dragOffset, exports.getSourceClientOffset = getSourceClientOffset, 
        exports.getDifferenceFromInitialOffset = getDifferenceFromInitialOffset;
        var _actionsDragDrop = __webpack_require__(87), initialState = {
            initialSourceClientOffset: null,
            initialClientOffset: null,
            clientOffset: null
        };
    }, /* 87 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function beginDrag(sourceIds) {
            var _ref = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1], _ref$publishSource = _ref.publishSource, publishSource = void 0 === _ref$publishSource || _ref$publishSource, _ref$clientOffset = _ref.clientOffset, clientOffset = void 0 === _ref$clientOffset ? null : _ref$clientOffset, getSourceClientOffset = _ref.getSourceClientOffset;
            _invariant2.default(_lodashIsArray2.default(sourceIds), "Expected sourceIds to be an array.");
            var monitor = this.getMonitor(), registry = this.getRegistry();
            _invariant2.default(!monitor.isDragging(), "Cannot call beginDrag while dragging.");
            for (var i = 0; i < sourceIds.length; i++) _invariant2.default(registry.getSource(sourceIds[i]), "Expected sourceIds to be registered.");
            for (var sourceId = null, i = sourceIds.length - 1; i >= 0; i--) if (monitor.canDragSource(sourceIds[i])) {
                sourceId = sourceIds[i];
                break;
            }
            if (null !== sourceId) {
                var sourceClientOffset = null;
                clientOffset && (_invariant2.default("function" == typeof getSourceClientOffset, "When clientOffset is provided, getSourceClientOffset must be a function."), 
                sourceClientOffset = getSourceClientOffset(sourceId));
                var source = registry.getSource(sourceId), item = source.beginDrag(monitor, sourceId);
                _invariant2.default(_lodashIsObject2.default(item), "Item must be an object."), 
                registry.pinSource(sourceId);
                var itemType = registry.getSourceType(sourceId);
                return {
                    type: BEGIN_DRAG,
                    itemType: itemType,
                    item: item,
                    sourceId: sourceId,
                    clientOffset: clientOffset,
                    sourceClientOffset: sourceClientOffset,
                    isSourcePublic: publishSource
                };
            }
        }
        function publishDragSource(manager) {
            var monitor = this.getMonitor();
            if (monitor.isDragging()) return {
                type: PUBLISH_DRAG_SOURCE
            };
        }
        function hover(targetIds) {
            var _ref2 = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1], _ref2$clientOffset = _ref2.clientOffset, clientOffset = void 0 === _ref2$clientOffset ? null : _ref2$clientOffset;
            _invariant2.default(_lodashIsArray2.default(targetIds), "Expected targetIds to be an array."), 
            targetIds = targetIds.slice(0);
            var monitor = this.getMonitor(), registry = this.getRegistry();
            _invariant2.default(monitor.isDragging(), "Cannot call hover while not dragging."), 
            _invariant2.default(!monitor.didDrop(), "Cannot call hover after drop.");
            // First check invariants.
            for (var i = 0; i < targetIds.length; i++) {
                var targetId = targetIds[i];
                _invariant2.default(targetIds.lastIndexOf(targetId) === i, "Expected targetIds to be unique in the passed array.");
                var target = registry.getTarget(targetId);
                _invariant2.default(target, "Expected targetIds to be registered.");
            }
            // Remove those targetIds that don't match the targetType.  This
            // fixes shallow isOver which would only be non-shallow because of
            // non-matching targets.
            for (var draggedItemType = monitor.getItemType(), i = targetIds.length - 1; i >= 0; i--) {
                var targetId = targetIds[i], targetType = registry.getTargetType(targetId);
                _utilsMatchesType2.default(targetType, draggedItemType) || targetIds.splice(i, 1);
            }
            // Finally call hover on all matching targets.
            for (var i = 0; i < targetIds.length; i++) {
                var targetId = targetIds[i], target = registry.getTarget(targetId);
                target.hover(monitor, targetId);
            }
            return {
                type: HOVER,
                targetIds: targetIds,
                clientOffset: clientOffset
            };
        }
        function drop() {
            var _this = this, monitor = this.getMonitor(), registry = this.getRegistry();
            _invariant2.default(monitor.isDragging(), "Cannot call drop while not dragging."), 
            _invariant2.default(!monitor.didDrop(), "Cannot call drop twice during one drag operation.");
            var targetIds = monitor.getTargetIds().filter(monitor.canDropOnTarget, monitor);
            targetIds.reverse(), targetIds.forEach(function(targetId, index) {
                var target = registry.getTarget(targetId), dropResult = target.drop(monitor, targetId);
                _invariant2.default("undefined" == typeof dropResult || _lodashIsObject2.default(dropResult), "Drop result must either be an object or undefined."), 
                "undefined" == typeof dropResult && (dropResult = 0 === index ? {} : monitor.getDropResult()), 
                _this.store.dispatch({
                    type: DROP,
                    dropResult: dropResult
                });
            });
        }
        function endDrag() {
            var monitor = this.getMonitor(), registry = this.getRegistry();
            _invariant2.default(monitor.isDragging(), "Cannot call endDrag while not dragging.");
            var sourceId = monitor.getSourceId(), source = registry.getSource(sourceId, !0);
            return source.endDrag(monitor, sourceId), registry.unpinSource(), {
                type: END_DRAG
            };
        }
        exports.__esModule = !0, exports.beginDrag = beginDrag, exports.publishDragSource = publishDragSource, 
        exports.hover = hover, exports.drop = drop, exports.endDrag = endDrag;
        var _utilsMatchesType = __webpack_require__(88), _utilsMatchesType2 = _interopRequireDefault(_utilsMatchesType), _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _lodashIsArray = __webpack_require__(89), _lodashIsArray2 = _interopRequireDefault(_lodashIsArray), _lodashIsObject = __webpack_require__(90), _lodashIsObject2 = _interopRequireDefault(_lodashIsObject), BEGIN_DRAG = "dnd-core/BEGIN_DRAG";
        exports.BEGIN_DRAG = BEGIN_DRAG;
        var PUBLISH_DRAG_SOURCE = "dnd-core/PUBLISH_DRAG_SOURCE";
        exports.PUBLISH_DRAG_SOURCE = PUBLISH_DRAG_SOURCE;
        var HOVER = "dnd-core/HOVER";
        exports.HOVER = HOVER;
        var DROP = "dnd-core/DROP";
        exports.DROP = DROP;
        var END_DRAG = "dnd-core/END_DRAG";
        exports.END_DRAG = END_DRAG;
    }, /* 88 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function matchesType(targetType, draggedItemType) {
            return _lodashIsArray2.default(targetType) ? targetType.some(function(t) {
                return t === draggedItemType;
            }) : targetType === draggedItemType;
        }
        exports.__esModule = !0, exports.default = matchesType;
        var _lodashIsArray = __webpack_require__(89), _lodashIsArray2 = _interopRequireDefault(_lodashIsArray);
        module.exports = exports.default;
    }, /* 89 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is classified as an `Array` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
	 * @example
	 *
	 * _.isArray([1, 2, 3]);
	 * // => true
	 *
	 * _.isArray(document.body.children);
	 * // => false
	 *
	 * _.isArray('abc');
	 * // => false
	 *
	 * _.isArray(_.noop);
	 * // => false
	 */
        var isArray = Array.isArray;
        module.exports = isArray;
    }, /* 90 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is the
	 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
	 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
	 * @example
	 *
	 * _.isObject({});
	 * // => true
	 *
	 * _.isObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isObject(_.noop);
	 * // => true
	 *
	 * _.isObject(null);
	 * // => false
	 */
        function isObject(value) {
            var type = typeof value;
            return null != value && ("object" == type || "function" == type);
        }
        module.exports = isObject;
    }, /* 91 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function dragOperation(state, action) {
            switch (void 0 === state && (state = initialState), action.type) {
              case _actionsDragDrop.BEGIN_DRAG:
                return _extends({}, state, {
                    itemType: action.itemType,
                    item: action.item,
                    sourceId: action.sourceId,
                    isSourcePublic: action.isSourcePublic,
                    dropResult: null,
                    didDrop: !1
                });

              case _actionsDragDrop.PUBLISH_DRAG_SOURCE:
                return _extends({}, state, {
                    isSourcePublic: !0
                });

              case _actionsDragDrop.HOVER:
                return _extends({}, state, {
                    targetIds: action.targetIds
                });

              case _actionsRegistry.REMOVE_TARGET:
                return state.targetIds.indexOf(action.targetId) === -1 ? state : _extends({}, state, {
                    targetIds: _lodashWithout2.default(state.targetIds, action.targetId)
                });

              case _actionsDragDrop.DROP:
                return _extends({}, state, {
                    dropResult: action.dropResult,
                    didDrop: !0,
                    targetIds: []
                });

              case _actionsDragDrop.END_DRAG:
                return _extends({}, state, {
                    itemType: null,
                    item: null,
                    sourceId: null,
                    dropResult: null,
                    didDrop: !1,
                    isSourcePublic: null,
                    targetIds: []
                });

              default:
                return state;
            }
        }
        exports.__esModule = !0;
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        };
        exports.default = dragOperation;
        var _actionsDragDrop = __webpack_require__(87), _actionsRegistry = __webpack_require__(92), _lodashWithout = __webpack_require__(93), _lodashWithout2 = _interopRequireDefault(_lodashWithout), initialState = {
            itemType: null,
            item: null,
            sourceId: null,
            targetIds: [],
            dropResult: null,
            didDrop: !1,
            isSourcePublic: null
        };
        module.exports = exports.default;
    }, /* 92 */
    /***/
    function(module, exports) {
        "use strict";
        function addSource(sourceId) {
            return {
                type: ADD_SOURCE,
                sourceId: sourceId
            };
        }
        function addTarget(targetId) {
            return {
                type: ADD_TARGET,
                targetId: targetId
            };
        }
        function removeSource(sourceId) {
            return {
                type: REMOVE_SOURCE,
                sourceId: sourceId
            };
        }
        function removeTarget(targetId) {
            return {
                type: REMOVE_TARGET,
                targetId: targetId
            };
        }
        exports.__esModule = !0, exports.addSource = addSource, exports.addTarget = addTarget, 
        exports.removeSource = removeSource, exports.removeTarget = removeTarget;
        var ADD_SOURCE = "dnd-core/ADD_SOURCE";
        exports.ADD_SOURCE = ADD_SOURCE;
        var ADD_TARGET = "dnd-core/ADD_TARGET";
        exports.ADD_TARGET = ADD_TARGET;
        var REMOVE_SOURCE = "dnd-core/REMOVE_SOURCE";
        exports.REMOVE_SOURCE = REMOVE_SOURCE;
        var REMOVE_TARGET = "dnd-core/REMOVE_TARGET";
        exports.REMOVE_TARGET = REMOVE_TARGET;
    }, /* 93 */
    /***/
    function(module, exports, __webpack_require__) {
        var baseDifference = __webpack_require__(94), baseRest = __webpack_require__(140), isArrayLikeObject = __webpack_require__(149), without = baseRest(function(array, values) {
            return isArrayLikeObject(array) ? baseDifference(array, values) : [];
        });
        module.exports = without;
    }, /* 94 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of methods like `_.difference` without support
	 * for excluding multiple arrays or iteratee shorthands.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {Array} values The values to exclude.
	 * @param {Function} [iteratee] The iteratee invoked per element.
	 * @param {Function} [comparator] The comparator invoked per element.
	 * @returns {Array} Returns the new array of filtered values.
	 */
        function baseDifference(array, values, iteratee, comparator) {
            var index = -1, includes = arrayIncludes, isCommon = !0, length = array.length, result = [], valuesLength = values.length;
            if (!length) return result;
            iteratee && (values = arrayMap(values, baseUnary(iteratee))), comparator ? (includes = arrayIncludesWith, 
            isCommon = !1) : values.length >= LARGE_ARRAY_SIZE && (includes = cacheHas, isCommon = !1, 
            values = new SetCache(values));
            outer: for (;++index < length; ) {
                var value = array[index], computed = iteratee ? iteratee(value) : value;
                if (value = comparator || 0 !== value ? value : 0, isCommon && computed === computed) {
                    for (var valuesIndex = valuesLength; valuesIndex--; ) if (values[valuesIndex] === computed) continue outer;
                    result.push(value);
                } else includes(values, computed, comparator) || result.push(value);
            }
            return result;
        }
        var SetCache = __webpack_require__(95), arrayIncludes = __webpack_require__(131), arrayIncludesWith = __webpack_require__(136), arrayMap = __webpack_require__(137), baseUnary = __webpack_require__(138), cacheHas = __webpack_require__(139), LARGE_ARRAY_SIZE = 200;
        module.exports = baseDifference;
    }, /* 95 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 *
	 * Creates an array cache object to store unique values.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [values] The values to cache.
	 */
        function SetCache(values) {
            var index = -1, length = values ? values.length : 0;
            for (this.__data__ = new MapCache(); ++index < length; ) this.add(values[index]);
        }
        var MapCache = __webpack_require__(96), setCacheAdd = __webpack_require__(129), setCacheHas = __webpack_require__(130);
        // Add methods to `SetCache`.
        SetCache.prototype.add = SetCache.prototype.push = setCacheAdd, SetCache.prototype.has = setCacheHas, 
        module.exports = SetCache;
    }, /* 96 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates a map cache object to store key-value pairs.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
        function MapCache(entries) {
            var index = -1, length = entries ? entries.length : 0;
            for (this.clear(); ++index < length; ) {
                var entry = entries[index];
                this.set(entry[0], entry[1]);
            }
        }
        var mapCacheClear = __webpack_require__(97), mapCacheDelete = __webpack_require__(123), mapCacheGet = __webpack_require__(126), mapCacheHas = __webpack_require__(127), mapCacheSet = __webpack_require__(128);
        // Add methods to `MapCache`.
        MapCache.prototype.clear = mapCacheClear, MapCache.prototype.delete = mapCacheDelete, 
        MapCache.prototype.get = mapCacheGet, MapCache.prototype.has = mapCacheHas, MapCache.prototype.set = mapCacheSet, 
        module.exports = MapCache;
    }, /* 97 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Removes all key-value entries from the map.
	 *
	 * @private
	 * @name clear
	 * @memberOf MapCache
	 */
        function mapCacheClear() {
            this.size = 0, this.__data__ = {
                hash: new Hash(),
                map: new (Map || ListCache)(),
                string: new Hash()
            };
        }
        var Hash = __webpack_require__(98), ListCache = __webpack_require__(114), Map = __webpack_require__(122);
        module.exports = mapCacheClear;
    }, /* 98 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates a hash object.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
        function Hash(entries) {
            var index = -1, length = entries ? entries.length : 0;
            for (this.clear(); ++index < length; ) {
                var entry = entries[index];
                this.set(entry[0], entry[1]);
            }
        }
        var hashClear = __webpack_require__(99), hashDelete = __webpack_require__(110), hashGet = __webpack_require__(111), hashHas = __webpack_require__(112), hashSet = __webpack_require__(113);
        // Add methods to `Hash`.
        Hash.prototype.clear = hashClear, Hash.prototype.delete = hashDelete, Hash.prototype.get = hashGet, 
        Hash.prototype.has = hashHas, Hash.prototype.set = hashSet, module.exports = Hash;
    }, /* 99 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Removes all key-value entries from the hash.
	 *
	 * @private
	 * @name clear
	 * @memberOf Hash
	 */
        function hashClear() {
            this.__data__ = nativeCreate ? nativeCreate(null) : {}, this.size = 0;
        }
        var nativeCreate = __webpack_require__(100);
        module.exports = hashClear;
    }, /* 100 */
    /***/
    function(module, exports, __webpack_require__) {
        var getNative = __webpack_require__(101), nativeCreate = getNative(Object, "create");
        module.exports = nativeCreate;
    }, /* 101 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Gets the native function at `key` of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {string} key The key of the method to get.
	 * @returns {*} Returns the function if it's native, else `undefined`.
	 */
        function getNative(object, key) {
            var value = getValue(object, key);
            return baseIsNative(value) ? value : void 0;
        }
        var baseIsNative = __webpack_require__(102), getValue = __webpack_require__(109);
        module.exports = getNative;
    }, /* 102 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.isNative` without bad shim checks.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a native function,
	 *  else `false`.
	 */
        function baseIsNative(value) {
            if (!isObject(value) || isMasked(value)) return !1;
            var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
            return pattern.test(toSource(value));
        }
        var isFunction = __webpack_require__(103), isMasked = __webpack_require__(104), isObject = __webpack_require__(90), toSource = __webpack_require__(108), reRegExpChar = /[\\^$.*+?()[\]{}|]/g, reIsHostCtor = /^\[object .+?Constructor\]$/, funcProto = Function.prototype, objectProto = Object.prototype, funcToString = funcProto.toString, hasOwnProperty = objectProto.hasOwnProperty, reIsNative = RegExp("^" + funcToString.call(hasOwnProperty).replace(reRegExpChar, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
        module.exports = baseIsNative;
    }, /* 103 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if `value` is classified as a `Function` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
	 * @example
	 *
	 * _.isFunction(_);
	 * // => true
	 *
	 * _.isFunction(/abc/);
	 * // => false
	 */
        function isFunction(value) {
            // The use of `Object#toString` avoids issues with the `typeof` operator
            // in Safari 9 which returns 'object' for typed array and other constructors.
            var tag = isObject(value) ? objectToString.call(value) : "";
            return tag == funcTag || tag == genTag || tag == proxyTag;
        }
        var isObject = __webpack_require__(90), funcTag = "[object Function]", genTag = "[object GeneratorFunction]", proxyTag = "[object Proxy]", objectProto = Object.prototype, objectToString = objectProto.toString;
        module.exports = isFunction;
    }, /* 104 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if `func` has its source masked.
	 *
	 * @private
	 * @param {Function} func The function to check.
	 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
	 */
        function isMasked(func) {
            return !!maskSrcKey && maskSrcKey in func;
        }
        var coreJsData = __webpack_require__(105), maskSrcKey = function() {
            var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || "");
            return uid ? "Symbol(src)_1." + uid : "";
        }();
        module.exports = isMasked;
    }, /* 105 */
    /***/
    function(module, exports, __webpack_require__) {
        var root = __webpack_require__(106), coreJsData = root["__core-js_shared__"];
        module.exports = coreJsData;
    }, /* 106 */
    /***/
    function(module, exports, __webpack_require__) {
        var freeGlobal = __webpack_require__(107), freeSelf = "object" == typeof self && self && self.Object === Object && self, root = freeGlobal || freeSelf || Function("return this")();
        module.exports = root;
    }, /* 107 */
    /***/
    function(module, exports) {
        /* WEBPACK VAR INJECTION */
        (function(global) {
            /** Detect free variable `global` from Node.js. */
            var freeGlobal = "object" == typeof global && global && global.Object === Object && global;
            module.exports = freeGlobal;
        }).call(exports, function() {
            return this;
        }());
    }, /* 108 */
    /***/
    function(module, exports) {
        /**
	 * Converts `func` to its source code.
	 *
	 * @private
	 * @param {Function} func The function to process.
	 * @returns {string} Returns the source code.
	 */
        function toSource(func) {
            if (null != func) {
                try {
                    return funcToString.call(func);
                } catch (e) {}
                try {
                    return func + "";
                } catch (e) {}
            }
            return "";
        }
        /** Used for built-in method references. */
        var funcProto = Function.prototype, funcToString = funcProto.toString;
        module.exports = toSource;
    }, /* 109 */
    /***/
    function(module, exports) {
        /**
	 * Gets the value at `key` of `object`.
	 *
	 * @private
	 * @param {Object} [object] The object to query.
	 * @param {string} key The key of the property to get.
	 * @returns {*} Returns the property value.
	 */
        function getValue(object, key) {
            return null == object ? void 0 : object[key];
        }
        module.exports = getValue;
    }, /* 110 */
    /***/
    function(module, exports) {
        /**
	 * Removes `key` and its value from the hash.
	 *
	 * @private
	 * @name delete
	 * @memberOf Hash
	 * @param {Object} hash The hash to modify.
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
        function hashDelete(key) {
            var result = this.has(key) && delete this.__data__[key];
            return this.size -= result ? 1 : 0, result;
        }
        module.exports = hashDelete;
    }, /* 111 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Gets the hash value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf Hash
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
        function hashGet(key) {
            var data = this.__data__;
            if (nativeCreate) {
                var result = data[key];
                return result === HASH_UNDEFINED ? void 0 : result;
            }
            return hasOwnProperty.call(data, key) ? data[key] : void 0;
        }
        var nativeCreate = __webpack_require__(100), HASH_UNDEFINED = "__lodash_hash_undefined__", objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty;
        module.exports = hashGet;
    }, /* 112 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if a hash value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf Hash
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
        function hashHas(key) {
            var data = this.__data__;
            return nativeCreate ? void 0 !== data[key] : hasOwnProperty.call(data, key);
        }
        var nativeCreate = __webpack_require__(100), objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty;
        module.exports = hashHas;
    }, /* 113 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Sets the hash `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf Hash
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the hash instance.
	 */
        function hashSet(key, value) {
            var data = this.__data__;
            return this.size += this.has(key) ? 0 : 1, data[key] = nativeCreate && void 0 === value ? HASH_UNDEFINED : value, 
            this;
        }
        var nativeCreate = __webpack_require__(100), HASH_UNDEFINED = "__lodash_hash_undefined__";
        module.exports = hashSet;
    }, /* 114 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates an list cache object.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
        function ListCache(entries) {
            var index = -1, length = entries ? entries.length : 0;
            for (this.clear(); ++index < length; ) {
                var entry = entries[index];
                this.set(entry[0], entry[1]);
            }
        }
        var listCacheClear = __webpack_require__(115), listCacheDelete = __webpack_require__(116), listCacheGet = __webpack_require__(119), listCacheHas = __webpack_require__(120), listCacheSet = __webpack_require__(121);
        // Add methods to `ListCache`.
        ListCache.prototype.clear = listCacheClear, ListCache.prototype.delete = listCacheDelete, 
        ListCache.prototype.get = listCacheGet, ListCache.prototype.has = listCacheHas, 
        ListCache.prototype.set = listCacheSet, module.exports = ListCache;
    }, /* 115 */
    /***/
    function(module, exports) {
        /**
	 * Removes all key-value entries from the list cache.
	 *
	 * @private
	 * @name clear
	 * @memberOf ListCache
	 */
        function listCacheClear() {
            this.__data__ = [], this.size = 0;
        }
        module.exports = listCacheClear;
    }, /* 116 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Removes `key` and its value from the list cache.
	 *
	 * @private
	 * @name delete
	 * @memberOf ListCache
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
        function listCacheDelete(key) {
            var data = this.__data__, index = assocIndexOf(data, key);
            if (index < 0) return !1;
            var lastIndex = data.length - 1;
            return index == lastIndex ? data.pop() : splice.call(data, index, 1), --this.size, 
            !0;
        }
        var assocIndexOf = __webpack_require__(117), arrayProto = Array.prototype, splice = arrayProto.splice;
        module.exports = listCacheDelete;
    }, /* 117 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Gets the index at which the `key` is found in `array` of key-value pairs.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} key The key to search for.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
        function assocIndexOf(array, key) {
            for (var length = array.length; length--; ) if (eq(array[length][0], key)) return length;
            return -1;
        }
        var eq = __webpack_require__(118);
        module.exports = assocIndexOf;
    }, /* 118 */
    /***/
    function(module, exports) {
        /**
	 * Performs a
	 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
	 * comparison between two values to determine if they are equivalent.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 * var other = { 'a': 1 };
	 *
	 * _.eq(object, object);
	 * // => true
	 *
	 * _.eq(object, other);
	 * // => false
	 *
	 * _.eq('a', 'a');
	 * // => true
	 *
	 * _.eq('a', Object('a'));
	 * // => false
	 *
	 * _.eq(NaN, NaN);
	 * // => true
	 */
        function eq(value, other) {
            return value === other || value !== value && other !== other;
        }
        module.exports = eq;
    }, /* 119 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Gets the list cache value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf ListCache
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
        function listCacheGet(key) {
            var data = this.__data__, index = assocIndexOf(data, key);
            return index < 0 ? void 0 : data[index][1];
        }
        var assocIndexOf = __webpack_require__(117);
        module.exports = listCacheGet;
    }, /* 120 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if a list cache value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf ListCache
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
        function listCacheHas(key) {
            return assocIndexOf(this.__data__, key) > -1;
        }
        var assocIndexOf = __webpack_require__(117);
        module.exports = listCacheHas;
    }, /* 121 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Sets the list cache `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf ListCache
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the list cache instance.
	 */
        function listCacheSet(key, value) {
            var data = this.__data__, index = assocIndexOf(data, key);
            return index < 0 ? (++this.size, data.push([ key, value ])) : data[index][1] = value, 
            this;
        }
        var assocIndexOf = __webpack_require__(117);
        module.exports = listCacheSet;
    }, /* 122 */
    /***/
    function(module, exports, __webpack_require__) {
        var getNative = __webpack_require__(101), root = __webpack_require__(106), Map = getNative(root, "Map");
        module.exports = Map;
    }, /* 123 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Removes `key` and its value from the map.
	 *
	 * @private
	 * @name delete
	 * @memberOf MapCache
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
        function mapCacheDelete(key) {
            var result = getMapData(this, key).delete(key);
            return this.size -= result ? 1 : 0, result;
        }
        var getMapData = __webpack_require__(124);
        module.exports = mapCacheDelete;
    }, /* 124 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Gets the data for `map`.
	 *
	 * @private
	 * @param {Object} map The map to query.
	 * @param {string} key The reference key.
	 * @returns {*} Returns the map data.
	 */
        function getMapData(map, key) {
            var data = map.__data__;
            return isKeyable(key) ? data["string" == typeof key ? "string" : "hash"] : data.map;
        }
        var isKeyable = __webpack_require__(125);
        module.exports = getMapData;
    }, /* 125 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is suitable for use as unique object key.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
	 */
        function isKeyable(value) {
            var type = typeof value;
            return "string" == type || "number" == type || "symbol" == type || "boolean" == type ? "__proto__" !== value : null === value;
        }
        module.exports = isKeyable;
    }, /* 126 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Gets the map value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf MapCache
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
        function mapCacheGet(key) {
            return getMapData(this, key).get(key);
        }
        var getMapData = __webpack_require__(124);
        module.exports = mapCacheGet;
    }, /* 127 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if a map value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf MapCache
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
        function mapCacheHas(key) {
            return getMapData(this, key).has(key);
        }
        var getMapData = __webpack_require__(124);
        module.exports = mapCacheHas;
    }, /* 128 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Sets the map `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf MapCache
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the map cache instance.
	 */
        function mapCacheSet(key, value) {
            var data = getMapData(this, key), size = data.size;
            return data.set(key, value), this.size += data.size == size ? 0 : 1, this;
        }
        var getMapData = __webpack_require__(124);
        module.exports = mapCacheSet;
    }, /* 129 */
    /***/
    function(module, exports) {
        /**
	 * Adds `value` to the array cache.
	 *
	 * @private
	 * @name add
	 * @memberOf SetCache
	 * @alias push
	 * @param {*} value The value to cache.
	 * @returns {Object} Returns the cache instance.
	 */
        function setCacheAdd(value) {
            return this.__data__.set(value, HASH_UNDEFINED), this;
        }
        /** Used to stand-in for `undefined` hash values. */
        var HASH_UNDEFINED = "__lodash_hash_undefined__";
        module.exports = setCacheAdd;
    }, /* 130 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is in the array cache.
	 *
	 * @private
	 * @name has
	 * @memberOf SetCache
	 * @param {*} value The value to search for.
	 * @returns {number} Returns `true` if `value` is found, else `false`.
	 */
        function setCacheHas(value) {
            return this.__data__.has(value);
        }
        module.exports = setCacheHas;
    }, /* 131 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * A specialized version of `_.includes` for arrays without support for
	 * specifying an index to search from.
	 *
	 * @private
	 * @param {Array} [array] The array to inspect.
	 * @param {*} target The value to search for.
	 * @returns {boolean} Returns `true` if `target` is found, else `false`.
	 */
        function arrayIncludes(array, value) {
            var length = array ? array.length : 0;
            return !!length && baseIndexOf(array, value, 0) > -1;
        }
        var baseIndexOf = __webpack_require__(132);
        module.exports = arrayIncludes;
    }, /* 132 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} value The value to search for.
	 * @param {number} fromIndex The index to search from.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
        function baseIndexOf(array, value, fromIndex) {
            return value === value ? strictIndexOf(array, value, fromIndex) : baseFindIndex(array, baseIsNaN, fromIndex);
        }
        var baseFindIndex = __webpack_require__(133), baseIsNaN = __webpack_require__(134), strictIndexOf = __webpack_require__(135);
        module.exports = baseIndexOf;
    }, /* 133 */
    /***/
    function(module, exports) {
        /**
	 * The base implementation of `_.findIndex` and `_.findLastIndex` without
	 * support for iteratee shorthands.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {Function} predicate The function invoked per iteration.
	 * @param {number} fromIndex The index to search from.
	 * @param {boolean} [fromRight] Specify iterating from right to left.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
        function baseFindIndex(array, predicate, fromIndex, fromRight) {
            for (var length = array.length, index = fromIndex + (fromRight ? 1 : -1); fromRight ? index-- : ++index < length; ) if (predicate(array[index], index, array)) return index;
            return -1;
        }
        module.exports = baseFindIndex;
    }, /* 134 */
    /***/
    function(module, exports) {
        /**
	 * The base implementation of `_.isNaN` without support for number objects.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
	 */
        function baseIsNaN(value) {
            return value !== value;
        }
        module.exports = baseIsNaN;
    }, /* 135 */
    /***/
    function(module, exports) {
        /**
	 * A specialized version of `_.indexOf` which performs strict equality
	 * comparisons of values, i.e. `===`.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} value The value to search for.
	 * @param {number} fromIndex The index to search from.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
        function strictIndexOf(array, value, fromIndex) {
            for (var index = fromIndex - 1, length = array.length; ++index < length; ) if (array[index] === value) return index;
            return -1;
        }
        module.exports = strictIndexOf;
    }, /* 136 */
    /***/
    function(module, exports) {
        /**
	 * This function is like `arrayIncludes` except that it accepts a comparator.
	 *
	 * @private
	 * @param {Array} [array] The array to inspect.
	 * @param {*} target The value to search for.
	 * @param {Function} comparator The comparator invoked per element.
	 * @returns {boolean} Returns `true` if `target` is found, else `false`.
	 */
        function arrayIncludesWith(array, value, comparator) {
            for (var index = -1, length = array ? array.length : 0; ++index < length; ) if (comparator(value, array[index])) return !0;
            return !1;
        }
        module.exports = arrayIncludesWith;
    }, /* 137 */
    /***/
    function(module, exports) {
        /**
	 * A specialized version of `_.map` for arrays without support for iteratee
	 * shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the new mapped array.
	 */
        function arrayMap(array, iteratee) {
            for (var index = -1, length = array ? array.length : 0, result = Array(length); ++index < length; ) result[index] = iteratee(array[index], index, array);
            return result;
        }
        module.exports = arrayMap;
    }, /* 138 */
    /***/
    function(module, exports) {
        /**
	 * The base implementation of `_.unary` without support for storing metadata.
	 *
	 * @private
	 * @param {Function} func The function to cap arguments for.
	 * @returns {Function} Returns the new capped function.
	 */
        function baseUnary(func) {
            return function(value) {
                return func(value);
            };
        }
        module.exports = baseUnary;
    }, /* 139 */
    /***/
    function(module, exports) {
        /**
	 * Checks if a `cache` value for `key` exists.
	 *
	 * @private
	 * @param {Object} cache The cache to query.
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
        function cacheHas(cache, key) {
            return cache.has(key);
        }
        module.exports = cacheHas;
    }, /* 140 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
	 *
	 * @private
	 * @param {Function} func The function to apply a rest parameter to.
	 * @param {number} [start=func.length-1] The start position of the rest parameter.
	 * @returns {Function} Returns the new function.
	 */
        function baseRest(func, start) {
            return setToString(overRest(func, start, identity), func + "");
        }
        var identity = __webpack_require__(141), overRest = __webpack_require__(142), setToString = __webpack_require__(144);
        module.exports = baseRest;
    }, /* 141 */
    /***/
    function(module, exports) {
        /**
	 * This method returns the first argument it receives.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Util
	 * @param {*} value Any value.
	 * @returns {*} Returns `value`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 *
	 * console.log(_.identity(object) === object);
	 * // => true
	 */
        function identity(value) {
            return value;
        }
        module.exports = identity;
    }, /* 142 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * A specialized version of `baseRest` which transforms the rest array.
	 *
	 * @private
	 * @param {Function} func The function to apply a rest parameter to.
	 * @param {number} [start=func.length-1] The start position of the rest parameter.
	 * @param {Function} transform The rest array transform.
	 * @returns {Function} Returns the new function.
	 */
        function overRest(func, start, transform) {
            return start = nativeMax(void 0 === start ? func.length - 1 : start, 0), function() {
                for (var args = arguments, index = -1, length = nativeMax(args.length - start, 0), array = Array(length); ++index < length; ) array[index] = args[start + index];
                index = -1;
                for (var otherArgs = Array(start + 1); ++index < start; ) otherArgs[index] = args[index];
                return otherArgs[start] = transform(array), apply(func, this, otherArgs);
            };
        }
        var apply = __webpack_require__(143), nativeMax = Math.max;
        module.exports = overRest;
    }, /* 143 */
    /***/
    function(module, exports) {
        /**
	 * A faster alternative to `Function#apply`, this function invokes `func`
	 * with the `this` binding of `thisArg` and the arguments of `args`.
	 *
	 * @private
	 * @param {Function} func The function to invoke.
	 * @param {*} thisArg The `this` binding of `func`.
	 * @param {Array} args The arguments to invoke `func` with.
	 * @returns {*} Returns the result of `func`.
	 */
        function apply(func, thisArg, args) {
            switch (args.length) {
              case 0:
                return func.call(thisArg);

              case 1:
                return func.call(thisArg, args[0]);

              case 2:
                return func.call(thisArg, args[0], args[1]);

              case 3:
                return func.call(thisArg, args[0], args[1], args[2]);
            }
            return func.apply(thisArg, args);
        }
        module.exports = apply;
    }, /* 144 */
    /***/
    function(module, exports, __webpack_require__) {
        var baseSetToString = __webpack_require__(145), shortOut = __webpack_require__(148), setToString = shortOut(baseSetToString);
        module.exports = setToString;
    }, /* 145 */
    /***/
    function(module, exports, __webpack_require__) {
        var constant = __webpack_require__(146), defineProperty = __webpack_require__(147), identity = __webpack_require__(141), baseSetToString = defineProperty ? function(func, string) {
            return defineProperty(func, "toString", {
                configurable: !0,
                enumerable: !1,
                value: constant(string),
                writable: !0
            });
        } : identity;
        module.exports = baseSetToString;
    }, /* 146 */
    /***/
    function(module, exports) {
        /**
	 * Creates a function that returns `value`.
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Util
	 * @param {*} value The value to return from the new function.
	 * @returns {Function} Returns the new constant function.
	 * @example
	 *
	 * var objects = _.times(2, _.constant({ 'a': 1 }));
	 *
	 * console.log(objects);
	 * // => [{ 'a': 1 }, { 'a': 1 }]
	 *
	 * console.log(objects[0] === objects[1]);
	 * // => true
	 */
        function constant(value) {
            return function() {
                return value;
            };
        }
        module.exports = constant;
    }, /* 147 */
    /***/
    function(module, exports, __webpack_require__) {
        var getNative = __webpack_require__(101), defineProperty = function() {
            try {
                var func = getNative(Object, "defineProperty");
                return func({}, "", {}), func;
            } catch (e) {}
        }();
        module.exports = defineProperty;
    }, /* 148 */
    /***/
    function(module, exports) {
        /**
	 * Creates a function that'll short out and invoke `identity` instead
	 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
	 * milliseconds.
	 *
	 * @private
	 * @param {Function} func The function to restrict.
	 * @returns {Function} Returns the new shortable function.
	 */
        function shortOut(func) {
            var count = 0, lastCalled = 0;
            return function() {
                var stamp = nativeNow(), remaining = HOT_SPAN - (stamp - lastCalled);
                if (lastCalled = stamp, remaining > 0) {
                    if (++count >= HOT_COUNT) return arguments[0];
                } else count = 0;
                return func.apply(void 0, arguments);
            };
        }
        /** Used to detect hot functions by number of calls within a span of milliseconds. */
        var HOT_COUNT = 500, HOT_SPAN = 16, nativeNow = Date.now;
        module.exports = shortOut;
    }, /* 149 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * This method is like `_.isArrayLike` except that it also checks if `value`
	 * is an object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an array-like object,
	 *  else `false`.
	 * @example
	 *
	 * _.isArrayLikeObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isArrayLikeObject(document.body.children);
	 * // => true
	 *
	 * _.isArrayLikeObject('abc');
	 * // => false
	 *
	 * _.isArrayLikeObject(_.noop);
	 * // => false
	 */
        function isArrayLikeObject(value) {
            return isObjectLike(value) && isArrayLike(value);
        }
        var isArrayLike = __webpack_require__(150), isObjectLike = __webpack_require__(16);
        module.exports = isArrayLikeObject;
    }, /* 150 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if `value` is array-like. A value is considered array-like if it's
	 * not a function and has a `value.length` that's an integer greater than or
	 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
	 * @example
	 *
	 * _.isArrayLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isArrayLike(document.body.children);
	 * // => true
	 *
	 * _.isArrayLike('abc');
	 * // => true
	 *
	 * _.isArrayLike(_.noop);
	 * // => false
	 */
        function isArrayLike(value) {
            return null != value && isLength(value.length) && !isFunction(value);
        }
        var isFunction = __webpack_require__(103), isLength = __webpack_require__(151);
        module.exports = isArrayLike;
    }, /* 151 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is a valid array-like length.
	 *
	 * **Note:** This method is loosely based on
	 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
	 * @example
	 *
	 * _.isLength(3);
	 * // => true
	 *
	 * _.isLength(Number.MIN_VALUE);
	 * // => false
	 *
	 * _.isLength(Infinity);
	 * // => false
	 *
	 * _.isLength('3');
	 * // => false
	 */
        function isLength(value) {
            return "number" == typeof value && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
        }
        /** Used as references for various `Number` constants. */
        var MAX_SAFE_INTEGER = 9007199254740991;
        module.exports = isLength;
    }, /* 152 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function refCount(state, action) {
            switch (void 0 === state && (state = 0), action.type) {
              case _actionsRegistry.ADD_SOURCE:
              case _actionsRegistry.ADD_TARGET:
                return state + 1;

              case _actionsRegistry.REMOVE_SOURCE:
              case _actionsRegistry.REMOVE_TARGET:
                return state - 1;

              default:
                return state;
            }
        }
        exports.__esModule = !0, exports.default = refCount;
        var _actionsRegistry = __webpack_require__(92);
        module.exports = exports.default;
    }, /* 153 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function dirtyHandlerIds(state, action, dragOperation) {
            switch (void 0 === state && (state = NONE), action.type) {
              case _actionsDragDrop.HOVER:
                break;

              case _actionsRegistry.ADD_SOURCE:
              case _actionsRegistry.ADD_TARGET:
              case _actionsRegistry.REMOVE_TARGET:
              case _actionsRegistry.REMOVE_SOURCE:
                return NONE;

              case _actionsDragDrop.BEGIN_DRAG:
              case _actionsDragDrop.PUBLISH_DRAG_SOURCE:
              case _actionsDragDrop.END_DRAG:
              case _actionsDragDrop.DROP:
              default:
                return ALL;
            }
            var targetIds = action.targetIds, prevTargetIds = dragOperation.targetIds, dirtyHandlerIds = _lodashXor2.default(targetIds, prevTargetIds), didChange = !1;
            if (0 === dirtyHandlerIds.length) {
                for (var i = 0; i < targetIds.length; i++) if (targetIds[i] !== prevTargetIds[i]) {
                    didChange = !0;
                    break;
                }
            } else didChange = !0;
            if (!didChange) return NONE;
            var prevInnermostTargetId = prevTargetIds[prevTargetIds.length - 1], innermostTargetId = targetIds[targetIds.length - 1];
            return prevInnermostTargetId !== innermostTargetId && (prevInnermostTargetId && dirtyHandlerIds.push(prevInnermostTargetId), 
            innermostTargetId && dirtyHandlerIds.push(innermostTargetId)), dirtyHandlerIds;
        }
        function areDirty(state, handlerIds) {
            return state !== NONE && (state === ALL || "undefined" == typeof handlerIds || _lodashIntersection2.default(handlerIds, state).length > 0);
        }
        exports.__esModule = !0, exports.default = dirtyHandlerIds, exports.areDirty = areDirty;
        var _lodashXor = __webpack_require__(154), _lodashXor2 = _interopRequireDefault(_lodashXor), _lodashIntersection = __webpack_require__(163), _lodashIntersection2 = _interopRequireDefault(_lodashIntersection), _actionsDragDrop = __webpack_require__(87), _actionsRegistry = __webpack_require__(92), NONE = [], ALL = [];
    }, /* 154 */
    /***/
    function(module, exports, __webpack_require__) {
        var arrayFilter = __webpack_require__(155), baseRest = __webpack_require__(140), baseXor = __webpack_require__(156), isArrayLikeObject = __webpack_require__(149), xor = baseRest(function(arrays) {
            return baseXor(arrayFilter(arrays, isArrayLikeObject));
        });
        module.exports = xor;
    }, /* 155 */
    /***/
    function(module, exports) {
        /**
	 * A specialized version of `_.filter` for arrays without support for
	 * iteratee shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} predicate The function invoked per iteration.
	 * @returns {Array} Returns the new filtered array.
	 */
        function arrayFilter(array, predicate) {
            for (var index = -1, length = array ? array.length : 0, resIndex = 0, result = []; ++index < length; ) {
                var value = array[index];
                predicate(value, index, array) && (result[resIndex++] = value);
            }
            return result;
        }
        module.exports = arrayFilter;
    }, /* 156 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of methods like `_.xor`, without support for
	 * iteratee shorthands, that accepts an array of arrays to inspect.
	 *
	 * @private
	 * @param {Array} arrays The arrays to inspect.
	 * @param {Function} [iteratee] The iteratee invoked per element.
	 * @param {Function} [comparator] The comparator invoked per element.
	 * @returns {Array} Returns the new array of values.
	 */
        function baseXor(arrays, iteratee, comparator) {
            for (var index = -1, length = arrays.length; ++index < length; ) var result = result ? arrayPush(baseDifference(result, arrays[index], iteratee, comparator), baseDifference(arrays[index], result, iteratee, comparator)) : arrays[index];
            return result && result.length ? baseUniq(result, iteratee, comparator) : [];
        }
        var arrayPush = __webpack_require__(157), baseDifference = __webpack_require__(94), baseUniq = __webpack_require__(158);
        module.exports = baseXor;
    }, /* 157 */
    /***/
    function(module, exports) {
        /**
	 * Appends the elements of `values` to `array`.
	 *
	 * @private
	 * @param {Array} array The array to modify.
	 * @param {Array} values The values to append.
	 * @returns {Array} Returns `array`.
	 */
        function arrayPush(array, values) {
            for (var index = -1, length = values.length, offset = array.length; ++index < length; ) array[offset + index] = values[index];
            return array;
        }
        module.exports = arrayPush;
    }, /* 158 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.uniqBy` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {Function} [iteratee] The iteratee invoked per element.
	 * @param {Function} [comparator] The comparator invoked per element.
	 * @returns {Array} Returns the new duplicate free array.
	 */
        function baseUniq(array, iteratee, comparator) {
            var index = -1, includes = arrayIncludes, length = array.length, isCommon = !0, result = [], seen = result;
            if (comparator) isCommon = !1, includes = arrayIncludesWith; else if (length >= LARGE_ARRAY_SIZE) {
                var set = iteratee ? null : createSet(array);
                if (set) return setToArray(set);
                isCommon = !1, includes = cacheHas, seen = new SetCache();
            } else seen = iteratee ? [] : result;
            outer: for (;++index < length; ) {
                var value = array[index], computed = iteratee ? iteratee(value) : value;
                if (value = comparator || 0 !== value ? value : 0, isCommon && computed === computed) {
                    for (var seenIndex = seen.length; seenIndex--; ) if (seen[seenIndex] === computed) continue outer;
                    iteratee && seen.push(computed), result.push(value);
                } else includes(seen, computed, comparator) || (seen !== result && seen.push(computed), 
                result.push(value));
            }
            return result;
        }
        var SetCache = __webpack_require__(95), arrayIncludes = __webpack_require__(131), arrayIncludesWith = __webpack_require__(136), cacheHas = __webpack_require__(139), createSet = __webpack_require__(159), setToArray = __webpack_require__(162), LARGE_ARRAY_SIZE = 200;
        module.exports = baseUniq;
    }, /* 159 */
    /***/
    function(module, exports, __webpack_require__) {
        var Set = __webpack_require__(160), noop = __webpack_require__(161), setToArray = __webpack_require__(162), INFINITY = 1 / 0, createSet = Set && 1 / setToArray(new Set([ , -0 ]))[1] == INFINITY ? function(values) {
            return new Set(values);
        } : noop;
        module.exports = createSet;
    }, /* 160 */
    /***/
    function(module, exports, __webpack_require__) {
        var getNative = __webpack_require__(101), root = __webpack_require__(106), Set = getNative(root, "Set");
        module.exports = Set;
    }, /* 161 */
    /***/
    function(module, exports) {
        /**
	 * This method returns `undefined`.
	 *
	 * @static
	 * @memberOf _
	 * @since 2.3.0
	 * @category Util
	 * @example
	 *
	 * _.times(2, _.noop);
	 * // => [undefined, undefined]
	 */
        function noop() {}
        module.exports = noop;
    }, /* 162 */
    /***/
    function(module, exports) {
        /**
	 * Converts `set` to an array of its values.
	 *
	 * @private
	 * @param {Object} set The set to convert.
	 * @returns {Array} Returns the values.
	 */
        function setToArray(set) {
            var index = -1, result = Array(set.size);
            return set.forEach(function(value) {
                result[++index] = value;
            }), result;
        }
        module.exports = setToArray;
    }, /* 163 */
    /***/
    function(module, exports, __webpack_require__) {
        var arrayMap = __webpack_require__(137), baseIntersection = __webpack_require__(164), baseRest = __webpack_require__(140), castArrayLikeObject = __webpack_require__(165), intersection = baseRest(function(arrays) {
            var mapped = arrayMap(arrays, castArrayLikeObject);
            return mapped.length && mapped[0] === arrays[0] ? baseIntersection(mapped) : [];
        });
        module.exports = intersection;
    }, /* 164 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of methods like `_.intersection`, without support
	 * for iteratee shorthands, that accepts an array of arrays to inspect.
	 *
	 * @private
	 * @param {Array} arrays The arrays to inspect.
	 * @param {Function} [iteratee] The iteratee invoked per element.
	 * @param {Function} [comparator] The comparator invoked per element.
	 * @returns {Array} Returns the new array of shared values.
	 */
        function baseIntersection(arrays, iteratee, comparator) {
            for (var includes = comparator ? arrayIncludesWith : arrayIncludes, length = arrays[0].length, othLength = arrays.length, othIndex = othLength, caches = Array(othLength), maxLength = 1 / 0, result = []; othIndex--; ) {
                var array = arrays[othIndex];
                othIndex && iteratee && (array = arrayMap(array, baseUnary(iteratee))), maxLength = nativeMin(array.length, maxLength), 
                caches[othIndex] = !comparator && (iteratee || length >= 120 && array.length >= 120) ? new SetCache(othIndex && array) : void 0;
            }
            array = arrays[0];
            var index = -1, seen = caches[0];
            outer: for (;++index < length && result.length < maxLength; ) {
                var value = array[index], computed = iteratee ? iteratee(value) : value;
                if (value = comparator || 0 !== value ? value : 0, !(seen ? cacheHas(seen, computed) : includes(result, computed, comparator))) {
                    for (othIndex = othLength; --othIndex; ) {
                        var cache = caches[othIndex];
                        if (!(cache ? cacheHas(cache, computed) : includes(arrays[othIndex], computed, comparator))) continue outer;
                    }
                    seen && seen.push(computed), result.push(value);
                }
            }
            return result;
        }
        var SetCache = __webpack_require__(95), arrayIncludes = __webpack_require__(131), arrayIncludesWith = __webpack_require__(136), arrayMap = __webpack_require__(137), baseUnary = __webpack_require__(138), cacheHas = __webpack_require__(139), nativeMin = Math.min;
        module.exports = baseIntersection;
    }, /* 165 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Casts `value` to an empty array if it's not an array like object.
	 *
	 * @private
	 * @param {*} value The value to inspect.
	 * @returns {Array|Object} Returns the cast array-like object.
	 */
        function castArrayLikeObject(value) {
            return isArrayLikeObject(value) ? value : [];
        }
        var isArrayLikeObject = __webpack_require__(149);
        module.exports = castArrayLikeObject;
    }, /* 166 */
    /***/
    function(module, exports) {
        "use strict";
        function stateId() {
            var state = arguments.length <= 0 || void 0 === arguments[0] ? 0 : arguments[0];
            return state + 1;
        }
        exports.__esModule = !0, exports.default = stateId, module.exports = exports.default;
    }, /* 167 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _utilsMatchesType = __webpack_require__(88), _utilsMatchesType2 = _interopRequireDefault(_utilsMatchesType), _lodashIsArray = __webpack_require__(89), _lodashIsArray2 = _interopRequireDefault(_lodashIsArray), _HandlerRegistry = __webpack_require__(168), _HandlerRegistry2 = _interopRequireDefault(_HandlerRegistry), _reducersDragOffset = __webpack_require__(86), _reducersDirtyHandlerIds = __webpack_require__(153), DragDropMonitor = function() {
            function DragDropMonitor(store) {
                _classCallCheck(this, DragDropMonitor), this.store = store, this.registry = new _HandlerRegistry2.default(store);
            }
            return DragDropMonitor.prototype.subscribeToStateChange = function(listener) {
                var _this = this, _ref = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1], handlerIds = _ref.handlerIds;
                _invariant2.default("function" == typeof listener, "listener must be a function."), 
                _invariant2.default("undefined" == typeof handlerIds || _lodashIsArray2.default(handlerIds), "handlerIds, when specified, must be an array of strings.");
                var prevStateId = this.store.getState().stateId, handleChange = function() {
                    var state = _this.store.getState(), currentStateId = state.stateId;
                    try {
                        var canSkipListener = currentStateId === prevStateId || currentStateId === prevStateId + 1 && !_reducersDirtyHandlerIds.areDirty(state.dirtyHandlerIds, handlerIds);
                        canSkipListener || listener();
                    } finally {
                        prevStateId = currentStateId;
                    }
                };
                return this.store.subscribe(handleChange);
            }, DragDropMonitor.prototype.subscribeToOffsetChange = function(listener) {
                var _this2 = this;
                _invariant2.default("function" == typeof listener, "listener must be a function.");
                var previousState = this.store.getState().dragOffset, handleChange = function() {
                    var nextState = _this2.store.getState().dragOffset;
                    nextState !== previousState && (previousState = nextState, listener());
                };
                return this.store.subscribe(handleChange);
            }, DragDropMonitor.prototype.canDragSource = function(sourceId) {
                var source = this.registry.getSource(sourceId);
                return _invariant2.default(source, "Expected to find a valid source."), !this.isDragging() && source.canDrag(this, sourceId);
            }, DragDropMonitor.prototype.canDropOnTarget = function(targetId) {
                var target = this.registry.getTarget(targetId);
                if (_invariant2.default(target, "Expected to find a valid target."), !this.isDragging() || this.didDrop()) return !1;
                var targetType = this.registry.getTargetType(targetId), draggedItemType = this.getItemType();
                return _utilsMatchesType2.default(targetType, draggedItemType) && target.canDrop(this, targetId);
            }, DragDropMonitor.prototype.isDragging = function() {
                return Boolean(this.getItemType());
            }, DragDropMonitor.prototype.isDraggingSource = function(sourceId) {
                var source = this.registry.getSource(sourceId, !0);
                if (_invariant2.default(source, "Expected to find a valid source."), !this.isDragging() || !this.isSourcePublic()) return !1;
                var sourceType = this.registry.getSourceType(sourceId), draggedItemType = this.getItemType();
                return sourceType === draggedItemType && source.isDragging(this, sourceId);
            }, DragDropMonitor.prototype.isOverTarget = function(targetId) {
                var _ref2 = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1], _ref2$shallow = _ref2.shallow, shallow = void 0 !== _ref2$shallow && _ref2$shallow;
                if (!this.isDragging()) return !1;
                var targetType = this.registry.getTargetType(targetId), draggedItemType = this.getItemType();
                if (!_utilsMatchesType2.default(targetType, draggedItemType)) return !1;
                var targetIds = this.getTargetIds();
                if (!targetIds.length) return !1;
                var index = targetIds.indexOf(targetId);
                return shallow ? index === targetIds.length - 1 : index > -1;
            }, DragDropMonitor.prototype.getItemType = function() {
                return this.store.getState().dragOperation.itemType;
            }, DragDropMonitor.prototype.getItem = function() {
                return this.store.getState().dragOperation.item;
            }, DragDropMonitor.prototype.getSourceId = function() {
                return this.store.getState().dragOperation.sourceId;
            }, DragDropMonitor.prototype.getTargetIds = function() {
                return this.store.getState().dragOperation.targetIds;
            }, DragDropMonitor.prototype.getDropResult = function() {
                return this.store.getState().dragOperation.dropResult;
            }, DragDropMonitor.prototype.didDrop = function() {
                return this.store.getState().dragOperation.didDrop;
            }, DragDropMonitor.prototype.isSourcePublic = function() {
                return this.store.getState().dragOperation.isSourcePublic;
            }, DragDropMonitor.prototype.getInitialClientOffset = function() {
                return this.store.getState().dragOffset.initialClientOffset;
            }, DragDropMonitor.prototype.getInitialSourceClientOffset = function() {
                return this.store.getState().dragOffset.initialSourceClientOffset;
            }, DragDropMonitor.prototype.getClientOffset = function() {
                return this.store.getState().dragOffset.clientOffset;
            }, DragDropMonitor.prototype.getSourceClientOffset = function() {
                return _reducersDragOffset.getSourceClientOffset(this.store.getState().dragOffset);
            }, DragDropMonitor.prototype.getDifferenceFromInitialOffset = function() {
                return _reducersDragOffset.getDifferenceFromInitialOffset(this.store.getState().dragOffset);
            }, DragDropMonitor;
        }();
        exports.default = DragDropMonitor, module.exports = exports.default;
    }, /* 168 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _typeof(obj) {
            return obj && obj.constructor === Symbol ? "symbol" : typeof obj;
        }
        function validateSourceContract(source) {
            _invariant2.default("function" == typeof source.canDrag, "Expected canDrag to be a function."), 
            _invariant2.default("function" == typeof source.beginDrag, "Expected beginDrag to be a function."), 
            _invariant2.default("function" == typeof source.endDrag, "Expected endDrag to be a function.");
        }
        function validateTargetContract(target) {
            _invariant2.default("function" == typeof target.canDrop, "Expected canDrop to be a function."), 
            _invariant2.default("function" == typeof target.hover, "Expected hover to be a function."), 
            _invariant2.default("function" == typeof target.drop, "Expected beginDrag to be a function.");
        }
        function validateType(type, allowArray) {
            return allowArray && _lodashIsArray2.default(type) ? void type.forEach(function(t) {
                return validateType(t, !1);
            }) : void _invariant2.default("string" == typeof type || "symbol" === ("undefined" == typeof type ? "undefined" : _typeof(type)), allowArray ? "Type can only be a string, a symbol, or an array of either." : "Type can only be a string or a symbol.");
        }
        function getNextHandlerId(role) {
            var id = _utilsGetNextUniqueId2.default().toString();
            switch (role) {
              case HandlerRoles.SOURCE:
                return "S" + id;

              case HandlerRoles.TARGET:
                return "T" + id;

              default:
                _invariant2.default(!1, "Unknown role: " + role);
            }
        }
        function parseRoleFromHandlerId(handlerId) {
            switch (handlerId[0]) {
              case "S":
                return HandlerRoles.SOURCE;

              case "T":
                return HandlerRoles.TARGET;

              default:
                _invariant2.default(!1, "Cannot parse handler ID: " + handlerId);
            }
        }
        exports.__esModule = !0;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _lodashIsArray = __webpack_require__(89), _lodashIsArray2 = _interopRequireDefault(_lodashIsArray), _utilsGetNextUniqueId = __webpack_require__(169), _utilsGetNextUniqueId2 = _interopRequireDefault(_utilsGetNextUniqueId), _actionsRegistry = __webpack_require__(92), _asap = __webpack_require__(170), _asap2 = _interopRequireDefault(_asap), HandlerRoles = {
            SOURCE: "SOURCE",
            TARGET: "TARGET"
        }, HandlerRegistry = function() {
            function HandlerRegistry(store) {
                _classCallCheck(this, HandlerRegistry), this.store = store, this.types = {}, this.handlers = {}, 
                this.pinnedSourceId = null, this.pinnedSource = null;
            }
            return HandlerRegistry.prototype.addSource = function(type, source) {
                validateType(type), validateSourceContract(source);
                var sourceId = this.addHandler(HandlerRoles.SOURCE, type, source);
                return this.store.dispatch(_actionsRegistry.addSource(sourceId)), sourceId;
            }, HandlerRegistry.prototype.addTarget = function(type, target) {
                validateType(type, !0), validateTargetContract(target);
                var targetId = this.addHandler(HandlerRoles.TARGET, type, target);
                return this.store.dispatch(_actionsRegistry.addTarget(targetId)), targetId;
            }, HandlerRegistry.prototype.addHandler = function(role, type, handler) {
                var id = getNextHandlerId(role);
                return this.types[id] = type, this.handlers[id] = handler, id;
            }, HandlerRegistry.prototype.containsHandler = function(handler) {
                var _this = this;
                return Object.keys(this.handlers).some(function(key) {
                    return _this.handlers[key] === handler;
                });
            }, HandlerRegistry.prototype.getSource = function(sourceId, includePinned) {
                _invariant2.default(this.isSourceId(sourceId), "Expected a valid source ID.");
                var isPinned = includePinned && sourceId === this.pinnedSourceId, source = isPinned ? this.pinnedSource : this.handlers[sourceId];
                return source;
            }, HandlerRegistry.prototype.getTarget = function(targetId) {
                return _invariant2.default(this.isTargetId(targetId), "Expected a valid target ID."), 
                this.handlers[targetId];
            }, HandlerRegistry.prototype.getSourceType = function(sourceId) {
                return _invariant2.default(this.isSourceId(sourceId), "Expected a valid source ID."), 
                this.types[sourceId];
            }, HandlerRegistry.prototype.getTargetType = function(targetId) {
                return _invariant2.default(this.isTargetId(targetId), "Expected a valid target ID."), 
                this.types[targetId];
            }, HandlerRegistry.prototype.isSourceId = function(handlerId) {
                var role = parseRoleFromHandlerId(handlerId);
                return role === HandlerRoles.SOURCE;
            }, HandlerRegistry.prototype.isTargetId = function(handlerId) {
                var role = parseRoleFromHandlerId(handlerId);
                return role === HandlerRoles.TARGET;
            }, HandlerRegistry.prototype.removeSource = function(sourceId) {
                var _this2 = this;
                _invariant2.default(this.getSource(sourceId), "Expected an existing source."), this.store.dispatch(_actionsRegistry.removeSource(sourceId)), 
                _asap2.default(function() {
                    delete _this2.handlers[sourceId], delete _this2.types[sourceId];
                });
            }, HandlerRegistry.prototype.removeTarget = function(targetId) {
                var _this3 = this;
                _invariant2.default(this.getTarget(targetId), "Expected an existing target."), this.store.dispatch(_actionsRegistry.removeTarget(targetId)), 
                _asap2.default(function() {
                    delete _this3.handlers[targetId], delete _this3.types[targetId];
                });
            }, HandlerRegistry.prototype.pinSource = function(sourceId) {
                var source = this.getSource(sourceId);
                _invariant2.default(source, "Expected an existing source."), this.pinnedSourceId = sourceId, 
                this.pinnedSource = source;
            }, HandlerRegistry.prototype.unpinSource = function() {
                _invariant2.default(this.pinnedSource, "No source is pinned at the time."), this.pinnedSourceId = null, 
                this.pinnedSource = null;
            }, HandlerRegistry;
        }();
        exports.default = HandlerRegistry, module.exports = exports.default;
    }, /* 169 */
    /***/
    function(module, exports) {
        "use strict";
        function getNextUniqueId() {
            return nextUniqueId++;
        }
        exports.__esModule = !0, exports.default = getNextUniqueId;
        var nextUniqueId = 0;
        module.exports = exports.default;
    }, /* 170 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function throwFirstError() {
            if (pendingErrors.length) throw pendingErrors.shift();
        }
        function asap(task) {
            var rawTask;
            rawTask = freeTasks.length ? freeTasks.pop() : new RawTask(), rawTask.task = task, 
            rawAsap(rawTask);
        }
        // We wrap tasks with recyclable task objects.  A task object implements
        // `call`, just like a function.
        function RawTask() {
            this.task = null;
        }
        // rawAsap provides everything we need except exception management.
        var rawAsap = __webpack_require__(171), freeTasks = [], pendingErrors = [], requestErrorThrow = rawAsap.makeRequestCallFromTimer(throwFirstError);
        /**
	 * Calls a task as soon as possible after returning, in its own event, with priority
	 * over other events like animation, reflow, and repaint. An error thrown from an
	 * event will not interrupt, nor even substantially slow down the processing of
	 * other events, but will be rather postponed to a lower priority event.
	 * @param {{call}} task A callable object, typically a function that takes no
	 * arguments.
	 */
        module.exports = asap, // The sole purpose of wrapping the task is to catch the exception and recycle
        // the task object after its single use.
        RawTask.prototype.call = function() {
            try {
                this.task.call();
            } catch (error) {
                asap.onerror ? // This hook exists purely for testing purposes.
                // Its name will be periodically randomized to break any code that
                // depends on its existence.
                asap.onerror(error) : (// In a web browser, exceptions are not fatal. However, to avoid
                // slowing down the queue of pending tasks, we rethrow the error in a
                // lower priority turn.
                pendingErrors.push(error), requestErrorThrow());
            } finally {
                this.task = null, freeTasks[freeTasks.length] = this;
            }
        };
    }, /* 171 */
    /***/
    function(module, exports) {
        /* WEBPACK VAR INJECTION */
        (function(global) {
            "use strict";
            function rawAsap(task) {
                queue.length || (requestFlush(), flushing = !0), // Equivalent to push, but avoids a function call.
                queue[queue.length] = task;
            }
            // The flush function processes all tasks that have been scheduled with
            // `rawAsap` unless and until one of those tasks throws an exception.
            // If a task throws an exception, `flush` ensures that its state will remain
            // consistent and will resume where it left off when called again.
            // However, `flush` does not make any arrangements to be called again if an
            // exception is thrown.
            function flush() {
                for (;index < queue.length; ) {
                    var currentIndex = index;
                    // Prevent leaking memory for long chains of recursive calls to `asap`.
                    // If we call `asap` within tasks scheduled by `asap`, the queue will
                    // grow, but to avoid an O(n) walk for every task we execute, we don't
                    // shift tasks off the queue after they have been executed.
                    // Instead, we periodically shift 1024 tasks off the queue.
                    if (// Advance the index before calling the task. This ensures that we will
                    // begin flushing on the next task the task throws an error.
                    index += 1, queue[currentIndex].call(), index > capacity) {
                        // Manually shift all values starting at the index back to the
                        // beginning of the queue.
                        for (var scan = 0, newLength = queue.length - index; scan < newLength; scan++) queue[scan] = queue[scan + index];
                        queue.length -= index, index = 0;
                    }
                }
                queue.length = 0, index = 0, flushing = !1;
            }
            // To request a high priority event, we induce a mutation observer by toggling
            // the text of a text node between "1" and "-1".
            function makeRequestCallFromMutationObserver(callback) {
                var toggle = 1, observer = new BrowserMutationObserver(callback), node = document.createTextNode("");
                return observer.observe(node, {
                    characterData: !0
                }), function() {
                    toggle = -toggle, node.data = toggle;
                };
            }
            // The message channel technique was discovered by Malte Ubl and was the
            // original foundation for this library.
            // http://www.nonblocking.io/2011/06/windownexttick.html
            // Safari 6.0.5 (at least) intermittently fails to create message ports on a
            // page's first load. Thankfully, this version of Safari supports
            // MutationObservers, so we don't need to fall back in that case.
            // function makeRequestCallFromMessageChannel(callback) {
            //     var channel = new MessageChannel();
            //     channel.port1.onmessage = callback;
            //     return function requestCall() {
            //         channel.port2.postMessage(0);
            //     };
            // }
            // For reasons explained above, we are also unable to use `setImmediate`
            // under any circumstances.
            // Even if we were, there is another bug in Internet Explorer 10.
            // It is not sufficient to assign `setImmediate` to `requestFlush` because
            // `setImmediate` must be called *by name* and therefore must be wrapped in a
            // closure.
            // Never forget.
            // function makeRequestCallFromSetImmediate(callback) {
            //     return function requestCall() {
            //         setImmediate(callback);
            //     };
            // }
            // Safari 6.0 has a problem where timers will get lost while the user is
            // scrolling. This problem does not impact ASAP because Safari 6.0 supports
            // mutation observers, so that implementation is used instead.
            // However, if we ever elect to use timers in Safari, the prevalent work-around
            // is to add a scroll event listener that calls for a flush.
            // `setTimeout` does not call the passed callback if the delay is less than
            // approximately 7 in web workers in Firefox 8 through 18, and sometimes not
            // even then.
            function makeRequestCallFromTimer(callback) {
                return function() {
                    function handleTimer() {
                        // Whichever timer succeeds will cancel both timers and
                        // execute the callback.
                        clearTimeout(timeoutHandle), clearInterval(intervalHandle), callback();
                    }
                    // We dispatch a timeout with a specified delay of 0 for engines that
                    // can reliably accommodate that request. This will usually be snapped
                    // to a 4 milisecond delay, but once we're flushing, there's no delay
                    // between events.
                    var timeoutHandle = setTimeout(handleTimer, 0), intervalHandle = setInterval(handleTimer, 50);
                };
            }
            // Use the fastest means possible to execute a task in its own turn, with
            // priority over other events including IO, animation, reflow, and redraw
            // events in browsers.
            //
            // An exception thrown by a task will permanently interrupt the processing of
            // subsequent tasks. The higher level `asap` function ensures that if an
            // exception is thrown by a task, that the task queue will continue flushing as
            // soon as possible, but if you use `rawAsap` directly, you are responsible to
            // either ensure that no exceptions are thrown from your task, or to manually
            // call `rawAsap.requestFlush` if an exception is thrown.
            module.exports = rawAsap;
            var requestFlush, queue = [], flushing = !1, index = 0, capacity = 1024, scope = "undefined" != typeof global ? global : self, BrowserMutationObserver = scope.MutationObserver || scope.WebKitMutationObserver;
            // MutationObservers are desirable because they have high priority and work
            // reliably everywhere they are implemented.
            // They are implemented in all modern browsers.
            //
            // - Android 4-4.3
            // - Chrome 26-34
            // - Firefox 14-29
            // - Internet Explorer 11
            // - iPad Safari 6-7.1
            // - iPhone Safari 7-7.1
            // - Safari 6-7
            requestFlush = "function" == typeof BrowserMutationObserver ? makeRequestCallFromMutationObserver(flush) : makeRequestCallFromTimer(flush), 
            // `requestFlush` requests that the high priority event queue be flushed as
            // soon as possible.
            // This is useful to prevent an error thrown in a task from stalling the event
            // queue if the exception handled by Node.js’s
            // `process.on("uncaughtException")` or by a domain.
            rawAsap.requestFlush = requestFlush, // This is for `asap.js` only.
            // Its name will be periodically randomized to break any code that depends on
            // its existence.
            rawAsap.makeRequestCallFromTimer = makeRequestCallFromTimer;
        }).call(exports, function() {
            return this;
        }());
    }, /* 172 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var DragSource = function() {
            function DragSource() {
                _classCallCheck(this, DragSource);
            }
            return DragSource.prototype.canDrag = function() {
                return !0;
            }, DragSource.prototype.isDragging = function(monitor, handle) {
                return handle === monitor.getSourceId();
            }, DragSource.prototype.endDrag = function() {}, DragSource;
        }();
        exports.default = DragSource, module.exports = exports.default;
    }, /* 173 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var DropTarget = function() {
            function DropTarget() {
                _classCallCheck(this, DropTarget);
            }
            return DropTarget.prototype.canDrop = function() {
                return !0;
            }, DropTarget.prototype.hover = function() {}, DropTarget.prototype.drop = function() {}, 
            DropTarget;
        }();
        exports.default = DropTarget, module.exports = exports.default;
    }, /* 174 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function createBackend(manager) {
            return new TestBackend(manager);
        }
        exports.__esModule = !0, exports.default = createBackend;
        var _lodashNoop = __webpack_require__(161), _lodashNoop2 = _interopRequireDefault(_lodashNoop), TestBackend = function() {
            function TestBackend(manager) {
                _classCallCheck(this, TestBackend), this.actions = manager.getActions();
            }
            return TestBackend.prototype.setup = function() {
                this.didCallSetup = !0;
            }, TestBackend.prototype.teardown = function() {
                this.didCallTeardown = !0;
            }, TestBackend.prototype.connectDragSource = function() {
                return _lodashNoop2.default;
            }, TestBackend.prototype.connectDragPreview = function() {
                return _lodashNoop2.default;
            }, TestBackend.prototype.connectDropTarget = function() {
                return _lodashNoop2.default;
            }, TestBackend.prototype.simulateBeginDrag = function(sourceIds, options) {
                this.actions.beginDrag(sourceIds, options);
            }, TestBackend.prototype.simulatePublishDragSource = function() {
                this.actions.publishDragSource();
            }, TestBackend.prototype.simulateHover = function(targetIds, options) {
                this.actions.hover(targetIds, options);
            }, TestBackend.prototype.simulateDrop = function() {
                this.actions.drop();
            }, TestBackend.prototype.simulateEndDrag = function() {
                this.actions.endDrag();
            }, TestBackend;
        }();
        module.exports = exports.default;
    }, /* 175 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function checkDecoratorArguments(functionName, signature) {
                if ("production" !== process.env.NODE_ENV) {
                    for (var _len = arguments.length, args = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) args[_key - 2] = arguments[_key];
                    for (var i = 0; i < args.length; i++) {
                        var arg = args[i];
                        if (arg && arg.prototype && arg.prototype.render) // eslint-disable-line no-console
                        return void console.error("You seem to be applying the arguments in the wrong order. " + ("It should be " + functionName + "(" + signature + ")(Component), not the other way around. ") + "Read more: http://gaearon.github.io/react-dnd/docs-troubleshooting.html#you-seem-to-be-applying-the-arguments-in-the-wrong-order");
                    }
                }
            }
            exports.__esModule = !0, exports.default = checkDecoratorArguments, module.exports = exports.default;
        }).call(exports, __webpack_require__(4));
    }, /* 176 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        function DragLayer(collect) {
            var options = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1];
            return _utilsCheckDecoratorArguments2.default.apply(void 0, [ "DragLayer", "collect[, options]" ].concat(_slice.call(arguments))), 
            _invariant2.default("function" == typeof collect, 'Expected "collect" provided as the first argument to DragLayer to be a function that collects props to inject into the component. ', "Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-layer.html", collect), 
            _invariant2.default(_lodashIsPlainObject2.default(options), 'Expected "options" provided as the second argument to DragLayer to be a plain object when specified. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-layer.html', options), 
            function(DecoratedComponent) {
                var _options$arePropsEqual = options.arePropsEqual, arePropsEqual = void 0 === _options$arePropsEqual ? _utilsShallowEqualScalar2.default : _options$arePropsEqual, displayName = DecoratedComponent.displayName || DecoratedComponent.name || "Component";
                return function(_Component) {
                    function DragLayerContainer(props, context) {
                        _classCallCheck(this, DragLayerContainer), _Component.call(this, props), this.handleChange = this.handleChange.bind(this), 
                        this.manager = context.dragDropManager, _invariant2.default("object" == typeof this.manager, "Could not find the drag and drop manager in the context of %s. Make sure to wrap the top-level component of your app with DragDropContext. Read more: http://gaearon.github.io/react-dnd/docs-troubleshooting.html#could-not-find-the-drag-and-drop-manager-in-the-context", displayName, displayName), 
                        this.state = this.getCurrentState();
                    }
                    return _inherits(DragLayerContainer, _Component), DragLayerContainer.prototype.getDecoratedComponentInstance = function() {
                        return this.refs.child;
                    }, DragLayerContainer.prototype.shouldComponentUpdate = function(nextProps, nextState) {
                        return !arePropsEqual(nextProps, this.props) || !_utilsShallowEqual2.default(nextState, this.state);
                    }, _createClass(DragLayerContainer, null, [ {
                        key: "DecoratedComponent",
                        value: DecoratedComponent,
                        enumerable: !0
                    }, {
                        key: "displayName",
                        value: "DragLayer(" + displayName + ")",
                        enumerable: !0
                    }, {
                        key: "contextTypes",
                        value: {
                            dragDropManager: _react.PropTypes.object.isRequired
                        },
                        enumerable: !0
                    } ]), DragLayerContainer.prototype.componentDidMount = function() {
                        this.isCurrentlyMounted = !0;
                        var monitor = this.manager.getMonitor();
                        this.unsubscribeFromOffsetChange = monitor.subscribeToOffsetChange(this.handleChange), 
                        this.unsubscribeFromStateChange = monitor.subscribeToStateChange(this.handleChange), 
                        this.handleChange();
                    }, DragLayerContainer.prototype.componentWillUnmount = function() {
                        this.isCurrentlyMounted = !1, this.unsubscribeFromOffsetChange(), this.unsubscribeFromStateChange();
                    }, DragLayerContainer.prototype.handleChange = function() {
                        if (this.isCurrentlyMounted) {
                            var nextState = this.getCurrentState();
                            _utilsShallowEqual2.default(nextState, this.state) || this.setState(nextState);
                        }
                    }, DragLayerContainer.prototype.getCurrentState = function() {
                        var monitor = this.manager.getMonitor();
                        return collect(monitor);
                    }, DragLayerContainer.prototype.render = function() {
                        return _react2.default.createElement(DecoratedComponent, _extends({}, this.props, this.state, {
                            ref: "child"
                        }));
                    }, DragLayerContainer;
                }(_react.Component);
            };
        }
        exports.__esModule = !0;
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _slice = Array.prototype.slice, _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }();
        exports.default = DragLayer;
        var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _utilsShallowEqual = __webpack_require__(177), _utilsShallowEqual2 = _interopRequireDefault(_utilsShallowEqual), _utilsShallowEqualScalar = __webpack_require__(178), _utilsShallowEqualScalar2 = _interopRequireDefault(_utilsShallowEqualScalar), _lodashIsPlainObject = __webpack_require__(13), _lodashIsPlainObject2 = _interopRequireDefault(_lodashIsPlainObject), _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _utilsCheckDecoratorArguments = __webpack_require__(175), _utilsCheckDecoratorArguments2 = _interopRequireDefault(_utilsCheckDecoratorArguments);
        module.exports = exports.default;
    }, /* 177 */
    /***/
    function(module, exports) {
        "use strict";
        function shallowEqual(objA, objB) {
            if (objA === objB) return !0;
            var keysA = Object.keys(objA), keysB = Object.keys(objB);
            if (keysA.length !== keysB.length) return !1;
            for (var hasOwn = Object.prototype.hasOwnProperty, i = 0; i < keysA.length; i++) {
                if (!hasOwn.call(objB, keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) return !1;
                var valA = objA[keysA[i]], valB = objB[keysA[i]];
                if (valA !== valB) return !1;
            }
            return !0;
        }
        exports.__esModule = !0, exports.default = shallowEqual, module.exports = exports.default;
    }, /* 178 */
    /***/
    function(module, exports) {
        "use strict";
        function shallowEqualScalar(objA, objB) {
            if (objA === objB) return !0;
            if ("object" != typeof objA || null === objA || "object" != typeof objB || null === objB) return !1;
            var keysA = Object.keys(objA), keysB = Object.keys(objB);
            if (keysA.length !== keysB.length) return !1;
            for (var hasOwn = Object.prototype.hasOwnProperty, i = 0; i < keysA.length; i++) {
                if (!hasOwn.call(objB, keysA[i])) return !1;
                var valA = objA[keysA[i]], valB = objB[keysA[i]];
                if (valA !== valB || "object" == typeof valA || "object" == typeof valB) return !1;
            }
            return !0;
        }
        exports.__esModule = !0, exports.default = shallowEqualScalar, module.exports = exports.default;
    }, /* 179 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function DragSource(type, spec, collect) {
            var options = arguments.length <= 3 || void 0 === arguments[3] ? {} : arguments[3];
            _utilsCheckDecoratorArguments2.default.apply(void 0, [ "DragSource", "type, spec, collect[, options]" ].concat(_slice.call(arguments)));
            var getType = type;
            "function" != typeof type && (_invariant2.default(_utilsIsValidType2.default(type), 'Expected "type" provided as the first argument to DragSource to be a string, or a function that returns a string given the current props. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html', type), 
            getType = function() {
                return type;
            }), _invariant2.default(_lodashIsPlainObject2.default(spec), 'Expected "spec" provided as the second argument to DragSource to be a plain object. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html', spec);
            var createSource = _createSourceFactory2.default(spec);
            return _invariant2.default("function" == typeof collect, 'Expected "collect" provided as the third argument to DragSource to be a function that returns a plain object of props to inject. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html', collect), 
            _invariant2.default(_lodashIsPlainObject2.default(options), 'Expected "options" provided as the fourth argument to DragSource to be a plain object when specified. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html', collect), 
            function(DecoratedComponent) {
                return _decorateHandler2.default({
                    connectBackend: function(backend, sourceId) {
                        return backend.connectDragSource(sourceId);
                    },
                    containerDisplayName: "DragSource",
                    createHandler: createSource,
                    registerHandler: _registerSource2.default,
                    createMonitor: _createSourceMonitor2.default,
                    createConnector: _createSourceConnector2.default,
                    DecoratedComponent: DecoratedComponent,
                    getType: getType,
                    collect: collect,
                    options: options
                });
            };
        }
        exports.__esModule = !0;
        var _slice = Array.prototype.slice;
        exports.default = DragSource;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _lodashIsPlainObject = __webpack_require__(13), _lodashIsPlainObject2 = _interopRequireDefault(_lodashIsPlainObject), _utilsCheckDecoratorArguments = __webpack_require__(175), _utilsCheckDecoratorArguments2 = _interopRequireDefault(_utilsCheckDecoratorArguments), _decorateHandler = __webpack_require__(180), _decorateHandler2 = _interopRequireDefault(_decorateHandler), _registerSource = __webpack_require__(186), _registerSource2 = _interopRequireDefault(_registerSource), _createSourceFactory = __webpack_require__(187), _createSourceFactory2 = _interopRequireDefault(_createSourceFactory), _createSourceMonitor = __webpack_require__(188), _createSourceMonitor2 = _interopRequireDefault(_createSourceMonitor), _createSourceConnector = __webpack_require__(189), _createSourceConnector2 = _interopRequireDefault(_createSourceConnector), _utilsIsValidType = __webpack_require__(193), _utilsIsValidType2 = _interopRequireDefault(_utilsIsValidType);
        module.exports = exports.default;
    }, /* 180 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function _inherits(subClass, superClass) {
                if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
                subClass.prototype = Object.create(superClass && superClass.prototype, {
                    constructor: {
                        value: subClass,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
            }
            function decorateHandler(_ref) {
                var DecoratedComponent = _ref.DecoratedComponent, createHandler = _ref.createHandler, createMonitor = _ref.createMonitor, createConnector = _ref.createConnector, registerHandler = _ref.registerHandler, containerDisplayName = _ref.containerDisplayName, getType = _ref.getType, collect = _ref.collect, options = _ref.options, _options$arePropsEqual = options.arePropsEqual, arePropsEqual = void 0 === _options$arePropsEqual ? _utilsShallowEqualScalar2.default : _options$arePropsEqual, displayName = DecoratedComponent.displayName || DecoratedComponent.name || "Component";
                return function(_Component) {
                    function DragDropContainer(props, context) {
                        _classCallCheck(this, DragDropContainer), _Component.call(this, props, context), 
                        this.handleChange = this.handleChange.bind(this), this.handleChildRef = this.handleChildRef.bind(this), 
                        _invariant2.default("object" == typeof this.context.dragDropManager, "Could not find the drag and drop manager in the context of %s. Make sure to wrap the top-level component of your app with DragDropContext. Read more: http://gaearon.github.io/react-dnd/docs-troubleshooting.html#could-not-find-the-drag-and-drop-manager-in-the-context", displayName, displayName), 
                        this.manager = this.context.dragDropManager, this.handlerMonitor = createMonitor(this.manager), 
                        this.handlerConnector = createConnector(this.manager.getBackend()), this.handler = createHandler(this.handlerMonitor), 
                        this.disposable = new _disposables.SerialDisposable(), this.receiveProps(props), 
                        this.state = this.getCurrentState(), this.dispose();
                    }
                    return _inherits(DragDropContainer, _Component), DragDropContainer.prototype.getHandlerId = function() {
                        return this.handlerId;
                    }, DragDropContainer.prototype.getDecoratedComponentInstance = function() {
                        return this.decoratedComponentInstance;
                    }, DragDropContainer.prototype.shouldComponentUpdate = function(nextProps, nextState) {
                        return !arePropsEqual(nextProps, this.props) || !_utilsShallowEqual2.default(nextState, this.state);
                    }, _createClass(DragDropContainer, null, [ {
                        key: "DecoratedComponent",
                        value: DecoratedComponent,
                        enumerable: !0
                    }, {
                        key: "displayName",
                        value: containerDisplayName + "(" + displayName + ")",
                        enumerable: !0
                    }, {
                        key: "contextTypes",
                        value: {
                            dragDropManager: _react.PropTypes.object.isRequired
                        },
                        enumerable: !0
                    } ]), DragDropContainer.prototype.componentDidMount = function() {
                        this.isCurrentlyMounted = !0, this.disposable = new _disposables.SerialDisposable(), 
                        this.currentType = null, this.receiveProps(this.props), this.handleChange();
                    }, DragDropContainer.prototype.componentWillReceiveProps = function(nextProps) {
                        arePropsEqual(nextProps, this.props) || (this.receiveProps(nextProps), this.handleChange());
                    }, DragDropContainer.prototype.componentWillUnmount = function() {
                        this.dispose(), this.isCurrentlyMounted = !1;
                    }, DragDropContainer.prototype.receiveProps = function(props) {
                        this.handler.receiveProps(props), this.receiveType(getType(props));
                    }, DragDropContainer.prototype.receiveType = function(type) {
                        if (type !== this.currentType) {
                            this.currentType = type;
                            var _registerHandler = registerHandler(type, this.handler, this.manager), handlerId = _registerHandler.handlerId, unregister = _registerHandler.unregister;
                            this.handlerId = handlerId, this.handlerMonitor.receiveHandlerId(handlerId), this.handlerConnector.receiveHandlerId(handlerId);
                            var globalMonitor = this.manager.getMonitor(), unsubscribe = globalMonitor.subscribeToStateChange(this.handleChange, {
                                handlerIds: [ handlerId ]
                            });
                            this.disposable.setDisposable(new _disposables.CompositeDisposable(new _disposables.Disposable(unsubscribe), new _disposables.Disposable(unregister)));
                        }
                    }, DragDropContainer.prototype.handleChange = function() {
                        if (this.isCurrentlyMounted) {
                            var nextState = this.getCurrentState();
                            _utilsShallowEqual2.default(nextState, this.state) || this.setState(nextState);
                        }
                    }, DragDropContainer.prototype.dispose = function() {
                        this.disposable.dispose(), this.handlerConnector.receiveHandlerId(null);
                    }, DragDropContainer.prototype.handleChildRef = function(component) {
                        this.decoratedComponentInstance = component, this.handler.receiveComponent(component);
                    }, DragDropContainer.prototype.getCurrentState = function() {
                        var nextState = collect(this.handlerConnector.hooks, this.handlerMonitor);
                        return "production" !== process.env.NODE_ENV && _invariant2.default(_lodashIsPlainObject2.default(nextState), "Expected `collect` specified as the second argument to %s for %s to return a plain object of props to inject. Instead, received %s.", containerDisplayName, displayName, nextState), 
                        nextState;
                    }, DragDropContainer.prototype.render = function() {
                        return _react2.default.createElement(DecoratedComponent, _extends({}, this.props, this.state, {
                            ref: this.handleChildRef
                        }));
                    }, DragDropContainer;
                }(_react.Component);
            }
            exports.__esModule = !0;
            var _extends = Object.assign || function(target) {
                for (var i = 1; i < arguments.length; i++) {
                    var source = arguments[i];
                    for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
                }
                return target;
            }, _createClass = function() {
                function defineProperties(target, props) {
                    for (var i = 0; i < props.length; i++) {
                        var descriptor = props[i];
                        descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                        "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                    }
                }
                return function(Constructor, protoProps, staticProps) {
                    return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                    Constructor;
                };
            }();
            exports.default = decorateHandler;
            var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _disposables = __webpack_require__(181), _utilsShallowEqual = __webpack_require__(177), _utilsShallowEqual2 = _interopRequireDefault(_utilsShallowEqual), _utilsShallowEqualScalar = __webpack_require__(178), _utilsShallowEqualScalar2 = _interopRequireDefault(_utilsShallowEqualScalar), _lodashIsPlainObject = __webpack_require__(13), _lodashIsPlainObject2 = _interopRequireDefault(_lodashIsPlainObject), _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant);
            module.exports = exports.default;
        }).call(exports, __webpack_require__(4));
    }, /* 181 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        var _interopRequireWildcard = function(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        };
        exports.__esModule = !0;
        var _isDisposable2 = __webpack_require__(182), _isDisposable3 = _interopRequireWildcard(_isDisposable2);
        exports.isDisposable = _isDisposable3.default;
        var _Disposable2 = __webpack_require__(183), _Disposable3 = _interopRequireWildcard(_Disposable2);
        exports.Disposable = _Disposable3.default;
        var _CompositeDisposable2 = __webpack_require__(184), _CompositeDisposable3 = _interopRequireWildcard(_CompositeDisposable2);
        exports.CompositeDisposable = _CompositeDisposable3.default;
        var _SerialDisposable2 = __webpack_require__(185), _SerialDisposable3 = _interopRequireWildcard(_SerialDisposable2);
        exports.SerialDisposable = _SerialDisposable3.default;
    }, /* 182 */
    /***/
    function(module, exports) {
        "use strict";
        function isDisposable(obj) {
            return Boolean(obj && "function" == typeof obj.dispose);
        }
        exports.__esModule = !0, exports.default = isDisposable, module.exports = exports.default;
    }, /* 183 */
    /***/
    function(module, exports) {
        "use strict";
        var _classCallCheck = function(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }, _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }();
        exports.__esModule = !0;
        var noop = function() {}, Disposable = function() {
            function Disposable(action) {
                _classCallCheck(this, Disposable), this.isDisposed = !1, this.action = action || noop;
            }
            return Disposable.prototype.dispose = function() {
                this.isDisposed || (this.action.call(null), this.isDisposed = !0);
            }, _createClass(Disposable, null, [ {
                key: "empty",
                enumerable: !0,
                value: {
                    dispose: noop
                }
            } ]), Disposable;
        }();
        exports.default = Disposable, module.exports = exports.default;
    }, /* 184 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        var _interopRequireWildcard = function(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }, _classCallCheck = function(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        };
        exports.__esModule = !0;
        var _isDisposable = __webpack_require__(182), _isDisposable2 = _interopRequireWildcard(_isDisposable), CompositeDisposable = function() {
            function CompositeDisposable() {
                for (var _len = arguments.length, disposables = Array(_len), _key = 0; _key < _len; _key++) disposables[_key] = arguments[_key];
                _classCallCheck(this, CompositeDisposable), Array.isArray(disposables[0]) && 1 === disposables.length && (disposables = disposables[0]);
                for (var i = 0; i < disposables.length; i++) if (!_isDisposable2.default(disposables[i])) throw new Error("Expected a disposable");
                this.disposables = disposables, this.isDisposed = !1;
            }
            /**
	   * Adds a disposable to the CompositeDisposable or disposes the disposable if the CompositeDisposable is disposed.
	   * @param {Disposable} item Disposable to add.
	   */
            /**
	   * Removes and disposes the first occurrence of a disposable from the CompositeDisposable.
	   * @param {Disposable} item Disposable to remove.
	   * @returns {Boolean} true if found; false otherwise.
	   */
            /**
	   * Disposes all disposables in the group and removes them from the group.
	   */
            return CompositeDisposable.prototype.add = function(item) {
                this.isDisposed ? item.dispose() : this.disposables.push(item);
            }, CompositeDisposable.prototype.remove = function(item) {
                if (this.isDisposed) return !1;
                var index = this.disposables.indexOf(item);
                return index !== -1 && (this.disposables.splice(index, 1), item.dispose(), !0);
            }, CompositeDisposable.prototype.dispose = function() {
                if (!this.isDisposed) {
                    for (var len = this.disposables.length, currentDisposables = new Array(len), i = 0; i < len; i++) currentDisposables[i] = this.disposables[i];
                    this.isDisposed = !0, this.disposables = [], this.length = 0;
                    for (var i = 0; i < len; i++) currentDisposables[i].dispose();
                }
            }, CompositeDisposable;
        }();
        exports.default = CompositeDisposable, module.exports = exports.default;
    }, /* 185 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        var _interopRequireWildcard = function(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }, _classCallCheck = function(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        };
        exports.__esModule = !0;
        var _isDisposable = __webpack_require__(182), _isDisposable2 = _interopRequireWildcard(_isDisposable), SerialDisposable = function() {
            function SerialDisposable() {
                _classCallCheck(this, SerialDisposable), this.isDisposed = !1, this.current = null;
            }
            /**
	   * Gets the underlying disposable.
	   * @return The underlying disposable.
	   */
            /**
	   * Sets the underlying disposable.
	   * @param {Disposable} value The new underlying disposable.
	   */
            /**
	   * Disposes the underlying disposable as well as all future replacements.
	   */
            return SerialDisposable.prototype.getDisposable = function() {
                return this.current;
            }, SerialDisposable.prototype.setDisposable = function() {
                var value = void 0 === arguments[0] ? null : arguments[0];
                if (null != value && !_isDisposable2.default(value)) throw new Error("Expected either an empty value or a valid disposable");
                var isDisposed = this.isDisposed, previous = void 0;
                isDisposed || (previous = this.current, this.current = value), previous && previous.dispose(), 
                isDisposed && value && value.dispose();
            }, SerialDisposable.prototype.dispose = function() {
                if (!this.isDisposed) {
                    this.isDisposed = !0;
                    var previous = this.current;
                    this.current = null, previous && previous.dispose();
                }
            }, SerialDisposable;
        }();
        exports.default = SerialDisposable, module.exports = exports.default;
    }, /* 186 */
    /***/
    function(module, exports) {
        "use strict";
        function registerSource(type, source, manager) {
            function unregisterSource() {
                registry.removeSource(sourceId);
            }
            var registry = manager.getRegistry(), sourceId = registry.addSource(type, source);
            return {
                handlerId: sourceId,
                unregister: unregisterSource
            };
        }
        exports.__esModule = !0, exports.default = registerSource, module.exports = exports.default;
    }, /* 187 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function createSourceFactory(spec) {
                Object.keys(spec).forEach(function(key) {
                    _invariant2.default(ALLOWED_SPEC_METHODS.indexOf(key) > -1, 'Expected the drag source specification to only have some of the following keys: %s. Instead received a specification with an unexpected "%s" key. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html', ALLOWED_SPEC_METHODS.join(", "), key), 
                    _invariant2.default("function" == typeof spec[key], "Expected %s in the drag source specification to be a function. Instead received a specification with %s: %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html", key, key, spec[key]);
                }), REQUIRED_SPEC_METHODS.forEach(function(key) {
                    _invariant2.default("function" == typeof spec[key], "Expected %s in the drag source specification to be a function. Instead received a specification with %s: %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html", key, key, spec[key]);
                });
                var Source = function() {
                    function Source(monitor) {
                        _classCallCheck(this, Source), this.monitor = monitor, this.props = null, this.component = null;
                    }
                    return Source.prototype.receiveProps = function(props) {
                        this.props = props;
                    }, Source.prototype.receiveComponent = function(component) {
                        this.component = component;
                    }, Source.prototype.canDrag = function() {
                        return !spec.canDrag || spec.canDrag(this.props, this.monitor);
                    }, Source.prototype.isDragging = function(globalMonitor, sourceId) {
                        return spec.isDragging ? spec.isDragging(this.props, this.monitor) : sourceId === globalMonitor.getSourceId();
                    }, Source.prototype.beginDrag = function() {
                        var item = spec.beginDrag(this.props, this.monitor, this.component);
                        return "production" !== process.env.NODE_ENV && _invariant2.default(_lodashIsPlainObject2.default(item), "beginDrag() must return a plain object that represents the dragged item. Instead received %s. Read more: http://gaearon.github.io/react-dnd/docs-drag-source.html", item), 
                        item;
                    }, Source.prototype.endDrag = function() {
                        spec.endDrag && spec.endDrag(this.props, this.monitor, this.component);
                    }, Source;
                }();
                return function(monitor) {
                    return new Source(monitor);
                };
            }
            exports.__esModule = !0, exports.default = createSourceFactory;
            var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _lodashIsPlainObject = __webpack_require__(13), _lodashIsPlainObject2 = _interopRequireDefault(_lodashIsPlainObject), ALLOWED_SPEC_METHODS = [ "canDrag", "beginDrag", "canDrag", "isDragging", "endDrag" ], REQUIRED_SPEC_METHODS = [ "beginDrag" ];
            module.exports = exports.default;
        }).call(exports, __webpack_require__(4));
    }, /* 188 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function createSourceMonitor(manager) {
            return new SourceMonitor(manager);
        }
        exports.__esModule = !0, exports.default = createSourceMonitor;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), isCallingCanDrag = !1, isCallingIsDragging = !1, SourceMonitor = function() {
            function SourceMonitor(manager) {
                _classCallCheck(this, SourceMonitor), this.internalMonitor = manager.getMonitor();
            }
            return SourceMonitor.prototype.receiveHandlerId = function(sourceId) {
                this.sourceId = sourceId;
            }, SourceMonitor.prototype.canDrag = function() {
                _invariant2.default(!isCallingCanDrag, "You may not call monitor.canDrag() inside your canDrag() implementation. Read more: http://gaearon.github.io/react-dnd/docs-drag-source-monitor.html");
                try {
                    return isCallingCanDrag = !0, this.internalMonitor.canDragSource(this.sourceId);
                } finally {
                    isCallingCanDrag = !1;
                }
            }, SourceMonitor.prototype.isDragging = function() {
                _invariant2.default(!isCallingIsDragging, "You may not call monitor.isDragging() inside your isDragging() implementation. Read more: http://gaearon.github.io/react-dnd/docs-drag-source-monitor.html");
                try {
                    return isCallingIsDragging = !0, this.internalMonitor.isDraggingSource(this.sourceId);
                } finally {
                    isCallingIsDragging = !1;
                }
            }, SourceMonitor.prototype.getItemType = function() {
                return this.internalMonitor.getItemType();
            }, SourceMonitor.prototype.getItem = function() {
                return this.internalMonitor.getItem();
            }, SourceMonitor.prototype.getDropResult = function() {
                return this.internalMonitor.getDropResult();
            }, SourceMonitor.prototype.didDrop = function() {
                return this.internalMonitor.didDrop();
            }, SourceMonitor.prototype.getInitialClientOffset = function() {
                return this.internalMonitor.getInitialClientOffset();
            }, SourceMonitor.prototype.getInitialSourceClientOffset = function() {
                return this.internalMonitor.getInitialSourceClientOffset();
            }, SourceMonitor.prototype.getSourceClientOffset = function() {
                return this.internalMonitor.getSourceClientOffset();
            }, SourceMonitor.prototype.getClientOffset = function() {
                return this.internalMonitor.getClientOffset();
            }, SourceMonitor.prototype.getDifferenceFromInitialOffset = function() {
                return this.internalMonitor.getDifferenceFromInitialOffset();
            }, SourceMonitor;
        }();
        module.exports = exports.default;
    }, /* 189 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function createSourceConnector(backend) {
            function reconnectDragSource() {
                disconnectCurrentDragSource && (disconnectCurrentDragSource(), disconnectCurrentDragSource = null), 
                currentHandlerId && currentDragSourceNode && (disconnectCurrentDragSource = backend.connectDragSource(currentHandlerId, currentDragSourceNode, currentDragSourceOptions));
            }
            function reconnectDragPreview() {
                disconnectCurrentDragPreview && (disconnectCurrentDragPreview(), disconnectCurrentDragPreview = null), 
                currentHandlerId && currentDragPreviewNode && (disconnectCurrentDragPreview = backend.connectDragPreview(currentHandlerId, currentDragPreviewNode, currentDragPreviewOptions));
            }
            function receiveHandlerId(handlerId) {
                handlerId !== currentHandlerId && (currentHandlerId = handlerId, reconnectDragSource(), 
                reconnectDragPreview());
            }
            var currentHandlerId = void 0, currentDragSourceNode = void 0, currentDragSourceOptions = void 0, disconnectCurrentDragSource = void 0, currentDragPreviewNode = void 0, currentDragPreviewOptions = void 0, disconnectCurrentDragPreview = void 0, hooks = _wrapConnectorHooks2.default({
                dragSource: function(node, options) {
                    node === currentDragSourceNode && _areOptionsEqual2.default(options, currentDragSourceOptions) || (currentDragSourceNode = node, 
                    currentDragSourceOptions = options, reconnectDragSource());
                },
                dragPreview: function(node, options) {
                    node === currentDragPreviewNode && _areOptionsEqual2.default(options, currentDragPreviewOptions) || (currentDragPreviewNode = node, 
                    currentDragPreviewOptions = options, reconnectDragPreview());
                }
            });
            return {
                receiveHandlerId: receiveHandlerId,
                hooks: hooks
            };
        }
        exports.__esModule = !0, exports.default = createSourceConnector;
        var _wrapConnectorHooks = __webpack_require__(190), _wrapConnectorHooks2 = _interopRequireDefault(_wrapConnectorHooks), _areOptionsEqual = __webpack_require__(192), _areOptionsEqual2 = _interopRequireDefault(_areOptionsEqual);
        module.exports = exports.default;
    }, /* 190 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function throwIfCompositeComponentElement(element) {
            // Custom components can no longer be wrapped directly in React DnD 2.0
            // so that we don't need to depend on findDOMNode() from react-dom.
            if ("string" != typeof element.type) {
                var displayName = element.type.displayName || element.type.name || "the component";
                throw new Error("Only native element nodes can now be passed to React DnD connectors. " + ("You can either wrap " + displayName + " into a <div>, or turn it into a ") + "drag source or a drop target itself.");
            }
        }
        function wrapHookToRecognizeElement(hook) {
            return function() {
                var elementOrNode = arguments.length <= 0 || void 0 === arguments[0] ? null : arguments[0], options = arguments.length <= 1 || void 0 === arguments[1] ? null : arguments[1];
                // When passed a node, call the hook straight away.
                if (!_react.isValidElement(elementOrNode)) {
                    var node = elementOrNode;
                    return void hook(node, options);
                }
                // If passed a ReactElement, clone it and attach this function as a ref.
                // This helps us achieve a neat API where user doesn't even know that refs
                // are being used under the hood.
                var element = elementOrNode;
                throwIfCompositeComponentElement(element);
                // When no options are passed, use the hook directly
                var ref = options ? function(node) {
                    return hook(node, options);
                } : hook;
                return _utilsCloneWithRef2.default(element, ref);
            };
        }
        function wrapConnectorHooks(hooks) {
            var wrappedHooks = {};
            return Object.keys(hooks).forEach(function(key) {
                var hook = hooks[key], wrappedHook = wrapHookToRecognizeElement(hook);
                wrappedHooks[key] = function() {
                    return wrappedHook;
                };
            }), wrappedHooks;
        }
        exports.__esModule = !0, exports.default = wrapConnectorHooks;
        var _utilsCloneWithRef = __webpack_require__(191), _utilsCloneWithRef2 = _interopRequireDefault(_utilsCloneWithRef), _react = __webpack_require__(5);
        module.exports = exports.default;
    }, /* 191 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function cloneWithRef(element, newRef) {
            var previousRef = element.ref;
            return _invariant2.default("string" != typeof previousRef, "Cannot connect React DnD to an element with an existing string ref. Please convert it to use a callback ref instead, or wrap it into a <span> or <div>. Read more: https://facebook.github.io/react/docs/more-about-refs.html#the-ref-callback-attribute"), 
            previousRef ? _react.cloneElement(element, {
                ref: function(node) {
                    newRef(node), previousRef && previousRef(node);
                }
            }) : _react.cloneElement(element, {
                ref: newRef
            });
        }
        exports.__esModule = !0, exports.default = cloneWithRef;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _react = __webpack_require__(5);
        module.exports = exports.default;
    }, /* 192 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function areOptionsEqual(nextOptions, currentOptions) {
            return currentOptions === nextOptions || null !== currentOptions && null !== nextOptions && _utilsShallowEqual2.default(currentOptions, nextOptions);
        }
        exports.__esModule = !0, exports.default = areOptionsEqual;
        var _utilsShallowEqual = __webpack_require__(177), _utilsShallowEqual2 = _interopRequireDefault(_utilsShallowEqual);
        module.exports = exports.default;
    }, /* 193 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function isValidType(type, allowArray) {
            return "string" == typeof type || "symbol" == typeof type || allowArray && _lodashIsArray2.default(type) && type.every(function(t) {
                return isValidType(t, !1);
            });
        }
        exports.__esModule = !0, exports.default = isValidType;
        var _lodashIsArray = __webpack_require__(89), _lodashIsArray2 = _interopRequireDefault(_lodashIsArray);
        module.exports = exports.default;
    }, /* 194 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function DropTarget(type, spec, collect) {
            var options = arguments.length <= 3 || void 0 === arguments[3] ? {} : arguments[3];
            _utilsCheckDecoratorArguments2.default.apply(void 0, [ "DropTarget", "type, spec, collect[, options]" ].concat(_slice.call(arguments)));
            var getType = type;
            "function" != typeof type && (_invariant2.default(_utilsIsValidType2.default(type, !0), 'Expected "type" provided as the first argument to DropTarget to be a string, an array of strings, or a function that returns either given the current props. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html', type), 
            getType = function() {
                return type;
            }), _invariant2.default(_lodashIsPlainObject2.default(spec), 'Expected "spec" provided as the second argument to DropTarget to be a plain object. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html', spec);
            var createTarget = _createTargetFactory2.default(spec);
            return _invariant2.default("function" == typeof collect, 'Expected "collect" provided as the third argument to DropTarget to be a function that returns a plain object of props to inject. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html', collect), 
            _invariant2.default(_lodashIsPlainObject2.default(options), 'Expected "options" provided as the fourth argument to DropTarget to be a plain object when specified. Instead, received %s. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html', collect), 
            function(DecoratedComponent) {
                return _decorateHandler2.default({
                    connectBackend: function(backend, targetId) {
                        return backend.connectDropTarget(targetId);
                    },
                    containerDisplayName: "DropTarget",
                    createHandler: createTarget,
                    registerHandler: _registerTarget2.default,
                    createMonitor: _createTargetMonitor2.default,
                    createConnector: _createTargetConnector2.default,
                    DecoratedComponent: DecoratedComponent,
                    getType: getType,
                    collect: collect,
                    options: options
                });
            };
        }
        exports.__esModule = !0;
        var _slice = Array.prototype.slice;
        exports.default = DropTarget;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _lodashIsPlainObject = __webpack_require__(13), _lodashIsPlainObject2 = _interopRequireDefault(_lodashIsPlainObject), _utilsCheckDecoratorArguments = __webpack_require__(175), _utilsCheckDecoratorArguments2 = _interopRequireDefault(_utilsCheckDecoratorArguments), _decorateHandler = __webpack_require__(180), _decorateHandler2 = _interopRequireDefault(_decorateHandler), _registerTarget = __webpack_require__(195), _registerTarget2 = _interopRequireDefault(_registerTarget), _createTargetFactory = __webpack_require__(196), _createTargetFactory2 = _interopRequireDefault(_createTargetFactory), _createTargetMonitor = __webpack_require__(197), _createTargetMonitor2 = _interopRequireDefault(_createTargetMonitor), _createTargetConnector = __webpack_require__(198), _createTargetConnector2 = _interopRequireDefault(_createTargetConnector), _utilsIsValidType = __webpack_require__(193), _utilsIsValidType2 = _interopRequireDefault(_utilsIsValidType);
        module.exports = exports.default;
    }, /* 195 */
    /***/
    function(module, exports) {
        "use strict";
        function registerTarget(type, target, manager) {
            function unregisterTarget() {
                registry.removeTarget(targetId);
            }
            var registry = manager.getRegistry(), targetId = registry.addTarget(type, target);
            return {
                handlerId: targetId,
                unregister: unregisterTarget
            };
        }
        exports.__esModule = !0, exports.default = registerTarget, module.exports = exports.default;
    }, /* 196 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(process) {
            "use strict";
            function _interopRequireDefault(obj) {
                return obj && obj.__esModule ? obj : {
                    default: obj
                };
            }
            function _classCallCheck(instance, Constructor) {
                if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
            }
            function createTargetFactory(spec) {
                Object.keys(spec).forEach(function(key) {
                    _invariant2.default(ALLOWED_SPEC_METHODS.indexOf(key) > -1, 'Expected the drop target specification to only have some of the following keys: %s. Instead received a specification with an unexpected "%s" key. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html', ALLOWED_SPEC_METHODS.join(", "), key), 
                    _invariant2.default("function" == typeof spec[key], "Expected %s in the drop target specification to be a function. Instead received a specification with %s: %s. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html", key, key, spec[key]);
                });
                var Target = function() {
                    function Target(monitor) {
                        _classCallCheck(this, Target), this.monitor = monitor, this.props = null, this.component = null;
                    }
                    return Target.prototype.receiveProps = function(props) {
                        this.props = props;
                    }, Target.prototype.receiveMonitor = function(monitor) {
                        this.monitor = monitor;
                    }, Target.prototype.receiveComponent = function(component) {
                        this.component = component;
                    }, Target.prototype.canDrop = function() {
                        return !spec.canDrop || spec.canDrop(this.props, this.monitor);
                    }, Target.prototype.hover = function() {
                        spec.hover && spec.hover(this.props, this.monitor, this.component);
                    }, Target.prototype.drop = function() {
                        if (spec.drop) {
                            var dropResult = spec.drop(this.props, this.monitor, this.component);
                            return "production" !== process.env.NODE_ENV && _invariant2.default("undefined" == typeof dropResult || _lodashIsPlainObject2.default(dropResult), "drop() must either return undefined, or an object that represents the drop result. Instead received %s. Read more: http://gaearon.github.io/react-dnd/docs-drop-target.html", dropResult), 
                            dropResult;
                        }
                    }, Target;
                }();
                return function(monitor) {
                    return new Target(monitor);
                };
            }
            exports.__esModule = !0, exports.default = createTargetFactory;
            var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), _lodashIsPlainObject = __webpack_require__(13), _lodashIsPlainObject2 = _interopRequireDefault(_lodashIsPlainObject), ALLOWED_SPEC_METHODS = [ "canDrop", "hover", "drop" ];
            module.exports = exports.default;
        }).call(exports, __webpack_require__(4));
    }, /* 197 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function createTargetMonitor(manager) {
            return new TargetMonitor(manager);
        }
        exports.__esModule = !0, exports.default = createTargetMonitor;
        var _invariant = __webpack_require__(27), _invariant2 = _interopRequireDefault(_invariant), isCallingCanDrop = !1, TargetMonitor = function() {
            function TargetMonitor(manager) {
                _classCallCheck(this, TargetMonitor), this.internalMonitor = manager.getMonitor();
            }
            return TargetMonitor.prototype.receiveHandlerId = function(targetId) {
                this.targetId = targetId;
            }, TargetMonitor.prototype.canDrop = function() {
                _invariant2.default(!isCallingCanDrop, "You may not call monitor.canDrop() inside your canDrop() implementation. Read more: http://gaearon.github.io/react-dnd/docs-drop-target-monitor.html");
                try {
                    return isCallingCanDrop = !0, this.internalMonitor.canDropOnTarget(this.targetId);
                } finally {
                    isCallingCanDrop = !1;
                }
            }, TargetMonitor.prototype.isOver = function(options) {
                return this.internalMonitor.isOverTarget(this.targetId, options);
            }, TargetMonitor.prototype.getItemType = function() {
                return this.internalMonitor.getItemType();
            }, TargetMonitor.prototype.getItem = function() {
                return this.internalMonitor.getItem();
            }, TargetMonitor.prototype.getDropResult = function() {
                return this.internalMonitor.getDropResult();
            }, TargetMonitor.prototype.didDrop = function() {
                return this.internalMonitor.didDrop();
            }, TargetMonitor.prototype.getInitialClientOffset = function() {
                return this.internalMonitor.getInitialClientOffset();
            }, TargetMonitor.prototype.getInitialSourceClientOffset = function() {
                return this.internalMonitor.getInitialSourceClientOffset();
            }, TargetMonitor.prototype.getSourceClientOffset = function() {
                return this.internalMonitor.getSourceClientOffset();
            }, TargetMonitor.prototype.getClientOffset = function() {
                return this.internalMonitor.getClientOffset();
            }, TargetMonitor.prototype.getDifferenceFromInitialOffset = function() {
                return this.internalMonitor.getDifferenceFromInitialOffset();
            }, TargetMonitor;
        }();
        module.exports = exports.default;
    }, /* 198 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function createTargetConnector(backend) {
            function reconnectDropTarget() {
                disconnectCurrentDropTarget && (disconnectCurrentDropTarget(), disconnectCurrentDropTarget = null), 
                currentHandlerId && currentDropTargetNode && (disconnectCurrentDropTarget = backend.connectDropTarget(currentHandlerId, currentDropTargetNode, currentDropTargetOptions));
            }
            function receiveHandlerId(handlerId) {
                handlerId !== currentHandlerId && (currentHandlerId = handlerId, reconnectDropTarget());
            }
            var currentHandlerId = void 0, currentDropTargetNode = void 0, currentDropTargetOptions = void 0, disconnectCurrentDropTarget = void 0, hooks = _wrapConnectorHooks2.default({
                dropTarget: function(node, options) {
                    node === currentDropTargetNode && _areOptionsEqual2.default(options, currentDropTargetOptions) || (currentDropTargetNode = node, 
                    currentDropTargetOptions = options, reconnectDropTarget());
                }
            });
            return {
                receiveHandlerId: receiveHandlerId,
                hooks: hooks
            };
        }
        exports.__esModule = !0, exports.default = createTargetConnector;
        var _wrapConnectorHooks = __webpack_require__(190), _wrapConnectorHooks2 = _interopRequireDefault(_wrapConnectorHooks), _areOptionsEqual = __webpack_require__(192), _areOptionsEqual2 = _interopRequireDefault(_areOptionsEqual);
        module.exports = exports.default;
    }, /* 199 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), ArrowKeyStepper = function(_PureComponent) {
            function ArrowKeyStepper(props, context) {
                _classCallCheck(this, ArrowKeyStepper);
                var _this = _possibleConstructorReturn(this, (ArrowKeyStepper.__proto__ || Object.getPrototypeOf(ArrowKeyStepper)).call(this, props, context));
                return _this.state = {
                    scrollToColumn: props.scrollToColumn,
                    scrollToRow: props.scrollToRow
                }, _this.columnStartIndex = 0, _this.columnStopIndex = 0, _this.rowStartIndex = 0, 
                _this.rowStopIndex = 0, _this.onKeyDown = _this.onKeyDown.bind(_this), _this.onSectionRendered = _this.onSectionRendered.bind(_this), 
                _this;
            }
            return _inherits(ArrowKeyStepper, _PureComponent), _createClass(ArrowKeyStepper, [ {
                key: "componentWillUpdate",
                value: function(nextProps) {
                    var scrollToColumn = nextProps.scrollToColumn, scrollToRow = nextProps.scrollToRow;
                    this.props.scrollToColumn !== scrollToColumn && this.setState({
                        scrollToColumn: scrollToColumn
                    }), this.props.scrollToRow !== scrollToRow && this.setState({
                        scrollToRow: scrollToRow
                    });
                }
            }, {
                key: "onKeyDown",
                value: function(event) {
                    var _props = this.props, columnCount = _props.columnCount, disabled = _props.disabled, mode = _props.mode, rowCount = _props.rowCount;
                    if (!disabled) {
                        var _state = this.state, scrollToColumnPrevious = _state.scrollToColumn, scrollToRowPrevious = _state.scrollToRow, _state2 = this.state, scrollToColumn = _state2.scrollToColumn, scrollToRow = _state2.scrollToRow;
                        // The above cases all prevent default event event behavior.
                        // This is to keep the grid from scrolling after the snap-to update.
                        switch (event.key) {
                          case "ArrowDown":
                            "cells" === mode ? scrollToRow = Math.min(scrollToRow + 1, rowCount - 1) : "align:top-left" === mode ? // To avoid weird behaviour when the grid has scrolled to the bottom
                            this.rowStopIndex + 1 < rowCount && (scrollToRow = Math.min(this.rowStartIndex + 1, rowCount - 1)) : scrollToRow = "align:bottom-right" === mode ? Math.min(this.rowStopIndex + 1, rowCount - 1) : Math.min(this.rowStopIndex + 1, rowCount - 1);
                            break;

                          case "ArrowLeft":
                            scrollToColumn = "cells" === mode ? Math.max(scrollToColumn - 1, 0) : "align:top-left" === mode ? Math.max(this.columnStartIndex - 1, 0) : "align:bottom-right" === mode ? Math.max(this.columnStopIndex - 1, 0) : Math.max(this.columnStartIndex - 1, 0);
                            break;

                          case "ArrowRight":
                            "cells" === mode ? scrollToColumn = Math.min(scrollToColumn + 1, columnCount - 1) : "align:top-left" === mode ? // To avoid weird behaviour when the grid has scrolled to the right
                            this.columnStopIndex + 1 < columnCount && (scrollToColumn = Math.min(this.columnStartIndex + 1, columnCount - 1)) : scrollToColumn = "align:bottom-right" === mode ? Math.min(this.columnStopIndex + 1, columnCount - 1) : Math.min(this.columnStopIndex + 1, columnCount - 1);
                            break;

                          case "ArrowUp":
                            scrollToRow = "cells" === mode ? Math.max(scrollToRow - 1, 0) : "align:top-left" === mode ? Math.max(this.rowStartIndex - 1, 0) : "align:bottom-right" === mode ? Math.max(this.rowStopIndex - 1, 0) : Math.max(this.rowStartIndex - 1, 0);
                        }
                        scrollToColumn === scrollToColumnPrevious && scrollToRow === scrollToRowPrevious || (event.preventDefault(), 
                        this.setState({
                            scrollToColumn: scrollToColumn,
                            scrollToRow: scrollToRow
                        }));
                    }
                }
            }, {
                key: "onSectionRendered",
                value: function(_ref) {
                    var columnStartIndex = _ref.columnStartIndex, columnStopIndex = _ref.columnStopIndex, rowStartIndex = _ref.rowStartIndex, rowStopIndex = _ref.rowStopIndex;
                    this.columnStartIndex = columnStartIndex, this.columnStopIndex = columnStopIndex, 
                    this.rowStartIndex = rowStartIndex, this.rowStopIndex = rowStopIndex;
                }
            }, {
                key: "render",
                value: function() {
                    var _props2 = this.props, className = _props2.className, children = _props2.children, _state3 = this.state, scrollToColumn = _state3.scrollToColumn, scrollToRow = _state3.scrollToRow;
                    return _react2.default.createElement("div", {
                        className: className,
                        onKeyDown: this.onKeyDown
                    }, children({
                        onSectionRendered: this.onSectionRendered,
                        scrollToColumn: scrollToColumn,
                        scrollToRow: scrollToRow
                    }));
                }
            } ]), ArrowKeyStepper;
        }(_react.PureComponent);
        exports.default = ArrowKeyStepper, ArrowKeyStepper.defaultProps = {
            disabled: !1,
            mode: "edges",
            scrollToColumn: 0,
            scrollToRow: 0
        }, ArrowKeyStepper.propTypes = {
            children: _react.PropTypes.func.isRequired,
            className: _react.PropTypes.string,
            columnCount: _react.PropTypes.number.isRequired,
            disabled: _react.PropTypes.bool.isRequired,
            mode: _react.PropTypes.oneOf([ "cells", "edges", "align:top-left", "align:bottom-right" ]),
            rowCount: _react.PropTypes.number.isRequired,
            scrollToColumn: _react.PropTypes.number.isRequired,
            scrollToRow: _react.PropTypes.number.isRequired
        };
    }, /* 200 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _reactRedux = __webpack_require__(2), _selectors = __webpack_require__(201), _DataCells = __webpack_require__(211), _DataCells2 = _interopRequireDefault(_DataCells), _copyService = __webpack_require__(214), _copyService2 = _interopRequireDefault(_copyService), mapStateToProps = function(state, ownProps) {
            var customFunctions = ownProps.customFunctions, rowUiAxis = (0, _selectors.getRowUiAxis)(state), columnUiAxis = (0, 
            _selectors.getColumnUiAxis)(state), rowDimensionHeaders = rowUiAxis.dimensionHeaders, columnDimensionHeaders = columnUiAxis.dimensionHeaders, rowHeaders = rowUiAxis.headers, columnHeaders = columnUiAxis.headers, dataHeadersLocation = state.config.dataHeadersLocation;
            return {
                columnCount: (0, _selectors.getLayout)(state).columnHorizontalCount,
                columnHeaders: columnHeaders,
                copy: function(_ref) {
                    var selectedCellStart = _ref.selectedCellStart, selectedCellEnd = _ref.selectedCellEnd;
                    return (0, _copyService2.default)({
                        columnDimensionHeaders: columnDimensionHeaders,
                        columnHeaders: columnHeaders,
                        dataHeadersLocation: dataHeadersLocation,
                        getCellValue: (0, _selectors.getCellValue)(state),
                        rowDimensionHeaders: rowDimensionHeaders,
                        rowHeaders: rowHeaders,
                        selectedCellEnd: selectedCellEnd,
                        selectedCellStart: selectedCellStart,
                        customFunctions: customFunctions
                    });
                },
                dataHeadersLocation: dataHeadersLocation,
                getCellValue: (0, _selectors.getCellValue)(state),
                getColumnWidth: (0, _selectors.getColumnWidth)(state),
                getRowHeight: (0, _selectors.getRowHeight)(state),
                height: (0, _selectors.getDataCellsHeight)(state),
                rowCount: (0, _selectors.getLayout)(state).rowVerticalCount,
                rowHeaders: rowHeaders,
                width: (0, _selectors.getDataCellsWidth)(state),
                zoom: state.config.zoom
            };
        };
        exports.default = (0, _reactRedux.connect)(mapStateToProps)(_DataCells2.default);
    }, /* 201 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.getFieldValues = exports.getDataCellsWidth = exports.getDataCellsHeight = exports.getPreviewSizes = exports.getColumnHeadersVisibleWidth = exports.getRowHeadersVisibleHeight = exports.getCellValue = exports.getHeaderSizes = exports.getRowHeight = exports.getColumnWidth = exports.getDimensionPositions = exports.getLastChildSize = exports.getDimensionSize = exports.getLayout = exports.getColumnUiAxis = exports.getRowUiAxis = exports.getColumnAxis = exports.getRowAxis = exports.getFilteredData = exports.getFilters = exports.getActivatedDataFields = exports.getAvailableFields = exports.getColumnFields = exports.getRowFields = exports.getCellSizes = void 0;
        var _reselect = __webpack_require__(202), _Axis = __webpack_require__(203), _AxisUi = __webpack_require__(205), _AxisUi2 = _interopRequireDefault(_AxisUi), _constants = __webpack_require__(207), _generic = __webpack_require__(208), _domHelpers = __webpack_require__(209), _Filtering = __webpack_require__(210), getCellSizes = exports.getCellSizes = (0, 
        _reselect.createSelector)([ function(state) {
            return state.config.cellHeight;
        }, function(state) {
            return state.config.cellWidth;
        }, function(state) {
            return state.config.zoom;
        } ], function(cellHeight, cellWidth, zoom) {
            return {
                height: zoom * cellHeight,
                width: zoom * cellWidth
            };
        }), getRowFields = exports.getRowFields = (0, _reselect.createSelector)([ function(state) {
            return state.axis.rows;
        }, function(state) {
            return state.fields;
        } ], function(rowAxis, fields) {
            return rowAxis.map(function(id) {
                return fields[id];
            });
        }), getColumnFields = exports.getColumnFields = (0, _reselect.createSelector)([ function(state) {
            return state.axis.columns;
        }, function(state) {
            return state.fields;
        } ], function(columnAxis, fields) {
            return columnAxis.map(function(id) {
                return fields[id];
            });
        }), getDataFields = (exports.getAvailableFields = (0, _reselect.createSelector)([ function(state) {
            return state.axis.fields;
        }, function(state) {
            return state.fields;
        } ], function(fieldAxis, fields) {
            return fieldAxis.map(function(id) {
                return fields[id];
            });
        }), function(state) {
            return state.datafields;
        }), getActivatedDataFields = exports.getActivatedDataFields = (0, _reselect.createSelector)([ getDataFields ], function(datafields) {
            return Object.keys(datafields).map(function(id) {
                return datafields[id];
            }).filter(function(field) {
                return field.activated;
            });
        }), getActivatedDataFieldsCount = (0, _reselect.createSelector)([ getActivatedDataFields ], function(datafields) {
            return datafields.length;
        }), getFilters = exports.getFilters = function(state) {
            return state.filters || {};
        }, getData = function(state) {
            return state.data;
        }, getFilteredData = exports.getFilteredData = (0, _reselect.createSelector)([ getData, getFilters ], function(data, filtersObject) {
            var filters = [].concat(_toConsumableArray(Object.keys(filtersObject).map(function(id) {
                return filtersObject[id];
            })));
            return 0 === filters.length ? data : data.filter(function(row) {
                return filters.every(function(filter) {
                    return (0, _Filtering.pass)(filter, row[filter.fieldId]);
                });
            });
        }), getRowAxis = exports.getRowAxis = (0, _reselect.createSelector)([ getRowFields, getFilteredData ], function(rowFields, filteredData) {
            return new _Axis.Axis(_Axis.AxisType.ROWS, rowFields, filteredData);
        }), getColumnAxis = exports.getColumnAxis = (0, _reselect.createSelector)([ getColumnFields, getFilteredData ], function(columnFields, filteredData) {
            return new _Axis.Axis(_Axis.AxisType.COLUMNS, columnFields, filteredData);
        }), getRowUiAxis = exports.getRowUiAxis = (0, _reselect.createSelector)([ getRowAxis, getActivatedDataFields, getActivatedDataFieldsCount, function(state) {
            return state.config.dataHeadersLocation;
        }, function(state) {
            return state.axis.columns;
        } ], function(rowAxis, activatedDataFields, activatedDataFieldsCount, dataHeadersLocation, crossFieldsCode) {
            return new _AxisUi2.default(rowAxis, {
                activatedDataFields: activatedDataFields,
                activatedDataFieldsCount: activatedDataFieldsCount,
                dataHeadersLocation: dataHeadersLocation
            }, crossFieldsCode);
        }), getColumnUiAxis = exports.getColumnUiAxis = (0, _reselect.createSelector)([ getColumnAxis, getActivatedDataFields, function(state) {
            return state.config.dataHeadersLocation;
        } ], function(columnAxis, activatedDataFields, dataHeadersLocation) {
            return new _AxisUi2.default(columnAxis, {
                activatedDataFields: activatedDataFields,
                activatedDataFieldsCount: activatedDataFields.length,
                dataHeadersLocation: dataHeadersLocation
            });
        }), getLeafHeaderSize = (exports.getLayout = (0, _reselect.createSelector)([ getRowAxis, getColumnAxis, getRowUiAxis, getColumnUiAxis, getActivatedDataFieldsCount, function(state) {
            return state.config.dataHeadersLocation;
        } ], function(rowAxis, columnAxis, rowsUi, columnsUi, activatedDataFieldsCount, dataHeadersLocation) {
            var rowHorizontalCount = (rowAxis.fields.length || 1) + ("rows" === dataHeadersLocation && activatedDataFieldsCount >= 1 ? 1 : 0), rowVerticalCount = rowsUi.headers.length, columnHorizontalCount = columnsUi.headers.length, columnVerticalCount = (columnAxis.fields.length || 1) + ("columns" === dataHeadersLocation && activatedDataFieldsCount >= 1 ? 1 : 0);
            return {
                rowHorizontalCount: rowHorizontalCount,
                rowVerticalCount: rowVerticalCount,
                columnHorizontalCount: columnHorizontalCount,
                columnVerticalCount: columnVerticalCount
            };
        }), function(axis, key, sizes, cellSizes) {
            return axis === _Axis.AxisType.COLUMNS ? sizes.columns.leafs[key] || cellSizes.width : sizes.rows.leafs[key] || cellSizes.height;
        }), getDimensionSize = exports.getDimensionSize = (0, _reselect.createSelector)([ function(state) {
            return state.sizes;
        }, getCellSizes ], function(sizes, cellSizes) {
            return function(axis, id) {
                return axis === _Axis.AxisType.COLUMNS ? sizes.columns.dimensions[id] || cellSizes.height : sizes.rows.dimensions[id] || cellSizes.width;
            };
        }), calculateDimensionPositions = (exports.getLastChildSize = (0, _reselect.createSelector)([ function(state) {
            return state.sizes;
        }, getCellSizes ], function(sizes, cellSizes) {
            return function(axis, header) {
                for (var lastChild = header; lastChild.subheaders && lastChild.subheaders.length; ) lastChild = lastChild.subheaders[lastChild.subheaders.length - 1];
                return getLeafHeaderSize(axis, lastChild.key, sizes, cellSizes);
            };
        }), function(axis, fields, hasMeasures, getDimensionSize) {
            var axisType = void 0;
            axisType = "columns" === axis ? 1 : 2;
            var res = {}, position = 0;
            // Total header if no fields
            return fields.length ? fields.forEach(function(field) {
                res[field.id] = position, position += getDimensionSize(axisType, field.id);
            }) : position += getDimensionSize(axisType, _constants.TOTAL_ID), hasMeasures && (res[_constants.MEASURE_ID] = position), 
            res;
        }), getHeaderSizes = (exports.getDimensionPositions = (0, _reselect.createSelector)([ getDimensionSize, function(state) {
            return state.config.dataHeadersLocation;
        }, getColumnFields, getRowFields ], function(getDimensionSize, dataHeadersLocation, columnFields, rowFields) {
            return {
                columns: calculateDimensionPositions("columns", columnFields, "columns" === dataHeadersLocation, getDimensionSize),
                rows: calculateDimensionPositions("rows", rowFields, "rows" === dataHeadersLocation, getDimensionSize)
            };
        }), exports.getColumnWidth = (0, _reselect.createSelector)([ getColumnUiAxis, function(state) {
            return state.sizes;
        }, getCellSizes ], function(columnsUi, sizes, cellSizes) {
            return function(_ref) {
                var index = _ref.index, headers = columnsUi.headers[index], key = headers[headers.length - 1].key;
                return getLeafHeaderSize(_Axis.AxisType.COLUMNS, key, sizes, cellSizes);
            };
        }), exports.getRowHeight = (0, _reselect.createSelector)([ getRowUiAxis, function(state) {
            return state.sizes;
        }, getCellSizes ], function(rowsUi, sizes, cellSizes) {
            return function(_ref2) {
                var index = _ref2.index, headers = rowsUi.headers[index], key = headers[headers.length - 1].key;
                return getLeafHeaderSize(_Axis.AxisType.ROWS, key, sizes, cellSizes);
            };
        }), exports.getHeaderSizes = (0, _reselect.createSelector)([ function(state) {
            return state.config.dataHeadersLocation;
        }, function(state) {
            return state.sizes;
        }, function(state) {
            return state.axis.rows;
        }, function(state) {
            return state.axis.columns;
        }, getRowUiAxis, getColumnUiAxis, getCellSizes, getDimensionSize ], function(dataHeadersLocation, sizes, rows, columns, rowsUi, columnsUi, cellSizes, getDimensionSize) {
            var columnsMeasures = "columns" === dataHeadersLocation, rowHeadersWidth = 0;
            // Measures are on the row axis
            columnsMeasures || (rowHeadersWidth += sizes.rows.dimensions[_constants.MEASURE_ID] || cellSizes.width), 
            // There are no fields on the row axis
            rows.length ? rowHeadersWidth = rows.reduce(function(width, field) {
                return width + getDimensionSize(_Axis.AxisType.ROWS, field, sizes, cellSizes);
            }, rowHeadersWidth) : rowHeadersWidth += getDimensionSize(_Axis.AxisType.ROWS, _constants.TOTAL_ID, sizes, cellSizes);
            var columnHeadersHeight = 0;
            // Measures are on the column axis
            columnsMeasures && (columnHeadersHeight += sizes.columns.dimensions[_constants.MEASURE_ID] || cellSizes.height), 
            // There are no fields on the column axis
            columns.length ? columnHeadersHeight = columns.reduce(function(height, field) {
                return height + getDimensionSize(_Axis.AxisType.COLUMNS, field, sizes, cellSizes);
            }, columnHeadersHeight) : columnHeadersHeight += getDimensionSize(_Axis.AxisType.COLUMNS, _constants.TOTAL_ID, sizes, cellSizes);
            var rowHeadersHeight = rowsUi.headers.reduce(// (height, headers) => height + this.getRowHeight({ index: headers[0].x }),
            function(height, headers) {
                return height + getLeafHeaderSize(_Axis.AxisType.ROWS, headers[headers.length - 1].key, sizes, cellSizes);
            }, 0), columnHeadersWidth = columnsUi.headers.reduce(// (width, headers) => width + this.getColumnWidth({ index: headers[0].x }),
            function(width, headers) {
                return width + getLeafHeaderSize(_Axis.AxisType.COLUMNS, headers[headers.length - 1].key, sizes, cellSizes);
            }, 0);
            return {
                rowHeadersWidth: rowHeadersWidth,
                columnHeadersWidth: columnHeadersWidth,
                rowHeadersHeight: rowHeadersHeight,
                columnHeadersHeight: columnHeadersHeight
            };
        })), hasScrollbar = (exports.getCellValue = (0, _reselect.createSelector)([ getActivatedDataFields, getFilteredData ], function(activatedDataFields, data) {
            return function(datafield, rowDimension, columnDimension) {
                var aggregateFunc = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : function() {
                    return null;
                };
                if (!rowDimension || !columnDimension) return null;
                var rowIndexes = rowDimension.isRoot ? null : rowDimension.getRowIndexes(), columnIndexes = columnDimension.isRoot ? null : columnDimension.getRowIndexes(), intersection = void 0;
                // At initialization, both rowIndexes and columnIndexes are null
                intersection = null === rowIndexes && null === columnIndexes ? null : null === rowIndexes ? columnIndexes : null === columnIndexes ? rowIndexes : (0, 
                _generic.twoArraysIntersect)(columnIndexes, rowIndexes);
                var emptyIntersection = !intersection || 0 === intersection.length;
                return emptyIntersection ? null : aggregateFunc(datafield.id, intersection, data);
            };
        }), (0, _reselect.createSelector)([ function(state) {
            return state.config.width;
        }, function(state) {
            return state.config.height;
        }, getHeaderSizes ], function(width, height, _ref3) {
            var columnHeadersWidth = _ref3.columnHeadersWidth, columnHeadersHeight = _ref3.columnHeadersHeight, rowHeadersWidth = _ref3.rowHeadersWidth, rowHeadersHeight = _ref3.rowHeadersHeight;
            return {
                bottom: width < columnHeadersWidth + rowHeadersWidth + (0, _domHelpers.scrollbarSize)(),
                right: height < columnHeadersHeight + rowHeadersHeight + (0, _domHelpers.scrollbarSize)()
            };
        }));
        exports.getRowHeadersVisibleHeight = (0, _reselect.createSelector)([ function(state) {
            return state.config.height;
        }, getHeaderSizes, hasScrollbar ], function(height, _ref4, hasScrollbar) {
            var columnHeadersHeight = _ref4.columnHeadersHeight, rowHeadersHeight = _ref4.rowHeadersHeight;
            return Math.min(height - columnHeadersHeight - (hasScrollbar.bottom ? (0, _domHelpers.scrollbarSize)() : 0), rowHeadersHeight);
        }), exports.getColumnHeadersVisibleWidth = (0, _reselect.createSelector)([ function(state) {
            return state.config.width;
        }, getHeaderSizes, hasScrollbar ], function(width, _ref5, hasScrollbar) {
            var rowHeadersWidth = _ref5.rowHeadersWidth, columnHeadersWidth = _ref5.columnHeadersWidth;
            return Math.min(width - rowHeadersWidth - (hasScrollbar.right ? (0, _domHelpers.scrollbarSize)() : 0), columnHeadersWidth);
        }), exports.getPreviewSizes = (0, _reselect.createSelector)([ function(state) {
            return state.config.height;
        }, function(state) {
            return state.config.width;
        }, hasScrollbar, getHeaderSizes ], function(height, width, hasScrollbar, _ref6) {
            var rowHeadersHeight = _ref6.rowHeadersHeight, rowHeadersWidth = _ref6.rowHeadersWidth, columnHeadersHeight = _ref6.columnHeadersHeight, columnHeadersWidth = _ref6.columnHeadersWidth;
            return {
                height: Math.min(height - (hasScrollbar.bottom ? (0, _domHelpers.scrollbarSize)() : 0), rowHeadersHeight + columnHeadersHeight),
                width: Math.min(width - (hasScrollbar.right ? (0, _domHelpers.scrollbarSize)() : 0), columnHeadersWidth + rowHeadersWidth)
            };
        }), exports.getDataCellsHeight = (0, _reselect.createSelector)([ function(state) {
            return state.config.height;
        }, getHeaderSizes, hasScrollbar ], function(height, _ref7, hasScrollbar) {
            var columnHeadersHeight = _ref7.columnHeadersHeight, rowHeadersHeight = _ref7.rowHeadersHeight;
            return Math.min(height - columnHeadersHeight, rowHeadersHeight + (hasScrollbar.bottom ? (0, 
            _domHelpers.scrollbarSize)() : 0));
        }), exports.getDataCellsWidth = (0, _reselect.createSelector)([ function(state) {
            return state.config.width;
        }, getHeaderSizes, hasScrollbar ], function(width, _ref8, hasScrollbar) {
            var columnHeadersWidth = _ref8.columnHeadersWidth, rowHeadersWidth = _ref8.rowHeadersWidth;
            return Math.min(width - rowHeadersWidth, columnHeadersWidth + (hasScrollbar.right ? (0, 
            _domHelpers.scrollbarSize)() : 0));
        }), exports.getFieldValues = (0, _reselect.createSelector)([ getData ], function(data) {
            return function(field, filterFunc) {
                // We use data here instead of filteredData
                // Otherwise you lose the filtered values the next time you open a Filter Panel
                for (var values = [], labels = [], res = [], labelsMap = {}, valuesMap = {}, containsBlank = !1, i = 0; i < data.length; i += 1) {
                    var row = data[i], val = row[field.id], label = row[field.name];
                    labelsMap[val] = label, valuesMap[label] = val, void 0 !== filterFunc ? (filterFunc === !0 || "function" == typeof filterFunc && filterFunc(val)) && (values.push(val), 
                    labels.push(label)) : null != val ? (values.push(val), labels.push(label)) : containsBlank = !0;
                }
                if (labels.length > 1) {
                    (0, _generic.isNumber)(labels[0]) || (0, _generic.isDate)(labels[0]) ? labels.sort(function(a, b) {
                        return a ? b ? a - b : 1 : b ? -1 : 0;
                    }) : labels.sort();
                    for (var vi = 0; vi < labels.length; vi += 1) 0 !== vi && labels[vi] === res[res.length - 1].label || res.push({
                        value: valuesMap[labels[vi]],
                        label: labels[vi]
                    });
                } else res = values.map(function(value) {
                    return {
                        value: value,
                        label: labelsMap[value]
                    };
                });
                return containsBlank && res.unshift({
                    value: null,
                    label: ""
                }), res;
            };
        });
    }, /* 202 */
    /***/
    function(module, exports) {
        "use strict";
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        function defaultEqualityCheck(a, b) {
            return a === b;
        }
        function defaultMemoize(func) {
            var equalityCheck = arguments.length <= 1 || void 0 === arguments[1] ? defaultEqualityCheck : arguments[1], lastArgs = null, lastResult = null;
            return function() {
                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) args[_key] = arguments[_key];
                return null !== lastArgs && lastArgs.length === args.length && args.every(function(value, index) {
                    return equalityCheck(value, lastArgs[index]);
                }) || (lastResult = func.apply(void 0, args)), lastArgs = args, lastResult;
            };
        }
        function getDependencies(funcs) {
            var dependencies = Array.isArray(funcs[0]) ? funcs[0] : funcs;
            if (!dependencies.every(function(dep) {
                return "function" == typeof dep;
            })) {
                var dependencyTypes = dependencies.map(function(dep) {
                    return typeof dep;
                }).join(", ");
                throw new Error("Selector creators expect all input-selectors to be functions, " + ("instead received the following types: [" + dependencyTypes + "]"));
            }
            return dependencies;
        }
        function createSelectorCreator(memoize) {
            for (var _len2 = arguments.length, memoizeOptions = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) memoizeOptions[_key2 - 1] = arguments[_key2];
            return function() {
                for (var _len3 = arguments.length, funcs = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) funcs[_key3] = arguments[_key3];
                var recomputations = 0, resultFunc = funcs.pop(), dependencies = getDependencies(funcs), memoizedResultFunc = memoize.apply(void 0, [ function() {
                    return recomputations++, resultFunc.apply(void 0, arguments);
                } ].concat(memoizeOptions)), selector = function(state, props) {
                    for (var _len4 = arguments.length, args = Array(_len4 > 2 ? _len4 - 2 : 0), _key4 = 2; _key4 < _len4; _key4++) args[_key4 - 2] = arguments[_key4];
                    var params = dependencies.map(function(dependency) {
                        return dependency.apply(void 0, [ state, props ].concat(args));
                    });
                    return memoizedResultFunc.apply(void 0, _toConsumableArray(params));
                };
                return selector.resultFunc = resultFunc, selector.recomputations = function() {
                    return recomputations;
                }, selector.resetRecomputations = function() {
                    return recomputations = 0;
                }, selector;
            };
        }
        function createStructuredSelector(selectors) {
            var selectorCreator = arguments.length <= 1 || void 0 === arguments[1] ? createSelector : arguments[1];
            if ("object" != typeof selectors) throw new Error("createStructuredSelector expects first argument to be an object where each property is a selector, instead received a " + typeof selectors);
            var objectKeys = Object.keys(selectors);
            return selectorCreator(objectKeys.map(function(key) {
                return selectors[key];
            }), function() {
                for (var _len5 = arguments.length, values = Array(_len5), _key5 = 0; _key5 < _len5; _key5++) values[_key5] = arguments[_key5];
                return values.reduce(function(composition, value, index) {
                    return composition[objectKeys[index]] = value, composition;
                }, {});
            });
        }
        exports.__esModule = !0, exports.defaultMemoize = defaultMemoize, exports.createSelectorCreator = createSelectorCreator, 
        exports.createStructuredSelector = createStructuredSelector;
        var createSelector = exports.createSelector = createSelectorCreator(defaultMemoize);
    }, /* 203 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.Axis = exports.AxisType = void 0;
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _Dimension = __webpack_require__(204), _Dimension2 = _interopRequireDefault(_Dimension);
        exports.AxisType = {
            COLUMNS: 1,
            ROWS: 2,
            DATA: 3,
            FIELDS: 4,
            CHART: 5
        }, exports.Axis = function() {
            /**
	   * Dimensions dictionary indexed by depth
	   * @type {Object} Dictionary of (depth, arrays)
	   */
            // this.dimensionsByDepth = null
            function Axis(type, fields, data) {
                var _this = this;
                _classCallCheck(this, Axis), /**
	     * Axis type (rows, columns, data)
	     * @type {orb.axe.Type}
	     */
                this.type = type, /**
	     * This axe dimension fields
	     * @type {Array}
	     */
                this.fields = fields, /**
	     * Root dimension
	     * @type {orb.dimension}
	     */
                this.root = new _Dimension2.default(-1, null, null, null, this.fields.length + 1, !0, !1), 
                this.fill(data), // initial sort
                this.fields.forEach(function(field) {
                    _this.sort(field.id, !0);
                });
            }
            return _createClass(Axis, [ {
                key: "sort",
                value: function(fieldId, doNotToggle) {
                    var field = this.fields[this.getFieldIndex(fieldId)];
                    doNotToggle !== !0 ? "asc" !== field.sort.order ? field.sort.order = "asc" : field.sort.order = "desc" : null === field.sort.order && (// If doNotToggle is true, fields without sort configuration are going to
                    // be sorted in ascending order. This ensures that it is correctly recorded.
                    field.sort.order = "asc");
                    var depth = this.fields.length - this.getFieldIndex(field.id), dimensions = void 0;
                    dimensions = depth === this.fields.length ? [ this.root ] : this.getDimensionsByDepth(depth + 1), 
                    dimensions.forEach(function(dimension) {
                        null !== field.sort.customfunc ? dimension.values.sort(field.sort.customfunc) : dimension.values.sort(), 
                        "desc" === field.sort.order && dimension.values.reverse();
                    });
                }
            }, {
                key: "getDimensionsByDepth",
                value: function(depth) {
                    var _ref, _this2 = this, dim = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.root;
                    // if (!dim) { dim = this.root; }
                    // if (!dim) { dim = this.root; }
                    return depth === this.fields.length + 1 ? [ dim ] : (_ref = []).concat.apply(_ref, _toConsumableArray(Object.keys(dim.subdimvals).map(function(dimValue) {
                        return _this2.getDimensionsByDepth(depth + 1, dim.subdimvals[dimValue]);
                    })));
                }
            }, {
                key: "getFieldIndex",
                value: function(fieldId) {
                    return this.fields.map(function(fld) {
                        return fld.id;
                    }).indexOf(fieldId);
                }
            }, {
                key: "fill",
                value: function(data) {
                    if (null != data && this.fields.length > 0 && null != data && Array.isArray(data) && data.length > 0) for (var rowIndex = 0, dataLength = data.length; rowIndex < dataLength; rowIndex += 1) for (var row = data[rowIndex], dim = this.root, findex = 0; findex < this.fields.length; findex += 1) {
                        var depth = this.fields.length - findex, field = this.fields[findex], name = row[field.name], id = row[field.id], subdimvals = dim.subdimvals;
                        void 0 !== subdimvals[id] ? dim = subdimvals[id] : (dim.values.push(id), dim = new _Dimension2.default(id, dim, name, field, depth, !1, findex === this.fields.length - 1), 
                        subdimvals[id] = dim, dim.rowIndexes = []), dim.rowIndexes.push(rowIndex);
                    }
                }
            } ]), Axis;
        }();
    }, /* 204 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), Dimension = function() {
            function Dimension(id, parent, caption, field, depth, isRoot, isLeaf) {
                _classCallCheck(this, Dimension), /**
	     * unique id within parent orb.axe instance.
	     * @type {Number}
	     */
                this.id = id, /**
	     * parent subdimension
	     * @type {orb.dimension}
	     */
                this.parent = parent, /**
	     * This instance dimension caption
	     * @type {object}
	     */
                this.caption = caption, /**
	     * Whether or not this is the root dimension for a given axe (row/column)
	     * @type {Boolean}
	     */
                this.isRoot = isRoot, /**
	     * Whether or not this is the leaf (deepest) dimension for a given axe (row/column)
	     * @type {Boolean}
	     */
                this.isLeaf = isLeaf, /**
	     * Dimension's data field
	     * @type {Array}
	     */
                this.field = field, /**
	     * Dimension's depth (to the deepest sub-dimension)
	     * @type {Number}
	     */
                this.depth = depth, /**
	     * Dimension's set of all values
	     * @type {Array}
	     */
                this.values = [], /**
	     * Dimension's set of all values
	     * @type {Array}
	     */
                this.subdimvals = {}, /**
	     * Direct descendant subdimensions dictionary
	     * @type {Object}
	     */
                this.rowIndexes = null;
            }
            return _createClass(Dimension, [ {
                key: "getRowIndexes",
                value: function(result) {
                    if (null === this.rowIndexes) {
                        this.rowIndexes = [];
                        for (var i = 0; i < this.values.length; i += 1) this.subdimvals[this.values[i]].getRowIndexes(this.rowIndexes);
                    }
                    if (null != result) {
                        for (var j = 0; j < this.rowIndexes.length; j += 1) result.push(this.rowIndexes[j]);
                        return result;
                    }
                    return this.rowIndexes;
                }
            } ]), Dimension;
        }();
        exports.default = Dimension;
    }, /* 205 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function getDataFieldsCount(_ref) {
            var axisType = _ref.axisType, dataHeadersLocation = _ref.dataHeadersLocation, activatedDataFieldsCount = _ref.activatedDataFieldsCount;
            return "columns" === dataHeadersLocation && axisType === _Axis.AxisType.COLUMNS || "rows" === dataHeadersLocation && axisType === _Axis.AxisType.ROWS ? activatedDataFieldsCount : 0;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.keyToIndex = void 0;
        var _slicedToArray = function() {
            function sliceIterator(arr, i) {
                var _arr = [], _n = !0, _d = !1, _e = void 0;
                try {
                    for (var _s, _i = arr[Symbol.iterator](); !(_n = (_s = _i.next()).done) && (_arr.push(_s.value), 
                    !i || _arr.length !== i); _n = !0) ;
                } catch (err) {
                    _d = !0, _e = err;
                } finally {
                    try {
                        !_n && _i.return && _i.return();
                    } finally {
                        if (_d) throw _e;
                    }
                }
                return _arr;
            }
            return function(arr, i) {
                if (Array.isArray(arr)) return arr;
                if (Symbol.iterator in Object(arr)) return sliceIterator(arr, i);
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            };
        }(), _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _Axis = __webpack_require__(203), _Cells = __webpack_require__(206), _constants = __webpack_require__(207), findHeader = function findHeader(headers, keys) {
            if (1 === keys.length) return headers.find(function(header) {
                return header.value === keys[0];
            });
            var parentHeader = headers.find(function(header) {
                return header.value === keys[0];
            });
            if (!parentHeader) throw new Error("header not found");
            return findHeader(parentHeader.subheaders, keys.slice(1));
        }, AxisUi = (exports.keyToIndex = function(headers, key) {
            var keys = key.split(_constants.KEY_SEPARATOR), depth = headers[0].length, ancestorHeaders = headers.filter(function(headersRow) {
                return headersRow.length === depth;
            }).map(function(headersRow) {
                return headersRow[0];
            });
            try {
                return findHeader(ancestorHeaders, keys).x;
            } catch (e) {
                // console.error(`Header with key ${key} not found in following headers`, headers);
                return -1;
            }
        }, function() {
            function AxisUi(axis, config, crossAxisFieldsCode) {
                _classCallCheck(this, AxisUi);
                var dataHeadersLocation = config.dataHeadersLocation, activatedDataFieldsCount = config.activatedDataFieldsCount, activatedDataFields = config.activatedDataFields;
                this.datafieldsCount = getDataFieldsCount({
                    axisType: axis.type,
                    dataHeadersLocation: dataHeadersLocation,
                    activatedDataFieldsCount: activatedDataFieldsCount
                }), this.hasDataFields = this.datafieldsCount >= 1, /**
	     * Headers render properties
	     * @type {Array}
	     */
                this.headers = [], /**
	     * Dimension headers render properties
	     * @type {Array}
	     */
                this.dimensionHeaders = [], this.x = 0;
                var headers = [], grandtotalHeader = void 0, y = void 0;
                null != axis && (axis.root.values.length > 0 && (headers.push([]), // Fill Rows layout infos
                y = this.getUiInfo({
                    infos: headers,
                    dimension: axis.root,
                    axisType: axis.type,
                    dataHeadersLocation: dataHeadersLocation,
                    activatedDataFields: activatedDataFields,
                    activatedDataFieldsCount: activatedDataFieldsCount
                })), 0 === headers.length && headers.push([ grandtotalHeader = new _Cells.Header(axis.type, _Cells.HeaderType.GRAND_TOTAL, axis.root, null, this.datafieldsCount, this.x, y = 0, null, crossAxisFieldsCode) ]), 
                grandtotalHeader && // add grand-total data headers if more than 1 data field and they will be the leaf headers
                this.addDataHeaders({
                    axisType: axis.type,
                    infos: headers,
                    parent: grandtotalHeader,
                    y: y + 1,
                    dataHeadersLocation: dataHeadersLocation,
                    activatedDataFields: activatedDataFields,
                    activatedDataFieldsCount: activatedDataFieldsCount
                })), this.headers = headers, this.dimensionHeaders = axis.fields.map(function(field) {
                    return new _Cells.DimensionHeader(axis.type, field);
                });
            }
            /**
	  * Fills the infos array given in argument with the dimension layout infos as row.
	  * @param  {orb.dimension}  dimension - the dimension to get ui info for
	  * @param  {object}  infos - array to fill with ui dimension info
	  * @param  {number}  axisType - type of the axe (rows or columns)
	  */
            return _createClass(AxisUi, [ {
                key: "getUiInfo",
                value: function(_ref2) {
                    var infos = _ref2.infos, dimension = _ref2.dimension, axisType = _ref2.axisType, dataHeadersLocation = _ref2.dataHeadersLocation, activatedDataFields = _ref2.activatedDataFields, activatedDataFieldsCount = _ref2.activatedDataFieldsCount, _ref2$y = _ref2.y, y = void 0 === _ref2$y ? 0 : _ref2$y;
                    if (dimension.values.length > 0) for (var infosMaxIndex = infos.length - 1, lastInfosArray = infos[infosMaxIndex], parent = lastInfosArray.length > 0 ? lastInfosArray[lastInfosArray.length - 1] : null, valIndex = 0; valIndex < dimension.values.length; valIndex += 1) {
                        var subvalue = dimension.values[valIndex], subdim = dimension.subdimvals[subvalue], subTotalHeader = void 0;
                        // x here will probably create bugs. To change when necessary
                        subTotalHeader = !subdim.isLeaf && subdim.field.subTotal.visible ? new _Cells.Header(axisType, _Cells.HeaderType.SUB_TOTAL, subdim, parent, this.datafieldsCount, this.x, y) : null;
                        var newHeader = new _Cells.Header(axisType, null, subdim, parent, this.datafieldsCount, this.x, y, subTotalHeader);
                        valIndex > 0 && infos.push(lastInfosArray = []), lastInfosArray.push(newHeader), 
                        subdim.isLeaf ? // add data headers if more than 1 data field and they will be the leaf headers
                        this.addDataHeaders({
                            axisType: axisType,
                            infos: infos,
                            activatedDataFields: activatedDataFields,
                            activatedDataFieldsCount: activatedDataFieldsCount,
                            dataHeadersLocation: dataHeadersLocation,
                            parent: newHeader,
                            y: y + 1
                        }) : (this.getUiInfo({
                            infos: infos,
                            dimension: subdim,
                            axisType: axisType,
                            y: y + 1,
                            dataHeadersLocation: dataHeadersLocation,
                            activatedDataFields: activatedDataFields,
                            activatedDataFieldsCount: activatedDataFieldsCount
                        }), subdim.field.subTotal.visible && (infos.push([ subTotalHeader ]), // add sub-total data headers
                        // if more than 1 data field and they will be the leaf headers
                        this.addDataHeaders({
                            axisType: axisType,
                            infos: infos,
                            activatedDataFields: activatedDataFields,
                            activatedDataFieldsCount: activatedDataFieldsCount,
                            dataHeadersLocation: dataHeadersLocation,
                            parent: subTotalHeader,
                            y: y + 1
                        })));
                    }
                    return y;
                }
            }, {
                key: "addDataHeaders",
                value: function(_ref3) {
                    var infos = _ref3.infos, parent = _ref3.parent, dataHeadersLocation = _ref3.dataHeadersLocation, activatedDataFields = _ref3.activatedDataFields, y = _ref3.y;
                    if (this.hasDataFields) {
                        var lastInfosArray = infos[infos.length - 1], _iteratorNormalCompletion = !0, _didIteratorError = !1, _iteratorError = void 0;
                        try {
                            for (var _step, _iterator = activatedDataFields.entries()[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = !0) {
                                var _step$value = _slicedToArray(_step.value, 2), index = _step$value[0], datafield = _step$value[1];
                                lastInfosArray.push(new _Cells.DataHeader("columns" === dataHeadersLocation ? _Axis.AxisType.COLUMNS : _Axis.AxisType.ROWS, datafield, parent, this.x += 1, y)), 
                                index < this.datafieldsCount - 1 && infos.push(lastInfosArray = []);
                            }
                        } catch (err) {
                            _didIteratorError = !0, _iteratorError = err;
                        } finally {
                            try {
                                !_iteratorNormalCompletion && _iterator.return && _iterator.return();
                            } finally {
                                if (_didIteratorError) throw _iteratorError;
                            }
                        }
                    } else this.x += 1;
                }
            } ]), AxisUi;
        }());
        exports.default = AxisUi;
    }, /* 206 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.EmptyCell = exports.ButtonCell = exports.DataCell = exports.DimensionHeader = exports.DataHeader = exports.Header = exports.HeaderType = void 0;
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _Axis = __webpack_require__(203), _constants = __webpack_require__(207), HeaderType = exports.HeaderType = {
            EMPTY: 1,
            DATA_HEADER: 2,
            DATA_VALUE: 3,
            FIELD_BUTTON: 4,
            INNER: 5,
            WRAPPER: 6,
            SUB_TOTAL: 7,
            GRAND_TOTAL: 8,
            DIMENSION_HEADER: 9,
            getHeaderClass: function(headerType, axetype) {
                var cssclass = void 0;
                switch (cssclass = axetype === _Axis.AxisType.ROWS ? "header-row" : axetype === _Axis.AxisType.COLUMNS ? "header-col" : "", 
                headerType) {
                  case HeaderType.EMPTY:
                  case HeaderType.FIELD_BUTTON:
                  default:
                    cssclass = "empty";
                    break;

                  case HeaderType.INNER:
                    cssclass = "header " + cssclass;
                    break;

                  case HeaderType.WRAPPER:
                    cssclass = "header " + cssclass;
                    break;

                  case HeaderType.SUB_TOTAL:
                    cssclass = "header header-st " + cssclass;
                    break;

                  case HeaderType.GRAND_TOTAL:
                    cssclass = "header header-gt " + cssclass;
                }
                return cssclass;
            },
            getCellClass: function(rowHeaderType, colHeaderType) {
                var cssclass = "";
                switch (rowHeaderType) {
                  case HeaderType.GRAND_TOTAL:
                    cssclass = "cell-gt";
                    break;

                  case HeaderType.SUB_TOTAL:
                    cssclass = colHeaderType === HeaderType.GRAND_TOTAL ? "cell-gt" : "cell-st";
                    break;

                  default:
                    cssclass = colHeaderType === HeaderType.GRAND_TOTAL ? "cell-gt" : colHeaderType === HeaderType.SUB_TOTAL ? "cell-st" : "";
                }
                return cssclass;
            }
        }, CellBase = function CellBase(options) {
            _classCallCheck(this, CellBase), // CellBase is an abstract class
            // Symbol new.target does not pass in Uglify.js
            // if (new.target === CellBase) {
            //   throw new Error('CellBase is an abstract class and cannot be instantiated directly.')
            // }
            /**
	   * axe type (COLUMNS, ROWS, DATA, ...)
	   * @type {orb.AxisType}
	   */
            this.axetype = options.axetype, /**
	   * cell type (EMPTY, DATA_VALUE, FIELD_BUTTON, INNER, WRAPPER, SUB_TOTAL, GRAND_TOTAL, ...)
	   * @type {HeaderType}
	   */
            this.type = options.type, /**
	   * header cell template
	   * @type {String}
	   */
            this.template = options.template, /**
	   * header cell value
	   * @type {Object}
	   */
            this.value = options.value, /**
	   * is header cell expanded
	   * @type {Boolean}
	   */
            this.expanded = !0, /**
	   * header cell css class(es)
	   * @type {String}
	   */
            this.cssclass = options.cssclass, /**
	   * header cell width
	   * @type {Number}
	   */
            this.hspan = options.hspan || function() {
                return 1;
            }, /**
	   * gets header cell's height
	   * @return {Number}
	   */
            this.vspan = options.vspan || function() {
                return 1;
            }, /**
	   * gets wether header cell is visible
	   * @return {Boolean}
	   */
            this.visible = options.isvisible || function() {
                return !0;
            }, this.key = this.axetype + this.type + this.value;
        };
        exports.Header = function(_CellBase) {
            function Header(axetype, headerTypeP, dim, parent, datafieldscount, x, y, subtotalHeader) {
                var crossAxisFieldsCode = arguments.length > 8 && void 0 !== arguments[8] ? arguments[8] : [];
                _classCallCheck(this, Header);
                var isOnRowAxis = axetype === _Axis.AxisType.ROWS, headerType = headerTypeP || (1 === dim.depth ? HeaderType.INNER : HeaderType.WRAPPER), value = void 0, hspan = void 0, vspan = void 0, key = void 0;
                switch (headerType) {
                  case HeaderType.GRAND_TOTAL:
                    value = "Total", hspan = isOnRowAxis ? dim.depth - 1 || 1 : datafieldscount || 1, 
                    vspan = isOnRowAxis ? datafieldscount || 1 : dim.depth - 1 || 1, key = "__total__-//-" + crossAxisFieldsCode.join(_constants.KEY_SEPARATOR);
                    break;

                  case HeaderType.SUB_TOTAL:
                    value = dim.caption, hspan = isOnRowAxis ? dim.depth : datafieldscount || 1, vspan = isOnRowAxis ? datafieldscount || 1 : dim.depth, 
                    key = parent ? parent.key + "-/-" + value : value;
                    break;

                  default:
                    value = dim.caption, hspan = isOnRowAxis ? 1 : null, vspan = isOnRowAxis ? null : 1, 
                    key = parent ? parent.key + "-/-" + value : value;
                }
                var options = {
                    axetype: axetype,
                    type: headerType,
                    template: isOnRowAxis ? "cell-template-row-header" : "cell-template-column-header",
                    value: value,
                    cssclass: HeaderType.getHeaderClass(headerType, axetype)
                }, _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, options));
                // this.expanded = this.getState()
                // ? this.getState().expanded
                // : (headerType !== HeaderType.SUB_TOTAL || !dim.field.subTotal.collapsed);
                return _this.isOnRowAxis = isOnRowAxis, _this.hspan = null != hspan ? function() {
                    return hspan;
                } : _this.calcSpan, _this.vspan = null != vspan ? function() {
                    return vspan;
                } : _this.calcSpan, _this.isvisible = _this.isParentExpanded, _this.subtotalHeader = subtotalHeader, 
                _this.parent = parent, _this.dim = dim, _this.subheaders = [], null != parent && _this.parent.subheaders.push(_this), 
                _this.datafieldscount = datafieldscount, _this.key = key, _this.caption = _this.value, 
                _this.x = x, _this.y = y, _this;
            }
            return _inherits(Header, _CellBase), _createClass(Header, [ {
                key: "expand",
                value: function() {
                    this.expanded = !0;
                }
            }, {
                key: "collapse",
                value: function() {
                    this.expanded = !1;
                }
            }, {
                key: "isParentExpanded",
                value: function() {
                    if (this.type === HeaderType.SUB_TOTAL) {
                        for (var hparent = this.parent; null != hparent; ) {
                            if (hparent.subtotalHeader && !hparent.subtotalHeader.expanded) return !1;
                            hparent = hparent.parent;
                        }
                        return !0;
                    }
                    var isexpanded = this.dim.isRoot || this.dim.isLeaf || !this.dim.field.subTotal.visible || this.subtotalHeader.expanded;
                    if (!isexpanded) return !1;
                    for (var par = this.parent; null != par && (!par.dim.field.subTotal.visible || null != par.subtotalHeader && par.subtotalHeader.expanded); ) par = par.parent;
                    return null == par || null == par.subtotalHeader ? isexpanded : par.subtotalHeader.expanded;
                }
            }, {
                key: "calcSpan",
                value: function(ignoreVisibility) {
                    var span = 0, subSpan = void 0, addone = !1;
                    if (this.isOnRowAxis || ignoreVisibility || this.visible()) {
                        if (this.dim.isLeaf) return this.datafieldscount || 1;
                        // subdimvals 'own' properties are the set of values for this dimension
                        if (this.subheaders.length > 0) for (var i = 0; i < this.subheaders.length; i += 1) {
                            var subheader = this.subheaders[i];
                            // if it's not an array
                            subheader.dim.isLeaf ? span += this.datafieldscount || 1 : (subSpan = this.isOnRowAxis ? subheader.vspan() : subheader.hspan(), 
                            span += subSpan, 0 === i && 0 === subSpan && (addone = !0));
                        } else span += this.datafieldscount || 1;
                        return span + (addone ? 1 : 0);
                    }
                    return span;
                }
            } ]), Header;
        }(CellBase), exports.DataHeader = function(_CellBase2) {
            function DataHeader(axetype, datafield, parent, x, y) {
                _classCallCheck(this, DataHeader);
                var _this2 = _possibleConstructorReturn(this, (DataHeader.__proto__ || Object.getPrototypeOf(DataHeader)).call(this, {
                    axetype: axetype,
                    type: HeaderType.DATA_HEADER,
                    template: "cell-template-dataheader",
                    value: datafield,
                    cssclass: HeaderType.getHeaderClass(parent.type, parent.axetype),
                    isvisible: parent.visible
                }));
                return _this2.parent = parent, null != parent && _this2.parent.subheaders.push(_this2), 
                _this2.key = parent ? parent.key + "-/-" + datafield.name : datafield.name, _this2.caption = _this2.value.caption, 
                _this2.x = x, _this2.y = y, _this2;
            }
            return _inherits(DataHeader, _CellBase2), DataHeader;
        }(CellBase), exports.DimensionHeader = function(_CellBase3) {
            function DimensionHeader(axetype, field) {
                _classCallCheck(this, DimensionHeader);
                var _this3 = _possibleConstructorReturn(this, (DimensionHeader.__proto__ || Object.getPrototypeOf(DimensionHeader)).call(this, {
                    axetype: axetype,
                    type: HeaderType.DIMENSION_HEADER,
                    template: "cell-template-dimensionheader",
                    value: field,
                    cssclass: HeaderType.getHeaderClass(HeaderType.DIMENSION_HEADER, axetype),
                    isvisible: function() {
                        return !0;
                    }
                }));
                return _this3.key = field.name, _this3.hspan = function() {
                    return 1;
                }, _this3.vspan = function() {
                    return 1;
                }, _this3;
            }
            return _inherits(DimensionHeader, _CellBase3), DimensionHeader;
        }(CellBase), exports.DataCell = function(_CellBase4) {
            function DataCell(getCellValue, dataHeadersLocation, rowinfo, colinfo, customFunctions) {
                _classCallCheck(this, DataCell);
                var rowDimension = rowinfo.type === HeaderType.DATA_HEADER ? rowinfo.parent.dim : rowinfo.dim, columnDimension = colinfo.type === HeaderType.DATA_HEADER ? colinfo.parent.dim : colinfo.dim, rowType = rowinfo.type === HeaderType.DATA_HEADER ? rowinfo.parent.type : rowinfo.type, colType = colinfo.type === HeaderType.DATA_HEADER ? colinfo.parent.type : colinfo.type, datafield = "rows" === dataHeadersLocation ? rowinfo.value : colinfo.value, value = getCellValue(datafield || null, rowDimension, columnDimension, customFunctions.aggregation[datafield.id]), _this4 = _possibleConstructorReturn(this, (DataCell.__proto__ || Object.getPrototypeOf(DataCell)).call(this, {
                    axetype: null,
                    type: HeaderType.DATA_VALUE,
                    template: "cell-template-datavalue",
                    value: value,
                    cssclass: "cell " + HeaderType.getCellClass(rowType, colType),
                    isvisible: !0
                }));
                return _this4.rowDimension = rowDimension, _this4.columnDimension = columnDimension, 
                _this4.rowType = rowType, _this4.colType = colType, _this4.datafield = datafield, 
                _this4.datafield && customFunctions.format[datafield.id] ? _this4.caption = customFunctions.format[datafield.id](value) : _this4.caption = _this4.value, 
                _this4;
            }
            return _inherits(DataCell, _CellBase4), DataCell;
        }(CellBase), exports.ButtonCell = function(_CellBase5) {
            function ButtonCell(field) {
                return _classCallCheck(this, ButtonCell), _possibleConstructorReturn(this, (ButtonCell.__proto__ || Object.getPrototypeOf(ButtonCell)).call(this, {
                    axetype: null,
                    type: HeaderType.FIELD_BUTTON,
                    template: "cell-template-fieldbutton",
                    value: field,
                    cssclass: HeaderType.getHeaderClass(HeaderType.FIELD_BUTTON)
                }));
            }
            return _inherits(ButtonCell, _CellBase5), ButtonCell;
        }(CellBase), exports.EmptyCell = function(_CellBase6) {
            function EmptyCell(hspan, vspan) {
                return _classCallCheck(this, EmptyCell), _possibleConstructorReturn(this, (EmptyCell.__proto__ || Object.getPrototypeOf(EmptyCell)).call(this, {
                    axetype: null,
                    type: HeaderType.EMPTY,
                    template: "cell-template-empty",
                    value: null,
                    cssclass: HeaderType.getHeaderClass(HeaderType.EMPTY),
                    hspan: function(_hspan) {
                        function hspan() {
                            return _hspan.apply(this, arguments);
                        }
                        return hspan.toString = function() {
                            return _hspan.toString();
                        }, hspan;
                    }(function() {
                        return hspan;
                    }),
                    vspan: function(_vspan) {
                        function vspan() {
                            return _vspan.apply(this, arguments);
                        }
                        return vspan.toString = function() {
                            return _vspan.toString();
                        }, vspan;
                    }(function() {
                        return vspan;
                    })
                }));
            }
            return _inherits(EmptyCell, _CellBase6), EmptyCell;
        }(CellBase);
    }, /* 207 */
    /***/
    function(module, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        exports.MEASURE_ID = "__measures__", exports.TOTAL_ID = "__total__", exports.KEY_SEPARATOR = "-/-", 
        exports.SET_FIELDS = "SET_FIELDS", exports.SET_DATAFIELDS = "SET_DATAFIELDS", exports.SET_CONFIG_PROPERTY = "SET_CONFIG_PROPERTY", 
        exports.TOGGLE_DATAFIELD = "TOGGLE_DATAFIELD", exports.MOVE_FIELD = "MOVE_FIELD", 
        exports.PUSH_DATA = "PUSH_DATA", exports.SET_DATA = "SET_DATA", exports.CHANGE_SORT_ORDER = "CHANGE_SORT_ORDER", 
        exports.ADD_FILTER = "ADD_FILTER", exports.DELETE_FILTER = "DELETE_FILTER", exports.UPDATE_CELL_SIZE = "UPDATE_CELL_SIZE";
    }, /* 208 */
    /***/
    function(module, exports) {
        "use strict";
        // /**
        //  * Creates a namespcae hierarchy if not exists
        //  * @param  {string} identifier - namespace identifier
        //  * @return {object}
        //  */
        // export function ns(identifier, parent) {
        //   const parts = identifier.split('.');
        //   let i = 0;
        //   parent = parent || window;
        //   while (i < parts.length) {
        //     parent[parts[i]] = parent[parts[i]] || {};
        //     parent = parent[parts[i]];
        //     i += 1;
        //   }
        //   return parent;
        // }
        // /**
        //  * Returns an array of object own properties
        //  * @param  {Object} obj
        //  * @return {Array}
        //  */
        // export function ownProperties(obj) {
        //   const arr = [];
        //   for (const prop in obj) {
        //     if (obj.hasOwnProperty(prop)) {
        //       arr.push(prop);
        //     }
        //   }
        //   return arr;
        // }
        // /**
        //  * Iterates over the list object and executes the callback on each item
        //  * if the callback returns a value that can be evaluated ti true,
        //  * the iteration will stop.
        //  * @param  {Array} list - the list to iterate over
        //  * @param  {Function} callback - function to be called on each iteration.
        //  * It will receive as arguments: current item and current item index.
        //  * @param {Boolean} forceContinue - Do not stop if the callback return value is true.
        //  * @return {Array}
        //  */
        // export function forEach(list, callback, forceContinue) {
        //   let ret;
        //   if (list) {
        //     for (let i = 0, l = list.length; i < l; i += 1) {
        //       ret = callback(list[i], i);
        //       if (ret && forceContinue !== true) {
        //         break;
        //       }
        //     }
        //   }
        //   return ret;
        // }
        // /**
        //  * Returns whether or not obj is a javascript array.
        //  * @param  {object}  obj
        //  * @return {Boolean}
        //  */
        // export function isArray(obj) {
        //   return Object.prototype.toString.apply(obj) === '[object Array]';
        // }
        /**
	 * Returns whether or not obj is a number
	 * @param  {object}  obj
	 * @return {Boolean}
	 */
        function isNumber(obj) {
            return "[object Number]" === Object.prototype.toString.apply(obj);
        }
        /**
	 * Returns whether or not obj is a Date object.
	 * @param  {object}  obj
	 * @return {Boolean}
	 */
        function isDate(obj) {
            return "[object Date]" === Object.prototype.toString.apply(obj);
        }
        /**
	 * Returns whether or not obj is a string
	 * @param  {object}  obj
	 * @return {Boolean}
	 */
        function isString(obj) {
            return "[object String]" === Object.prototype.toString.apply(obj);
        }
        /**
	 * Returns whether or not obj is a regular expression object
	 * @param  {object}  obj
	 * @return {Boolean}
	 */
        function isRegExp(obj) {
            return "[object RegExp]" === Object.prototype.toString.apply(obj);
        }
        /**
	 * Returns whether or not obj is a function object
	 * @param  {object}  obj
	 * @return {Boolean}
	 */
        function isFunction(obj) {
            return "[object Function]" === Object.prototype.toString.apply(obj);
        }
        /**
	 * Escapes all RegExp special characters.
	 */
        function escapeRegex(re) {
            return re.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
        }
        /**
	 * Returns the first element in the array that satisfies the given predicate
	 * @param  {Array} array     the array to search
	 * @param  {function} predicate Function to apply to each element until it returns true
	 * @return {Object}           The first object in the array that satisfies the predicate or undefined.
	 */
        function findInArray(array, predicate) {
            if (this.isArray(array) && predicate) for (var i = 0; i < array.length; i += 1) {
                var item = array[i];
                if (predicate(item)) return item;
            }
        }
        /**
	//  * Returns a JSON string represenation of an object
	//  * @param {object} obj
	//  * @return {string}
	//  */
        // export function jsonStringify(obj, censorKeywords) {
        //   function censor(key, value) {
        //     return censorKeywords && censorKeywords.indexOf(key) > -1 ? undefined : value;
        //   }
        //   return JSON.stringify(obj, censor, 2);
        // }
        // export function addEventListener(element, eventName, handler) {
        //   if (element.addEventListener) {
        //     element.addEventListener(eventName, handler, false);
        //   } else if (element.attachEvent) {
        //     element.attachEvent(`on${eventName}`, handler);
        //   } else {
        //     element[`on${eventName}`] = handler;
        //   }
        // }
        // export function removeEventListener(element, eventName, handler) {
        //   if (element.removeEventListener) {
        //     element.removeEventListener(eventName, handler, false);
        //   } else if (element.detachEvent) {
        //     element.detachEvent(`on${eventName}`, handler);
        //   } else {
        //     element[`on${eventName}`] = null;
        //   }
        // }
        // export function preventDefault(e) {
        //   e = e || window.event;
        //
        //   if (e.preventDefault) {
        //     e.preventDefault();
        //   } else {
        //     e.returnValue = false;
        //   }
        // }
        // export function stopPropagation(e) {
        //   e = e || window.event;
        //
        //   if (e.stopPropagation) {
        //     e.stopPropagation();
        //   } else {
        //     e.cancelBubble = true;
        //   }
        // }
        // export function getEventButton(e) {
        //   const button = e.button;
        //   if ('which' in e) {
        //     return button;
        //   }
        //   // IE 8
        //   return button === 1 ? 0 // left
        //     : button === 4 ? 1 // middle
        //       : 2; // right
        // }
        // export function getMousePageXY(e) {
        //   e = e || window.event;
        //
        //   let pageX = e.pageX;
        //   let pageY = e.pageY;
        //   if (pageX === undefined) {
        //     pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        //     pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        //   }
        //   return {
        //     pageX,
        //     pageY,
        //   };
        // }
        // // from: https://github.com/davidchambers/Base64.js
        //
        // var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
        //
        // function InvalidCharacterError(message) {
        //     this.message = message
        // }
        // InvalidCharacterError.prototype = new Error()
        // InvalidCharacterError.prototype.name = 'InvalidCharacterError'
        // // encoder
        // // [https://gist.github.com/999166] by [https://github.com/nignag]
        // export function btoa(input) {
        //     if (window && window.btoa){
        //       return window.btoa(input)
        //     }
        //     else {
        //             var str = String(input)
        //             for (
        //                 // initialize result and counter
        //                 var block, charCode, idx = 0, map = chars, output = ''
        //                 // if the next str index does not exist:
        //                 // change the mapping table to "="
        //                 // check if d has no fractional digits
        //                 str.charAt(idx | 0) || (map = '=', idx % 1)
        //                 // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
        //                 output += map.charAt(63 & block >> 8 - idx % 1 * 8)
        //             ) {
        //                 charCode = str.charCodeAt(idx += 3 / 4)
        //                 if (charCode > 0xFF) {
        //                     throw new InvalidCharacterError("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.")
        //                 }
        //                 block = block << 8 | charCode
        //             }
        //             return output
        //         }
        // }
        //
        // // decoder
        // // [https://gist.github.com/1020396] by [https://github.com/atk]
        // export function atob(input){
        //  if (window && window.atob){
        //    return window.atob(input)
        //  }
        // else {
        //         var str = String(input).replace(/=+$/, '')
        //         if (str.length % 4 == 1) {
        //             throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.")
        //         }
        //         for (
        //             // initialize result and counters
        //             var bc = 0, bs, buffer, idx = 0, output = ''
        //             // get next character
        //             (buffer = str.charAt(idx += 1))
        //             // character found in table? initialize bit storage and add its ascii value
        //             ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
        //                 // and if not first of each 4 characters,
        //                 // convert the first 8 bits to one ascii character
        //                 bc += 1 % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
        //         ) {
        //             // try to find character in table (0-63, not found => -1)
        //             buffer = chars.indexOf(buffer)
        //         }
        //         return output
        //     }
        //   }
        //
        function twoArraysIntersect(arg0, arg1) {
            for (var n = arg0.length, m = arg1.length, i = 0, j = 0, res = []; i < n && j < m; ) arg0[i] > arg1[j] ? j += 1 : arg0[i] < arg1[j] ? i += 1 : (res.push(arg0[i]), 
            i += 1, j += 1);
            return res;
        }
        // export function arraysIntersect() {
        //   let n,
        //     len;
        //   const ret = [];
        //   const obj = {};
        //   const nOthers = arguments.length - 1;
        //   let nShortest = arguments[0].length;
        //   let shortest = 0;
        //   for (let i = 0; i <= nOthers; i += 1) {
        //     n = arguments[i].length;
        //     if (n < nShortest) {
        //       shortest = i;
        //       nShortest = n;
        //     }
        //   }
        //   for (let i = 0; i <= nOthers; i += 1) {
        //     n = (i === shortest) ? 0 : (i || shortest); // Read the shortest array first. Read the first array instead of the shortest
        //     len = arguments[n].length;
        //     for (let j = 0; j < len; j += 1) {
        //       const elem = arguments[n][j];
        //       if (obj[elem] === i - 1) {
        //         if (i === nOthers) {
        //           ret.push(elem);
        //           obj[elem] = 0;
        //         } else {
        //           obj[elem] = i;
        //         }
        //       } else if (i === 0) {
        //         obj[elem] = 0;
        //       }
        //     }
        //   }
        //   return ret;
        // }
        function isInRange(_ref, _ref2, _ref3) {
            var _ref6 = _slicedToArray(_ref, 2), columnIndex = _ref6[0], rowIndex = _ref6[1], _ref5 = _slicedToArray(_ref2, 2), columnIndexStart = _ref5[0], rowIndexStart = _ref5[1], _ref4 = _slicedToArray(_ref3, 2), columnIndexEnd = _ref4[0], rowIndexEnd = _ref4[1], inRows = !1;
            inRows = columnIndexStart <= columnIndexEnd ? columnIndexStart <= columnIndex && columnIndex <= columnIndexEnd : columnIndexEnd <= columnIndex && columnIndex <= columnIndexStart;
            var inColumns = !1;
            return inColumns = rowIndexStart <= rowIndexEnd ? rowIndexStart <= rowIndex && rowIndex <= rowIndexEnd : rowIndexEnd <= rowIndex && rowIndex <= rowIndexStart, 
            inRows && inColumns;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _slicedToArray = function() {
            function sliceIterator(arr, i) {
                var _arr = [], _n = !0, _d = !1, _e = void 0;
                try {
                    for (var _s, _i = arr[Symbol.iterator](); !(_n = (_s = _i.next()).done) && (_arr.push(_s.value), 
                    !i || _arr.length !== i); _n = !0) ;
                } catch (err) {
                    _d = !0, _e = err;
                } finally {
                    try {
                        !_n && _i.return && _i.return();
                    } finally {
                        if (_d) throw _e;
                    }
                }
                return _arr;
            }
            return function(arr, i) {
                if (Array.isArray(arr)) return arr;
                if (Symbol.iterator in Object(arr)) return sliceIterator(arr, i);
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            };
        }();
        exports.isNumber = isNumber, exports.isDate = isDate, exports.isString = isString, 
        exports.isRegExp = isRegExp, exports.isFunction = isFunction, exports.escapeRegex = escapeRegex, 
        exports.findInArray = findInArray, exports.twoArraysIntersect = twoArraysIntersect, 
        exports.isInRange = isInRange;
    }, /* 209 */
    /***/
    function(module, exports) {
        "use strict";
        function scrollbarSize(recalc) {
            if ((!calculatedScrollBarSize || recalc) && inDOM) {
                var inner = document.createElement("p");
                inner.style.width = "100% !important", inner.style.height = "200px !important";
                var outer = document.createElement("div");
                outer.style.position = "absolute !important", outer.style.top = "0px !important", 
                outer.style.left = "0px !important", outer.style.visibility = "hidden !important", 
                outer.style.width = "200px !important", outer.style.height = "150px !important", 
                outer.style.overflow = "hidden !important", outer.appendChild(inner), document.body.appendChild(outer);
                var w1 = inner.offsetWidth;
                outer.style.overflow = "scroll";
                var w2 = inner.offsetWidth;
                w1 === w2 && (w2 = outer.clientWidth), document.body.removeChild(outer), calculatedScrollBarSize = w1 - w2;
            }
            return calculatedScrollBarSize || 0;
        }
        function removeClass(element, classname) {
            if (element && classname) for (;element.className.indexOf(classname) >= 0; ) element.className = element.className.replace(classname, "");
        }
        function addClass(element, classname) {
            element && classname && element.className.indexOf(classname) < 0 && (element.className += " " + classname);
        }
        function getOffset(element) {
            if (element) {
                var rect = element.getBoundingClientRect();
                return {
                    x: rect.left,
                    y: rect.top
                };
            }
            return {
                x: 0,
                y: 0
            };
        }
        function getParentOffset(element) {
            if (element) {
                var rect = element.getBoundingClientRect(), rectParent = null != element.parentNode ? element.parentNode.getBoundingClientRect() : {
                    top: 0,
                    left: 0
                };
                return {
                    x: rect.left - rectParent.left,
                    y: rect.top - rectParent.top
                };
            }
            return {
                x: 0,
                y: 0
            };
        }
        function getSize(element) {
            if (element) {
                var rect = element.getBoundingClientRect();
                return {
                    width: rect.right - rect.left,
                    height: rect.bottom - rect.top
                };
            }
            return {
                width: 0,
                height: 0
            };
        }
        function replaceHyphenByUcase(val) {
            return val.replace(reHyphenToUcase, function(m, m1) {
                return m1.toUpperCase();
            });
        }
        function getStyle(element, styleProps, keepString) {
            var values = [];
            return element && styleProps && !function() {
                var currStyle = void 0, f = void 0, fixProp = void 0;
                element.currentStyle ? (currStyle = element.currentStyle, f = function(prop) {
                    return currStyle[prop];
                }, fixProp = !0) : window && window.getComputedStyle && (currStyle = window.getComputedStyle(element, null), 
                f = function(prop) {
                    return currStyle.getPropertyValue(prop);
                });
                for (var i = 0; i < styleProps.length; i += 1) {
                    var val = f(fixProp ? replaceHyphenByUcase(styleProps[i]) : styleProps[i]);
                    values.push(val && keepString !== !0 ? Math.ceil(parseFloat(val)) : val);
                }
            }(), values;
        }
        function isVisible(element) {
            return !!element && ("none" !== element.style.display && (0 !== element.offsetWidth || 0 !== element.offsetHeight));
        }
        function updateTableColGroup(tableNode, widths) {
            if (tableNode) {
                var colGroupNode = tableNode.firstChild;
                if (colGroupNode && "COLGROUP" === colGroupNode.nodeName) {
                    for (tableNode.style.tableLayout = "auto", tableNode.style.width = ""; colGroupNode.firstChild; ) colGroupNode.removeChild(colGroupNode.firstChild);
                    for (var i = 0; i < widths.length; i += 1) {
                        var col = document.createElement("col");
                        col.style.width = widths[i] + "px", colGroupNode.appendChild(col);
                    }
                    tableNode.style.tableLayout = "fixed";
                }
            }
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.scrollbarSize = scrollbarSize, exports.removeClass = removeClass, exports.addClass = addClass, 
        exports.getOffset = getOffset, exports.getParentOffset = getParentOffset, exports.getSize = getSize, 
        exports.getStyle = getStyle, exports.isVisible = isVisible, exports.updateTableColGroup = updateTableColGroup;
        var calculatedScrollBarSize = void 0, inDOM = !("undefined" == typeof window || !window.document || !window.document.createElement), reHyphenToUcase = /\-(\w)/g;
    }, /* 210 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function pass(filter, value) {
            if (Array.isArray(filter.staticValue)) {
                var found = filter.staticValue.includes(value);
                return filter.excludeStatic && !found || !filter.excludeStatic && found;
            }
            return filter.term ? filter.operator.func(value, filter.term) : filter.staticValue === !0 || filter.staticValue === ALL || filter.staticValue !== !1 && filter.staticValue !== NONE;
        }
        function expressionFilter(fieldId, operator, term, staticValue, excludeStatic) {
            var expressionFilter = {
                fieldId: fieldId,
                regexpMode: !1,
                operator: Operators.get(operator),
                staticValue: staticValue,
                excludeStatic: excludeStatic
            };
            return term && operator && operator.regexpSupported && (0, _generic.isRegExp)(term) && (expressionFilter.regexpMode = !0, 
            term.ignoreCase || (expressionFilter.term = new RegExp(term.source, "i"))), expressionFilter;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.Operators = exports.BLANK = exports.NONE = exports.ALL = void 0, exports.pass = pass, 
        exports.expressionFilter = expressionFilter;
        var _generic = __webpack_require__(208), ALL = exports.ALL = "#All#", NONE = exports.NONE = "#None#", Operators = (exports.BLANK = '#Blank#"', 
        exports.Operators = {
            get: function(opname) {
                switch (opname) {
                  case this.MATCH.name:
                    return this.MATCH;

                  case this.NOTMATCH.name:
                    return this.NOTMATCH;

                  case this.EQ.name:
                    return this.EQ;

                  case this.NEQ.name:
                    return this.NEQ;

                  case this.GT.name:
                    return this.GT;

                  case this.GTE.name:
                    return this.GTE;

                  case this.LT.name:
                    return this.LT;

                  case this.LTE.name:
                    return this.LTE;

                  default:
                    return this.NONE;
                }
            },
            NONE: null,
            MATCH: {
                name: "Matches",
                func: function(value, term) {
                    return value ? value.toString().search((0, _generic.isRegExp)(term) ? term : new RegExp(term, "i")) >= 0 : !term;
                },
                regexpSupported: !0
            },
            NOTMATCH: {
                name: "Does Not Match",
                func: function(value, term) {
                    return value ? value.toString().search((0, _generic.isRegExp)(term) ? term : new RegExp(term, "i")) < 0 : !!term;
                },
                regexpSupported: !0
            },
            EQ: {
                name: "=",
                func: function(value, term) {
                    return value === term;
                },
                regexpSupported: !1
            },
            NEQ: {
                name: "<>",
                func: function(value, term) {
                    return value !== term;
                },
                regexpSupported: !1
            },
            GT: {
                name: ">",
                func: function(value, term) {
                    return value > term;
                },
                regexpSupported: !1
            },
            GTE: {
                name: ">=",
                func: function(value, term) {
                    return value >= term;
                },
                regexpSupported: !1
            },
            LT: {
                name: "<",
                func: function(value, term) {
                    return value < term;
                },
                regexpSupported: !1
            },
            LTE: {
                name: "<=",
                func: function(value, term) {
                    return value <= term;
                },
                regexpSupported: !1
            }
        });
    }, /* 211 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _DataCells = __webpack_require__(212);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_DataCells).default;
            }
        });
    }, /* 212 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _slicedToArray = function() {
            function sliceIterator(arr, i) {
                var _arr = [], _n = !0, _d = !1, _e = void 0;
                try {
                    for (var _s, _i = arr[Symbol.iterator](); !(_n = (_s = _i.next()).done) && (_arr.push(_s.value), 
                    !i || _arr.length !== i); _n = !0) ;
                } catch (err) {
                    _d = !0, _e = err;
                } finally {
                    try {
                        !_n && _i.return && _i.return();
                    } finally {
                        if (_d) throw _e;
                    }
                }
                return _arr;
            }
            return function(arr, i) {
                if (Array.isArray(arr)) return arr;
                if (Symbol.iterator in Object(arr)) return sliceIterator(arr, i);
                throw new TypeError("Invalid attempt to destructure non-iterable instance");
            };
        }(), _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactDom = __webpack_require__(40), _reactVirtualized = __webpack_require__(29), _generic = __webpack_require__(208), _Cells = __webpack_require__(206), _DataCell = __webpack_require__(213), _DataCell2 = _interopRequireDefault(_DataCell), DataCells = function(_PureComponent) {
            function DataCells(props) {
                _classCallCheck(this, DataCells);
                var _this = _possibleConstructorReturn(this, (DataCells.__proto__ || Object.getPrototypeOf(DataCells)).call(this, props));
                return _this.cellRenderer = _this.cellRenderer.bind(_this), _this.handleCopy = _this.handleCopy.bind(_this), 
                _this.handleMouseDown = _this.handleMouseDown.bind(_this), _this.handleMouseUp = _this.handleMouseUp.bind(_this), 
                _this.handleMouseOver = _this.handleMouseOver.bind(_this), _this.handleKeyDown = _this.handleKeyDown.bind(_this), 
                _this.handleDocumentMouseDown = _this.handleDocumentMouseDown.bind(_this), _this.state = {
                    cellsCache: {},
                    selectedCellStart: null,
                    selectedCellEnd: null
                }, _this;
            }
            return _inherits(DataCells, _PureComponent), _createClass(DataCells, [ {
                key: "componentDidMount",
                value: function() {
                    document.addEventListener("mouseup", this.handleMouseUp), document.addEventListener("mousedown", this.handleDocumentMouseDown), 
                    document.addEventListener("keydown", this.handleKeyDown), document.addEventListener("copy", this.handleCopy);
                }
            }, {
                key: "componentWillReceiveProps",
                value: function() {
                    this.setState({
                        cellsCache: this.datacellsCache
                    });
                }
            }, {
                key: "componentWillUpdate",
                value: function() {
                    this.isUpdating = !0;
                }
            }, {
                key: "componentDidUpdate",
                value: function() {
                    this.isUpdating = !1, this.grid.recomputeGridSize();
                }
            }, {
                key: "componentDidUnMount",
                value: function() {
                    document.removeEventListener("mouseup", this.handleMouseUp), document.removeEventListener("mousedown", this.handleDocumentMouseDown), 
                    document.removeEventListener("keydown", this.handleKeyDown), document.removeEventListener("copy", this.handleCopy);
                }
            }, {
                key: "handleMouseDown",
                value: function(e, _ref) {
                    var _ref2 = _slicedToArray(_ref, 2), columnIndex = _ref2[0], rowIndex = _ref2[1];
                    0 === e.button && (this.isMouseDown = !0, this.setState({
                        selectedCellStart: [ columnIndex, rowIndex ]
                    }), this.setState({
                        selectedCellEnd: [ columnIndex, rowIndex ]
                    }));
                }
            }, {
                key: "handleMouseUp",
                value: function() {
                    this.isMouseDown = !1;
                }
            }, {
                key: "handleMouseOver",
                value: function(_ref3) {
                    var _ref4 = _slicedToArray(_ref3, 2), columnIndex = _ref4[0], rowIndex = _ref4[1];
                    this.isMouseDown && this.setState({
                        selectedCellEnd: [ columnIndex, rowIndex ]
                    });
                }
            }, {
                key: "handleDocumentMouseDown",
                value: function(e) {
                    0 === e.button && this.state.selectedCellStart && (this.isMouseDown || this.setState({
                        selectedCellStart: null,
                        selectedCellEnd: null
                    }));
                }
            }, {
                key: "handleKeyDown",
                value: function(e) {
                    var _props = this.props, columnHeaders = _props.columnHeaders, rowHeaders = _props.rowHeaders;
                    69 === e.which && (this.perf ? (this.perf = !1, window.Perf.stop()) : (window.Perf.start(), 
                    this.perf = !0)), 65 === e.which && (e.metaKey || e.ctrlKey) && (// Works only if the data cells are focused
                    // Later we could make it work if any part of the grid
                    // (row and columns headers...) are focused
                    (0, _reactDom.findDOMNode)(this.grid) === e.target && this.setState({
                        selectedCellStart: [ 0, 0 ],
                        selectedCellEnd: [ columnHeaders.length, rowHeaders.length ]
                    }), e.preventDefault());
                }
            }, {
                key: "handleCopy",
                value: function() {
                    if (// Works only if the data cells are focused
                    // Later we could make it work if any part of the grid
                    // (row and columns headers...) are focused
                    (0, _reactDom.findDOMNode)(this.grid) === document.activeElement) {
                        var _state = this.state, selectedCellStart = _state.selectedCellStart, selectedCellEnd = _state.selectedCellEnd;
                        this.props.copy({
                            selectedCellStart: selectedCellStart,
                            selectedCellEnd: selectedCellEnd
                        });
                    }
                }
            }, {
                key: "cellRenderer",
                value: function(_ref5) {
                    var columnIndex = _ref5.columnIndex, key = _ref5.key, rowIndex = _ref5.rowIndex, position = _ref5.style, _state2 = this.state, selectedCellStart = _state2.selectedCellStart, selectedCellEnd = _state2.selectedCellEnd, _props2 = this.props, drilldown = _props2.drilldown, getCellValue = _props2.getCellValue, dataHeadersLocation = _props2.dataHeadersLocation, customFunctions = _props2.customFunctions, _props3 = this.props, rowHeaders = _props3.rowHeaders, columnHeaders = _props3.columnHeaders, rowHeaderRow = rowHeaders[rowIndex], rowHeader = rowHeaderRow[rowHeaderRow.length - 1], columnHeaderColumn = columnHeaders[columnIndex], columnHeader = columnHeaderColumn[columnHeaderColumn.length - 1], selected = !1;
                    selectedCellStart && selectedCellEnd && (selected = (0, _generic.isInRange)([ columnIndex, rowIndex ], selectedCellStart, selectedCellEnd));
                    var cell = new _Cells.DataCell(getCellValue, dataHeadersLocation, rowHeader, columnHeader, customFunctions), cellKey = rowHeader.key + "-//-" + columnHeader.key;
                    this.datacellsCache[cellKey] = cell.value;
                    var valueHasChanged = !1;
                    if (this.isUpdating) {
                        var oldcell = this.state.cellsCache[cellKey];
                        void 0 !== oldcell && cell.value !== oldcell && (valueHasChanged = !0);
                    }
                    return _react2.default.createElement(_DataCell2.default, {
                        key: key,
                        valueHasChanged: valueHasChanged,
                        position: position,
                        rowIndex: rowIndex,
                        columnIndex: columnIndex,
                        cell: cell,
                        drilldown: drilldown,
                        handleMouseDown: this.handleMouseDown,
                        handleMouseOver: this.handleMouseOver,
                        selected: selected
                    });
                }
            }, {
                key: "render",
                value: function() {
                    var _this2 = this, _props4 = this.props, getColumnWidth = _props4.getColumnWidth, getRowHeight = _props4.getRowHeight, onScroll = _props4.onScroll, columnCount = _props4.columnCount, rowCount = _props4.rowCount, height = _props4.height, width = _props4.width, scrollToColumn = _props4.scrollToColumn, scrollToRow = _props4.scrollToRow, onSectionRendered = _props4.onSectionRendered, zoom = _props4.zoom;
                    return this.datacellsCache = {}, _react2.default.createElement(_reactVirtualized.Grid, {
                        cellRenderer: this.cellRenderer,
                        className: "orb-data-cells",
                        columnCount: columnCount,
                        columnWidth: getColumnWidth,
                        height: height,
                        onScroll: onScroll,
                        ref: function(_ref6) {
                            _this2.grid = _ref6;
                        },
                        rowCount: rowCount,
                        rowHeight: getRowHeight,
                        scrollToAlignment: "start",
                        onSectionRendered: onSectionRendered,
                        scrollToColumn: scrollToColumn,
                        scrollToRow: scrollToRow,
                        style: {
                            fontSize: 100 * zoom + "%"
                        },
                        width: width
                    });
                }
            } ]), DataCells;
        }(_react.PureComponent);
        exports.default = DataCells;
    }, /* 213 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _classnames = __webpack_require__(45), _classnames2 = _interopRequireDefault(_classnames), DataCell = function(_PureComponent) {
            function DataCell() {
                _classCallCheck(this, DataCell);
                var _this = _possibleConstructorReturn(this, (DataCell.__proto__ || Object.getPrototypeOf(DataCell)).call(this));
                return _this.handleDoubleClick = _this.handleDoubleClick.bind(_this), _this.handleMouseDown = _this.handleMouseDown.bind(_this), 
                _this.handleMouseOver = _this.handleMouseOver.bind(_this), _this;
            }
            return _inherits(DataCell, _PureComponent), _createClass(DataCell, [ {
                key: "handleMouseDown",
                value: function(e) {
                    this.props.handleMouseDown(e, [ this.props.columnIndex, this.props.rowIndex ]);
                }
            }, {
                key: "handleMouseOver",
                value: function() {
                    this.props.handleMouseOver([ this.props.columnIndex, this.props.rowIndex ]);
                }
            }, {
                key: "handleDoubleClick",
                value: function() {
                    this.props.drilldown(this.props.cell);
                }
            }, {
                key: "render",
                value: function() {
                    var _props = this.props, cell = _props.cell, position = _props.position, rowIndex = _props.rowIndex, selected = _props.selected, valueHasChanged = _props.valueHasChanged, style = {
                        boxSizing: "border-box",
                        overflow: "hidden"
                    }, className = (0, _classnames2.default)("orb-cell", "orb-data-cell", {
                        "orb-data-cell-even": !(rowIndex % 2),
                        "orb-data-cell-uneven": rowIndex % 2,
                        "orb-data-cell-highlighted": valueHasChanged,
                        "orb-data-cell-selected": selected
                    });
                    return _react2.default.createElement("div", {
                        className: className,
                        style: _extends({}, style, position),
                        onMouseDown: this.handleMouseDown,
                        onMouseOver: this.handleMouseOver,
                        onDoubleClick: this.handleDoubleClick
                    }, cell.caption);
                }
            } ]), DataCell;
        }(_react.PureComponent);
        exports.default = DataCell;
    }, /* 214 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function replaceNullAndUndefined(val) {
            return null === val || void 0 === val ? "" : val;
        }
        function getSelectedText(_ref) {
            for (var selectedCellStart = _ref.selectedCellStart, selectedCellEnd = _ref.selectedCellEnd, dataHeadersLocation = _ref.dataHeadersLocation, getCellValue = _ref.getCellValue, columnHeaders = _ref.columnHeaders, rowHeaders = _ref.rowHeaders, columnDimensionHeaders = _ref.columnDimensionHeaders, rowDimensionHeaders = _ref.rowDimensionHeaders, customFunctions = _ref.customFunctions, rowsRange = [ Math.min(selectedCellStart[1], selectedCellEnd[1]), Math.max(selectedCellStart[1], selectedCellEnd[1]) + 1 ], rowHeaderLeafs = rowHeaders.slice.apply(rowHeaders, rowsRange).map(function(headers) {
                return headers[headers.length - 1];
            }), rows = rowHeaderLeafs.map(function(header) {
                for (var res = [], currentHeader = header; currentHeader; ) res.unshift(currentHeader.caption), 
                currentHeader = currentHeader.parent;
                return res;
            }), columnsRange = [ Math.min(selectedCellStart[0], selectedCellEnd[0]), Math.max(selectedCellStart[0], selectedCellEnd[0]) + 1 ], columnHeaderLeafs = columnHeaders.slice.apply(columnHeaders, columnsRange).map(function(headers) {
                return headers[headers.length - 1];
            }), columns = columnHeaderLeafs.map(function(header) {
                for (var res = [], currentHeader = header; currentHeader; ) res.unshift(currentHeader.caption), 
                currentHeader = currentHeader.parent;
                return res;
            }), cells = rowHeaderLeafs.map(function(rowHeader) {
                return columnHeaderLeafs.map(function(columnHeader) {
                    return new _Cells.DataCell(getCellValue, !0, rowHeader, columnHeader, customFunctions).caption;
                });
            }), rowDimensions = rowDimensionHeaders.map(function(header) {
                return header.value.caption;
            }), columnDimensions = columnDimensionHeaders.map(function(header) {
                return header.value.caption;
            }), output = "", depth = columns[0].length, width = rows[0].length, _loop = function(y) {
                for (var _x2 = 0; _x2 < width; _x2 += 1) output += _x2 === width - 1 && y < depth - 1 ? replaceNullAndUndefined(columnDimensions[y]) + "\t" : y === depth - 1 && _x2 < width - 1 ? replaceNullAndUndefined(rowDimensions[_x2]) + "\t" : y === depth - 1 && _x2 === width - 1 ? // Handle corner case
                // Dimension header in bottom right cell can refer to a column header
                // or a row header depending on data headers location
                "columns" === dataHeadersLocation ? replaceNullAndUndefined(rowDimensions[_x2]) + "\t" : replaceNullAndUndefined(columnDimensions[y]) + "\t" : "\t";
                output = columns.reduce(function(accumulator, column) {
                    return "" + accumulator + replaceNullAndUndefined(column[y]) + "\t";
                }, output), output = output.slice(0, -1), output += "\n";
            }, y = 0; y < depth; y += 1) _loop(y);
            // Other rows with rows headers and data
            for (var _y = 0; _y < rows.length; _y += 1) {
                for (var x = 0; x < width; x += 1) output += replaceNullAndUndefined(rows[_y][x]) + "\t";
                for (var _x = 0; _x < columnHeaderLeafs.length; _x += 1) output += replaceNullAndUndefined(cells[_y][_x]) + "\t";
                output = output.slice(0, -1), output += "\n";
            }
            return output = output.slice(0, -1);
        }
        function copy(_ref2) {
            var selectedCellStart = _ref2.selectedCellStart, selectedCellEnd = _ref2.selectedCellEnd, rowHeaders = _ref2.rowHeaders, columnHeaders = _ref2.columnHeaders, getCellValue = _ref2.getCellValue, dataHeadersLocation = _ref2.dataHeadersLocation, columnDimensionHeaders = _ref2.columnDimensionHeaders, rowDimensionHeaders = _ref2.rowDimensionHeaders, customFunctions = _ref2.customFunctions;
            try {
                !function() {
                    var bodyElement = document.getElementsByTagName("body")[0], clipboardTextArea = document.createElement("textarea");
                    clipboardTextArea.style.position = "absolute", clipboardTextArea.style.left = "-10000px", 
                    bodyElement.appendChild(clipboardTextArea), clipboardTextArea.innerHTML = getSelectedText({
                        selectedCellStart: selectedCellStart,
                        selectedCellEnd: selectedCellEnd,
                        getCellValue: getCellValue,
                        rowHeaders: rowHeaders,
                        columnHeaders: columnHeaders,
                        dataHeadersLocation: dataHeadersLocation,
                        columnDimensionHeaders: columnDimensionHeaders,
                        rowDimensionHeaders: rowDimensionHeaders,
                        customFunctions: customFunctions
                    }), clipboardTextArea.select(), window.setTimeout(function() {
                        bodyElement.removeChild(clipboardTextArea);
                    }, 0);
                }();
            } catch (error) {
                console.error("error during copy", error);
            }
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = copy;
        var _Cells = __webpack_require__(206);
    }, /* 215 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _reactRedux = __webpack_require__(2), _selectors = __webpack_require__(201), _DimensionHeaders = __webpack_require__(216), _DimensionHeaders2 = _interopRequireDefault(_DimensionHeaders), mapStateToProps = function(state) {
            return {
                columnDimensionHeaders: (0, _selectors.getColumnUiAxis)(state).dimensionHeaders,
                columns: (0, _selectors.getColumnAxis)(state),
                dataHeadersLocation: state.config.dataHeadersLocation,
                dimensionPositions: (0, _selectors.getDimensionPositions)(state),
                getDimensionSize: (0, _selectors.getDimensionSize)(state),
                height: (0, _selectors.getHeaderSizes)(state).columnHeadersHeight,
                previewSizes: (0, _selectors.getPreviewSizes)(state),
                rowDimensionHeaders: (0, _selectors.getRowUiAxis)(state).dimensionHeaders,
                rows: (0, _selectors.getRowAxis)(state),
                width: (0, _selectors.getHeaderSizes)(state).rowHeadersWidth,
                zoom: state.config.zoom
            };
        };
        exports.default = (0, _reactRedux.connect)(mapStateToProps)(_DimensionHeaders2.default);
    }, /* 216 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _DimensionHeaders = __webpack_require__(217);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_DimensionHeaders).default;
            }
        });
    }, /* 217 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _Axis = __webpack_require__(203), _constants = __webpack_require__(207), _DimensionHeader = __webpack_require__(218), _DimensionHeader2 = _interopRequireDefault(_DimensionHeader), DimensionHeaders = function(_PureComponent) {
            function DimensionHeaders() {
                return _classCallCheck(this, DimensionHeaders), _possibleConstructorReturn(this, (DimensionHeaders.__proto__ || Object.getPrototypeOf(DimensionHeaders)).apply(this, arguments));
            }
            return _inherits(DimensionHeaders, _PureComponent), _createClass(DimensionHeaders, [ {
                key: "render",
                value: function() {
                    var _props = this.props, columnDimensionHeaders = _props.columnDimensionHeaders, columns = _props.columns, dataHeadersLocation = _props.dataHeadersLocation, dimensionPositions = _props.dimensionPositions, getDimensionSize = _props.getDimensionSize, height = _props.height, previewSizes = _props.previewSizes, rowDimensionHeaders = _props.rowDimensionHeaders, rows = _props.rows, width = _props.width, zoom = _props.zoom, headers = [], fieldWhoseWidthToGet = void 0;
                    // Dimension headers are on top of the measures column
                    fieldWhoseWidthToGet = "rows" === dataHeadersLocation ? _constants.MEASURE_ID : rows.fields.length ? rows.fields[rows.fields.length - 1].id : null;
                    var headerWidth = getDimensionSize(_Axis.AxisType.ROWS, fieldWhoseWidthToGet);
                    headers.push.apply(headers, _toConsumableArray(columnDimensionHeaders.map(function(dimensionHeader) {
                        var field = dimensionHeader.value, top = dimensionPositions.columns[field.id], headerHeight = getDimensionSize(_Axis.AxisType.COLUMNS, field.id);
                        return _react2.default.createElement(_DimensionHeader2.default, {
                            key: "dimension-header-" + field.id,
                            left: width - headerWidth,
                            top: top,
                            width: headerWidth,
                            height: headerHeight,
                            field: field,
                            mainDirection: "right",
                            crossFieldId: fieldWhoseWidthToGet,
                            previewSizes: previewSizes
                        });
                    })));
                    // Get height for row dimension headers in different cases
                    var fieldWhoseHeightToGet = void 0;
                    // Dimension headers are to the left of the measures row
                    fieldWhoseHeightToGet = "columns" === dataHeadersLocation ? _constants.MEASURE_ID : columns.fields.length ? columns.fields[columns.fields.length - 1].id : null;
                    var headerHeight = getDimensionSize(_Axis.AxisType.COLUMNS, fieldWhoseHeightToGet);
                    // Putting position as relative here allows its children (the dimension headers)
                    // to be absolutely positioned relatively to their parent
                    return headers.push.apply(headers, _toConsumableArray(rowDimensionHeaders.map(function(dimensionHeader) {
                        var field = dimensionHeader.value, left = dimensionPositions.rows[field.id], headerWidth = getDimensionSize(_Axis.AxisType.ROWS, field.id);
                        return _react2.default.createElement(_DimensionHeader2.default, {
                            top: height - headerHeight,
                            crossFieldId: fieldWhoseHeightToGet,
                            field: field,
                            height: headerHeight,
                            key: "dimension-header-" + field.id,
                            left: left,
                            mainDirection: "down",
                            previewSizes: previewSizes,
                            width: headerWidth
                        });
                    }))), _react2.default.createElement("div", {
                        style: {
                            position: "relative",
                            height: height,
                            width: width,
                            fontSize: 100 * zoom + "%",
                            overflow: "hidden"
                        },
                        className: "orb-dimension-headers"
                    }, headers);
                }
            } ]), DimensionHeaders;
        }(_react.PureComponent);
        exports.default = DimensionHeaders;
    }, /* 218 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _DimensionHeader = __webpack_require__(219);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_DimensionHeader).default;
            }
        });
    }, /* 219 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _Axis = __webpack_require__(203), _ResizeHandle = __webpack_require__(220), _ResizeHandle2 = _interopRequireDefault(_ResizeHandle), DimensionHeader = function(_ref) {
            var field = _ref.field, left = _ref.left, top = _ref.top, height = _ref.height, width = _ref.width, crossFieldId = _ref.crossFieldId, mainDirection = _ref.mainDirection, previewSizes = _ref.previewSizes, ids = {};
            return "down" === mainDirection ? (ids.right = field.id, ids.bottom = crossFieldId) : (ids.bottom = field.id, 
            ids.right = crossFieldId), _react2.default.createElement("div", {
                key: "fixed-dim-" + field.id,
                className: "orb-cell orb-dimension-header",
                style: {
                    position: "absolute",
                    left: left,
                    top: top,
                    width: width,
                    height: height,
                    zIndex: 3,
                    // border: 'lightgrey 0.1em solid',
                    boxSizing: "border-box",
                    // textAlign: 'left',
                    display: "flex"
                }
            }, _react2.default.createElement("span", {
                className: "orb-dimension-header-inner"
            }, field.caption), _react2.default.createElement(_ResizeHandle2.default, {
                position: "right",
                size: height,
                id: ids.right,
                isOnDimensionHeader: !0,
                axis: _Axis.AxisType.ROWS,
                previewSize: previewSizes.height,
                previewOffset: top
            }), _react2.default.createElement(_ResizeHandle2.default, {
                position: "bottom",
                size: width,
                id: ids.bottom,
                isOnDimensionHeader: !0,
                axis: _Axis.AxisType.COLUMNS,
                previewSize: previewSizes.width,
                previewOffset: left
            }));
        };
        exports.default = DimensionHeader;
    }, /* 220 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _ResizeHandle = __webpack_require__(221);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_ResizeHandle).default;
            }
        });
    }, /* 221 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactDnd = __webpack_require__(81), resizeHandleSpec = {
            beginDrag: function(props) {
                return {
                    id: props.id,
                    axis: props.axis,
                    position: props.position,
                    leafSubheaders: props.leafSubheaders,
                    previewSize: props.previewSize,
                    previewOffset: props.previewOffset
                };
            }
        }, sourceCollect = function(connect, monitor) {
            return {
                connectDragSource: connect.dragSource(),
                connectDragPreview: connect.dragPreview(),
                isDragging: monitor.isDragging()
            };
        }, ResizeHandle = function(_ref) {
            var position = _ref.position, size = _ref.size, connectDragSource = _ref.connectDragSource, handle = void 0;
            return handle = "right" === position ? _react2.default.createElement("div", {
                style: {
                    position: "absolute",
                    right: 0,
                    width: 4,
                    height: size,
                    cursor: "col-resize",
                    opacity: 0
                }
            }) : "bottom" === position ? _react2.default.createElement("div", {
                style: {
                    position: "absolute",
                    bottom: 0,
                    height: 4,
                    width: size,
                    cursor: "row-resize",
                    opacity: 0
                }
            }) : null, connectDragSource(handle);
        };
        exports.default = (0, _reactDnd.DragSource)("cell-resize-handle", resizeHandleSpec, sourceCollect)(ResizeHandle);
    }, /* 222 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _reactRedux = __webpack_require__(2), _Axis = __webpack_require__(203), _selectors = __webpack_require__(201), _ColumnHeaders = __webpack_require__(223), _ColumnHeaders2 = _interopRequireDefault(_ColumnHeaders), mapStateToProps = function(state) {
            return {
                columnCount: (0, _selectors.getLayout)(state).columnHorizontalCount,
                columnHeaders: (0, _selectors.getColumnUiAxis)(state).headers,
                dimensionPositions: (0, _selectors.getDimensionPositions)(state),
                getColumnWidth: (0, _selectors.getColumnWidth)(state),
                getDimensionSize: (0, _selectors.getDimensionSize)(state),
                getLastChildSize: (0, _selectors.getLastChildSize)(state),
                getRowHeight: function(_ref) {
                    var index = _ref.index;
                    return (0, _selectors.getDimensionSize)(state)(_Axis.AxisType.COLUMNS, state.axis.columns[index]);
                },
                height: (0, _selectors.getHeaderSizes)(state).columnHeadersHeight,
                previewSizes: (0, _selectors.getPreviewSizes)(state),
                rowCount: (0, _selectors.getLayout)(state).columnVerticalCount,
                rowHeadersWidth: (0, _selectors.getHeaderSizes)(state).rowHeadersWidth,
                width: (0, _selectors.getColumnHeadersVisibleWidth)(state),
                zoom: state.config.zoom
            };
        };
        exports.default = (0, _reactRedux.connect)(mapStateToProps)(_ColumnHeaders2.default);
    }, /* 223 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _ColumnHeaders = __webpack_require__(224);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_ColumnHeaders).default;
            }
        });
    }, /* 224 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactVirtualized = __webpack_require__(29), _Axis = __webpack_require__(203), _constants = __webpack_require__(207), _Header = __webpack_require__(225), _Header2 = _interopRequireDefault(_Header), _headerSize = __webpack_require__(227), _headerSize2 = _interopRequireDefault(_headerSize), ColumnHeaders = function(_PureComponent) {
            function ColumnHeaders() {
                _classCallCheck(this, ColumnHeaders);
                var _this = _possibleConstructorReturn(this, (ColumnHeaders.__proto__ || Object.getPrototypeOf(ColumnHeaders)).call(this));
                return _this.columnHeadersRenderer = _this.columnHeadersRenderer.bind(_this), _this;
            }
            return _inherits(ColumnHeaders, _PureComponent), _createClass(ColumnHeaders, [ {
                key: "componentDidUpdate",
                value: function() {
                    this.grid.recomputeGridSize();
                }
            }, {
                key: "columnHeadersRenderer",
                value: function(_ref) {
                    var columnSizeAndPositionManager = _ref.columnSizeAndPositionManager, columnStartIndex = _ref.columnStartIndex, columnStopIndex = _ref.columnStopIndex, horizontalOffsetAdjustment = _ref.horizontalOffsetAdjustment, scrollLeft = _ref.scrollLeft, _props = this.props, columnHeaders = _props.columnHeaders, dimensionPositions = _props.dimensionPositions, getDimensionSize = _props.getDimensionSize, getLastChildSize = _props.getLastChildSize, previewSizes = _props.previewSizes, rowCount = _props.rowCount, rowHeadersWidth = _props.rowHeadersWidth, renderedCells = [], correctColumnStopIndex = Math.min(columnStopIndex, columnHeaders.length - 1);
                    // Render fixed header rows
                    // Render big cells on top of current cells if necessary
                    // The check on the presence of the header is necessary
                    // because it can be out of bounds when the headers array is modified
                    if (columnHeaders[columnStartIndex] && columnHeaders[columnStartIndex].length < rowCount) for (var header = columnHeaders[columnStartIndex][0]; header.parent; ) {
                        header = header.parent;
                        var main = columnSizeAndPositionManager.getSizeAndPositionOfCell(header.x), left = main.offset + horizontalOffsetAdjustment, span = header.hspan(), width = (0, 
                        _headerSize2.default)(columnSizeAndPositionManager, header.x, span), top = 0 + dimensionPositions.columns[header.dim.field.id], height = getDimensionSize(_Axis.AxisType.COLUMNS, header.dim.field.id), positionStyle = {
                            position: "absolute",
                            left: left,
                            top: top,
                            height: height,
                            width: width
                        }, previewOffsets = {};
                        previewOffsets.right = top - 0, previewOffsets.bottom = left - scrollLeft + rowHeadersWidth, 
                        renderedCells.push(_react2.default.createElement(_Header2.default, {
                            key: "header-" + header.key,
                            axis: _Axis.AxisType.COLUMNS,
                            header: header,
                            positionStyle: positionStyle,
                            span: span,
                            startIndex: columnStartIndex,
                            scrollLeft: scrollLeft,
                            scrollTop: 0,
                            previewSizes: previewSizes,
                            previewOffsets: previewOffsets,
                            getLastChildSize: getLastChildSize
                        }));
                    }
                    for (var _loop = function(columnIndex) {
                        var main = columnSizeAndPositionManager.getSizeAndPositionOfCell(columnIndex), left = main.offset + horizontalOffsetAdjustment;
                        renderedCells.push.apply(renderedCells, _toConsumableArray(columnHeaders[columnIndex].map(function(header) {
                            var span = header.hspan(), width = (0, _headerSize2.default)(columnSizeAndPositionManager, columnIndex, span), top = 0, height = void 0;
                            header.dim ? header.dim.field ? (// Normal dimension header
                            height = getDimensionSize(_Axis.AxisType.COLUMNS, header.dim.field.id), top += dimensionPositions.columns[header.dim.field.id]) : // Total header
                            height = getDimensionSize(_Axis.AxisType.COLUMNS, _constants.TOTAL_ID) : (// Measure header
                            height = getDimensionSize(_Axis.AxisType.COLUMNS, _constants.MEASURE_ID), top += dimensionPositions.columns[_constants.MEASURE_ID]);
                            var positionStyle = {
                                position: "absolute",
                                left: left,
                                top: top,
                                height: height,
                                width: width
                            }, previewOffsets = {};
                            return previewOffsets.right = top - 0, previewOffsets.bottom = left - scrollLeft + rowHeadersWidth, 
                            _react2.default.createElement(_Header2.default, {
                                key: "header-" + header.key,
                                axis: _Axis.AxisType.COLUMNS,
                                header: header,
                                positionStyle: positionStyle,
                                span: span,
                                startIndex: columnStartIndex,
                                scrollLeft: scrollLeft,
                                scrollTop: 0,
                                previewSizes: previewSizes,
                                previewOffsets: previewOffsets,
                                getLastChildSize: getLastChildSize
                            });
                        })));
                    }, columnIndex = columnStartIndex; columnIndex <= correctColumnStopIndex; columnIndex += 1) _loop(columnIndex);
                    return renderedCells;
                }
            }, {
                key: "render",
                value: function() {
                    var _this2 = this, _props2 = this.props, columnCount = _props2.columnCount, getColumnWidth = _props2.getColumnWidth, getRowHeight = _props2.getRowHeight, height = _props2.height, rowCount = _props2.rowCount, scrollLeft = _props2.scrollLeft, width = _props2.width, zoom = _props2.zoom;
                    return _react2.default.createElement(_reactVirtualized.Grid, {
                        cellRangeRenderer: this.columnHeadersRenderer,
                        cellRenderer: function() {},
                        className: "orb-column-headers",
                        columnCount: columnCount,
                        columnWidth: getColumnWidth,
                        height: height,
                        overscanColumnCount: 0,
                        ref: function(_ref2) {
                            _this2.grid = _ref2;
                        },
                        rowCount: rowCount,
                        rowHeight: getRowHeight,
                        scrollLeft: scrollLeft,
                        scrollTop: 0,
                        style: {
                            fontSize: 100 * zoom + "%",
                            overflowX: "hidden",
                            overflowY: "hidden"
                        },
                        width: width
                    });
                }
            } ]), ColumnHeaders;
        }(_react.PureComponent);
        exports.default = ColumnHeaders;
    }, /* 225 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _Header = __webpack_require__(226);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_Header).default;
            }
        });
    }, /* 226 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function getLeafSubheaders(header, result) {
            return header.subheaders && header.subheaders.length ? (header.subheaders.forEach(function(subheader) {
                return getLeafSubheaders(subheader, result);
            }), result) : (result.push(header), result);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _Axis = __webpack_require__(203), _constants = __webpack_require__(207), _ResizeHandle = __webpack_require__(220), _ResizeHandle2 = _interopRequireDefault(_ResizeHandle), Header = function(_ref) {
            var axis = _ref.axis, getLastChildSize = _ref.getLastChildSize, header = _ref.header, positionStyle = _ref.positionStyle, previewSizes = _ref.previewSizes, previewOffsets = _ref.previewOffsets, span = _ref.span, startIndex = _ref.startIndex, scrollLeft = _ref.scrollLeft, scrollTop = _ref.scrollTop, left = positionStyle.left, top = positionStyle.top, width = positionStyle.width, height = positionStyle.height, x = header.x, y = header.y, style = void 0;
            if (span > 1 && x <= startIndex) {
                var offset = void 0, lastChildSize = getLastChildSize(axis, header);
                axis === _Axis.AxisType.COLUMNS ? (offset = Math.min(scrollLeft - left, width - (lastChildSize || 0)), 
                style = {
                    position: "relative",
                    left: offset
                }) : (offset = Math.min(scrollTop - top, height - (lastChildSize || 0)), style = {
                    position: "relative",
                    top: offset
                });
            }
            var innerHeader = _react2.default.createElement(InnerHeader, {
                key: axis + "-" + x + "-" + y,
                cell: header,
                style: style
            }), leafHeaderId = header.key, dimensionId = void 0;
            // Normal header
            dimensionId = header.dim ? header.dim.field ? header.dim.field.id : _constants.TOTAL_ID : _constants.MEASURE_ID;
            var leafSubheaders = header.subheaders ? getLeafSubheaders(header, []) : [];
            return _react2.default.createElement("div", {
                key: "fixed-" + axis + "-" + x + "-" + y,
                className: "orb-cell orb-header orb-column-header",
                style: _extends({
                    boxSizing: "border-box",
                    overflow: "hidden",
                    // border: 'solid lightgrey thin',
                    // backgroundColor: '#eef8fb',
                    zIndex: 1,
                    display: "flex"
                }, positionStyle)
            }, innerHeader, _react2.default.createElement(_ResizeHandle2.default, {
                position: "right",
                size: height,
                id: axis === _Axis.AxisType.COLUMNS ? leafHeaderId : dimensionId,
                leafSubheaders: leafSubheaders,
                axis: axis,
                previewSize: previewSizes.height,
                previewOffset: previewOffsets.right
            }), _react2.default.createElement(_ResizeHandle2.default, {
                position: "bottom",
                size: width,
                id: axis === _Axis.AxisType.ROWS ? leafHeaderId : dimensionId,
                leafSubheaders: leafSubheaders,
                axis: axis,
                previewSize: previewSizes.width,
                previewOffset: previewOffsets.bottom
            }));
        }, InnerHeader = function(_ref2) {
            var cell = _ref2.cell, style = _ref2.style, value = void 0;
            switch (cell.template) {
              case "cell-template-row-header":
              case "cell-template-column-header":
                value = cell.value;
                break;

              case "cell-template-dataheader":
              case "cell-template-dimensionheader":
                value = cell.value.caption;
            }
            var computedStyle = _extends({
                whiteSpace: "nowrap",
                overflow: "hidden"
            }, style);
            return _react2.default.createElement("span", {
                className: "orb-header-inner",
                style: computedStyle
            }, value);
        };
        exports.default = Header;
    }, /* 227 */
    /***/
    function(module, exports) {
        "use strict";
        function getHeaderSize(sizeAndPositionManager, index, span) {
            for (var res = 0, i = 0; i < span; i += 1) res += sizeAndPositionManager.getSizeAndPositionOfCell(index + i).size;
            return res;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.default = getHeaderSize;
    }, /* 228 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _reactRedux = __webpack_require__(2), _Axis = __webpack_require__(203), _selectors = __webpack_require__(201), _RowHeaders = __webpack_require__(229), _RowHeaders2 = _interopRequireDefault(_RowHeaders), mapStateToProps = function(state) {
            return {
                columnCount: (0, _selectors.getLayout)(state).rowHorizontalCount,
                columnHeadersHeight: (0, _selectors.getHeaderSizes)(state).columnHeadersHeight,
                dimensionPositions: (0, _selectors.getDimensionPositions)(state),
                getColumnWidth: function(_ref) {
                    var index = _ref.index;
                    return (0, _selectors.getDimensionSize)(state)(_Axis.AxisType.ROWS, state.axis.rows[index]);
                },
                getDimensionSize: (0, _selectors.getDimensionSize)(state),
                getLastChildSize: (0, _selectors.getLastChildSize)(state),
                getRowHeight: (0, _selectors.getRowHeight)(state),
                height: (0, _selectors.getRowHeadersVisibleHeight)(state),
                previewSizes: (0, _selectors.getPreviewSizes)(state),
                rowCount: (0, _selectors.getLayout)(state).rowVerticalCount,
                rowHeaders: (0, _selectors.getRowUiAxis)(state).headers,
                width: (0, _selectors.getHeaderSizes)(state).rowHeadersWidth,
                zoom: state.config.zoom
            };
        };
        exports.default = (0, _reactRedux.connect)(mapStateToProps)(_RowHeaders2.default);
    }, /* 229 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _RowHeaders = __webpack_require__(230);
        Object.defineProperty(exports, "default", {
            enumerable: !0,
            get: function() {
                return _interopRequireDefault(_RowHeaders).default;
            }
        });
    }, /* 230 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactVirtualized = __webpack_require__(29), _Axis = __webpack_require__(203), _constants = __webpack_require__(207), _Header = __webpack_require__(225), _Header2 = _interopRequireDefault(_Header), _headerSize = __webpack_require__(227), _headerSize2 = _interopRequireDefault(_headerSize), RowHeaders = function(_PureComponent) {
            function RowHeaders() {
                _classCallCheck(this, RowHeaders);
                var _this = _possibleConstructorReturn(this, (RowHeaders.__proto__ || Object.getPrototypeOf(RowHeaders)).call(this));
                return _this.rowHeadersRenderer = _this.rowHeadersRenderer.bind(_this), _this;
            }
            return _inherits(RowHeaders, _PureComponent), _createClass(RowHeaders, [ {
                key: "componentDidUpdate",
                value: function() {
                    this.grid.recomputeGridSize();
                }
            }, {
                key: "rowHeadersRenderer",
                value: function(_ref) {
                    var rowSizeAndPositionManager = _ref.rowSizeAndPositionManager, rowStartIndex = _ref.rowStartIndex, rowStopIndex = _ref.rowStopIndex, scrollTop = _ref.scrollTop, verticalOffsetAdjustment = _ref.verticalOffsetAdjustment, _props = this.props, rowHeaders = _props.rowHeaders, columnHeadersHeight = _props.columnHeadersHeight, columnCount = _props.columnCount, previewSizes = _props.previewSizes, getLastChildSize = _props.getLastChildSize, getDimensionSize = _props.getDimensionSize, dimensionPositions = _props.dimensionPositions;
                    this.firstLeafHeader = rowHeaders[rowStartIndex][rowHeaders[rowStartIndex].length - 1];
                    var renderedCells = [], correctRowStopIndex = Math.min(rowStopIndex, rowHeaders.length - 1);
                    // Render fixed left columns
                    // Render big cells on the left of current cells if necessary
                    // The check on the presence of the header is necessary
                    // because it can be out of bounds when the headers array is modified
                    if (rowHeaders[rowStartIndex] && rowHeaders[rowStartIndex].length < columnCount) for (var header = rowHeaders[rowStartIndex][0]; header.parent; ) {
                        header = header.parent;
                        var main = rowSizeAndPositionManager.getSizeAndPositionOfCell(header.x), span = header.vspan(), top = main.offset + verticalOffsetAdjustment, height = (0, 
                        _headerSize2.default)(rowSizeAndPositionManager, header.x, span), width = getDimensionSize(_Axis.AxisType.ROWS, header.dim.field.id), left = 0 + dimensionPositions.rows[header.dim.field.id], positionStyle = {
                            position: "absolute",
                            left: left,
                            top: top,
                            height: height,
                            width: width
                        }, previewOffsets = {};
                        previewOffsets.right = top - scrollTop + columnHeadersHeight, previewOffsets.bottom = left - 0, 
                        renderedCells.push(_react2.default.createElement(_Header2.default, {
                            key: "header-" + header.key,
                            axis: _Axis.AxisType.ROWS,
                            header: header,
                            positionStyle: positionStyle,
                            span: span,
                            startIndex: rowStartIndex,
                            scrollLeft: 0,
                            scrollTop: scrollTop,
                            previewSizes: previewSizes,
                            previewOffsets: previewOffsets,
                            getLastChildSize: getLastChildSize
                        }));
                    }
                    for (var _loop = function(rowIndex) {
                        var main = rowSizeAndPositionManager.getSizeAndPositionOfCell(rowIndex), top = main.offset + verticalOffsetAdjustment;
                        renderedCells.push.apply(renderedCells, _toConsumableArray(rowHeaders[rowIndex].map(function(header) {
                            var span = header.vspan(), height = (0, _headerSize2.default)(rowSizeAndPositionManager, rowIndex, span), width = void 0, left = 0;
                            header.dim ? header.dim.field ? (// Normal dimension header
                            width = getDimensionSize(_Axis.AxisType.ROWS, header.dim.field.id), left += dimensionPositions.rows[header.dim.field.id]) : // Total header
                            width = getDimensionSize(_Axis.AxisType.ROWS, _constants.TOTAL_ID) : (// Measure header
                            width = getDimensionSize(_Axis.AxisType.ROWS, _constants.MEASURE_ID), left += dimensionPositions.rows[_constants.MEASURE_ID]);
                            var positionStyle = {
                                position: "absolute",
                                left: left,
                                top: top,
                                height: height,
                                width: width
                            }, previewOffsets = {};
                            return previewOffsets.right = top - scrollTop + columnHeadersHeight, previewOffsets.bottom = left - 0, 
                            _react2.default.createElement(_Header2.default, {
                                key: "header-" + header.key,
                                axis: _Axis.AxisType.ROWS,
                                header: header,
                                positionStyle: positionStyle,
                                span: span,
                                startIndex: rowStartIndex,
                                scrollLeft: 0,
                                scrollTop: scrollTop,
                                previewSizes: previewSizes,
                                previewOffsets: previewOffsets,
                                getLastChildSize: getLastChildSize
                            });
                        })));
                    }, rowIndex = rowStartIndex; rowIndex <= correctRowStopIndex; rowIndex += 1) _loop(rowIndex);
                    return renderedCells;
                }
            }, {
                key: "render",
                value: function() {
                    var _this2 = this, _props2 = this.props, zoom = _props2.zoom, getRowHeight = _props2.getRowHeight, getColumnWidth = _props2.getColumnWidth, columnCount = _props2.columnCount, rowCount = _props2.rowCount, scrollTop = _props2.scrollTop, height = _props2.height, width = _props2.width;
                    return _react2.default.createElement(_reactVirtualized.Grid, {
                        cellRangeRenderer: this.rowHeadersRenderer,
                        cellRenderer: function() {},
                        className: "orb-row-headers",
                        columnCount: columnCount,
                        columnWidth: getColumnWidth,
                        height: height,
                        overscanRowCount: 0,
                        ref: function(_ref2) {
                            _this2.grid = _ref2;
                        },
                        rowCount: rowCount,
                        rowHeight: getRowHeight,
                        scrollLeft: 0,
                        scrollTop: scrollTop,
                        style: {
                            fontSize: 100 * zoom + "%",
                            overflowX: "hidden",
                            overflowY: "hidden"
                        },
                        width: width
                    });
                }
            } ]), RowHeaders;
        }(_react.PureComponent);
        exports.default = RowHeaders;
    }, /* 231 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _reactDnd = __webpack_require__(81), collectDragLayer = function(monitor) {
            return {
                item: monitor.getItem(),
                itemType: monitor.getItemType(),
                initialOffset: monitor.getInitialSourceClientOffset(),
                currentOffset: monitor.getSourceClientOffset(),
                isDragging: monitor.isDragging()
            };
        }, getItemPosition = function(_ref) {
            var initialOffset = _ref.initialOffset, currentOffset = _ref.currentOffset, item = _ref.item;
            if (!initialOffset || !currentOffset) return {
                display: "none"
            };
            var x = currentOffset.x, y = currentOffset.y;
            "right" === item.position ? y = initialOffset.y - item.previewOffset : x = initialOffset.x - item.previewOffset;
            var transform = "translate(" + x + "px, " + y + "px)";
            return {
                transform: transform,
                WebkitTransform: transform
            };
        }, CustomDragLayer = function(_Component) {
            function CustomDragLayer() {
                return _classCallCheck(this, CustomDragLayer), _possibleConstructorReturn(this, (CustomDragLayer.__proto__ || Object.getPrototypeOf(CustomDragLayer)).apply(this, arguments));
            }
            return _inherits(CustomDragLayer, _Component), _createClass(CustomDragLayer, [ {
                key: "render",
                value: function() {
                    var height = void 0, width = void 0;
                    if (!this.props.item || "cell-resize-handle" !== this.props.itemType) return null;
                    var _props$item = this.props.item, position = _props$item.position, previewSize = _props$item.previewSize;
                    return "right" === position ? (width = 2, height = previewSize) : (width = previewSize, 
                    height = 2), _react2.default.createElement("div", {
                        style: {
                            position: "fixed",
                            pointerEvents: "none",
                            zIndex: 100,
                            left: 0,
                            top: 0,
                            width: "100%",
                            height: "100%"
                        }
                    }, _react2.default.createElement("div", {
                        style: _extends({
                            height: height,
                            width: width,
                            backgroundColor: "grey"
                        }, getItemPosition(this.props))
                    }));
                }
            } ]), CustomDragLayer;
        }(_react.Component);
        exports.default = (0, _reactDnd.DragLayer)(collectDragLayer)(CustomDragLayer);
    }, /* 232 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _dataActions = __webpack_require__(233);
        Object.keys(_dataActions).forEach(function(key) {
            "default" !== key && "__esModule" !== key && Object.defineProperty(exports, key, {
                enumerable: !0,
                get: function() {
                    return _dataActions[key];
                }
            });
        });
        var _configActions = __webpack_require__(234);
        Object.keys(_configActions).forEach(function(key) {
            "default" !== key && "__esModule" !== key && Object.defineProperty(exports, key, {
                enumerable: !0,
                get: function() {
                    return _configActions[key];
                }
            });
        });
        var _sizesActions = __webpack_require__(236);
        Object.keys(_sizesActions).forEach(function(key) {
            "default" !== key && "__esModule" !== key && Object.defineProperty(exports, key, {
                enumerable: !0,
                get: function() {
                    return _sizesActions[key];
                }
            });
        });
        var _fieldActions = __webpack_require__(237);
        Object.keys(_fieldActions).forEach(function(key) {
            "default" !== key && "__esModule" !== key && Object.defineProperty(exports, key, {
                enumerable: !0,
                get: function() {
                    return _fieldActions[key];
                }
            });
        });
        var _filterActions = __webpack_require__(238);
        Object.keys(_filterActions).forEach(function(key) {
            "default" !== key && "__esModule" !== key && Object.defineProperty(exports, key, {
                enumerable: !0,
                get: function() {
                    return _filterActions[key];
                }
            });
        });
    }, /* 233 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.setData = exports.pushData = void 0;
        var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(obj) {
            return typeof obj;
        } : function(obj) {
            return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
        }, _constants = __webpack_require__(207);
        exports.pushData = function(payload) {
            return Array.isArray(payload) && (Array.isArray(payload[0]) || "object" === _typeof(payload[0])) ? {
                type: _constants.PUSH_DATA,
                payload: payload
            } : Array.isArray(payload) || "object" === ("undefined" == typeof payload ? "undefined" : _typeof(payload)) ? {
                type: _constants.PUSH_DATA,
                payload: [ payload ]
            } : {
                type: _constants.PUSH_DATA,
                payload: []
            };
        }, exports.setData = function(payload) {
            return Array.isArray(payload) && (Array.isArray(payload[0]) || "object" === _typeof(payload[0])) ? {
                type: _constants.SET_DATA,
                payload: payload
            } : Array.isArray(payload) || "object" === ("undefined" == typeof payload ? "undefined" : _typeof(payload)) ? {
                type: _constants.SET_DATA,
                payload: [ payload ]
            } : {
                type: _constants.SET_DATA,
                payload: []
            };
        };
    }, /* 234 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.moveField = exports.toggleDatafield = exports.setConfigProperty = exports.setDatafields = exports.setFields = void 0;
        var _fields = __webpack_require__(235), _constants = __webpack_require__(207);
        exports.setFields = function(configObject) {
            return {
                type: _constants.SET_FIELDS,
                fields: configObject.fields.map(function(field) {
                    return (0, _fields.fieldFactory)(field);
                })
            };
        }, exports.setDatafields = function(configObject) {
            return {
                type: _constants.SET_DATAFIELDS,
                datafields: configObject.datafields.map(function(field) {
                    return (0, _fields.datafieldFactory)(field);
                })
            };
        }, exports.setConfigProperty = function(configObject, property, defaultValue) {
            return {
                type: _constants.SET_CONFIG_PROPERTY,
                property: property,
                value: configObject[property] || defaultValue
            };
        }, exports.toggleDatafield = function(datafieldId) {
            return {
                type: _constants.TOGGLE_DATAFIELD,
                id: datafieldId
            };
        }, exports.moveField = function(fieldId, oldAxis, newAxis, position) {
            return {
                type: _constants.MOVE_FIELD,
                id: fieldId,
                oldAxis: oldAxis,
                newAxis: newAxis,
                position: position
            };
        };
    }, /* 235 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function fieldFactory(fieldConfig) {
            var id = fieldConfig.id, name = fieldConfig.name, caption = fieldConfig.caption, sort = fieldConfig.sort;
            id || console.error("Configuration error: field definition needs an id.", fieldConfig);
            var field = {
                id: id
            };
            field.name = name || field.id, field.caption = caption || field.name;
            var sortValue = void 0;
            return sortValue = sort ? {
                order: sort.order || (sort.customfunc ? "asc" : null),
                customfunc: sort.customfunc
            } : {
                order: null
            }, field.sort = sortValue, field.subTotal = {}, field;
        }
        function datafieldFactory(fieldConfig) {
            var id = fieldConfig.id, name = fieldConfig.name, caption = fieldConfig.caption, aggregateFuncName = fieldConfig.aggregateFuncName, aggregateFunc = fieldConfig.aggregateFunc;
            id || console.error("Configuration error: datafield definition needs an id.", fieldConfig);
            var datafield = {
                id: id
            };
            return datafield.name = name || datafield.id, datafield.caption = caption || datafield.name, 
            aggregateFuncName ? datafield.aggregateFuncName = aggregateFuncName : aggregateFunc ? (0, 
            _generic.isString)(aggregateFunc) ? datafield.aggregateFuncName = aggregateFunc : datafield.aggregateFuncName = "custom" : datafield.aggregateFuncName = null, 
            datafield;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.fieldFactory = fieldFactory, exports.datafieldFactory = datafieldFactory;
        var _generic = __webpack_require__(208);
    }, /* 236 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function getNewCellSize(size, offset) {
            return Math.max(size + offset, 10);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.updateCellSize = void 0;
        var _Axis = __webpack_require__(203), _constants = __webpack_require__(207);
        exports.updateCellSize = function(_ref) {
            var handle = _ref.handle, offset = _ref.offset, initialOffset = _ref.initialOffset, defaultCellSizes = _ref.defaultCellSizes, sizes = _ref.sizes, size = void 0, axis = void 0, direction = void 0;
            return handle.isOnDimensionHeader ? (direction = "dimensions", handle.axis === _Axis.AxisType.COLUMNS ? (size = getNewCellSize(sizes.columns.dimensions[handle.id] || defaultCellSizes.height, offset.y - initialOffset.y), 
            axis = "columns") : (size = getNewCellSize(sizes.rows.dimensions[handle.id] || defaultCellSizes.width, offset.x - initialOffset.x), 
            axis = "rows")) : handle.axis === _Axis.AxisType.COLUMNS && "right" === handle.position ? (direction = "leafs", 
            axis = "columns", size = getNewCellSize(sizes.columns.leafs[handle.id] || defaultCellSizes.width, offset.x - initialOffset.x)) : handle.axis === _Axis.AxisType.ROWS && "bottom" === handle.position ? (direction = "leafs", 
            axis = "rows", size = getNewCellSize(sizes.rows.leafs[handle.id] || defaultCellSizes.height, offset.y - initialOffset.y)) : handle.axis === _Axis.AxisType.COLUMNS && "bottom" === handle.position ? (axis = "columns", 
            direction = "dimensions", size = getNewCellSize(sizes.columns.dimensions[handle.id] || defaultCellSizes.height, offset.y - initialOffset.y)) : handle.axis === _Axis.AxisType.ROWS && "right" === handle.position && (axis = "rows", 
            direction = "dimensions", size = getNewCellSize(sizes.rows.dimensions[handle.id] || defaultCellSizes.width, offset.x - initialOffset.x)), 
            {
                type: _constants.UPDATE_CELL_SIZE,
                id: handle.id,
                size: size,
                axis: axis,
                direction: direction
            };
        };
    }, /* 237 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.changeSortOrder = void 0;
        var _constants = __webpack_require__(207);
        exports.changeSortOrder = function(fieldId) {
            return {
                type: _constants.CHANGE_SORT_ORDER,
                field: fieldId
            };
        };
    }, /* 238 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.deleteFilter = exports.addFilter = void 0;
        var _constants = __webpack_require__(207);
        exports.addFilter = function(fieldId, operator, term, staticValue, excludeStatic) {
            return {
                type: _constants.ADD_FILTER,
                filter: {
                    fieldId: fieldId,
                    operator: operator,
                    term: term,
                    staticValue: staticValue,
                    excludeStatic: excludeStatic
                },
                field: fieldId
            };
        }, exports.deleteFilter = function(fieldId) {
            return {
                type: _constants.DELETE_FILTER,
                field: fieldId
            };
        };
    }, /* 239 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireWildcard(obj) {
            if (obj && obj.__esModule) return obj;
            var newObj = {};
            if (null != obj) for (var key in obj) Object.prototype.hasOwnProperty.call(obj, key) && (newObj[key] = obj[key]);
            return newObj.default = obj, newObj;
        }
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _possibleConstructorReturn(self, call) {
            if (!self) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !call || "object" != typeof call && "function" != typeof call ? self : call;
        }
        function _inherits(subClass, superClass) {
            if ("function" != typeof superClass && null !== superClass) throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), superClass && (Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, 
                    "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), 
                Constructor;
            };
        }(), _react = __webpack_require__(5), _react2 = _interopRequireDefault(_react), _redux = __webpack_require__(11), _reactRedux = __webpack_require__(2), _reactDnd = __webpack_require__(81), _reactDndHtml5Backend = __webpack_require__(240), _reactDndHtml5Backend2 = _interopRequireDefault(_reactDndHtml5Backend), _reducers = __webpack_require__(277), _reducers2 = _interopRequireDefault(_reducers), _PivotGrid = __webpack_require__(1), _PivotGrid2 = _interopRequireDefault(_PivotGrid), _hydrateStore = __webpack_require__(285), _hydrateStore2 = _interopRequireDefault(_hydrateStore), _actions = __webpack_require__(232), actions = _interopRequireWildcard(_actions), WrappedGrid = function(_Component) {
            function WrappedGrid(props) {
                _classCallCheck(this, WrappedGrid);
                var _this = _possibleConstructorReturn(this, (WrappedGrid.__proto__ || Object.getPrototypeOf(WrappedGrid)).call(this, props)), _this$props = _this.props, data = _this$props.data, config = _this$props.config;
                return _this.store = (0, _redux.createStore)(_reducers2.default), _this.customFunctions = (0, 
                _hydrateStore2.default)(_this.store, config), _this.store.dispatch(actions.setData(data)), 
                _this;
            }
            // componentWillReceiveProps(nextProps) {
            // const { pushData } = nextProps;
            // if (this.props.pushData !== nextProps.pushData) {
            //   this.store.dispatch(actions.pushData(pushData));
            // }
            // this.store = createStore(reducer);
            // this.customFunctions = hydrateStore(this.store, config);
            // this.store.dispatch(actions.data(data));
            // }
            return _inherits(WrappedGrid, _Component), _createClass(WrappedGrid, [ {
                key: "render",
                value: function() {
                    return _react2.default.createElement(_reactRedux.Provider, {
                        store: this.store
                    }, _react2.default.createElement(_PivotGrid2.default, {
                        customFunctions: this.customFunctions,
                        height: this.props.height,
                        width: this.props.width
                    }));
                }
            } ]), WrappedGrid;
        }(_react.Component);
        Object.keys(actions).forEach(function(action) {
            WrappedGrid.prototype[action] = function() {
                this.store.dispatch(actions[action].apply(actions, arguments));
            };
        }), exports.default = (0, _reactDnd.DragDropContext)(_reactDndHtml5Backend2.default)(WrappedGrid);
    }, /* 240 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireWildcard(obj) {
            if (obj && obj.__esModule) return obj;
            var newObj = {};
            if (null != obj) for (var key in obj) Object.prototype.hasOwnProperty.call(obj, key) && (newObj[key] = obj[key]);
            return newObj.default = obj, newObj;
        }
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function createHTML5Backend(manager) {
            return new _HTML5Backend2.default(manager);
        }
        exports.__esModule = !0, exports.default = createHTML5Backend;
        var _HTML5Backend = __webpack_require__(241), _HTML5Backend2 = _interopRequireDefault(_HTML5Backend), _getEmptyImage = __webpack_require__(276), _getEmptyImage2 = _interopRequireDefault(_getEmptyImage), _NativeTypes = __webpack_require__(275), NativeTypes = _interopRequireWildcard(_NativeTypes);
        exports.NativeTypes = NativeTypes, exports.getEmptyImage = _getEmptyImage2.default;
    }, /* 241 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireWildcard(obj) {
            if (obj && obj.__esModule) return obj;
            var newObj = {};
            if (null != obj) for (var key in obj) Object.prototype.hasOwnProperty.call(obj, key) && (newObj[key] = obj[key]);
            return newObj.default = obj, newObj;
        }
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var _lodashDefaults = __webpack_require__(242), _lodashDefaults2 = _interopRequireDefault(_lodashDefaults), _shallowEqual = __webpack_require__(264), _shallowEqual2 = _interopRequireDefault(_shallowEqual), _EnterLeaveCounter = __webpack_require__(265), _EnterLeaveCounter2 = _interopRequireDefault(_EnterLeaveCounter), _BrowserDetector = __webpack_require__(270), _OffsetUtils = __webpack_require__(272), _NativeDragSources = __webpack_require__(274), _NativeTypes = __webpack_require__(275), NativeTypes = _interopRequireWildcard(_NativeTypes), HTML5Backend = function() {
            function HTML5Backend(manager) {
                _classCallCheck(this, HTML5Backend), this.actions = manager.getActions(), this.monitor = manager.getMonitor(), 
                this.registry = manager.getRegistry(), this.sourcePreviewNodes = {}, this.sourcePreviewNodeOptions = {}, 
                this.sourceNodes = {}, this.sourceNodeOptions = {}, this.enterLeaveCounter = new _EnterLeaveCounter2.default(), 
                this.getSourceClientOffset = this.getSourceClientOffset.bind(this), this.handleTopDragStart = this.handleTopDragStart.bind(this), 
                this.handleTopDragStartCapture = this.handleTopDragStartCapture.bind(this), this.handleTopDragEndCapture = this.handleTopDragEndCapture.bind(this), 
                this.handleTopDragEnter = this.handleTopDragEnter.bind(this), this.handleTopDragEnterCapture = this.handleTopDragEnterCapture.bind(this), 
                this.handleTopDragLeaveCapture = this.handleTopDragLeaveCapture.bind(this), this.handleTopDragOver = this.handleTopDragOver.bind(this), 
                this.handleTopDragOverCapture = this.handleTopDragOverCapture.bind(this), this.handleTopDrop = this.handleTopDrop.bind(this), 
                this.handleTopDropCapture = this.handleTopDropCapture.bind(this), this.handleSelectStart = this.handleSelectStart.bind(this), 
                this.endDragIfSourceWasRemovedFromDOM = this.endDragIfSourceWasRemovedFromDOM.bind(this), 
                this.endDragNativeItem = this.endDragNativeItem.bind(this);
            }
            return HTML5Backend.prototype.setup = function() {
                if ("undefined" != typeof window) {
                    if (this.constructor.isSetUp) throw new Error("Cannot have two HTML5 backends at the same time.");
                    this.constructor.isSetUp = !0, this.addEventListeners(window);
                }
            }, HTML5Backend.prototype.teardown = function() {
                "undefined" != typeof window && (this.constructor.isSetUp = !1, this.removeEventListeners(window), 
                this.clearCurrentDragSourceNode());
            }, HTML5Backend.prototype.addEventListeners = function(target) {
                target.addEventListener("dragstart", this.handleTopDragStart), target.addEventListener("dragstart", this.handleTopDragStartCapture, !0), 
                target.addEventListener("dragend", this.handleTopDragEndCapture, !0), target.addEventListener("dragenter", this.handleTopDragEnter), 
                target.addEventListener("dragenter", this.handleTopDragEnterCapture, !0), target.addEventListener("dragleave", this.handleTopDragLeaveCapture, !0), 
                target.addEventListener("dragover", this.handleTopDragOver), target.addEventListener("dragover", this.handleTopDragOverCapture, !0), 
                target.addEventListener("drop", this.handleTopDrop), target.addEventListener("drop", this.handleTopDropCapture, !0);
            }, HTML5Backend.prototype.removeEventListeners = function(target) {
                target.removeEventListener("dragstart", this.handleTopDragStart), target.removeEventListener("dragstart", this.handleTopDragStartCapture, !0), 
                target.removeEventListener("dragend", this.handleTopDragEndCapture, !0), target.removeEventListener("dragenter", this.handleTopDragEnter), 
                target.removeEventListener("dragenter", this.handleTopDragEnterCapture, !0), target.removeEventListener("dragleave", this.handleTopDragLeaveCapture, !0), 
                target.removeEventListener("dragover", this.handleTopDragOver), target.removeEventListener("dragover", this.handleTopDragOverCapture, !0), 
                target.removeEventListener("drop", this.handleTopDrop), target.removeEventListener("drop", this.handleTopDropCapture, !0);
            }, HTML5Backend.prototype.connectDragPreview = function(sourceId, node, options) {
                var _this = this;
                return this.sourcePreviewNodeOptions[sourceId] = options, this.sourcePreviewNodes[sourceId] = node, 
                function() {
                    delete _this.sourcePreviewNodes[sourceId], delete _this.sourcePreviewNodeOptions[sourceId];
                };
            }, HTML5Backend.prototype.connectDragSource = function(sourceId, node, options) {
                var _this2 = this;
                this.sourceNodes[sourceId] = node, this.sourceNodeOptions[sourceId] = options;
                var handleDragStart = function(e) {
                    return _this2.handleDragStart(e, sourceId);
                }, handleSelectStart = function(e) {
                    return _this2.handleSelectStart(e, sourceId);
                };
                return node.setAttribute("draggable", !0), node.addEventListener("dragstart", handleDragStart), 
                node.addEventListener("selectstart", handleSelectStart), function() {
                    delete _this2.sourceNodes[sourceId], delete _this2.sourceNodeOptions[sourceId], 
                    node.removeEventListener("dragstart", handleDragStart), node.removeEventListener("selectstart", handleSelectStart), 
                    node.setAttribute("draggable", !1);
                };
            }, HTML5Backend.prototype.connectDropTarget = function(targetId, node) {
                var _this3 = this, handleDragEnter = function(e) {
                    return _this3.handleDragEnter(e, targetId);
                }, handleDragOver = function(e) {
                    return _this3.handleDragOver(e, targetId);
                }, handleDrop = function(e) {
                    return _this3.handleDrop(e, targetId);
                };
                return node.addEventListener("dragenter", handleDragEnter), node.addEventListener("dragover", handleDragOver), 
                node.addEventListener("drop", handleDrop), function() {
                    node.removeEventListener("dragenter", handleDragEnter), node.removeEventListener("dragover", handleDragOver), 
                    node.removeEventListener("drop", handleDrop);
                };
            }, HTML5Backend.prototype.getCurrentSourceNodeOptions = function() {
                var sourceId = this.monitor.getSourceId(), sourceNodeOptions = this.sourceNodeOptions[sourceId];
                return _lodashDefaults2.default(sourceNodeOptions || {}, {
                    dropEffect: "move"
                });
            }, HTML5Backend.prototype.getCurrentDropEffect = function() {
                return this.isDraggingNativeItem() ? "copy" : this.getCurrentSourceNodeOptions().dropEffect;
            }, HTML5Backend.prototype.getCurrentSourcePreviewNodeOptions = function() {
                var sourceId = this.monitor.getSourceId(), sourcePreviewNodeOptions = this.sourcePreviewNodeOptions[sourceId];
                return _lodashDefaults2.default(sourcePreviewNodeOptions || {}, {
                    anchorX: .5,
                    anchorY: .5,
                    captureDraggingState: !1
                });
            }, HTML5Backend.prototype.getSourceClientOffset = function(sourceId) {
                return _OffsetUtils.getNodeClientOffset(this.sourceNodes[sourceId]);
            }, HTML5Backend.prototype.isDraggingNativeItem = function() {
                var itemType = this.monitor.getItemType();
                return Object.keys(NativeTypes).some(function(key) {
                    return NativeTypes[key] === itemType;
                });
            }, HTML5Backend.prototype.beginDragNativeItem = function(type) {
                this.clearCurrentDragSourceNode();
                var SourceType = _NativeDragSources.createNativeDragSource(type);
                this.currentNativeSource = new SourceType(), this.currentNativeHandle = this.registry.addSource(type, this.currentNativeSource), 
                this.actions.beginDrag([ this.currentNativeHandle ]), // On Firefox, if mousemove fires, the drag is over but browser failed to tell us.
                // This is not true for other browsers.
                _BrowserDetector.isFirefox() && window.addEventListener("mousemove", this.endDragNativeItem, !0);
            }, HTML5Backend.prototype.endDragNativeItem = function() {
                this.isDraggingNativeItem() && (_BrowserDetector.isFirefox() && window.removeEventListener("mousemove", this.endDragNativeItem, !0), 
                this.actions.endDrag(), this.registry.removeSource(this.currentNativeHandle), this.currentNativeHandle = null, 
                this.currentNativeSource = null);
            }, HTML5Backend.prototype.endDragIfSourceWasRemovedFromDOM = function() {
                var node = this.currentDragSourceNode;
                document.body.contains(node) || this.clearCurrentDragSourceNode() && this.actions.endDrag();
            }, HTML5Backend.prototype.setCurrentDragSourceNode = function(node) {
                this.clearCurrentDragSourceNode(), this.currentDragSourceNode = node, this.currentDragSourceNodeOffset = _OffsetUtils.getNodeClientOffset(node), 
                this.currentDragSourceNodeOffsetChanged = !1, // Receiving a mouse event in the middle of a dragging operation
                // means it has ended and the drag source node disappeared from DOM,
                // so the browser didn't dispatch the dragend event.
                window.addEventListener("mousemove", this.endDragIfSourceWasRemovedFromDOM, !0);
            }, HTML5Backend.prototype.clearCurrentDragSourceNode = function() {
                return !!this.currentDragSourceNode && (this.currentDragSourceNode = null, this.currentDragSourceNodeOffset = null, 
                this.currentDragSourceNodeOffsetChanged = !1, window.removeEventListener("mousemove", this.endDragIfSourceWasRemovedFromDOM, !0), 
                !0);
            }, HTML5Backend.prototype.checkIfCurrentDragSourceRectChanged = function() {
                var node = this.currentDragSourceNode;
                return !!node && (!!this.currentDragSourceNodeOffsetChanged || (this.currentDragSourceNodeOffsetChanged = !_shallowEqual2.default(_OffsetUtils.getNodeClientOffset(node), this.currentDragSourceNodeOffset), 
                this.currentDragSourceNodeOffsetChanged));
            }, HTML5Backend.prototype.handleTopDragStartCapture = function() {
                this.clearCurrentDragSourceNode(), this.dragStartSourceIds = [];
            }, HTML5Backend.prototype.handleDragStart = function(e, sourceId) {
                this.dragStartSourceIds.unshift(sourceId);
            }, HTML5Backend.prototype.handleTopDragStart = function(e) {
                var _this4 = this, dragStartSourceIds = this.dragStartSourceIds;
                this.dragStartSourceIds = null;
                var clientOffset = _OffsetUtils.getEventClientOffset(e);
                // Don't publish the source just yet (see why below)
                this.actions.beginDrag(dragStartSourceIds, {
                    publishSource: !1,
                    getSourceClientOffset: this.getSourceClientOffset,
                    clientOffset: clientOffset
                });
                var dataTransfer = e.dataTransfer, nativeType = _NativeDragSources.matchNativeItemType(dataTransfer);
                if (this.monitor.isDragging()) {
                    if ("function" == typeof dataTransfer.setDragImage) {
                        // Use custom drag image if user specifies it.
                        // If child drag source refuses drag but parent agrees,
                        // use parent's node as drag image. Neither works in IE though.
                        var sourceId = this.monitor.getSourceId(), sourceNode = this.sourceNodes[sourceId], dragPreview = this.sourcePreviewNodes[sourceId] || sourceNode, _getCurrentSourcePreviewNodeOptions = this.getCurrentSourcePreviewNodeOptions(), anchorX = _getCurrentSourcePreviewNodeOptions.anchorX, anchorY = _getCurrentSourcePreviewNodeOptions.anchorY, anchorPoint = {
                            anchorX: anchorX,
                            anchorY: anchorY
                        }, dragPreviewOffset = _OffsetUtils.getDragPreviewOffset(sourceNode, dragPreview, clientOffset, anchorPoint);
                        dataTransfer.setDragImage(dragPreview, dragPreviewOffset.x, dragPreviewOffset.y);
                    }
                    try {
                        // Firefox won't drag without setting data
                        dataTransfer.setData("application/json", {});
                    } catch (err) {}
                    // IE doesn't support MIME types in setData
                    // Store drag source node so we can check whether
                    // it is removed from DOM and trigger endDrag manually.
                    this.setCurrentDragSourceNode(e.target);
                    // Now we are ready to publish the drag source.. or are we not?
                    var _getCurrentSourcePreviewNodeOptions2 = this.getCurrentSourcePreviewNodeOptions(), captureDraggingState = _getCurrentSourcePreviewNodeOptions2.captureDraggingState;
                    captureDraggingState ? // In some cases the user may want to override this behavior, e.g.
                    // to work around IE not supporting custom drag previews.
                    //
                    // When using a custom drag layer, the only way to prevent
                    // the default drag preview from drawing in IE is to screenshot
                    // the dragging state in which the node itself has zero opacity
                    // and height. In this case, though, returning null from render()
                    // will abruptly end the dragging, which is not obvious.
                    //
                    // This is the reason such behavior is strictly opt-in.
                    this.actions.publishDragSource() : // Usually we want to publish it in the next tick so that browser
                    // is able to screenshot the current (not yet dragging) state.
                    //
                    // It also neatly avoids a situation where render() returns null
                    // in the same tick for the source element, and browser freaks out.
                    setTimeout(function() {
                        return _this4.actions.publishDragSource();
                    });
                } else if (nativeType) // A native item (such as URL) dragged from inside the document
                this.beginDragNativeItem(nativeType); else {
                    if (!(dataTransfer.types || e.target.hasAttribute && e.target.hasAttribute("draggable"))) // Looks like a Safari bug: dataTransfer.types is null, but there was no draggable.
                    // Just let it drag. It's a native type (URL or text) and will be picked up in dragenter handler.
                    return;
                    // If by this time no drag source reacted, tell browser not to drag.
                    e.preventDefault();
                }
            }, HTML5Backend.prototype.handleTopDragEndCapture = function() {
                this.clearCurrentDragSourceNode() && // Firefox can dispatch this event in an infinite loop
                // if dragend handler does something like showing an alert.
                // Only proceed if we have not handled it already.
                this.actions.endDrag();
            }, HTML5Backend.prototype.handleTopDragEnterCapture = function(e) {
                this.dragEnterTargetIds = [];
                var isFirstEnter = this.enterLeaveCounter.enter(e.target);
                if (isFirstEnter && !this.monitor.isDragging()) {
                    var dataTransfer = e.dataTransfer, nativeType = _NativeDragSources.matchNativeItemType(dataTransfer);
                    nativeType && // A native item (such as file or URL) dragged from outside the document
                    this.beginDragNativeItem(nativeType);
                }
            }, HTML5Backend.prototype.handleDragEnter = function(e, targetId) {
                this.dragEnterTargetIds.unshift(targetId);
            }, HTML5Backend.prototype.handleTopDragEnter = function(e) {
                var _this5 = this, dragEnterTargetIds = this.dragEnterTargetIds;
                if (this.dragEnterTargetIds = [], this.monitor.isDragging()) {
                    _BrowserDetector.isFirefox() || // Don't emit hover in `dragenter` on Firefox due to an edge case.
                    // If the target changes position as the result of `dragenter`, Firefox
                    // will still happily dispatch `dragover` despite target being no longer
                    // there. The easy solution is to only fire `hover` in `dragover` on FF.
                    this.actions.hover(dragEnterTargetIds, {
                        clientOffset: _OffsetUtils.getEventClientOffset(e)
                    });
                    var canDrop = dragEnterTargetIds.some(function(targetId) {
                        return _this5.monitor.canDropOnTarget(targetId);
                    });
                    canDrop && (// IE requires this to fire dragover events
                    e.preventDefault(), e.dataTransfer.dropEffect = this.getCurrentDropEffect());
                }
            }, HTML5Backend.prototype.handleTopDragOverCapture = function() {
                this.dragOverTargetIds = [];
            }, HTML5Backend.prototype.handleDragOver = function(e, targetId) {
                this.dragOverTargetIds.unshift(targetId);
            }, HTML5Backend.prototype.handleTopDragOver = function(e) {
                var _this6 = this, dragOverTargetIds = this.dragOverTargetIds;
                if (this.dragOverTargetIds = [], !this.monitor.isDragging()) // This is probably a native item type we don't understand.
                // Prevent default "drop and blow away the whole document" action.
                return e.preventDefault(), void (e.dataTransfer.dropEffect = "none");
                this.actions.hover(dragOverTargetIds, {
                    clientOffset: _OffsetUtils.getEventClientOffset(e)
                });
                var canDrop = dragOverTargetIds.some(function(targetId) {
                    return _this6.monitor.canDropOnTarget(targetId);
                });
                canDrop ? (// Show user-specified drop effect.
                e.preventDefault(), e.dataTransfer.dropEffect = this.getCurrentDropEffect()) : this.isDraggingNativeItem() ? (// Don't show a nice cursor but still prevent default
                // "drop and blow away the whole document" action.
                e.preventDefault(), e.dataTransfer.dropEffect = "none") : this.checkIfCurrentDragSourceRectChanged() && (// Prevent animating to incorrect position.
                // Drop effect must be other than 'none' to prevent animation.
                e.preventDefault(), e.dataTransfer.dropEffect = "move");
            }, HTML5Backend.prototype.handleTopDragLeaveCapture = function(e) {
                this.isDraggingNativeItem() && e.preventDefault();
                var isLastLeave = this.enterLeaveCounter.leave(e.target);
                isLastLeave && this.isDraggingNativeItem() && this.endDragNativeItem();
            }, HTML5Backend.prototype.handleTopDropCapture = function(e) {
                this.dropTargetIds = [], e.preventDefault(), this.isDraggingNativeItem() && this.currentNativeSource.mutateItemByReadingDataTransfer(e.dataTransfer), 
                this.enterLeaveCounter.reset();
            }, HTML5Backend.prototype.handleDrop = function(e, targetId) {
                this.dropTargetIds.unshift(targetId);
            }, HTML5Backend.prototype.handleTopDrop = function(e) {
                var dropTargetIds = this.dropTargetIds;
                this.dropTargetIds = [], this.actions.hover(dropTargetIds, {
                    clientOffset: _OffsetUtils.getEventClientOffset(e)
                }), this.actions.drop(), this.isDraggingNativeItem() ? this.endDragNativeItem() : this.endDragIfSourceWasRemovedFromDOM();
            }, HTML5Backend.prototype.handleSelectStart = function(e) {
                var target = e.target;
                // Only IE requires us to explicitly say
                // we want drag drop operation to start
                "function" == typeof target.dragDrop && (// Inputs and textareas should be selectable
                "INPUT" === target.tagName || "SELECT" === target.tagName || "TEXTAREA" === target.tagName || target.isContentEditable || (// For other targets, ask IE
                // to enable drag and drop
                e.preventDefault(), target.dragDrop()));
            }, HTML5Backend;
        }();
        exports.default = HTML5Backend, module.exports = exports.default;
    }, /* 242 */
    /***/
    function(module, exports, __webpack_require__) {
        var apply = __webpack_require__(143), assignInDefaults = __webpack_require__(243), assignInWith = __webpack_require__(244), baseRest = __webpack_require__(140), defaults = baseRest(function(args) {
            return args.push(void 0, assignInDefaults), apply(assignInWith, void 0, args);
        });
        module.exports = defaults;
    }, /* 243 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Used by `_.defaults` to customize its `_.assignIn` use.
	 *
	 * @private
	 * @param {*} objValue The destination value.
	 * @param {*} srcValue The source value.
	 * @param {string} key The key of the property to assign.
	 * @param {Object} object The parent object of `objValue`.
	 * @returns {*} Returns the value to assign.
	 */
        function assignInDefaults(objValue, srcValue, key, object) {
            return void 0 === objValue || eq(objValue, objectProto[key]) && !hasOwnProperty.call(object, key) ? srcValue : objValue;
        }
        var eq = __webpack_require__(118), objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty;
        module.exports = assignInDefaults;
    }, /* 244 */
    /***/
    function(module, exports, __webpack_require__) {
        var copyObject = __webpack_require__(245), createAssigner = __webpack_require__(248), keysIn = __webpack_require__(251), assignInWith = createAssigner(function(object, source, srcIndex, customizer) {
            copyObject(source, keysIn(source), object, customizer);
        });
        module.exports = assignInWith;
    }, /* 245 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Copies properties of `source` to `object`.
	 *
	 * @private
	 * @param {Object} source The object to copy properties from.
	 * @param {Array} props The property identifiers to copy.
	 * @param {Object} [object={}] The object to copy properties to.
	 * @param {Function} [customizer] The function to customize copied values.
	 * @returns {Object} Returns `object`.
	 */
        function copyObject(source, props, object, customizer) {
            var isNew = !object;
            object || (object = {});
            for (var index = -1, length = props.length; ++index < length; ) {
                var key = props[index], newValue = customizer ? customizer(object[key], source[key], key, object, source) : void 0;
                void 0 === newValue && (newValue = source[key]), isNew ? baseAssignValue(object, key, newValue) : assignValue(object, key, newValue);
            }
            return object;
        }
        var assignValue = __webpack_require__(246), baseAssignValue = __webpack_require__(247);
        module.exports = copyObject;
    }, /* 246 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Assigns `value` to `key` of `object` if the existing value is not equivalent
	 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
	 * for equality comparisons.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
        function assignValue(object, key, value) {
            var objValue = object[key];
            hasOwnProperty.call(object, key) && eq(objValue, value) && (void 0 !== value || key in object) || baseAssignValue(object, key, value);
        }
        var baseAssignValue = __webpack_require__(247), eq = __webpack_require__(118), objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty;
        module.exports = assignValue;
    }, /* 247 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `assignValue` and `assignMergeValue` without
	 * value checks.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
        function baseAssignValue(object, key, value) {
            "__proto__" == key && defineProperty ? defineProperty(object, key, {
                configurable: !0,
                enumerable: !0,
                value: value,
                writable: !0
            }) : object[key] = value;
        }
        var defineProperty = __webpack_require__(147);
        module.exports = baseAssignValue;
    }, /* 248 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates a function like `_.assign`.
	 *
	 * @private
	 * @param {Function} assigner The function to assign values.
	 * @returns {Function} Returns the new assigner function.
	 */
        function createAssigner(assigner) {
            return baseRest(function(object, sources) {
                var index = -1, length = sources.length, customizer = length > 1 ? sources[length - 1] : void 0, guard = length > 2 ? sources[2] : void 0;
                for (customizer = assigner.length > 3 && "function" == typeof customizer ? (length--, 
                customizer) : void 0, guard && isIterateeCall(sources[0], sources[1], guard) && (customizer = length < 3 ? void 0 : customizer, 
                length = 1), object = Object(object); ++index < length; ) {
                    var source = sources[index];
                    source && assigner(object, source, index, customizer);
                }
                return object;
            });
        }
        var baseRest = __webpack_require__(140), isIterateeCall = __webpack_require__(249);
        module.exports = createAssigner;
    }, /* 249 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if the given arguments are from an iteratee call.
	 *
	 * @private
	 * @param {*} value The potential iteratee value argument.
	 * @param {*} index The potential iteratee index or key argument.
	 * @param {*} object The potential iteratee object argument.
	 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
	 *  else `false`.
	 */
        function isIterateeCall(value, index, object) {
            if (!isObject(object)) return !1;
            var type = typeof index;
            return !!("number" == type ? isArrayLike(object) && isIndex(index, object.length) : "string" == type && index in object) && eq(object[index], value);
        }
        var eq = __webpack_require__(118), isArrayLike = __webpack_require__(150), isIndex = __webpack_require__(250), isObject = __webpack_require__(90);
        module.exports = isIterateeCall;
    }, /* 250 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is a valid array-like index.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
	 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
	 */
        function isIndex(value, length) {
            return length = null == length ? MAX_SAFE_INTEGER : length, !!length && ("number" == typeof value || reIsUint.test(value)) && value > -1 && value % 1 == 0 && value < length;
        }
        /** Used as references for various `Number` constants. */
        var MAX_SAFE_INTEGER = 9007199254740991, reIsUint = /^(?:0|[1-9]\d*)$/;
        module.exports = isIndex;
    }, /* 251 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates an array of the own and inherited enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keysIn(new Foo);
	 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
	 */
        function keysIn(object) {
            return isArrayLike(object) ? arrayLikeKeys(object, !0) : baseKeysIn(object);
        }
        var arrayLikeKeys = __webpack_require__(252), baseKeysIn = __webpack_require__(261), isArrayLike = __webpack_require__(150);
        module.exports = keysIn;
    }, /* 252 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates an array of the enumerable property names of the array-like `value`.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @param {boolean} inherited Specify returning inherited property names.
	 * @returns {Array} Returns the array of property names.
	 */
        function arrayLikeKeys(value, inherited) {
            var isArr = isArray(value), isArg = !isArr && isArguments(value), isBuff = !isArr && !isArg && isBuffer(value), isType = !isArr && !isArg && !isBuff && isTypedArray(value), skipIndexes = isArr || isArg || isBuff || isType, result = skipIndexes ? baseTimes(value.length, String) : [], length = result.length;
            for (var key in value) !inherited && !hasOwnProperty.call(value, key) || skipIndexes && (// Safari 9 has enumerable `arguments.length` in strict mode.
            "length" == key || // Node.js 0.10 has enumerable non-index properties on buffers.
            isBuff && ("offset" == key || "parent" == key) || // PhantomJS 2 has enumerable non-index properties on typed arrays.
            isType && ("buffer" == key || "byteLength" == key || "byteOffset" == key) || // Skip index properties.
            isIndex(key, length)) || result.push(key);
            return result;
        }
        var baseTimes = __webpack_require__(253), isArguments = __webpack_require__(254), isArray = __webpack_require__(89), isBuffer = __webpack_require__(256), isIndex = __webpack_require__(250), isTypedArray = __webpack_require__(258), objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty;
        module.exports = arrayLikeKeys;
    }, /* 253 */
    /***/
    function(module, exports) {
        /**
	 * The base implementation of `_.times` without support for iteratee shorthands
	 * or max array length checks.
	 *
	 * @private
	 * @param {number} n The number of times to invoke `iteratee`.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the array of results.
	 */
        function baseTimes(n, iteratee) {
            for (var index = -1, result = Array(n); ++index < n; ) result[index] = iteratee(index);
            return result;
        }
        module.exports = baseTimes;
    }, /* 254 */
    /***/
    function(module, exports, __webpack_require__) {
        var baseIsArguments = __webpack_require__(255), isObjectLike = __webpack_require__(16), objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty, propertyIsEnumerable = objectProto.propertyIsEnumerable, isArguments = baseIsArguments(function() {
            return arguments;
        }()) ? baseIsArguments : function(value) {
            return isObjectLike(value) && hasOwnProperty.call(value, "callee") && !propertyIsEnumerable.call(value, "callee");
        };
        module.exports = isArguments;
    }, /* 255 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.isArguments`.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
	 */
        function baseIsArguments(value) {
            return isObjectLike(value) && objectToString.call(value) == argsTag;
        }
        var isObjectLike = __webpack_require__(16), argsTag = "[object Arguments]", objectProto = Object.prototype, objectToString = objectProto.toString;
        module.exports = baseIsArguments;
    }, /* 256 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(module) {
            var root = __webpack_require__(106), stubFalse = __webpack_require__(257), freeExports = "object" == typeof exports && exports && !exports.nodeType && exports, freeModule = freeExports && "object" == typeof module && module && !module.nodeType && module, moduleExports = freeModule && freeModule.exports === freeExports, Buffer = moduleExports ? root.Buffer : void 0, nativeIsBuffer = Buffer ? Buffer.isBuffer : void 0, isBuffer = nativeIsBuffer || stubFalse;
            module.exports = isBuffer;
        }).call(exports, __webpack_require__(19)(module));
    }, /* 257 */
    /***/
    function(module, exports) {
        /**
	 * This method returns `false`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.13.0
	 * @category Util
	 * @returns {boolean} Returns `false`.
	 * @example
	 *
	 * _.times(2, _.stubFalse);
	 * // => [false, false]
	 */
        function stubFalse() {
            return !1;
        }
        module.exports = stubFalse;
    }, /* 258 */
    /***/
    function(module, exports, __webpack_require__) {
        var baseIsTypedArray = __webpack_require__(259), baseUnary = __webpack_require__(138), nodeUtil = __webpack_require__(260), nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray, isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;
        module.exports = isTypedArray;
    }, /* 259 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.isTypedArray` without Node.js optimizations.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
	 */
        function baseIsTypedArray(value) {
            return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objectToString.call(value)];
        }
        var isLength = __webpack_require__(151), isObjectLike = __webpack_require__(16), argsTag = "[object Arguments]", arrayTag = "[object Array]", boolTag = "[object Boolean]", dateTag = "[object Date]", errorTag = "[object Error]", funcTag = "[object Function]", mapTag = "[object Map]", numberTag = "[object Number]", objectTag = "[object Object]", regexpTag = "[object RegExp]", setTag = "[object Set]", stringTag = "[object String]", weakMapTag = "[object WeakMap]", arrayBufferTag = "[object ArrayBuffer]", dataViewTag = "[object DataView]", float32Tag = "[object Float32Array]", float64Tag = "[object Float64Array]", int8Tag = "[object Int8Array]", int16Tag = "[object Int16Array]", int32Tag = "[object Int32Array]", uint8Tag = "[object Uint8Array]", uint8ClampedTag = "[object Uint8ClampedArray]", uint16Tag = "[object Uint16Array]", uint32Tag = "[object Uint32Array]", typedArrayTags = {};
        typedArrayTags[float32Tag] = typedArrayTags[float64Tag] = typedArrayTags[int8Tag] = typedArrayTags[int16Tag] = typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] = typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] = typedArrayTags[uint32Tag] = !0, 
        typedArrayTags[argsTag] = typedArrayTags[arrayTag] = typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] = typedArrayTags[dataViewTag] = typedArrayTags[dateTag] = typedArrayTags[errorTag] = typedArrayTags[funcTag] = typedArrayTags[mapTag] = typedArrayTags[numberTag] = typedArrayTags[objectTag] = typedArrayTags[regexpTag] = typedArrayTags[setTag] = typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = !1;
        /** Used for built-in method references. */
        var objectProto = Object.prototype, objectToString = objectProto.toString;
        module.exports = baseIsTypedArray;
    }, /* 260 */
    /***/
    function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */
        (function(module) {
            var freeGlobal = __webpack_require__(107), freeExports = "object" == typeof exports && exports && !exports.nodeType && exports, freeModule = freeExports && "object" == typeof module && module && !module.nodeType && module, moduleExports = freeModule && freeModule.exports === freeExports, freeProcess = moduleExports && freeGlobal.process, nodeUtil = function() {
                try {
                    return freeProcess && freeProcess.binding("util");
                } catch (e) {}
            }();
            module.exports = nodeUtil;
        }).call(exports, __webpack_require__(19)(module));
    }, /* 261 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
        function baseKeysIn(object) {
            if (!isObject(object)) return nativeKeysIn(object);
            var isProto = isPrototype(object), result = [];
            for (var key in object) ("constructor" != key || !isProto && hasOwnProperty.call(object, key)) && result.push(key);
            return result;
        }
        var isObject = __webpack_require__(90), isPrototype = __webpack_require__(262), nativeKeysIn = __webpack_require__(263), objectProto = Object.prototype, hasOwnProperty = objectProto.hasOwnProperty;
        module.exports = baseKeysIn;
    }, /* 262 */
    /***/
    function(module, exports) {
        /**
	 * Checks if `value` is likely a prototype object.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
	 */
        function isPrototype(value) {
            var Ctor = value && value.constructor, proto = "function" == typeof Ctor && Ctor.prototype || objectProto;
            return value === proto;
        }
        /** Used for built-in method references. */
        var objectProto = Object.prototype;
        module.exports = isPrototype;
    }, /* 263 */
    /***/
    function(module, exports) {
        /**
	 * This function is like
	 * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
	 * except that it includes inherited enumerable properties.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
        function nativeKeysIn(object) {
            var result = [];
            if (null != object) for (var key in Object(object)) result.push(key);
            return result;
        }
        module.exports = nativeKeysIn;
    }, /* 264 */
    /***/
    function(module, exports) {
        "use strict";
        function shallowEqual(objA, objB) {
            if (objA === objB) return !0;
            var keysA = Object.keys(objA), keysB = Object.keys(objB);
            if (keysA.length !== keysB.length) return !1;
            for (var hasOwn = Object.prototype.hasOwnProperty, i = 0; i < keysA.length; i++) {
                if (!hasOwn.call(objB, keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) return !1;
                var valA = objA[keysA[i]], valB = objB[keysA[i]];
                if (valA !== valB) return !1;
            }
            return !0;
        }
        exports.__esModule = !0, exports.default = shallowEqual, module.exports = exports.default;
    }, /* 265 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var _lodashUnion = __webpack_require__(266), _lodashUnion2 = _interopRequireDefault(_lodashUnion), _lodashWithout = __webpack_require__(93), _lodashWithout2 = _interopRequireDefault(_lodashWithout), EnterLeaveCounter = function() {
            function EnterLeaveCounter() {
                _classCallCheck(this, EnterLeaveCounter), this.entered = [];
            }
            return EnterLeaveCounter.prototype.enter = function(enteringNode) {
                var previousLength = this.entered.length;
                return this.entered = _lodashUnion2.default(this.entered.filter(function(node) {
                    return document.documentElement.contains(node) && (!node.contains || node.contains(enteringNode));
                }), [ enteringNode ]), 0 === previousLength && this.entered.length > 0;
            }, EnterLeaveCounter.prototype.leave = function(leavingNode) {
                var previousLength = this.entered.length;
                return this.entered = _lodashWithout2.default(this.entered.filter(function(node) {
                    return document.documentElement.contains(node);
                }), leavingNode), previousLength > 0 && 0 === this.entered.length;
            }, EnterLeaveCounter.prototype.reset = function() {
                this.entered = [];
            }, EnterLeaveCounter;
        }();
        exports.default = EnterLeaveCounter, module.exports = exports.default;
    }, /* 266 */
    /***/
    function(module, exports, __webpack_require__) {
        var baseFlatten = __webpack_require__(267), baseRest = __webpack_require__(140), baseUniq = __webpack_require__(158), isArrayLikeObject = __webpack_require__(149), union = baseRest(function(arrays) {
            return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, !0));
        });
        module.exports = union;
    }, /* 267 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * The base implementation of `_.flatten` with support for restricting flattening.
	 *
	 * @private
	 * @param {Array} array The array to flatten.
	 * @param {number} depth The maximum recursion depth.
	 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
	 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
	 * @param {Array} [result=[]] The initial result value.
	 * @returns {Array} Returns the new flattened array.
	 */
        function baseFlatten(array, depth, predicate, isStrict, result) {
            var index = -1, length = array.length;
            for (predicate || (predicate = isFlattenable), result || (result = []); ++index < length; ) {
                var value = array[index];
                depth > 0 && predicate(value) ? depth > 1 ? // Recursively flatten arrays (susceptible to call stack limits).
                baseFlatten(value, depth - 1, predicate, isStrict, result) : arrayPush(result, value) : isStrict || (result[result.length] = value);
            }
            return result;
        }
        var arrayPush = __webpack_require__(157), isFlattenable = __webpack_require__(268);
        module.exports = baseFlatten;
    }, /* 268 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Checks if `value` is a flattenable `arguments` object or array.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
	 */
        function isFlattenable(value) {
            return isArray(value) || isArguments(value) || !!(spreadableSymbol && value && value[spreadableSymbol]);
        }
        var Symbol = __webpack_require__(269), isArguments = __webpack_require__(254), isArray = __webpack_require__(89), spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : void 0;
        module.exports = isFlattenable;
    }, /* 269 */
    /***/
    function(module, exports, __webpack_require__) {
        var root = __webpack_require__(106), Symbol = root.Symbol;
        module.exports = Symbol;
    }, /* 270 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        exports.__esModule = !0;
        var _lodashMemoize = __webpack_require__(271), _lodashMemoize2 = _interopRequireDefault(_lodashMemoize), isFirefox = _lodashMemoize2.default(function() {
            return /firefox/i.test(navigator.userAgent);
        });
        exports.isFirefox = isFirefox;
        var isSafari = _lodashMemoize2.default(function() {
            return Boolean(window.safari);
        });
        exports.isSafari = isSafari;
    }, /* 271 */
    /***/
    function(module, exports, __webpack_require__) {
        /**
	 * Creates a function that memoizes the result of `func`. If `resolver` is
	 * provided, it determines the cache key for storing the result based on the
	 * arguments provided to the memoized function. By default, the first argument
	 * provided to the memoized function is used as the map cache key. The `func`
	 * is invoked with the `this` binding of the memoized function.
	 *
	 * **Note:** The cache is exposed as the `cache` property on the memoized
	 * function. Its creation may be customized by replacing the `_.memoize.Cache`
	 * constructor with one whose instances implement the
	 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
	 * method interface of `delete`, `get`, `has`, and `set`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Function
	 * @param {Function} func The function to have its output memoized.
	 * @param {Function} [resolver] The function to resolve the cache key.
	 * @returns {Function} Returns the new memoized function.
	 * @example
	 *
	 * var object = { 'a': 1, 'b': 2 };
	 * var other = { 'c': 3, 'd': 4 };
	 *
	 * var values = _.memoize(_.values);
	 * values(object);
	 * // => [1, 2]
	 *
	 * values(other);
	 * // => [3, 4]
	 *
	 * object.a = 2;
	 * values(object);
	 * // => [1, 2]
	 *
	 * // Modify the result cache.
	 * values.cache.set(object, ['a', 'b']);
	 * values(object);
	 * // => ['a', 'b']
	 *
	 * // Replace `_.memoize.Cache`.
	 * _.memoize.Cache = WeakMap;
	 */
        function memoize(func, resolver) {
            if ("function" != typeof func || resolver && "function" != typeof resolver) throw new TypeError(FUNC_ERROR_TEXT);
            var memoized = function() {
                var args = arguments, key = resolver ? resolver.apply(this, args) : args[0], cache = memoized.cache;
                if (cache.has(key)) return cache.get(key);
                var result = func.apply(this, args);
                return memoized.cache = cache.set(key, result) || cache, result;
            };
            return memoized.cache = new (memoize.Cache || MapCache)(), memoized;
        }
        var MapCache = __webpack_require__(96), FUNC_ERROR_TEXT = "Expected a function";
        // Expose `MapCache`.
        memoize.Cache = MapCache, module.exports = memoize;
    }, /* 272 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        function getNodeClientOffset(node) {
            var el = node.nodeType === ELEMENT_NODE ? node : node.parentElement;
            if (!el) return null;
            var _el$getBoundingClientRect = el.getBoundingClientRect(), top = _el$getBoundingClientRect.top, left = _el$getBoundingClientRect.left;
            return {
                x: left,
                y: top
            };
        }
        function getEventClientOffset(e) {
            return {
                x: e.clientX,
                y: e.clientY
            };
        }
        function getDragPreviewOffset(sourceNode, dragPreview, clientOffset, anchorPoint) {
            // The browsers will use the image intrinsic size under different conditions.
            // Firefox only cares if it's an image, but WebKit also wants it to be detached.
            var isImage = "IMG" === dragPreview.nodeName && (_BrowserDetector.isFirefox() || !document.documentElement.contains(dragPreview)), dragPreviewNode = isImage ? sourceNode : dragPreview, dragPreviewNodeOffsetFromClient = getNodeClientOffset(dragPreviewNode), offsetFromDragPreview = {
                x: clientOffset.x - dragPreviewNodeOffsetFromClient.x,
                y: clientOffset.y - dragPreviewNodeOffsetFromClient.y
            }, sourceWidth = sourceNode.offsetWidth, sourceHeight = sourceNode.offsetHeight, anchorX = anchorPoint.anchorX, anchorY = anchorPoint.anchorY, dragPreviewWidth = isImage ? dragPreview.width : sourceWidth, dragPreviewHeight = isImage ? dragPreview.height : sourceHeight;
            // Work around @2x coordinate discrepancies in browsers
            _BrowserDetector.isSafari() && isImage ? (dragPreviewHeight /= window.devicePixelRatio, 
            dragPreviewWidth /= window.devicePixelRatio) : _BrowserDetector.isFirefox() && !isImage && (dragPreviewHeight *= window.devicePixelRatio, 
            dragPreviewWidth *= window.devicePixelRatio);
            // Interpolate coordinates depending on anchor point
            // If you know a simpler way to do this, let me know
            var interpolantX = new _MonotonicInterpolant2.default([ 0, .5, 1 ], [ // Dock to the left
            offsetFromDragPreview.x, // Align at the center
            offsetFromDragPreview.x / sourceWidth * dragPreviewWidth, // Dock to the right
            offsetFromDragPreview.x + dragPreviewWidth - sourceWidth ]), interpolantY = new _MonotonicInterpolant2.default([ 0, .5, 1 ], [ // Dock to the top
            offsetFromDragPreview.y, // Align at the center
            offsetFromDragPreview.y / sourceHeight * dragPreviewHeight, // Dock to the bottom
            offsetFromDragPreview.y + dragPreviewHeight - sourceHeight ]), x = interpolantX.interpolate(anchorX), y = interpolantY.interpolate(anchorY);
            // Work around Safari 8 positioning bug
            // We'll have to wait for @3x to see if this is entirely correct
            return _BrowserDetector.isSafari() && isImage && (y += (window.devicePixelRatio - 1) * dragPreviewHeight), 
            {
                x: x,
                y: y
            };
        }
        exports.__esModule = !0, exports.getNodeClientOffset = getNodeClientOffset, exports.getEventClientOffset = getEventClientOffset, 
        exports.getDragPreviewOffset = getDragPreviewOffset;
        var _BrowserDetector = __webpack_require__(270), _MonotonicInterpolant = __webpack_require__(273), _MonotonicInterpolant2 = _interopRequireDefault(_MonotonicInterpolant), ELEMENT_NODE = 1;
    }, /* 273 */
    /***/
    function(module, exports) {
        "use strict";
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        exports.__esModule = !0;
        var MonotonicInterpolant = function() {
            function MonotonicInterpolant(xs, ys) {
                _classCallCheck(this, MonotonicInterpolant);
                for (var length = xs.length, indexes = [], i = 0; i < length; i++) indexes.push(i);
                indexes.sort(function(a, b) {
                    return xs[a] < xs[b] ? -1 : 1;
                });
                for (var dys = [], dxs = [], ms = [], dx = void 0, dy = void 0, i = 0; i < length - 1; i++) dx = xs[i + 1] - xs[i], 
                dy = ys[i + 1] - ys[i], dxs.push(dx), dys.push(dy), ms.push(dy / dx);
                for (var c1s = [ ms[0] ], i = 0; i < dxs.length - 1; i++) {
                    var _m = ms[i], mNext = ms[i + 1];
                    if (_m * mNext <= 0) c1s.push(0); else {
                        dx = dxs[i];
                        var dxNext = dxs[i + 1], common = dx + dxNext;
                        c1s.push(3 * common / ((common + dxNext) / _m + (common + dx) / mNext));
                    }
                }
                c1s.push(ms[ms.length - 1]);
                for (var c2s = [], c3s = [], m = void 0, i = 0; i < c1s.length - 1; i++) {
                    m = ms[i];
                    var c1 = c1s[i], invDx = 1 / dxs[i], common = c1 + c1s[i + 1] - m - m;
                    c2s.push((m - c1 - common) * invDx), c3s.push(common * invDx * invDx);
                }
                this.xs = xs, this.ys = ys, this.c1s = c1s, this.c2s = c2s, this.c3s = c3s;
            }
            return MonotonicInterpolant.prototype.interpolate = function(x) {
                var xs = this.xs, ys = this.ys, c1s = this.c1s, c2s = this.c2s, c3s = this.c3s, i = xs.length - 1;
                if (x === xs[i]) return ys[i];
                for (// Search for the interval x is in, returning the corresponding y if x is one of the original xs
                var low = 0, high = c3s.length - 1, mid = void 0; low <= high; ) {
                    mid = Math.floor(.5 * (low + high));
                    var xHere = xs[mid];
                    if (xHere < x) low = mid + 1; else {
                        if (!(xHere > x)) return ys[mid];
                        high = mid - 1;
                    }
                }
                i = Math.max(0, high);
                // Interpolate
                var diff = x - xs[i], diffSq = diff * diff;
                return ys[i] + c1s[i] * diff + c2s[i] * diffSq + c3s[i] * diff * diffSq;
            }, MonotonicInterpolant;
        }();
        exports.default = MonotonicInterpolant, module.exports = exports.default;
    }, /* 274 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireWildcard(obj) {
            if (obj && obj.__esModule) return obj;
            var newObj = {};
            if (null != obj) for (var key in obj) Object.prototype.hasOwnProperty.call(obj, key) && (newObj[key] = obj[key]);
            return newObj.default = obj, newObj;
        }
        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function");
        }
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        function getDataFromDataTransfer(dataTransfer, typesToTry, defaultValue) {
            var result = typesToTry.reduce(function(resultSoFar, typeToTry) {
                return resultSoFar || dataTransfer.getData(typeToTry);
            }, null);
            // eslint-disable-line eqeqeq
            return null != result ? result : defaultValue;
        }
        function createNativeDragSource(type) {
            var _nativeTypesConfig$type = nativeTypesConfig[type], exposeProperty = _nativeTypesConfig$type.exposeProperty, matchesTypes = _nativeTypesConfig$type.matchesTypes, getData = _nativeTypesConfig$type.getData;
            return function() {
                function NativeDragSource() {
                    _classCallCheck(this, NativeDragSource), this.item = Object.defineProperties({}, _defineProperty({}, exposeProperty, {
                        get: function() {
                            // eslint-disable-line no-console
                            return console.warn("Browser doesn't allow reading \"" + exposeProperty + '" until the drop event.'), 
                            null;
                        },
                        configurable: !0,
                        enumerable: !0
                    }));
                }
                return NativeDragSource.prototype.mutateItemByReadingDataTransfer = function(dataTransfer) {
                    delete this.item[exposeProperty], this.item[exposeProperty] = getData(dataTransfer, matchesTypes);
                }, NativeDragSource.prototype.canDrag = function() {
                    return !0;
                }, NativeDragSource.prototype.beginDrag = function() {
                    return this.item;
                }, NativeDragSource.prototype.isDragging = function(monitor, handle) {
                    return handle === monitor.getSourceId();
                }, NativeDragSource.prototype.endDrag = function() {}, NativeDragSource;
            }();
        }
        function matchNativeItemType(dataTransfer) {
            var dataTransferTypes = Array.prototype.slice.call(dataTransfer.types || []);
            return Object.keys(nativeTypesConfig).filter(function(nativeItemType) {
                var matchesTypes = nativeTypesConfig[nativeItemType].matchesTypes;
                return matchesTypes.some(function(t) {
                    return dataTransferTypes.indexOf(t) > -1;
                });
            })[0] || null;
        }
        exports.__esModule = !0;
        var _nativeTypesConfig;
        exports.createNativeDragSource = createNativeDragSource, exports.matchNativeItemType = matchNativeItemType;
        var _NativeTypes = __webpack_require__(275), NativeTypes = _interopRequireWildcard(_NativeTypes), nativeTypesConfig = (_nativeTypesConfig = {}, 
        _defineProperty(_nativeTypesConfig, NativeTypes.FILE, {
            exposeProperty: "files",
            matchesTypes: [ "Files" ],
            getData: function(dataTransfer) {
                return Array.prototype.slice.call(dataTransfer.files);
            }
        }), _defineProperty(_nativeTypesConfig, NativeTypes.URL, {
            exposeProperty: "urls",
            matchesTypes: [ "Url", "text/uri-list" ],
            getData: function(dataTransfer, matchesTypes) {
                return getDataFromDataTransfer(dataTransfer, matchesTypes, "").split("\n");
            }
        }), _defineProperty(_nativeTypesConfig, NativeTypes.TEXT, {
            exposeProperty: "text",
            matchesTypes: [ "Text", "text/plain" ],
            getData: function(dataTransfer, matchesTypes) {
                return getDataFromDataTransfer(dataTransfer, matchesTypes, "");
            }
        }), _nativeTypesConfig);
    }, /* 275 */
    /***/
    function(module, exports) {
        "use strict";
        exports.__esModule = !0;
        var FILE = "__NATIVE_FILE__";
        exports.FILE = FILE;
        var URL = "__NATIVE_URL__";
        exports.URL = URL;
        var TEXT = "__NATIVE_TEXT__";
        exports.TEXT = TEXT;
    }, /* 276 */
    /***/
    function(module, exports) {
        "use strict";
        function getEmptyImage() {
            return emptyImage || (emptyImage = new Image(), emptyImage.src = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="), 
            emptyImage;
        }
        exports.__esModule = !0, exports.default = getEmptyImage;
        var emptyImage = void 0;
        module.exports = exports.default;
    }, /* 277 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _redux = __webpack_require__(11), _dataReducer = __webpack_require__(278), _dataReducer2 = _interopRequireDefault(_dataReducer), _configReducer = __webpack_require__(279), _configReducer2 = _interopRequireDefault(_configReducer), _fieldsReducer = __webpack_require__(280), _fieldsReducer2 = _interopRequireDefault(_fieldsReducer), _datafieldsReducer = __webpack_require__(281), _datafieldsReducer2 = _interopRequireDefault(_datafieldsReducer), _axisReducer = __webpack_require__(282), _axisReducer2 = _interopRequireDefault(_axisReducer), _sizesReducer = __webpack_require__(283), _sizesReducer2 = _interopRequireDefault(_sizesReducer), _filtersReducer = __webpack_require__(284), _filtersReducer2 = _interopRequireDefault(_filtersReducer);
        exports.default = (0, _redux.combineReducers)({
            data: _dataReducer2.default,
            config: _configReducer2.default,
            fields: _fieldsReducer2.default,
            datafields: _datafieldsReducer2.default,
            axis: _axisReducer2.default,
            sizes: _sizesReducer2.default,
            filters: _filtersReducer2.default
        });
    }, /* 278 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], action = arguments[1];
            switch (action.type) {
              case _constants.PUSH_DATA:
                return [].concat(_toConsumableArray(state), _toConsumableArray(action.payload));

              case _constants.SET_DATA:
                return action.payload;

              default:
                return state;
            }
        };
    }, /* 279 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, action = arguments[1], type = action.type, property = action.property, value = action.value;
            switch (type) {
              case _constants.SET_CONFIG_PROPERTY:
                return _extends({}, state, _defineProperty({}, property, value));

              default:
                return state;
            }
        };
    }, /* 280 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, action = arguments[1], type = action.type, fields = action.fields, field = action.field, sort = void 0;
            switch (type) {
              case _constants.SET_FIELDS:
                return fields.reduce(function(acc, field) {
                    return _extends({}, acc, _defineProperty({}, field.id, field));
                }, {});

              case _constants.CHANGE_SORT_ORDER:
                return sort = state[field].sort, "asc" === sort.order ? sort.order = "desc" : sort.order = "asc", 
                _extends({}, state, _defineProperty({}, field, _extends({}, state[field], {
                    sort: sort
                })));

              default:
                return state;
            }
        };
    }, /* 281 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, action = arguments[1], type = action.type, datafields = action.datafields, id = action.id, datafield = void 0;
            switch (type) {
              case _constants.SET_DATAFIELDS:
                return datafields.reduce(function(acc, field) {
                    return _extends({}, acc, _defineProperty({}, field.id, field));
                }, {});

              case _constants.TOGGLE_DATAFIELD:
                return datafield = state[id], _extends({}, state, _defineProperty({}, id, _extends({}, datafield, {
                    activated: !datafield.activated
                })));

              default:
                return state;
            }
        };
    }, /* 282 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        function _toConsumableArray(arr) {
            if (Array.isArray(arr)) {
                for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];
                return arr2;
            }
            return Array.from(arr);
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
                rows: [],
                columns: [],
                fields: []
            }, action = arguments[1], type = action.type, id = action.id, position = action.position, oldAxis = action.oldAxis, newAxis = action.newAxis, newAxisValue = void 0, oldAxisValue = void 0, oldPosition = void 0, positionToRemove = void 0;
            switch (type) {
              case _constants.MOVE_FIELD:
                if (oldAxis !== newAxis) {
                    var _extends2;
                    return oldAxisValue = state[oldAxis].filter(function(field) {
                        return field !== id;
                    }), newAxisValue = [].concat(_toConsumableArray(state[newAxis].slice(0, position)), [ id ], _toConsumableArray(state[newAxis].slice(position))), 
                    _extends({}, state, (_extends2 = {}, _defineProperty(_extends2, oldAxis, oldAxisValue), 
                    _defineProperty(_extends2, newAxis, newAxisValue), _extends2));
                }
                return oldPosition = state[oldAxis].indexOf(id), oldPosition === position ? state : (newAxisValue = [].concat(_toConsumableArray(state[oldAxis].slice(0, position)), [ id ], _toConsumableArray(state[oldAxis].slice(position))), 
                positionToRemove = oldPosition < position ? oldPosition : oldPosition + 1, newAxisValue = [].concat(_toConsumableArray(newAxisValue.slice(0, positionToRemove)), _toConsumableArray(newAxisValue.slice(positionToRemove + 1))), 
                _extends({}, state, _defineProperty({}, newAxis, newAxisValue)));

              default:
                return state;
            }
        };
    }, /* 283 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
                rows: {
                    leafs: {},
                    dimensions: {}
                },
                columns: {
                    leafs: {},
                    dimensions: {}
                }
            }, action = arguments[1], type = action.type, axis = action.axis, direction = action.direction, size = action.size, id = action.id;
            switch (type) {
              case _constants.UPDATE_CELL_SIZE:
                return _extends({}, state, _defineProperty({}, axis, _extends({}, state[axis], _defineProperty({}, direction, _extends({}, state[axis][direction], _defineProperty({}, id, size))))));

              default:
                return state;
            }
        };
    }, /* 284 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _objectWithoutProperties(obj, keys) {
            var target = {};
            for (var i in obj) keys.indexOf(i) >= 0 || Object.prototype.hasOwnProperty.call(obj, i) && (target[i] = obj[i]);
            return target;
        }
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        }, _constants = __webpack_require__(207);
        exports.default = function() {
            var state = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, action = arguments[1], type = action.type, field = action.field, filter = action.filter;
            switch (type) {
              case _constants.SET_CONFIG:
                return state;

              case _constants.ADD_FILTER:
                return _extends({}, state, _defineProperty({}, field, filter));

              case _constants.DELETE_FILTER:
                var newState = (state[field], _objectWithoutProperties(state, [ field ]));
                return newState;

              default:
                return state;
            }
        };
    }, /* 285 */
    /***/
    function(module, exports, __webpack_require__) {
        "use strict";
        function _defineProperty(obj, key, value) {
            return key in obj ? Object.defineProperty(obj, key, {
                value: value,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : obj[key] = value, obj;
        }
        function hydrateStore(store, config, datasource) {
            store.dispatch((0, _actions.pushData)(datasource)), store.dispatch((0, _actions.setFields)(config)), 
            store.dispatch((0, _actions.setDatafields)(config)), store.dispatch((0, _actions.setConfigProperty)(config, "dataHeadersLocation", "columns")), 
            store.dispatch((0, _actions.setConfigProperty)(config, "height", 600)), store.dispatch((0, 
            _actions.setConfigProperty)(config, "width", 800)), store.dispatch((0, _actions.setConfigProperty)(config, "cellHeight", 30)), 
            store.dispatch((0, _actions.setConfigProperty)(config, "cellWidth", 100)), store.dispatch((0, 
            _actions.setConfigProperty)(config, "zoom", 1)), config.rows.forEach(function(fieldCaption, index) {
                var fieldId = config.fields.find(function(field) {
                    return field.caption === fieldCaption;
                }).id;
                store.dispatch((0, _actions.moveField)(fieldId, "fields", "rows", index));
            }), config.columns.forEach(function(fieldCaption, index) {
                var fieldId = config.fields.find(function(field) {
                    return field.caption === fieldCaption;
                }).id;
                store.dispatch((0, _actions.moveField)(fieldId, "fields", "columns", index));
            }), Object.values(config.fields).filter(function(field) {
                var state = store.getState(), rows = state.axis.rows, columns = state.axis.columns;
                return !(rows.includes(field.id) || columns.includes(field.id));
            }).forEach(function(field, index) {
                store.dispatch((0, _actions.moveField)(field.id, "fields", "fields", index));
            }), config.data.forEach(function(fieldCaption) {
                var fieldId = config.datafields.find(function(field) {
                    return field.caption === fieldCaption;
                }).id;
                store.dispatch((0, _actions.toggleDatafield)(fieldId));
            });
            var customFunctions = {
                sort: config.fields.reduce(function(acc, field) {
                    return _extends({}, acc, _defineProperty({}, field.id, field.sort && field.sort.customfunc));
                }, {}),
                format: config.datafields.reduce(function(acc, field) {
                    return _extends({}, acc, _defineProperty({}, field.id, field.formatFunc));
                }, {}),
                aggregation: config.datafields.reduce(function(acc, field) {
                    return _extends({}, acc, _defineProperty({}, field.id, (0, _Aggregation.toAggregateFunc)(field.aggregateFunc)));
                }, {})
            };
            return customFunctions;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        });
        var _extends = Object.assign || function(target) {
            for (var i = 1; i < arguments.length; i++) {
                var source = arguments[i];
                for (var key in source) Object.prototype.hasOwnProperty.call(source, key) && (target[key] = source[key]);
            }
            return target;
        };
        exports.default = hydrateStore;
        var _actions = __webpack_require__(232), _Aggregation = __webpack_require__(286);
    }, /* 286 */
    /***/
    function(module, exports) {
        "use strict";
        function forEachIntersection(datafield, intersection, data, callback) {
            if (intersection.length > 0) for (var i = 0; i < intersection.length; i += 1) callback(data[intersection[i]][datafield]);
        }
        function count(datafield, intersection, data) {
            return "all" === intersection ? data.length : intersection.length;
        }
        function sum(datafield, intersection, data) {
            var sum = 0;
            return forEachIntersection(datafield, intersection, data, function(val) {
                sum += val;
            }), sum;
        }
        function min(datafield, intersection, data) {
            var min = null;
            return forEachIntersection(datafield, intersection, data, function(val) {
                (null == min || val < min) && (min = val);
            }), min;
        }
        function max(datafield, intersection, data) {
            var max = null;
            return forEachIntersection(datafield, intersection, data, function(val) {
                (null == max || val > max) && (max = val);
            }), max;
        }
        function avg(datafield, intersection, data) {
            var avg = 0, len = ("all" === intersection ? data : intersection).length;
            return len > 0 && (forEachIntersection(datafield, intersection, data, function(val) {
                avg += val;
            }), avg /= len), avg;
        }
        function prod(datafield, intersection, data) {
            var prod = void 0, len = ("all" === intersection ? data : intersection).length;
            return len > 0 && (prod = 1, forEachIntersection(datafield, intersection, data, function(val) {
                prod *= val;
            })), prod;
        }
        function calcVariance(datafield, intersection, data, population) {
            var variance = 0, avg = 0, len = ("all" === intersection ? data : intersection).length;
            return len > 0 && (population || len > 1 ? (forEachIntersection(datafield, intersection, data, function(val) {
                avg += val;
            }), avg /= len, forEachIntersection(datafield, intersection, data, function(val) {
                variance += (val - avg) * (val - avg);
            }), variance /= population ? len : len - 1) : variance = NaN), variance;
        }
        function stdev(datafield, intersection, data) {
            return Math.sqrt(calcVariance(datafield, intersection, data, !1));
        }
        function stdevp(datafield, intersection, data) {
            return Math.sqrt(calcVariance(datafield, intersection, data, !0));
        }
        function _var(datafield, intersection, data) {
            return calcVariance(datafield, intersection, data, !1);
        }
        function varp(datafield, intersection, data) {
            return calcVariance(datafield, intersection, data, !0);
        }
        function toAggregateFunc(func) {
            if (func) {
                if ("string" == typeof func && eval(func)) // eslint-disable-line no-eval
                return eval(func);
                if ("function" == typeof func) return func;
            }
            return sum;
        }
        Object.defineProperty(exports, "__esModule", {
            value: !0
        }), exports.forEachIntersection = forEachIntersection, exports.count = count, exports.sum = sum, 
        exports.min = min, exports.max = max, exports.avg = avg, exports.prod = prod, exports.calcVariance = calcVariance, 
        exports.stdev = stdev, exports.stdevp = stdevp, exports._var = _var, exports.varp = varp, 
        exports.toAggregateFunc = toAggregateFunc;
    } ]);
});
//# sourceMappingURL=zebulon-grid.js.map