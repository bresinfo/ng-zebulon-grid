# ng-zebulon-grid

Angular wrapper for [zebulon-grid](https://bitbucket.org/bresinfo/zebulon_pivottable/)

`app.js` is an example of application using the wrapped grid.

To use, do

```
bower install angular react ngReact
```

and open `index.html`.
