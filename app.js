var app = angular.module( 'app', ['react'] );

app.controller( 'mainCtrl', function( $scope ) {

  function getMockDatasource(dataRepetition = 1, nToto = 10, nTiti = 10) {
    const nTutu = 2;
    let obj = [];
    const res = [];
    for (let k = 0; k < dataRepetition; k += 1) {
      for (let o = 0; o < nToto; o += 1) {
        for (let i = 0; i < nTiti; i += 1) {
          for (let u = 0; u < nTutu; u += 1) {
            obj = {};
            obj.toto = String(o);
            obj.toto_lb = `toto ${String(o)}`;
            obj.titi = `titi ${String(i)}`;
            obj.tutu = String(u);
            obj.qty = u + (10 * i) + (100 * o); // +9999999999.1234567890123456
            obj.amt = u + (10 * i) + (100 * o);// +9999999999.1234567890123456
            res.push(obj);
          }
        }
      }
    }
    return res;
  };

  const config = {
    canMoveFields: true,
    dataHeadersLocation: 'columns',
    width: 1099,
    height: 601,
    cellHeight: 30,
    cellWidth: 100,
    theme: 'green',
    toolbar: {
      visible: true,
    },
    grandTotal: {
      rowsvisible: false,
      columnsvisible: false,
    },
    subTotal: {
      visible: false,
      collapsed: false,
      collapsible: false,
    },
    rowSettings: {
      subTotal: {
        visible: false,
        collapsed: false,
        collapsible: false,
      },
    },
    columnSettings: {
      subTotal: {
        visible: false,
        collapsed: false,
        collapsible: false,
      },
    },
    fields: [
      {
        name: 'toto_lb',
        id: 'toto',
        caption: 'Toto',
        sort: {
          order: 'asc',
        },
      },
      {
        id: 'titi',
        caption: 'Titi',
      },
      {
        id: 'tutu',
        caption: 'Tutu',
      },
    ],
    datafields: [
      {
        id: 'qty',
        caption: 'Quantity',
        aggregateFunc: 'sum',
      },
      {
        id: 'amt',
        caption: 'Amount',
        aggregateFunc: 'sum',
        aggregateFuncName: 'whatever',
        formatFunc: (value) => {
          if (value || value === 0) {
            return `${Number(value).toFixed(0)} $`;
          }
          return '';
        },
      },
    ],
    columns: ['Titi'],
    rows: ['Toto', 'Tutu'],
    data: ['Quantity', 'Amount'],
    preFilters: {
    },
  };

  $scope.data = getMockDatasource(1, 100, 100);
  $scope.config = config;
  $scope.ref = function(ref) { if (ref) $scope.grid = ref.getDecoratedComponentInstance(); }

  $scope.pushData = function () {
    $scope.grid.pushData([
      { toto: '0', toto_lb: 'toto 0', qty: 100, amt: 100, titi: 'titi 0', tutu: '0' },
    ]);
  }

  $scope.toggleDatafield = function () {
    $scope.grid.toggleDatafield('amt');
  }

var i = 0;
  $scope.moveField = function () {
    if (i % 2) {
      $scope.grid.moveField('tutu', 'columns', 'rows', 1);
    } else {
      $scope.grid.moveField('tutu', 'rows', 'columns', 1);
    }
    i += 1;
  }

} );

app.value( "MyGrid", ZebulonGrid.WrappedGrid);

app.directive( 'myGrid', function( reactDirective ) {
  return reactDirective( 'MyGrid', ['data', 'config', 'ref'] );
} );
